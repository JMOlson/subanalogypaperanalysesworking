function plotSpeedByROIandAnimal(SubRec, HpcRec)
%PLOTSPEEDBYROIANDANIMAL - SUPP FIG SubCA1 Analogy Paper
%		Figures showing speed on paths of animals broken down by animal and
%		splitting the animals according to ROI of their recordings.
%
% INPUTS: 
%		SubRec - SubRec struct with behavior data
%		HpcRec - HpcRec struct with behavior data
%		                      
%
% OUTPUTS: Speed plots
%		                       
% EXAMPLES: 
% 		 plotSpeedByROIandAnimal(SubRec, HpcRec)
%
% REMARKS
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:
% CREATED WITH MATLAB VERSION: 9.6.0.1150989 (R2019a) Update 4
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: September 2021
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
PATHS = [1:4,9:10];
Y_MAX = 120;

pathLengths = HpcRec.Behavior.pathLengths{1};
rois = [SubRec, HpcRec];
roiNames = ["SUB", "CA1"];

vsROIFig = figure;
vsROIFig2 = figure;
for iROI = 1:2
    roi = rois(iROI);
    nRecs = numel(roi.Behavior.trackingSampleRate_Hz);
    
    rats = unique(roi.rat);
    disp(rats(end:-1:1));
    colors = distinguishable_colors(length(rats),[1,1,1]);
    
    perROIFig = figure;
    perROIFig2 = figure;
    for iPath = numel(PATHS):-1:1 %backwards so I don't have to init vars.
        thisPath = PATHS(iPath);
        pathLength = pathLengths(iPath);
        meanSpeedPerRat = nan(numel(rats),pathLength);
        for iRat = numel(rats):-1:1 %backwards so I don't have to init vars.
            thisRat = rats{iRat};
            thisRatRecs = strcmp(thisRat, roi.rat);
            
            meanSpeed = reshape([roi.Behavior.speedMeanPathAligned_CmPerSec{thisRatRecs, thisPath}],...
                [pathLength,sum(thisRatRecs)]);
            meanSpeedPerRat(iRat,:) = nanmean(meanSpeed,2);
            goodSpots = ~isnan(meanSpeedPerRat(iRat,:));
            pathIndices = 1:pathLength;
            pathIndices = pathIndices(goodSpots);
%             % by ROI: per animal mean, std
%             figure(perROIFig);
%             subplot(2,4,iPath);
%             ylim([0 Y_MAX]);
%             title(thisPath);
%             hold on;
%             boundedline(pathIndices,meanSpeedPerRat(iRat,goodSpots),nanstd(meanSpeed(goodSpots,:),0,2)','-','cmap',colors(iRat,:));
%             sgtitle('each animal u=-std')
            
            % by ROI: each rec, diff animals diff colors
            figure(perROIFig2);
            subplot(2,4,iPath);
            ylim([0 Y_MAX]);
            title(thisPath);
            hold on;
            plot(meanSpeed,'Color',colors(iRat,:));
            sgtitle(sprintf('all recs each animal - %s',roiNames(iROI))); 
        end
%         % across ROI: mean std w/ animal means as inputs
%         figure(vsROIFig);
%         subplot(2,4,iPath);
%         ylim([0 Y_MAX]);
%         title(thisPath);
%         hold on;
%         boundedline(1:pathLength,nanmean(meanSpeedPerRat,1),nanstd(meanSpeedPerRat,0,1),'-','cmap',colors(iROI,:));
%         sgtitle('Sub v Hpc - U+-std peranimal u as inputs')
        
        % across ROI: mean std w/ all recs as inputs
        figure(vsROIFig2);
        subplot(2,4,iPath);
        ylim([0 Y_MAX]);
        title(thisPath);
        hold on;
        meanSpeedROI{iROI,iPath} = reshape([roi.Behavior.speedMeanPathAligned_CmPerSec{:, thisPath}],[pathLength,nRecs]);
        boundedline(1:pathLength,nanmean(meanSpeedROI{iROI,iPath},2),nanstd(meanSpeedROI{iROI,iPath},0,2),'-','cmap',colors(iROI,:));
        sgtitle('Sub v Hpc - U+-std all recs')
        legend(" ", "Sub", " ", "CA1","bkgd",'boxoff');
    end
end
% for iPath = numel(PATHS):-1:1 %backwards so I don't have to init vars.
%     thisPath = PATHS(iPath);
%     pathLength = pathLengths(iPath);
%     for iLoc = pathLength:-1:1
%         roiDiffStat(iPath,iLoc) = ranksum(meanSpeedROI{1,iPath}(iLoc,:),meanSpeedROI{2,iPath}(iLoc,:));
%     end
%     figure(vsROIFig2);
%     subplot(2,4,iPath);
%     xVals = find(roiDiffStat(iPath,1:pathLength) < 0.05/pathLength);
%     plot(xVals,120*ones(numel(xVals,1)),'*k');
% end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%