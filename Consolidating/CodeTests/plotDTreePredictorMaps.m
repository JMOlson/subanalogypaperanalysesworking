function plotDTreePredictorMaps(inputVar, firingRate, nBins, isCircular)
[counts,edges,binInd] = histcounts(inputVar, nBins);
if isCircular
    polarplot(edges(1:end-1), accumarray(binInd, firingRate, [], @mean))
    hold on;
    polarplot(edges(1:end-1), accumarray(binInd, firingRate, [], @std))
    hold off;
else
    plot(edges(1:end-1), accumarray(binInd, firingRate, [], @mean));
    hold on;
    plot(edges(1:end-1), accumarray(binInd, firingRate, [], @std))
    hold off;
end
end