% sub_rec = RecStruct;

thisRec = 1;
thisPath = 5;
% thisRun = 4;

nRuns = sub_rec.Behavior.nRunsEachPath{thisRec}(thisPath);



histogram(sub_rec.Behavior.hdAlignedNorth_Rad{thisRec});


figure;
for iRun = 1:nRuns
    binsToPlotStart = sub_rec.Behavior.pathRunsLineMarkers{thisRec}{thisPath,1}(iRun);
    binsToPlotEnd = sub_rec.Behavior.pathRunsLineMarkers{thisRec}{thisPath,2}(iRun);
    
    subplot(1,2,1)
    scatter(sub_rec.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,1),...
        sub_rec.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,2),16,...
        sub_rec.Behavior.velSmoothed_CmPerSec{thisRec}(binsToPlotStart:binsToPlotEnd),'filled');
    cmocean('thermal')
    caxis([0,80]);
    subplot(1,2,2)
    scatter(sub_rec.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,1),...
        sub_rec.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,2),16,...
        sub_rec.Behavior.angVelSmoothed_RadPerSec{thisRec}(binsToPlotStart:binsToPlotEnd),'filled');
    cmocean('thermal')
    caxis([-20,20]);
%     subplot(2,2,3)
%     scatter(sub_rec.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,1),...
%         sub_rec.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,2),4,...
%         sub_rec.Behavior.hdAlignedNorth_Rad{thisRec}(binsToPlotStart:binsToPlotEnd));
%     caxis([-pi,pi]);
%     cmocean('phase')
%     subplot(2,2,4)
%     scatter(sub_rec.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,1),...
%         sub_rec.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,2),4,...
%         sub_rec.Behavior.axisAlignedNorth_Rad{thisRec}(binsToPlotStart:binsToPlotEnd));
%     cmocean('phase')
%     caxis([0,pi]);
    pause;
end

%%

figure;
for iPath = 1:sub_rec.Behavior.nPaths(thisRec)
    thisPath = sub_rec.Behavior.pathList{thisRec}(iPath);
subplot(1,2,1)
    scatter(sub_rec.Behavior.posXYMeanPathAligned_Pixels{thisRec,thisPath}(1,:),...
        sub_rec.Behavior.posXYMeanPathAligned_Pixels{thisRec,thisPath}(2,:),16,...
        sub_rec.Behavior.speedMeanPathAligned_CmPerSec{thisRec,thisPath},'filled');
    cmocean('thermal')
    caxis([0,80]);
    subplot(1,2,2)
    scatter(sub_rec.Behavior.posXYMeanPathAligned_Pixels{thisRec,thisPath}(1,:),...
        sub_rec.Behavior.posXYMeanPathAligned_Pixels{thisRec,thisPath}(2,:),16,...
        sub_rec.Behavior.angVelMeanPathAligned_RadPerSec{thisRec,thisPath},'filled');
    cmocean('thermal')
    pause;
end


%%

figure;
for iPath = 1:sub_rec.Behavior.nPaths(thisRec)
    thisPath = sub_rec.Behavior.pathList{thisRec}(iPath);
    nRuns = sub_rec.Behavior.nRunsEachPath{thisRec}(thisPath);
    for iRun = 1:nRuns

subplot(1,2,1)
    scatter(sub_rec.Behavior.posXPathAligned{thisRec,thisPath}(iRun,:),...
        sub_rec.Behavior.posYPathAligned{thisRec,thisPath}(iRun,:),16,...
        sub_rec.Behavior.speedPathAligned_CmPerSec{thisRec,thisPath}(iRun,:),'filled');
    cmocean('thermal')
    caxis([0,80]);
    subplot(1,2,2)
    scatter(sub_rec.Behavior.posXPathAligned{thisRec,thisPath}(iRun,:),...
        sub_rec.Behavior.posYPathAligned{thisRec,thisPath}(iRun,:),16,...
        sub_rec.Behavior.angVelPathAligned_RadPerSec{thisRec,thisPath}(iRun,:),'filled');
    cmocean('thermal')
    pause;
    end
end


%%

figure;
for iPath = 1:sub_rec.Behavior.nPaths(thisRec)
    thisPath = sub_rec.Behavior.pathList{thisRec}(iPath);
subplot(1,2,1)
    scatter(sub_rec.Behavior.posXYMeanPathAligned_Pixels{thisRec,thisPath}(1,:),...
        sub_rec.Behavior.posXYMeanPathAligned_Pixels{thisRec,thisPath}(2,:),16,...
        sub_rec.Behavior.speedMeanPathAligned_CmPerSec{thisRec,thisPath},'filled');
    cmocean('thermal')
    caxis([0,80]);
    subplot(1,2,2)
    scatter(sub_rec.Behavior.posXYMeanPathAligned_Pixels{thisRec,thisPath}(1,:),...
        sub_rec.Behavior.posXYMeanPathAligned_Pixels{thisRec,thisPath}(2,:),16,...
        sub_rec.Behavior.angVelMeanPathAligned_RadPerSec{thisRec,thisPath},'filled');
    cmocean('thermal')
    pause;
end

%%
binsToPlotStart = RecStruct.sessionTimeStamps{thisRec}(3);
binsToPlotEnd = RecStruct.sessionTimeStamps{thisRec}(4);

figure;
scatter(RecStruct.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,1),...
    RecStruct.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,2),4,...
    RecStruct.Behavior.hdAlignedNorth_Rad{thisRec}(binsToPlotStart:binsToPlotEnd));
cmocean('phase')

figure;
scatter(RecStruct.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,1),...
    RecStruct.Behavior.posXY{thisRec}(binsToPlotStart:binsToPlotEnd,2),4,...
    RecStruct.Behavior.axisAlignedNorth_Rad{thisRec}(binsToPlotStart:binsToPlotEnd));
cmocean('phase')