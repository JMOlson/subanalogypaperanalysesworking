%% Smoothing Testing

% Settings from Linear Rate maps
filterWidth = 13; %Must be odd.
filterMedian = (filterWidth-1)/2;
filterSigma = 1;
filterBins=0:1:filterWidth-1;
filterValues=gaussmf(filterBins,[filterSigma, filterMedian]);
filterValuesNormed=filterValues/sum(filterValues);


dataPoints = [2,12,20,32,36,40,40,60,60];
fakeTestData = zeros(100,1);
fakeTestData2 = zeros(100,1);
for iDP = 1:numel(dataPoints)
    if dataPoints(iDP) <= filterMedian
        indices = [1:(dataPoints(iDP)+filterMedian)];
        filterChop = filterWidth - numel(indices);
        fakeTestData(indices) = fakeTestData(indices)+filterValuesNormed(1+filterChop:end)';
    else
        indices = [(dataPoints(iDP)-filterMedian):(dataPoints(iDP)+filterMedian)];
        fakeTestData(indices) = fakeTestData(indices)+filterValuesNormed';
    end
    fakeTestData2(dataPoints(iDP)) = fakeTestData2(dataPoints(iDP))+1;
end


figure;
hold on;
plot(fakeTestData); % way spikes are calced as of Apr 2020, then dev by occ calced the same way.

% Smoothdata uses a std dev on it's gaussian smoothing kernel equal to 5*the window size.
% So we input the window size to be std dev x 5
plot(smoothdata(fakeTestData2,'gaussian',5*filterSigma));

% They are the same, but spike is weighted more at beginning/end so pdf still adds to 1. Once
% normed, i believe still identical.



