minLeafSize = [1 5 10 15 20 50];

maxTrees = 100;
nNeu = length(DTreeDataTable);
treeBaggerError = nan(maxTrees, length(minLeafSize));
for iNeu = 1:nNeu
    for i=1:length(minLeafSize)
        b = TreeBagger(maxTrees,DTreeDataTable{iNeu},'meanFR','Method','regression',...
            'OOBPrediction','On', 'MinLeafSize', minLeafSize(i));
        treeBaggerError(:, i, iNeu) = oobError(b);
    end
end

figure;
plot(nanmean(treeBaggerError,3));
xlabel('Number of Grown Trees')
ylabel('Mean Squared Error')
legend({'1' '5' '10' '20' '50'},'Location','NorthEast')

figure;
plot(nanstd(treeBaggerError,'',3));
xlabel('Number of Grown Trees')
ylabel('std dev')
legend({'1' '5' '10' '20' '50'},'Location','NorthEast')


%%
for iNeu = 1:nNeu
    figure;
    plot(treeBaggerError(:,:,iNeu));
    xlabel('Number of Grown Trees')
    ylabel('Mean Squared Error')
    legend({'1' '5' '10' '20' '50'},'Location','NorthEast')
end
