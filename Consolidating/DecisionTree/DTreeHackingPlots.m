
%%
figure;
plotSpread({baselineErrs_Hpc, oobErrs_Hpc, baselineErrs_Sub, oobErrs_Sub});
title('Test Error');
ylabel('Absolute Error');
xlabel('ROIs');
h = gca;
h.XTickLabel = {'HPC Std Dev','HPC';'SUB Std Dev','SUB'};
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';

%%
% Counts of ranks of each predictor.
nPreds = size(impVals_Hpc,2);
nHpcNeu = length(oobErrs_Hpc);
nSubNeu = length(oobErrs_Sub);

[~,sortInds_Hpc] = sort(impVals_Hpc, 2, 'descend');
[~,sortInds_Sub] = sort(impVals_Sub, 2, 'descend');

goodFitsOnly_Hpc = improvement_Hpc >= 2; % doubled the naive model
goodFitsOnly_Sub = improvement_Sub >= 2; % doubled the naive model

% each row is the probability of that rank for each predictor (columns)
impValRankCounts_Hpc = nan(nPreds);
impValRankCounts_Sub = nan(nPreds);
impValRankCountsGFO_Hpc = nan(nPreds); % good fits only
impValRankCountsGFO_Sub = nan(nPreds); % good fits only
for iRank = 1:nPreds
 impValRankCounts_Hpc(iRank,:) = histcounts(sortInds_Hpc(:,iRank),[0.5:1:nPreds+0.5],'Normalization','probability')';
 impValRankCounts_Sub(iRank,:) = histcounts(sortInds_Sub(:,iRank),[0.5:1:nPreds+0.5],'Normalization','probability')';

 impValRankCountsGFO_Hpc(iRank,:) = histcounts(sortInds_Hpc(goodFitsOnly_Hpc,iRank),[0.5:1:nPreds+0.5],'Normalization','probability')';
 impValRankCountsGFO_Sub(iRank,:) = histcounts(sortInds_Sub(goodFitsOnly_Sub,iRank),[0.5:1:nPreds+0.5],'Normalization','probability')';

end


impValMeanRanks_Hpc = [1:nPreds]*impValRankCounts_Hpc;
impValMeanRanks_Sub = [1:nPreds]*impValRankCounts_Sub;

impValMeanRanksGFO_Hpc = [1:nPreds]*impValRankCountsGFO_Hpc;
impValMeanRanksGFO_Sub = [1:nPreds]*impValRankCountsGFO_Sub;

%%
figure;
bar([impValMeanRanksGFO_Hpc;impValMeanRanksGFO_Sub]);
title('Mean Rank');
ylabel('Mean Ranks');
xlabel('Predictors');
h = gca;
h.XTickLabel = {'Hpc';'Sub'};
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';
legend(predictorList);

%% distributions of importance values
figure;
subplot(2,1,1);
plotSpread(impVals_Scaled_Hpc,'distributionColor','b');
title('Importance Values');
ylabel('Importance Values');
xlabel('Predictors');
h = gca;
h.XTickLabel =  predictorList;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';

subplot(2,1,2);
plotSpread(impVals_Scaled_Sub,'distributionColor','r');
ylabel('Importance Values');
xlabel('Predictors');
h = gca;
h.XTickLabel = predictorList;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';

%%
figure;
subplot(2,1,1);
bar(impValRankCounts_Hpc');
title('Hpc');
ylabel('Ranks');
xlabel('Predictors');
h = gca;
h.YLim = [0,0.5];
h.XTickLabel = predictorList;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';

subplot(2,1,2);
bar(impValRankCounts_Sub');
title('Sub');
ylabel('Ranks');
xlabel('Predictors');
h = gca;
h.YLim = [0,0.5];
h.XTickLabel = predictorList;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';
legend('1','2','3','4','5','6');

%%


figure;
for iPred = 1:nPreds
    subplot(1,nPreds,iPred);
    
    title(predictorList{iPred});

end
title('Sub');
ylabel('Ranks');
xlabel('Predictors');
h = gca;
h.YLim = [0,nPreds+1];
h.XTickLabel = predictorList;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';


figure;
plotSpread({oobErrs_Hpc, oobErrs_Sub});
title('Test Error');
ylabel('Predictor importance estimates');
xlabel('Predictors');
h = gca;
h.YLim = [0,5];
h.XTickLabel = predictorList;
h.XTickLabelRotation = 45;
h.TickLabelInterpreter = 'none';

