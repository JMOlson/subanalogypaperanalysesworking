%% Single Trees for comparison

nPreds = size(impVals_Hpc,2);
nHpcNeu = length(oobErrs_Hpc);
nSubNeu = length(oobErrs_Sub);

hpcImp = nan(nHpcNeu,nPreds);
for iNeu = 1:nHpcNeu
    hpcTree{iNeu} = fitrtree(DTreeInputTables_Hpc{iNeu}, 'meanFR', 'PredictorNames',...
        predictorList,'PredictorSelection','interaction-curvature');
    hpcImp(iNeu,:) = predictorImportance(hpcTree{iNeu});
end

subImp = nan(nSubNeu,nPreds);
for iNeu = 1:nSubNeu
    subTree{iNeu}  = fitrtree(DTreeInputTables_Sub{iNeu}, 'meanFR', 'PredictorNames',...
        predictorList,'PredictorSelection','interaction-curvature');
    subImp(iNeu,:) = predictorImportance(subTree{iNeu});
end