function [DTreeInputTables] = makeDTreeInputs(NeuronStruct, RecStruct, outboundPaths, returnPaths, nBins)
%makeDTreeInputs Convert behavioral variables into inputs ready for decision tree analysis
%       Also creates track space, proportion through and task phase variables and rotates
%       circular variables so that the break point is at a location where the value is a minimum.
%      (See Remarks)
%
%		Used in the 2020 Olson, Alexander, ... Nitz paper
%
% 	[DTreeStruct] = makeDTreeInputs(NeuronStruct, RecStruct, outboundPaths, returnPaths)
%
% INPUTS:
%		NeuronStruct - output from SubAnalogyDraft compile Recs fn - neuron based struct
%		RecStruct - output from SubAnalogyDraft compile Recs fn    recording based struct
%		outboundPaths - list of the path IDs of outbound paths to reward locations - treated as
%                       equal in task phase.
%       returnPaths - list of the path IDs of return paths - treated as equal in task phase.
%       nBins - binning all input variables to 100 bins - doing this so slice opportunities are
%       equal across input parameters. Should help stabilize and unbias our importance value results.
%
% OUTPUTS:
%		DTreeInputTables - Cell vector nNeu x 1 of tables of length nTrackPositions of the behavior
%                        predictors and firing rate by track position
%
% EXAMPLES:
%		DTreeStruct = makeDTreeInputs(HpcNeuron, HpcRec, [1,2,3,4],[9,10]);
%
%
% REMARKS - assumes we have complete data for all paths we are going to analyze, for all recordings.
%   The rotation of circular variables is done so binary decision splits can be made without
%   potentially splitting firing fields - just like is the case for the standard linear variables.
%
% OTHER M-FILES REQUIRED: none
% SUBFUNCTIONS:
% MAT-FILES REQUIRED:
%
% SEE ALSO: SUBANALOGY_ANALYSIS_Draft
%
% CREATED BY: Jacob M Olson based on code by Xuefei Wang & Alex Johnson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: Aug-2020
% LAST MODIFIED BY:
% LAST MODIFIED ON:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

VARIABLE_NAMES = {'meanFR'; 'angVel'; 'hd'; 'axis'; 'propThru'; 'taskPhase'; 'linVel'; 'acc';...
    'trackSpace'; 'fieldFaker';'xs';'ys'};

% linearizing the track as best as possible. This is so that local place fields will be best
% predicted by this feature instead of one of our other features.
TRACK_SPACE = round([...
    1:140,... % path 1
    1:118, 141:162,... % path 2 - splits from path 1 at 118
    1:51, 163:251,... % path 3 - splits from path 1/2 at 51
    1:51, 163:229, 252:273,... % path 4 - splits from path 1/2 at 51, path 3 at 118
    274:470, 471:667])'; % paths 9 and 10 are separate

%% Some helpful variables to define.
paths = [outboundPaths, returnPaths];
nNeu = size(NeuronStruct.rat,2);

% Get path lengths - should be the same for all recs, so just using 1st rec
iRec = 1;
[~,pathListInds] = ismember(paths,RecStruct.Behavior.pathList{iRec});
if numel(pathListInds) ~= numel(paths)
    error('this rec doesn''t have all paths');
end
pathLengths = RecStruct.Behavior.pathLengths{iRec}(pathListInds);

%% Proportion Through - same for all neurons
propThru = cell(numel(paths),1);
for iPath = 1:numel(pathLengths)
    propThru{iPath} = (0:(1/(pathLengths(iPath)-1)):1);
end
propThruCatPaths = cell2mat(propThru')';

%% Task Phase - same for all neurons
[~,outboundPathsInds] = ismember(outboundPaths,RecStruct.Behavior.pathList{iRec});
[~,returnPathsInds] = ismember(returnPaths,RecStruct.Behavior.pathList{iRec});
outboundPathLength = pathLengths(outboundPathsInds(1));
returnPathLength = pathLengths(returnPathsInds(1));

taskCycleLength = outboundPathLength + returnPathLength;
taskPhase = cell(numel(paths),1);
for iPath = 1:numel(pathLengths)
    if ismember(iPath,outboundPathsInds)
        taskPhase{iPath} = (0:outboundPathLength-1)./taskCycleLength;
    else
        taskPhase{iPath} = (outboundPathLength:outboundPathLength+returnPathLength-1)./taskCycleLength;
    end
end
taskPhaseCatPaths = cell2mat(taskPhase')';

%% Reformat behavioral variables for input to decision tree. Organizing them by neuron not rec.
DTreeInputTables = cell(nNeu,1);

for iNeu = 1:nNeu
    recIndex = NeuronStruct.recStructIndex(iNeu);
    
    xs = cell2mat(cellfun(@(x) x(1,:),...
        RecStruct.Behavior.posXYMeanPathAligned_Pixels(recIndex,paths),'UniformOutput', false))';
    ys = cell2mat(cellfun(@(x) x(2,:),...
        RecStruct.Behavior.posXYMeanPathAligned_Pixels(recIndex,paths),'UniformOutput', false))';
    
    theta = pi/4;
    rotMat = [cos(theta), -sin(theta); sin(theta), cos(theta)];
    combinedRotXYs = rotMat * [xs';ys'];
    
    
    meanFR = cell2mat(cellfun(@(x) x(:,1)',... % columns are FR, Std Dev, SEM.
        NeuronStruct.MeanLinearRates(iNeu, paths), 'UniformOutput', false))';
    
    angVel = cell2mat(RecStruct.Behavior.angVelMeanPathAligned_RadPerSec(recIndex,paths))';
    hd = cell2mat(RecStruct.Behavior.hdMeanPathAligned_Rad(recIndex,paths))';
    axis = cell2mat(RecStruct.Behavior.axisMeanPathAligned_Rad(recIndex,paths))';
    linVel = cell2mat(RecStruct.Behavior.speedMeanPathAligned_CmPerSec(recIndex,paths))';
    acc = cell2mat(RecStruct.Behavior.accMeanPathAligned_CmPerSecSQ(recIndex,paths))';
    
    %% rotate the indicies of the circular variables to align the break at a minimum.
    rotatedHD = rotateCircularVariable(hd, meanFR);
    rotatedAxis = rotateCircularVariable(2*axis, meanFR); % gotta scale axis to circle
    rotatedTaskPhase = rotateCircularVariable(2*pi*taskPhaseCatPaths, meanFR); % gotta scale task phase to circle
    
    %% Alex's Field FakerVar - create something that looks like a field at the max - may work for single place fields
    fieldFaker = zeros(length(meanFR),1);
    [~, maxInd] = max(meanFR);
    halfFieldWidth = 15;
    if maxInd <= halfFieldWidth
        fieldFaker(1:maxInd+halfFieldWidth) = normpdf([-maxInd+1:halfFieldWidth],20,10); % arbitrary #s, gets the idea.
    elseif maxInd >= length(meanFR)-halfFieldWidth
        fieldFaker(maxInd-halfFieldWidth:end) = normpdf([-halfFieldWidth:length(meanFR)-maxInd],20,10); % arbitrary #s, gets the idea.
    else
        fieldFaker(maxInd-halfFieldWidth:maxInd+halfFieldWidth) = normpdf([-halfFieldWidth:halfFieldWidth],20,10); % arbitrary #s, gets the idea.
    end
    
    % DTreeInputTables{iNeu} = table(meanFR, angVel, rotatedHD, rotatedAxis, propThruCatPaths,...
    %         rotatedTaskPhase, linVel, acc, TRACK_SPACE, fieldFaker,combinedRotXYs(1,:)',combinedRotXYs(2,:)',  'VariableNames',VARIABLE_NAMES);
    
    %% Bin and assign to outputs
    binnedMeanFR = binVariable(meanFR, nBins);
    binnedAngVel = binVariable(angVel, nBins);
    binnedRotHD = binVariable(rotatedHD, nBins);
    binnedRotAxis = binVariable(rotatedAxis, nBins);
    binnedPropThruCatPaths = binVariable(propThruCatPaths, nBins);
    binnedRotTaskPhase = binVariable(rotatedTaskPhase, nBins);
    binnedLinVel = binVariable(linVel, nBins);
    binnedAcc = binVariable(acc, nBins);
    binnedTrackSpace = binVariable(TRACK_SPACE, nBins);
    binnedFieldFaker = binVariable(fieldFaker, nBins);
    binnedRotXs = binVariable(combinedRotXYs(1,:)', nBins);
    binnedRotYs = binVariable(combinedRotXYs(2,:)', nBins);
    
    
    
    DTreeInputTables{iNeu} = table(...
        binnedMeanFR,...
        binnedAngVel,...
        binnedRotHD,...
        binnedRotAxis,...
        binnedPropThruCatPaths,...
        binnedRotTaskPhase,...
        binnedLinVel,...
        binnedAcc,...
        binnedTrackSpace,...
        binnedFieldFaker,...
        binnedRotXs,...
        binnedRotYs, 'VariableNames',VARIABLE_NAMES);
end

end

function rotatedVar = rotateCircularVariable(circVar, firingRate)
% Going to use the circular variable value at each track position and the firing rate at each
% position as a proxy for a

N_BINS = 360/15; % 15 degree bins
binStepSize = 2*pi/N_BINS;
binEdges = [0 : binStepSize : 2*pi];

%% Input handling
% transpose if neccesary for correct shape cuz 'accumarry' requires (xx,1)
if (size(circVar,2) ~= 1)
    circVar = circVar';
end
if (size(firingRate,2) ~= 1)
    firingRate = firingRate';
end

%% Circular maps
% find corresponding bin for each circVar value.
[~, ~, binInd] = histcounts(mod(circVar, 2*pi), binEdges);
% averaged bin'd firing rates, according to their corresponding circVar value
ratesBins = accumarray(binInd, firingRate, [], @mean);

% find indices of minima, randomly choose one.
minInds = find(min(ratesBins) == ratesBins);
shiftVal = minInds(randsample(length(minInds),1));

% rotate so that is the cut point
rotatedVar = mod(circVar - binEdges(shiftVal), 2*pi);

end

function binnedVar = binVariable(inputVar, nBins)
% Going to bin all input variables so there are equal opportunities for granularity for the decision
% tree across variables.

% get range of variable
minVal = min(inputVar);
maxVal = max(inputVar);
range = maxVal - minVal;

% create bins
binStepSize = range/nBins;
binCenters = [minVal+binStepSize/2 : binStepSize : maxVal-binStepSize/2];
if isempty(binCenters) %% only 1 val - probably FR is 0 everywhere
    binCenters = [minVal, minVal+1];
end

% transform values into binned values (essentially by rounding to the nearest bincenter)
binnedVar = interp1(binCenters,binCenters,inputVar,'nearest','extrap');
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
