function [treeBags, impVals, baselineErrs, oobErrs] = calcTreeBagWrapper(dataTable, predictorList,...
 outputVarName, nTrees, varargin)
%calcTreeBagWrapper 
%
%		Used in the 2020 Olson, Alexander, ... Nitz paper
%
% 	[DTreeStruct] = makeDTreeInputs(NeuronStruct, RecStruct, outboundPaths, returnPaths)
%
% INPUTS:
%		dataTable - output from SubAnalogyDraft compile Recs fn - neuron based struct
%		predictorList - output from SubAnalogyDraft compile Recs fn    recording based struct
%		outputVarName - list of the path IDs of outbound paths to reward locations - treated as
%                       equal in task phase.
%       nTrees - list of the path IDs of return paths - treated as equal in task phase.
%       varargin - binning all input variables to 100 bins - doing this so slice opportunities are
%
% OUTPUTS:
%		treeBags - output from SubAnalogyDraft compile Recs fn - neuron based struct
%		impVals - output from SubAnalogyDraft compile Recs fn    recording based struct
%		baselineErrs - list of the path IDs of outbound paths to reward locations - treated as
%                       equal in task phase.
%       oobErrs - list of the path IDs of return paths - treated as equal in task phase.
%
% EXAMPLES:
%		DTreeStruct = makeDTreeInputs(HpcNeuron, HpcRec, [1,2,3,4],[9,10]);
%
%
% REMARKS 
%
% OTHER M-FILES REQUIRED: none
% SUBFUNCTIONS:
% MAT-FILES REQUIRED:
%
% SEE ALSO:
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: Aug-2020
% LAST MODIFIED BY:
% LAST MODIFIED ON:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('nTrees','var')
    nTrees = 100;
elseif isempty(nTrees)
    nTrees = 100;
end



% takes arguments as pairs, passes them on to treebagger.
nNeu = length(dataTable);
nPredictors = length(predictorList);

for iNeu = 1:nNeu
    treeBags{iNeu} = TreeBagger(nTrees, dataTable{iNeu}, outputVarName,...
        'PredictorNames', predictorList, 'Method','regression',...
        'NumPredictorsToSample', ceil(sqrt(nPredictors)), varargin{:});
end

impVals = cell2mat(cellfun(@(x) x.OOBPermutedPredictorDeltaError', treeBags, 'UniformOutput', false))';
baselineErrs = cellfun(@(x) std(x.meanFR),dataTable);
oobErrs = cellfun(@(x) sqrt(x.oobError('mode','ensemble')),treeBags)';
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

