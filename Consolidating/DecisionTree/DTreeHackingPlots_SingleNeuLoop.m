DTreeInputTables = DTreeInputTables_Hpc;
nPredictors = size(impVals_Hpc,2);

for iNeu = 1:length(DTreeInputTables)
    figure(1);
    subplot(1,nPredictors,1); plotDTreePredictorMaps(DTreeInputTables{iNeu}.angVel, DTreeInputTables{iNeu}.meanFR, 25, false);
    title('angVel');
    subplot(1,nPredictors,2); plotDTreePredictorMaps(DTreeInputTables{iNeu}.hd, DTreeInputTables{iNeu}.meanFR, 25, true);
    title('hd');
    subplot(1,nPredictors,3); plotDTreePredictorMaps(DTreeInputTables{iNeu}.axis, DTreeInputTables{iNeu}.meanFR, 25, true);
    title('axis');
    subplot(1,nPredictors,4); plotDTreePredictorMaps(DTreeInputTables{iNeu}.taskPhase, DTreeInputTables{iNeu}.meanFR, 25, true);
    title('taskPhase');
    subplot(1,nPredictors,5); plotDTreePredictorMaps(DTreeInputTables{iNeu}.linVel, DTreeInputTables{iNeu}.meanFR, 25, false);
    title('linVel');
    subplot(1,nPredictors,6); plotDTreePredictorMaps(DTreeInputTables{iNeu}.acc, DTreeInputTables{iNeu}.meanFR, 25, false);
    title('acc');
    subplot(1,nPredictors,7); plotDTreePredictorMaps(DTreeInputTables{iNeu}.trackSpace, DTreeInputTables{iNeu}.meanFR, 100, false);
    title('trackSpace');

    figure(2);
    bar(treeBags_Hpc{iNeu}.OOBPermutedPredictorDeltaError);
    title(sprintf('interaction-curvature Test: Neuron %i', iNeu));
    ylabel('Predictor importance estimates');
    xlabel('Predictors');
    h = gca;
    h.YLim = [0,5];
    h.XTickLabel = predictorList;
    h.XTickLabelRotation = 45;
    h.TickLabelInterpreter = 'none';

    figure(3);
    subplot(6,1,1)
    plot(DTreeInputTables{iNeu}.meanFR(1:140))
    subplot(6,1,2)
    plot(DTreeInputTables{iNeu}.meanFR(141:280))
    subplot(6,1,3)
    plot(DTreeInputTables{iNeu}.meanFR(281:420))
    subplot(6,1,4)
    plot(DTreeInputTables{iNeu}.meanFR(421:560))
    subplot(6,1,5)
    plot(DTreeInputTables{iNeu}.meanFR(561:560+197))
    subplot(6,1,6)
    plot(DTreeInputTables{iNeu}.meanFR(560+198:end))
    
    figure(4);
    bar([baselineErrs_Hpc(iNeu),oobErrs_Hpc(iNeu)]);
    title(sprintf('OOB Error: Neuron %i', iNeu));
    ylabel('MSE Test Error');
    h = gca;
    h.XTickLabel = {'std dev';'Hpc'};
    h.TickLabelInterpreter = 'none';
    
    twoDRMapPlotter(HpcNeuron, HpcRec, iNeu, 'sampleNSpikes');
    pause;
    



pause;
end

