DTreeInputTables = DTreeInputTables_Hpc;
NeuStruct = HpcNeuron;
RecStruct = HpcRec;

debugFlag = true;

%% I think we should add a local parameter
variableNames = {'meanFR'; 'angVel'; 'hd'; 'axis'; 'propThru'; 'taskPhase'; 'linVel'; 'acc';...
    'trackSpace'; 'fieldFaker';'xs';'ys'};
nNeu = length(DTreeInputTables);

% need to find this w/ hyperparameter search
nTrees = 100;
minLeafSize = 5;

predictorList = variableNames([2,3,4,6,7,8,9]);
nPredictors = length(predictorList);
neuList = [51, 162, 149, 143, 233, 228];
% for iNeu = neuList
for iNeu = 1:randperm(nNeu) % get a variety of neurons I'm looking at
    treeBag_IntCurv{iNeu} = TreeBagger(nTrees, DTreeInputTables{iNeu}, 'meanFR',...
        'PredictorNames', predictorList,...
        'Method','regression', 'MinLeafSize', minLeafSize, ...
        'PredictorSelection','interaction-curvature', 'OOBPredictorImportance','on',...
        'OOBPrediction','On', 'NumPredictorsToSample', ceil(sqrt(nPredictors)));
    
    treeBag_Curv{iNeu} = TreeBagger(nTrees, DTreeInputTables{iNeu}, 'meanFR',...
        'PredictorNames', predictorList,...
        'Method','regression', 'MinLeafSize', minLeafSize, 'MaxNumSplits', 5,...
        'PredictorSelection','interaction-curvature', 'OOBPredictorImportance','on',...
        'OOBPrediction','On', 'NumPredictorsToSample', ceil(sqrt(nPredictors)));
    
    treeBag_IntCurv5{iNeu} = TreeBagger(nTrees, DTreeInputTables{iNeu}, 'meanFR',...
        'PredictorNames', predictorList,...
        'Method','regression', 'MinLeafSize', minLeafSize, 'MaxNumSplits', 10,...
        'PredictorSelection','interaction-curvature', 'OOBPredictorImportance','on',...
        'OOBPrediction','On', 'NumPredictorsToSample', ceil(sqrt(nPredictors)));
    
    if debugFlag
        figure(1);
        subplot(1,nPredictors,1); plotDTreePredictorMaps(DTreeInputTables{iNeu}.angVel, DTreeInputTables{iNeu}.meanFR, 25, false);
        title('angVel');
        subplot(1,nPredictors,2); plotDTreePredictorMaps(DTreeInputTables{iNeu}.hd, DTreeInputTables{iNeu}.meanFR, 25, true);
        title('hd');
        subplot(1,nPredictors,3); plotDTreePredictorMaps(DTreeInputTables{iNeu}.axis, DTreeInputTables{iNeu}.meanFR, 25, true);
        title('axis');
%         subplot(1,nPredictors,4); plotDTreePredictorMaps(DTreeInputTables{iNeu}.propThru, DTreeInputTables{iNeu}.meanFR, 25, false);
%         title('propThrough');
        subplot(1,nPredictors,4); plotDTreePredictorMaps(DTreeInputTables{iNeu}.taskPhase, DTreeInputTables{iNeu}.meanFR, 25, true);
        title('taskPhase');
        subplot(1,nPredictors,5); plotDTreePredictorMaps(DTreeInputTables{iNeu}.linVel, DTreeInputTables{iNeu}.meanFR, 25, false);
        title('linVel');
        subplot(1,nPredictors,6); plotDTreePredictorMaps(DTreeInputTables{iNeu}.acc, DTreeInputTables{iNeu}.meanFR, 25, false);
        title('acc');
        subplot(1,nPredictors,7); plotDTreePredictorMaps(DTreeInputTables{iNeu}.trackSpace, DTreeInputTables{iNeu}.meanFR, 100, false);
        title('trackSpace');
%         subplot(1,nPredictors,7); plotDTreePredictorMaps(DTreeInputTables{iNeu}.xs, DTreeInputTables{iNeu}.meanFR, 25, false);
%         title('xs');
%         subplot(1,nPredictors,8); plotDTreePredictorMaps(DTreeInputTables{iNeu}.ys, DTreeInputTables{iNeu}.meanFR, 25, false);
%         title('ys');
        
        figure(2);
        bar(treeBag_IntCurv{iNeu}.OOBPermutedPredictorDeltaError);
        title(sprintf('interaction-curvature Test: Neuron %i', iNeu));
        ylabel('Predictor importance estimates');
        xlabel('Predictors');
        h = gca;
        h.YLim = [0,5];
        h.XTickLabel = predictorList;
        h.XTickLabelRotation = 45;
        h.TickLabelInterpreter = 'none';
        
        figure(3);
        bar(treeBag_Curv{iNeu}.OOBPermutedPredictorDeltaError);
        title(sprintf('curvature Test: Neuron %i', iNeu));
        ylabel('Predictor importance estimates');
        xlabel('Predictors');
        h = gca;
        h.YLim = [0,5];
        h.XTickLabel = predictorList;
        h.XTickLabelRotation = 45;
        h.TickLabelInterpreter = 'none';
        
%         figure(4);
%         bar(treeBag_CART{iNeu}.OOBPermutedPredictorDeltaError);
%         title(sprintf('classic: Neuron %i', iNeu));
%         ylabel('Predictor importance estimates');
%         xlabel('Predictors');
%         h = gca;
%         h.YLim = [0,5];
%         h.XTickLabel = predictorList;
%         h.XTickLabelRotation = 45;
%         h.TickLabelInterpreter = 'none';
        
        figure(5);
        bar(treeBag_IntCurv5{iNeu}.OOBPermutedPredictorDeltaError);
        title(sprintf('interaction-curvature Test 5Min: Neuron %i', iNeu));
        ylabel('Predictor importance estimates');
        xlabel('Predictors');
        h = gca;
        h.YLim = [0,5];
        h.XTickLabel = predictorList;
        h.XTickLabelRotation = 45;
        h.TickLabelInterpreter = 'none';
        
%         figure(6);
%         bar(treeBag_Curv5{iNeu}.OOBPermutedPredictorDeltaError);
%         title(sprintf('curvature Test 5Min: Neuron %i', iNeu));
%         ylabel('Predictor importance estimates');
%         xlabel('Predictors');
%         h = gca;
%         h.YLim = [0,5];
%         h.XTickLabel = predictorList;
%         h.XTickLabelRotation = 45;
%         h.TickLabelInterpreter = 'none';
        
%         figure(7);
%         plot([treeBag_CART{iNeu}.oobError, treeBag_Curv{iNeu}.oobError, treeBag_Curv5{iNeu}.oobError,...
%             treeBag_IntCurv{iNeu}.oobError, treeBag_IntCurv5{iNeu}.oobError]);
%         legend({'CART'; 'Curv'; 'Curv5'; 'IntCurv'; 'IntCurv5'});
        
        figure(8);
        subplot(6,1,1)
        plot(DTreeInputTables{iNeu}.meanFR(1:140))
        subplot(6,1,2)
        plot(DTreeInputTables{iNeu}.meanFR(141:280))
        subplot(6,1,3)
        plot(DTreeInputTables{iNeu}.meanFR(281:420))
        subplot(6,1,4)
        plot(DTreeInputTables{iNeu}.meanFR(421:560))
        subplot(6,1,5)
        plot(DTreeInputTables{iNeu}.meanFR(561:560+197))
        subplot(6,1,6)
        plot(DTreeInputTables{iNeu}.meanFR(560+198:end))
        
        figure(9);
        bar([std(DTreeInputTables{iNeu}.meanFR),...
            sqrt(treeBag_Curv{iNeu}.oobError('mode','ensemble')),...
            sqrt(treeBag_IntCurv{iNeu}.oobError('mode','ensemble')),...
            sqrt(treeBag_IntCurv5{iNeu}.oobError('mode','ensemble'))]);
        title(sprintf('OOB Error: Neuron %i', iNeu));
        ylabel('MSE Test Error');
        xlabel('Predictors');
        h = gca;
        h.XTickLabel = {'std dev';'Curv';'IntCurv';'IntCurv5'};
        h.XTickLabelRotation = 45;
        h.TickLabelInterpreter = 'none';

        twoDRMapPlotter(NeuStruct, RecStruct, iNeu, 'sampleNSpikes');
        pause;
    end
end

importanceValuesAllNeu_IntCurv = cell2mat(cellfun(@(x) x.OOBPermutedPredictorDeltaError', treeBag_IntCurv, 'UniformOutput', false))';
importanceValuesAllNeu_Curv = cell2mat(cellfun(@(x) x.OOBPermutedPredictorDeltaError', treeBag_Curv, 'UniformOutput', false))';
importanceValuesAllNeu_IntCurv = cell2mat(cellfun(@(x) x.OOBPermutedPredictorDeltaError', treeBag_IntCurv5, 'UniformOutput', false))';

%%

for iNeu = 1:nNeu
    b
    figure;
    bar(importanceValuesAllNeu(iNeu,:));
    title('interaction-curvature Test');
    ylabel('Predictor importance estimates');
    xlabel('Predictors');
    h = gca;
    h.XTickLabel = predictorList;
    h.XTickLabelRotation = 45;
    h.TickLabelInterpreter = 'none';
    
    twoDRMapPlotter(SubNeuron, SubRec, iNeu, 'sampleNSpikes');
    pause;
end