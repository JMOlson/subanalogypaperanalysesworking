function [output1, output2] = ssssss(input1, input2)
%SSSSSS One line description of what the function or script performs (H1 line)
%		Optional file header info (to give more details about the function than in the H1 line)  
%
% 	[output1, output2] = ssssss(input1, input2)
%
% INPUTS: 
%		input1 - Description
%		input2 - Description
%		input3 - Description
%		                      
%
% OUTPUTS: 
%		output1 - Description
%		output2 - Description
%		output3 - Description
%		                       
%
% EXAMPLES: 
%		Line 1 of example
%		Line 2 of example
%		Line 3 of example
%		                   
%
% REMARKS
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:%
% CREATED WITH MATLAB VERSION: 9.6.0.1150989 (R2019a) Update 4 on Microsoft Windows 10 Enterprise Version 10.0 (Build 18363)
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 20-Mar-2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
