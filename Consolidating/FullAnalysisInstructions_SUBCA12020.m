%% Instructions for Sub/CA1 Analogy Paper - Written starting in January 2018. This data is a 
% collection of data collected by Jake Olson & Alex Johnson's teams in Doug Nitz's lab at  UCSD.

% ---------------------- Data Collection ----------------------------- %
% Plexon System Acquiring Data
% Records 3 files with data acquisition
% 	- PLX file with waveforms and lfp data
% 	- avi of the video
% 	- dvt of light tracking

% ------------------ Initial Data Processing ------------------------- %
% There are two versions of these steps - one in the .\Data_Processing\TTTools\IndRecStruct
% and one in .\Data_Processing\TTTools\OLDTTTools
% The only substantial difference is that the new version saves results into a struct,
% whereas the old version saved things to files with the variables each separate.

% ------ Files run, in order:
% (Line numbers correspond to the new IndRecStruct files.)

% TTTrackingPreprocessor
% 	Loads in the .DVT file from CinePlex and outputs a "processedDVT" file. This is used in the 
% 	next steps of processing.
%   Creates the 'master' struct "indRecStruct" with the following fields:
% 		"dvtFileName" (Line 24) : what DVT filename is
% 		"dvtPathName" (Line 25) : where DVT file is located
% 		"trackingSampleRate" (Line 49) : frames per second we record position at
% 		"maxGapFilled" (Line 50) : Currently 0.5s - size of lost samples we allow to be automatically filled in.
% 		"samplesLost" (Line 112) : Vector for 1-Light inactive or 0-light active
% 		"samplesFilled" (Line 113) : Vector for 1-samples automatically filled or 0-samples left untouched
% 		"samplesUnfilled" (Line 114) : Inverse of samplesFilled
% 		"processedDVT" (Line 162) : Matrix for the entire recording consisting of indices, timestamps, X & Y.
% 			C3&4 = X&Y(respecively) for light 1
% 			C5&6 = X&Y(respecively) for light 2
% 			C7&8 = X&Y(respecively) for light average
% 			C9&10 = X&Y(respecively) for light mashup
% 		"velSmoothWinSecs" (Line 169) : number of seconds to calculate smoothed velocity over.
% 		"velInst" (Line 211) : Unsmoothed linear and angular velocity (pixels/radians per second) (samples missing are at back end)
% ?			(:,1,1) = Light X linear pixel displacement per second
% ?			(:,2,1) = Light Y angular radian displacement per second
% ?			(:,1,2) = Light X linear pixel displacement per second
% ?			(:,2,2) = Light Y angular radian displacement per second
% 		"accInst" (Line 212) : diff(velInst) in pixels/second/second (samples missing are at back end)
% 		"velSmoothed" (Line 213) : boxcar smoothed version of velInst(samples missing are at back end)
% 		"accSmoothed" (Line 214) : boxcar smoothed version of accInst (samples missing are at back end)
% 		"HDRadians" (Line 228) : Uses both lights to calculate radians for current Heading Direction.

% TTAutoPosTracker
% 	Uses "processedDVT" file and utilizes a GUI to determine timestamps for 'events' (Check notes 
%   to know for that recording the order of sessions)
% 	Creates new fields in "indRecStruct"  saves data as "PostAutoEvents"
% 		"sessionTimeStamps" (Line 436) : indices for epochs in the experiment
% 			C1 = Start of experiment
% 			C2 = End of experiment
% 			C3 = Start of session 1
% 			C4 = End of session 1
% 			C5 = Start of session 2
% 			C6 = End of session 2
% 		"events" (Line 437) : List of indices/timestamps/and start/stop times for all traversals (negative is a dirty run)
% 		"pixelDVT" (Line 438) : same as processedDVT with column11 (route animal is on)
% 		"minSpeedAllowed" (Line 439) : in pixels/second - if animal travels slower than this the automatic cutoff for a 'stalled/bad' run - is marked bad.

% TTPosTracker
% 	Human manually checks all runs in the "PostAutoEvents" version saves data as "Events".
% 	Changes to fields:
% 		"events" - human changed events go to the end

% TTLinearRatemapper
% TTLinearRatemapperMPC2017(TTBinTemplate);
% 	- This program provides an interface so that clicks can be made to mark
% 	the linear paths. It then maps tracking of clean runs to the closest
% 	bins to form the linear rate map. Smoothing is done while the map is
% 	created.
%	Uses "Events" file, TTBinTemplate, and sig.m file (clusters cut from .plx file exported w/ NEX)
%   and utilizes a GUI to determine which samples are binned to which linear position.
% 		TTBinTemplate = Independant matrix determining the number of bins between all click events
% 			This ought to be stereotyped so no alteration to this file is made
% 	Creates new fields in "indRecStruct"
% 		"tfileList" (Line 228) : Each cell's spiketimes are listed as a cell array
% 		"allTfiles" (Line 228) : Index of each cell
% 		"nCells" (Line 228) : Total number of cells for this recording
% 		"binTemplateMatrix" (Line 228) : The matrix of x&y values that defines the binning setup for that recording
% 		"filterWidthLinearRMaps" (Line 228) : The width for filtering the
% 		"filterSigmaLinearRMaps" (Line 228) :
% 		"filterLinearRMaps" (Line 228) : filtered linearized ratemaps. This is unbinned
% 		"pathList" (Line 228) : index to what each path is named
% 		"pathRunsLineMarkers" (Line 228) : index to when each path has started and stopped
% 		"pathRunsTimeMarkers" (Line 228) :  Timestamsp for when each path has started and stopped
% 		"nRunsEachPath" (Line 228) : number of completed runs for each of the paths
% 		"masterTemplate" (Line 228) :  X & Y coordinates where each click was made to fit the template to the runs
% 		"Clicks" (Line 228) : X & Y for where each click was made within the GUI framework
% 		"PathTemplate" (Line 228) : Similar to binTemplateMatrix, however this is broken up by path
% 		"meanTemplateOcc" (Line 228) : Mean occupancy for any binned position
% 		"TemplateOccupancyStats" (Line 228) : struct of positional occupancy stats.
% 		"LinearRates" (Line 228) : Cell array that is (numtrials x numbins x 3 - firing rate, spikes, occupancy) for each route.
% 		"MeanLinearRates" (Line 228) : Mean for above - (numbins x 3 - mean, std, sem)gives the average spiking for each position along each route.

% ------------------ Gathering Final Dataset ------------------------- %
%%Post ratemap
%Analysis type
%Analysis is identical for both formats, just differs in what variables are
%called:
%1 - indRecStruct format
%2 - pre-indRecStruct (old)

%% Select local data folder, define paths
% projectFolder = 'C:\Users\Aurif\Documents\Work\FinalIteration_AnalogyPaper\Processed_Data\Compiled'; %Alex Running code
% compiledDataFolder = '\FromJake\Finale';
projectFolder = 'E:\SubCa1Analogy\Data'; %Jake Running code
compiledDataFolder = '\20210904_Compiled';
% compiledDataFolder = '\20200922_Compiled';

%% Load neuron cluster data and compute quality metrics.
% clusterQualityFolder = [projectFolder,'\RAW_Data\CQ']; 
% [ca1_CQstruct, sub_CQstruct] = compileCQ_suban(clusterQualityFolder);
% save(fullfile(projectFolder,compiledDataFolder,'CQStructs.mat'),'ca1_CQstruct', 'sub_CQstruct');
load(fullfile(projectFolder,compiledDataFolder,'CQStructs.mat'),'ca1_CQstruct', 'sub_CQstruct');

%% Load neuron list, make activity-based exclusions (minimal activity, or high activity (putative inhibitory interneuron)
% Only needed if data not compiled yet below.
% load(fullfile(projectFolder,'RAW_Data\ListOfAllCells'),'ListOfAllCells');
% [ListofCells_PostExclusions, ClusterQuality_PostExclusions, ListofCells_TooActive, ListofCells_TooInactive, IncludeCellInfo] =...
%     excludeByFR_SUBAN(ListOfAllCells,[projectFolder,'\RAW_Data\All_Recordings'],...
%     [1,2],cat(1,ca1_CQstruct,sub_CQstruct),3,3);

% Last 2 inputs are to define exclusion criteria:
% threshold in Hz for Mean Firing rate being too high (never fires under 3Hz)
% threshold in Hz for Mean Linear Firing rate being too low (never fires above 3Hz)

% save(fullfile(projectFolder,compiledDataFolder,'ProcessedNeuronLists.mat'),...
%     'ListofCells_PostExclusions', 'ListofCells_TooActive', 'ListofCells_TooInactive');
% save(fullfile(projectFolder,compiledDataFolder,'ProcessedClusterQuality.mat'),...
%     'ClusterQuality_PostExclusions');
load(fullfile(projectFolder,compiledDataFolder,'ProcessedNeuronLists.mat'),'ListofCells_PostExclusions');
load(fullfile(projectFolder,compiledDataFolder,'ProcessedClusterQuality.mat'),'ClusterQuality_PostExclusions');

% Additional exclusions come from recordings
% without sufficient behavior (denoted in column 4 of the 'ListOfAllCells')

%% Create compiled datasets.
% [Neuron_Compilation]=compileRecs(regionIndex,cellSelectionListFileName,dataFolder)
% [HpcRec, HpcNeuron] = compileRecs('HPC',ListofCells_PostExclusions, [projectFolder,'\RAW_Data\All_Recordings']);
% [SubRec, SubNeuron] = compileRecs('SUB',ListofCells_PostExclusions, [projectFolder,'\RAW_Data\All_Recordings']);
% BehRec = compileRecs('BEH',ListofCells_PostExclusions, [projectFolder,'\RAW_Data\All_Recordings']);
% save(fullfile(projectFolder,compiledDataFolder,'FinalDatasets.mat'),...
%     'HpcRec', 'HpcNeuron', 'SubRec', 'SubNeuron','BehRec');
load(fullfile(projectFolder,compiledDataFolder,'FinalDatasets.mat'),...
    'HpcRec', 'HpcNeuron', 'SubRec', 'SubNeuron', 'BehRec');

%% Make compilations for excluded datasets - for verification
% [HpcRec_2H, HpcNeuron_2H] = compileRecs('HPC',ListofCells_TooActive, [projectFolder,'\RAW_Data\All_Recordings']);
% [SubRec_2H, SubNeuron_2H] = compileRecs('SUB',ListofCells_TooActive, [projectFolder,'\RAW_Data\All_Recordings']);
% BehRec_2H = compileRecs('BEH',ListofCells_TooActive, [projectFolder,'\RAW_Data\All_Recordings']);
% 
% [HpcRec_2L, HpcNeuron_2L] = compileRecs('HPC',ListofCells_TooInactive, [projectFolder,'\RAW_Data\All_Recordings']);
% [SubRec_2L, SubNeuron_2L] = compileRecs('SUB',ListofCells_TooInactive, [projectFolder,'\RAW_Data\All_Recordings']);
% BehRec_2L = compileRecs('BEH',ListofCells_TooInactive, [projectFolder,'\RAW_Data\All_Recordings']);
% 
% save(fullfile(projectFolder,compiledDataFolder,'ExclusionDatasets.mat'),...
%     'HpcRec_2H', 'HpcNeuron_2H', 'SubRec_2H', 'SubNeuron_2H','BehRec_2H',...
%     'HpcRec_2L', 'HpcNeuron_2L', 'SubRec_2L', 'SubNeuron_2L','BehRec_2L');

% if curious, not used in paper
% load (fullfile(projectFolder,compiledDataFolder,'ExclusionDatasets.mat'),...
%     'HpcRec_2H', 'HpcNeuron_2H', 'SubRec_2H', 'SubNeuron_2H','BehRec_2H',...
%     'HpcRec_2L', 'HpcNeuron_2L', 'SubRec_2L', 'SubNeuron_2L','BehRec_2L');

%% Figures - The figure sections do not depend on each other unless noted - they only depend on above

%% Figure 1 - Behavioral Performance
%% Sim behavior for statistical comparison:
nTrials = 1000;
nRepeats = 1000;
for iRep = 1:nRepeats
SimRecStruct.rec{iRep} = iRep;
SimRecStruct.Behavior.behaviorEvents{iRep}(:,3) = randi(4,[nTrials,1]);
end
SimPerformance = calcPerformanceTTT(SimRecStruct, [1,2,3,4]);

%% act beh analysis and fig 1
Performance = calcPerformanceTTT(BehRec, [1,2,3,4], [9,10]);
figure1_SUBAN(BehRec,Performance, SimPerformance);
figure1Sup_SUBAN(SubRec, HpcRec, SimPerformance);

%% Classic Spatial Measures Analyses
SpatialSummaryStats = classicSpatialMeasuresAnalysis(HpcRec, HpcNeuron, SubRec, SubNeuron);

%% Figure 2 - Neuron Examples
figure2_SUBAN(SubNeuron, HpcNeuron, SubRec, HpcRec,ClusterQuality_PostExclusions,'Mean3SD');

%% Figure 2 - Linear Ratemaps / Correlations
% Makes corrrelation across different paths & across different path segments
% Fn to be run on the compiled ratemap struct 
%Correlations are for non-overlapping portions and overlapping portions 
minPathCorrFR = 1;
HpcCorr = makePathCorrelations(HpcRec, HpcNeuron, minPathCorrFR);
SubCorr = makePathCorrelations(SubRec, SubNeuron, minPathCorrFR);
% PpcCorr = makePathCorrelations(PpcRec, PpcNeuron);

Figure2Stats = figure2pt2_SUBAN(SubNeuron, HpcNeuron, SubRec, HpcRec, SubCorr.corrResults_all_nonoverlap, HpcCorr.corrResults_all_nonoverlap,'Mean3SD');

%% Figure 3, 4, & 5 Prep - Make pop correlation
PATHS = [1,2,3,4,9,10];
PATHLENGTHS = [140,140,140,140,197,197];

HpcPopCorr_All = populationCorrAnalyses(HpcNeuron.MeanLinearRates, HpcNeuron.LinearRates, PATHS, false);
SubPopCorr_All = populationCorrAnalyses(SubNeuron.MeanLinearRates, SubNeuron.LinearRates, PATHS, false);

corrMatList = {'oddEvenCorr', 'oddEvenCorr_maxNormed'};

%% Figure 3 - Other Population Correlation Analyses
figure3_SUBAN(HpcRec, SubRec, HpcNeuron, SubNeuron);

%% New 4 Rasters?
rasterPlotter(HpcPopCorr_All,PATHLENGTHS);
rasterPlotter(SubPopCorr_All,PATHLENGTHS);

%% Figure 4 Prep - pop correlation Bootstrap and scale and rep analysis 

% Constants & Settings for Scale of Representation Analysis - Includes bootstrap for stats
ScaleAndBiasSettings.PATHS = PATHS;
ScaleAndBiasSettings.PATH_LENGTHS = [140,140,140,140,197,197];
ScaleAndBiasSettings.CORR_THRESHOLDS = [0.1:0.05:0.7];
 % rounding to deal with comparing floating point numbers.

% bias representation settings
ScaleAndBiasSettings.DIFF_OFFSET = 10;
ScaleAndBiasSettings.DIFF_SMOOTH_STD = 1.5; % https://www.mathworks.com/matlabcentral/answers/406563-how-does-smoothdata-function-using-gaussian-method-define-the-standard-deviation-for-different-w
ScaleAndBiasSettings.DO_BS = true;

% Bootstrap stats settings.
ScaleAndBiasSettings.BS_PERCENTILE_THRESHOLD = 1;
ScaleAndBiasSettings.N_BS_ITERATIONS = 1000;

% Bootstrap is enormous ~30GB - use, save dependent results, don't save BS
%  parfor iBSIter = 1:ScaleAndBiasSettings.N_BS_ITERATIONS
%      HpcPopCorr_BS{iBSIter} = populationCorrAnalyses(HpcNeuron.MeanLinearRates, HpcNeuron.LinearRates, PATHS, true);
%  end
%  parfor iBSIter = 1:ScaleAndBiasSettings.N_BS_ITERATIONS
%      SubPopCorr_BS{iBSIter} = populationCorrAnalyses(SubNeuron.MeanLinearRates, SubNeuron.LinearRates, PATHS, true);
%  end

% Actual scale of rep and bias analysis.
% for iCorrMatStyle = 1:length(corrMatList)
%     SandBCorrResults_Hpc(iCorrMatStyle) = popCorrScaleAndBias(HpcPopCorr_All, HpcPopCorr_BS,...
%         corrMatList{iCorrMatStyle}, ScaleAndBiasSettings);
%     SandBCorrResults_Sub(iCorrMatStyle) = popCorrScaleAndBias(SubPopCorr_All, SubPopCorr_BS,...
%         corrMatList{iCorrMatStyle}, ScaleAndBiasSettings);
% end
% save(fullfile(projectFolder,compiledDataFolder,'SandBCorrResults'),...
%     'SandBCorrResults_Hpc', 'SandBCorrResults_Sub');
load(fullfile(projectFolder,compiledDataFolder,'SandBCorrResults'),...
   'SandBCorrResults_Hpc', 'SandBCorrResults_Sub');

%% Figure 4 - Population Correlation Scale and Bias of Representation.
% chosenCorrMatForAnalysis = 'oddEvenCorr_MaxNormed';
chosenCorrMatForAnalysis = 'oddEvenCorr';
chosenCorrMatIndex = find(strcmpi(chosenCorrMatForAnalysis, {SandBCorrResults_Hpc.corrMarixUsed}));

ScaleAndBiasSettings.CHOSEN_THRESH = 0.5;
ScaleAndBiasSettings.thisThresh = find(round(ScaleAndBiasSettings.CORR_THRESHOLDS,2) ==...
    round(ScaleAndBiasSettings.CHOSEN_THRESH,2));

fig4_SUBAN(ScaleAndBiasSettings.thisThresh, chosenCorrMatForAnalysis, SubNeuron,...
    SandBCorrResults_Hpc(chosenCorrMatIndex).coreStats,...
    SandBCorrResults_Sub(chosenCorrMatIndex).coreStats, SubPopCorr_All, HpcPopCorr_All, ...
    SandBCorrResults_Sub(chosenCorrMatIndex).pathPropDistFromTurn,...
    SandBCorrResults_Sub(chosenCorrMatIndex).transitionPeaks,...
    SandBCorrResults_Hpc(chosenCorrMatIndex).transitionPeaks,...
    ScaleAndBiasSettings.PATH_LENGTHS, ScaleAndBiasSettings.CORR_THRESHOLDS);

fig4Supp_SUBAN(ScaleAndBiasSettings.thisThresh,...
    SandBCorrResults_Hpc(chosenCorrMatIndex).coreStats,...
    SandBCorrResults_Sub(chosenCorrMatIndex).coreStats,...
    SandBCorrResults_Sub(chosenCorrMatIndex).pathPropDistFromTurn,...
    SandBCorrResults_Sub(chosenCorrMatIndex).transitionPeaks,...
    SandBCorrResults_Hpc(chosenCorrMatIndex).transitionPeaks,...
    ScaleAndBiasSettings.DIFF_OFFSET, ScaleAndBiasSettings.PATH_LENGTHS, ScaleAndBiasSettings.CORR_THRESHOLDS);

%% Figure 5 - GLM on population corrmatrix - out.
% glmAnalysis_SUBAN;
% Figure5_GLM_Scratchpad; sample plotting - may want to grab some
% Scribble_GLM; % old plotting - may want to grab some.
% 
% %% Figure 6 - Decision Tree examples & pop stats
% % We are interested in the importance values for each neuron as an indicator of the influence of
% % each input.
% 
% % name/value pairs of settings to pass through to the DTree analysis.
% DTREE_SETTINGS = {...
%     'MaxNumSplits'; 10;...
%     'PredictorSelection'; 'interaction-curvature';...
%     'OOBPredictorImportance';'on';...
%     'OOBPrediction';'on'};
% N_TREES = 100;
% N_PREDICTOR_BINS = 100;
% 
% % Reformat data for ease of use, add some variables to use as inputs.
% DTreeInputTables_Hpc = makeDTreeInputs(HpcNeuron, HpcRec, [1,2,3,4],[9,10], N_PREDICTOR_BINS);
% DTreeInputTables_Sub = makeDTreeInputs(SubNeuron, SubRec, [1,2,3,4],[9,10], N_PREDICTOR_BINS);
% 
% % Run actual decision tree model, train on training set, using best hyperparameters from above. Test
% % on test set.
% % Using Bagged Tree with random forest and curvature as the algorithm.
% % Use out of bag error for test error, and so algorithm train/test splits for us.
% 
% fprintf('All DTree Input Fields, in order: %s \n\n',...
%  strjoin(DTreeInputTables_Hpc{1}.Properties.VariableNames,'  '));
% predictorList = DTreeInputTables_Hpc{1}.Properties.VariableNames([2,3,4,6,7,8,9]);
% fprintf('Chosen DTree Input Parameters, in order: %s \n\n',...
%  strjoin(predictorList,'  '));
% 
% [treeBags_Sub, impVals_Sub, baselineErrs_Sub, oobErrs_Sub] = calcTreeBagWrapper(...
%     DTreeInputTables_Sub, predictorList, 'meanFR', N_TREES, DTREE_SETTINGS{:});
% [treeBags_Hpc, impVals_Hpc, baselineErrs_Hpc, oobErrs_Hpc] = calcTreeBagWrapper(...
%     DTreeInputTables_Hpc, predictorList, 'meanFR', N_TREES, DTREE_SETTINGS{:});
% 
% 
% %% Actual Figure 6
% Figure6_SUBAN;

%% Supplemental Figure - Rat x Rat Behavior Breakdown
% plotSuppRatStruct(BehRec, Performance, 14) %NS14 plots 
% plotSuppRatStruct(BehRec, Performance, 15) %NS15 plots
% plotSuppRatStruct(BehRec, Performance, 16) %NS16 plots
% plotSuppRatStruct(BehRec, Performance, 23) %NS23 plots
% plotSuppRatStruct(BehRec, Performance, 2) %BL2 plots
% plotSuppRatStruct(BehRec, Performance, 1) %JL1 plots

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

