function [Behavior] = alignOrientationsToTrack(Behavior, hdUnaligned, iRec, refPaths, refStraightNorthBins )
%alignOrientationsToTrack aligns HD orientations to the movement of the animal at given track positions
%   Aligns given head direction data to the orientation the animal is in at given path locations. If
%   path and bins give a time when the animal is facing a known direction w.r.t. the track, hd is
%   now aligned so 0 is this direction.
%
% 	[Behavior] = alignOrientationsToTrack(Behavior, iRec, hdUnaligned, refPaths, refStraightNorthBins )
%
% INPUTS: 
%		Behavior - struct made in compileRecs containing basic behavioral variables from many
%                   recordings. Data is put together, not 1 struct/per rec, for ease of later
%                   analysis.
%		iRec - index of current recording in Behavior stuct
%		hdUnaligned - vector of currend head direction
%		refPaths - paths to use for alignment purposes
%       refStraightNorthBins - bins to use for alignment purposes. SHould be either a M x 1 vector 
%           or a N x 1 cell array containing column vectors, where N is the number of refPaths. If 
%           it is a M x 1 vector, then each value is a bin to use for each of the refPaths. If a 
%           cell array, should contain vectors of all of the bin numbers for each corresponding
%           refPath (i.e., cell 1 contains the vector of bin numbers for the first refPath).                    
%
% OUTPUTS: 
%		Behavior - struct made in compileRecs containing basic behavioral variables from many
%                   recordings. Data is put together, not 1 struct/per rec, for ease of later
%                   analysis.
%		                       
% REMARKS - Assumes HD is -pi:pi - (what the circular stats toolbox uses
%
% OTHER M-FILES REQUIRED: circular stats toolbox, calcRecNorth
%
% SEE ALSO:
%   calcRecNorth, compileRecs
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: https://www.jmolson.com
% CREATED ON: 03-APR-2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[ recNorth ] = calcRecNorth(Behavior.samplePaths{iRec}, Behavior.samplePathBins{iRec},...
    hdUnaligned, refPaths, refStraightNorthBins );

Behavior.hdAlignedNorth_Rad{iRec} = circ_dist(hdUnaligned,recNorth); % -pi:pi
Behavior.axisAlignedNorth_Rad{iRec} = mod(Behavior.hdAlignedNorth_Rad{iRec}+pi,pi);  % 0 : pi

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




