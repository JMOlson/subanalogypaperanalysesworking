function Behavior = mapPathBinsToSamples( Behavior, PathTemplates, iRec)
%mapSamplesToPathBins Create vectors corresponding to behavior samples with the path, and the path bin.
%   Detailed explanation goes here
%
%   Behavior = mapSamplesToPathBins( Behavior, PathTemplates, iRec)
%  
%
%   Written by Jacob Olson, March 2020
TOL = 1e-10; % tolerance for floating point comparison
    
    nRecSamples = Behavior.recLength_Samples(iRec);

Behavior.samplePaths{iRec} = nan(nRecSamples,1);
Behavior.samplePathBins{iRec} = nan(nRecSamples,1);

%% Sanity check: sample indices & timeStamps are in ascending order.
if any(diff(Behavior.sampleTimes{iRec}) < 0) || any(abs(diff(Behavior.sampleIndices{iRec})-1) > TOL)
    disp(['Behavior sample vectors are not in chronological order or are missing a sample.']);
    return
end

%% Find the closest bin for each sample path
pathList = Behavior.pathList{iRec};
for iPath = 1:length(pathList)
    template = PathTemplates{iPath};
    thisPathPosMarkers = [Behavior.pathRunsLineMarkers{iRec}{iPath,1},...
        Behavior.pathRunsLineMarkers{iRec}{iPath,2}];
    for iRun = 1:length(thisPathPosMarkers(:,1))  % each run
        for iPoint = thisPathPosMarkers(iRun,1):thisPathPosMarkers(iRun,2)  % each time-point during this run
            if Behavior.hasPositionTracking{iRec}(iPoint)  % if we have valid tracking
                % Find the closest bin for the current path.
                templateDist = dist(Behavior.posXY{iRec}(iPoint,:),template(:,2:3)');
                [~, nearestPointInd] = min(templateDist);  % find the template point closest to this actual point
                Behavior.samplePaths{iRec}(iPoint) = pathList(iPath);
                Behavior.samplePathBins{iRec}(iPoint) = nearestPointInd(1);
            end
        end
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


