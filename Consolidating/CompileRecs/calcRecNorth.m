function [ recNorth ] = calcRecNorth(samplePaths, samplePathBins, sampleHdRadians, refPaths, refStraightNorthBins )
%calcRecNorth Calculate rat's orientation when moving "north", as defined by the paths
%   Calculates orientation of animal's lights when the animal is in the specified bins. If the bins
%   given are while the animal is moving straight in a stereotyped direction, this can be used to 
%   align the head direction values to the track. This assumes that the heading direction is the 
%   same as the head direction for these bins, an assumption most accurate at high run speeds. Best 
%   to use multiple paths where the animal moves in the same direction to average across any 
%   behavioral quirks (i.e. head tilt) at one.
%
%   If more than one path is given, averages are taken first for each path, then across the paths
%   for the final value.
% 
%   For the Nitz Lab triple-T maze, north is defined as when the animal runs on the outside return
%   runs in the normal maze configuration for the day. (In TTTools parlance, paths 9 and 10)
%
% INPUTS: 
%		samplePaths - vector of current path identity for each behavior sample.
%		samplePathBins - vector of current bin identity of current path for each behavior sample.
%		sampleHdRadians - vector of currend head direction
%		refPaths - paths to use for alignment purposes
%       refStraightNorthBins - bins to use for alignment purposes. SHould be either a M x 1 vector 
%           or a N x 1 cell array containing column vectors, where N is the number of refPaths. If 
%           it is a M x 1 vector, then each value is a bin to use for each of the refPaths. If a 
%           cell array, should contain vectors of all of the bin numbers for each corresponding
%           refPath (i.e., cell 1 contains the vector of bin numbers for the first refPath).
%
% OUTPUTS: 
%		recNorth - orientation that aligns to the HD when the animal is in the paths and bins given.
%           Can be used to align HD to the track in a meaningful way.
%                   
% REMARKS
%
% OTHER M-FILES REQUIRED: circular statistics toolbox
%
% SEE ALSO:
%   alignorientationsToTrack, compileRecs
%
% CREATED BY: Jake Olson, October 2015
% EMAIL: 
% WEBSITE: 
% CREATED ON: 
% LAST MODIFIED BY: Jacob M Olson
% LAST MODIFIED ON: 01-Apr-2020
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Initialization and input handling
nRefPaths = length(refPaths);

thisPathMeanHDRad = nan(nRefPaths,1);

% handle two different refStraightNorthBins formats, make sure are row vectors, fix if needed.
if iscell(refStraightNorthBins)
    if length(refStraightNorthBins) ~= nRefPaths
        error('If using a cell array, one cell is needed for each refPath');
    end
    for iRefPath = 1:nRefPaths
        if iscolumn(refStraightNorthBins{iRefPath})
            refStraightNorthBins{iRefPath} = refStraightNorthBins{iRefPath}';
        end
    end
else
    if iscolumn(refStraightNorthBins)
        refStraightNorthBins = refStraightNorthBins';
    end
end

% need sample data as a column vector. verify, fix if needed.
if ~iscolumn(samplePathBins)
    samplePathBins = samplePathBins';
end
if ~iscolumn(samplePaths)
    samplePaths = samplePaths';
end

%% Find offset
% circ_mean can't handle NaNs.
hasHd = ~isnan(sampleHdRadians);

for iRefPath = 1:nRefPaths
    thisPath = refPaths(iRefPath);
    % Matches this path and relevant bins
    if iscell(refStraightNorthBins)
        relevantTrackingSamples = samplePaths == thisPath & any(samplePathBins == refStraightNorthBins{iRefPath},2);
    else
        relevantTrackingSamples = samplePaths == thisPath & any(samplePathBins == refStraightNorthBins,2);
    end
    % find circle mean of samples in range with HD values
    thisPathMeanHDRad(iRefPath) = circ_mean(sampleHdRadians(relevantTrackingSamples & hasHd));
end
% average HDvalue from each path
recNorth = circ_mean(thisPathMeanHDRad);
    
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


