function [RecStruct, NeuronStruct] = compileRecs(regionIndex,ListOfCells,dataFolder)
%COMPILEERECS Combine ephys and behavior from multiple recordings into one dataset for analysis
%		Optional file header info (to give more details about the function than in the H1 line)
%
% 	[Recording_Compilation, Neuron_Comp] = compileRecs(regionIndex,cellSelectionListFileName,dataFolder)
%
% INPUTS:
%		regionIndex - 3 letter code for brain region to compile data for. Used to index into List of
%           Cells workspace.
%           region* 1 - HPC
%           region* 2 - SUB
%           region* 3 - PPC
%		ListOfCells - struct with a table of recordings to include.
%		dataFolder - path containing all data (rmap & dvt) files for recordings
%           File names must be in the format of:
%           FolderLoc = 'C:\Users\Aurif\Documents\Work\FinalIteration_AnalogyPaper\RAW_Data\All_Recordings';
%
% IMPORTANT NOTE: This requires you have both rmap & dvt files for all recs and they be named so that they
% occur in the correct order. If one is missing, all following recs will be off by one. Just be
% careful.
%
% OUTPUTS:
%		RecStruct - Compiled recording data
%       NeuronStruct - Compiled neural data 
%
% EXAMPLES:
% [HpcRec, HpcNeuron] = compileRecs('HPC',ListofCells_PostExclusions, [projectFolder,'\RAW_Data\All_Recordings']);
% [SubRec, SubNeuron] = compileRecs('SUB',ListofCells_PostExclusions, [projectFolder,'\RAW_Data\All_Recordings']);
%
% REMARKS
%
% OTHER M-FILES REQUIRED:
%     CircStat Toolbox
%     calcMovementFromPosition.m
%     alignOrientationsToTrack.m
%     calcRecNorth.m
%     mapPathBinsToSamples.m
%     mapSpikesToSamples.m
%     mapBehaviorToPathBins
%
% SUBFUNCTIONS:
%     formatOldJMORecsToRecStructs
% MAT-FILES REQUIRED:
%
% SEE ALSO:%
% CREATED WITH MATLAB VERSION: 9.6.0.1150989 (R2019a) Update 4 on Microsoft Windows 10 Enterprise Version 10.0 (Build 18363)
%
% CREATED BY: Alexander Johnson
% CREATED ON: 2018?
% LAST MODIFIED BY: Jacob Olson
% LAST MODIFIED ON: 04/06/2020
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Constants
FILE_NAME_SAME_INDEX = [1:9];

% Used for orientation alignment to track - aligning North as direction during occupancy of the
% middle of the two return runs on the triple T
REF_PATHS = [9,10];
REF_STRAIGHT_NORTH_BINS = [51:91]; % 71 should be center of north part of return. Is 112 long turn apex<->apex

% Constants defining columns of the two lights in the DVT file. Used to calc Head direction values.
LIGHT1_COLS_XY = [3,4];
LIGHT2_COLS_XY = [5,6];

% average light used in TTLinearRatemapper as the position - this is our official position.
% we'll use it for all positioa/velocity calculations
LIGHTAVG_COLS_XY = [9,10];

%% Input handling
% Verify ROI input is correct. If not, display instruction message and exit.
switch regionIndex
    case 'HPC'
        regionSelected = 'tCellFileIndexHPC';
        ignoreNeurons = false;
    case 'SUB'
        regionSelected = 'tCellFileIndexSUB';
        ignoreNeurons = false;
    case 'PPC'
        regionSelected = 'tCellFileIndexPPC';
        ignoreNeurons = false;
    case 'BEH'
        regionSelected = 'Beh_HPC_SUB';
        ignoreNeurons = true;
    otherwise
        disp('We only got 3 regions. - HPC,SUB,PPC - Try again.');
        return;
end

%% Load folder information and get list of recording files (ephys & behavior)
% Get current folder so we can return after finishing.
startingDir = pwd;
cd(dataFolder);

% Get lists of the two types of data files.
RMap_file_info = dir('*rmaps*'); % Processed ephys + beh files.
RMap_file_list = {RMap_file_info.name}';
DVT_file_info = dir('*DVT*');
DVT_file_list = {DVT_file_info.name}';
Analysis_type_flag = cellfun(@(x) str2double(x(1)), RMap_file_list);

% make sure DVT and RMAPs are same recording
namesMatch = cellfun(@(x,y) strcmpi(x(FILE_NAME_SAME_INDEX),y(FILE_NAME_SAME_INDEX)),RMap_file_list,DVT_file_list);
if ~all(namesMatch)
    disp('RMAP and DVT files don''t match in folder.');
    return;
end

%loads the variable that contains the list of cells we want for analyses


% make sure ListOfCells corresponds with recordings.
rmapRatName = cellfun(@(x) x(3:6),RMap_file_list,'UniformOutput',false);
rmapRecNum = cellfun(@(x) str2double(x(8:9)),RMap_file_list);

rightName = cellfun(@(x,y) strcmpi(x,y),rmapRatName,{ListOfCells.ratName}');
rightRec = rmapRecNum == [ListOfCells.recIndex]';
if ~all(rightName) || ~all(rightRec)
    disp('ListOfFilesStruct doesn''t match RMAP list in folder.');
    return;
end

%% Start Assembling
iOverallNeuronCount = 0;
iOverallRecordingCount = 0;
NeuronStruct = struct();
RecStruct = struct();
RecStruct.Behavior = struct();

% Initialize loop through recordings
nRecFiles = size(RMap_file_list,1);
for iRec = 1:nRecFiles
    tic; % start timer
    if ListOfCells(iRec).isGoodBeh &&... % 1-used 0-excluded on basis of tracking/behavior/etc
            numel(ListOfCells(iRec).(regionSelected)) > 0 %are neurons for this region & recording
        iOverallRecordingCount = iOverallRecordingCount+1;
        
        % load data and if necessary, format input data to expected struct and var names
        if Analysis_type_flag(iRec) == 1 %If Analysis type 1 - data saved as indRecStruct
            load(RMap_file_list{iRec},'indRecStruct');
        elseif Analysis_type_flag(iRec) == 2
            indRecStruct = formatOldJMORecsToRecStructs(RMap_file_list{iRec},DVT_file_list{iRec});
        end
        
        % Recording Info & Settings
        RecStruct.rmapFile{iOverallRecordingCount} = RMap_file_list{iRec};
        RecStruct.dvtFile{iOverallRecordingCount} = DVT_file_list{iRec};
        RecStruct.rat{iOverallRecordingCount} = ListOfCells(iRec).ratName;
        RecStruct.rec(iOverallRecordingCount) = ListOfCells(iRec).recIndex;
        
        RecStruct.sessionTimeStamps{iOverallRecordingCount} = indRecStruct.sessionTimeStamps;
        
        % Add basic tracking sample info and the position data
        RecStruct.Behavior.trackingSampleRate_Hz(iOverallRecordingCount) = indRecStruct.trackingSampleRate;
        RecStruct.Behavior.trackingMaxGapFilled_Samples(iOverallRecordingCount) = indRecStruct.maxGapFilled;
        RecStruct.Behavior.recLength_Samples(iOverallRecordingCount) = size(indRecStruct.pixelDVT,1);
        RecStruct.Behavior.sampleIndices{iOverallRecordingCount} = indRecStruct.pixelDVT(:,1);
        RecStruct.Behavior.sampleTimes{iOverallRecordingCount} = indRecStruct.pixelDVT(:,2);
        RecStruct.Behavior.posXY{iOverallRecordingCount} = indRecStruct.pixelDVT(:,LIGHTAVG_COLS_XY(1):LIGHTAVG_COLS_XY(2));
        % Previously, 1s were used to code for NaNs. Going to make code explicitly put that back in
        % so we don't forget for any positional calculations. Not sure if any 0s, but if so, also
        % denote NaN spots.
        RecStruct.Behavior.posXY{iOverallRecordingCount}(RecStruct.Behavior.posXY{iOverallRecordingCount}<=1) = NaN;
        % Add vectors showing tracking status
        RecStruct.Behavior = addTrackingStatus(RecStruct.Behavior,indRecStruct.samplesUnfilled,iOverallRecordingCount);
        
        % Add beh run labeling
        RecStruct.Behavior.behaviorEvents{iOverallRecordingCount} = sortrows(indRecStruct.events,1); %Fix: Sort events for later analyses by index/time
        RecStruct.Behavior.behaviorMinSpeedAllowed_PixPerSecs(iOverallRecordingCount) = indRecStruct.minSpeedAllowed;
        RecStruct.Behavior.pathList{iOverallRecordingCount} = indRecStruct.pathList;
        RecStruct.Behavior.pathLengths{iOverallRecordingCount} = cellfun(@(x) size(x,1), indRecStruct.PathTemplate);
        RecStruct.Behavior.nPaths(iOverallRecordingCount) = length(indRecStruct.pathList);
        RecStruct.Behavior.nRunsEachPath{iOverallRecordingCount} = indRecStruct.nRunsEachPath;
        RecStruct.Behavior.nRunsClean(iOverallRecordingCount) = sum(indRecStruct.nRunsEachPath);
        RecStruct.Behavior.pathRunsLineMarkers{iOverallRecordingCount} = indRecStruct.pathRunsLineMarkers;
        RecStruct.Behavior.pathRunsTimeMarkers{iOverallRecordingCount} = indRecStruct.pathRunsTimeMarkers;
        
        % Create vectors corresponding to behavior samples with the path, and the path bin.
        RecStruct.Behavior = mapPathBinsToSamples(RecStruct.Behavior, indRecStruct.PathTemplate, iOverallRecordingCount);
        
        % Behavior Movement Tracking and Stats
        RecStruct.Behavior = calcMovementFromPosition(RecStruct.Behavior,...
            REF_PATHS, REF_STRAIGHT_NORTH_BINS,...
            indRecStruct.pixelDVT(:,LIGHT1_COLS_XY),indRecStruct.pixelDVT(:,LIGHT2_COLS_XY),...
            iOverallRecordingCount);
        
        % Neurons recorded
        RecStruct.nCells{iOverallRecordingCount} = 0;
        RecStruct.tfileIndexList{iOverallRecordingCount} = [];
        RecStruct.tfileNames{iOverallRecordingCount} = [];
        RecStruct.neuronStructIndices{iOverallRecordingCount} = [];
        
        %add the cells to use (ListOfCells is a manually coded list of which tflie is used)
        neuronIndicesThisRec = ListOfCells(iRec).(regionSelected);
        if ~ignoreNeurons
            for neuronIndex = neuronIndicesThisRec
                iOverallNeuronCount = iOverallNeuronCount+1;
                
                % Record the cell in recStruct
                RecStruct.nCells{iOverallRecordingCount} = RecStruct.nCells{iOverallRecordingCount}+1;
                RecStruct.tfileIndexList{iOverallRecordingCount} = ...
                    [RecStruct.tfileIndexList{iOverallRecordingCount},neuronIndex];
                RecStruct.neuronStructIndices{iOverallRecordingCount} = ...
                    [RecStruct.neuronStructIndices{iOverallRecordingCount},iOverallNeuronCount];
                RecStruct.tfileNames{iOverallRecordingCount} = ...
                    [RecStruct.tfileNames{iOverallRecordingCount},indRecStruct.tfileList(neuronIndex)];
                
                %Identifying info
                NeuronStruct.rat{iOverallNeuronCount} = ListOfCells(iRec).ratName;
                NeuronStruct.rec(iOverallNeuronCount) = ListOfCells(iRec).recIndex;
                NeuronStruct.recStructIndex(iOverallNeuronCount) = iOverallRecordingCount;
                NeuronStruct.neuronName(iOverallNeuronCount) = indRecStruct.tfileList(neuronIndex);
                
                % All spikes (times)
                NeuronStruct.spikeTimes{iOverallNeuronCount} = indRecStruct.allTfiles{neuronIndex};
                % Create vectors corresponding to behavior samples with nSpikes per sample
                NeuronStruct.sampleNSpikes{iOverallNeuronCount} = mapSpikesToSamples(indRecStruct.allTfiles{neuronIndex},...
                    RecStruct.Behavior.sampleTimes{iOverallRecordingCount},...
                    indRecStruct.trackingSampleRate, iOverallNeuronCount);
                
                % Neural spiking smooted and mapped to paths
                NeuronStruct.linearRates_filterSigma_Bins(iOverallNeuronCount) = indRecStruct.filterSigmaLinearRMaps;
                for iPath = 1:length(indRecStruct.pathList)
                    NeuronStruct.LinearRates{iOverallNeuronCount,indRecStruct.pathList(iPath)} = ...
                        squeeze(indRecStruct.LinearRates{iPath}(neuronIndex,:,:,:));
                    NeuronStruct.MeanLinearRates{iOverallNeuronCount,indRecStruct.pathList(iPath)} = ...
                        squeeze(indRecStruct.MeanLinearRates{iPath}(neuronIndex,:,:));
                end
            end
        end
        timeElapsed = round(toc); % time for this recording
        fprintf('Recording %i of %i files complete after %i seconds. %i recs with data .\n',...
            iRec, nRecFiles, timeElapsed, iOverallRecordingCount);
    end
end

% Add path aligned behavior data in.
[RecStruct.Behavior] = mapBehaviorToPathBins(RecStruct.Behavior);

% Return to original folder.
cd(startingDir);
end


%% Helper Functions
function IndRecStruct = formatOldJMORecsToRecStructs(rmapFileName,dvtFileName)
%formatOldJMORecsToRecStructs Convert output from TTTrackingPreprocessor_old to indRecStruct format.
%		Old datasets (2015-2017?) from Nitz lab collected by Jacob Olson and team are completely
%		processed but not in a struct format like recent datasets. This function opens those files
%		and puts all needed variables into the newer struct format as in TTTrackingPreprocessor used
%		currently (2018-2020+).

load(dvtFileName); %#ok<*LOAD>
load(rmapFileName);

IndRecStruct.trackingSampleRate = sampleRate;
if exist('minSpeedAllowed','var')
    IndRecStruct.minSpeedAllowed = minSpeedAllowed;
else
    IndRecStruct.minSpeedAllowed = NaN;
end
if exist('maxGapFilled','var')
    IndRecStruct.maxGapFilled = maxGapFilled;
else
    IndRecStruct.maxGapFilled = NaN;
end

% Sessions in the recording
if exist('sessionTimeStamps','var')
    IndRecStruct.sessionTimeStamps = sessionTimeStamps;
elseif exist('timeStamps','var')
    IndRecStruct.sessionTimeStamps = timeStamps;
else
    
    IndRecStruct.sessionTimeStamps = NaN;
end

% Tracking Data
IndRecStruct.pixelDVT = pixelDvt;

% Beh Run Labeling
IndRecStruct.events = spiral_events;
IndRecStruct.pathList = pathList;
IndRecStruct.nRunsEachPath = nRuns;
IndRecStruct.pathRunsLineMarkers = runPosMarkers;
IndRecStruct.pathRunsTimeMarkers = runTimeMarkers;
IndRecStruct.PathTemplate = PathTemplate;
IndRecStruct.samplesUnfilled = samplesUnfilled;

% Neurons recorded
IndRecStruct.tfileList = tfileList;
IndRecStruct.allTfiles = allTfiles;
IndRecStruct.nCells = nCells;

% Neural spiking smooted and mapped to paths
IndRecStruct.filterSigmaLinearRMaps = filterSigma;
IndRecStruct.LinearRates = LinearRates;
IndRecStruct.MeanLinearRates = MeanLinearRates;
end

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%% %%



