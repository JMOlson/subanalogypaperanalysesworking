function sampleNSpikes = mapSpikesToSamples( tfile, sampleTimes, sampleRate, iNeu)
%mapSpikesToSamples Summary of this function goes here
%   Create a dvt w/ nSpikes at that sample and the path and bin of the
%   sample. Can then get HD, vel, etc, for each sample from the vectors of
%   corresponding length already made.
%
%
%   NeuronStruct.sampleNSpikes{iOverallNeuronCount} = mapSpikesToSamples(tfile,...
%       RecStruct.Behavior.sampleTimes{iRec}, RecStruct.Behavior.sampleRate(iRec), iOverallNeuronCount);
%            
%   Written by Jacob Olson, March 2020
    
%% Check that the spikes are all sorted ascending
if any(diff(tfile)<0)
    disp(['Neuron ', num2str(iNeu),' spikes are not in chronological order.']);
    return;
end

%% Calc nSpikes for each sample.
nSamples = size(sampleTimes,1);
sampleNSpikes = zeros(nSamples,1);

posIndex = 1;
for iSpike =1:length(tfile)
    % Cycle through dvt until you hit the time that this spike goes to.
    while tfile(iSpike) > (sampleTimes(posIndex)+(1/(2*sampleRate))) &&...
            posIndex <= nSamples
        posIndex = posIndex+1;
    end
    if posIndex > nSamples
        disp('Spikes exist after the last sample!');
        return
    else
        sampleNSpikes(posIndex) = sampleNSpikes(posIndex) + 1;
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



