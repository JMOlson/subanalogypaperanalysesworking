function Behavior = addTrackingStatus(Behavior,samplesUnfilled, iRec)
%addTrackingStatus 
%       d
%
%
% INPUTS: 
%		Behavior - struct made in compileRecs containing basic behavioral variables from many
%                   recordings. Data is put together, not 1 struct/per rec, for ease of later
%                   analysis.
%       samplesUnfilled -   
%       iRec - index of current recording in Behavior stuct
%		                      
%
% OUTPUTS: 
%		Behavior - struct made in compileRecs containing basic behavioral variables from many
%                   recordings. Data is put together, not 1 struct/per rec, for ease of later
%                   analysis.
%		                       
%
% EXAMPLES: 
%		                   
%
% SEE ALSO:
%       compileRecs
%
% CREATED BY: Jacob Olson
% CREATED ON: 04/06/2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Behavior.hasTwoLights{iRec} = ~any(samplesUnfilled,2); % or was successfully interpolated...
Behavior.hasOneLight{iRec} = any(samplesUnfilled,2) & ~all(samplesUnfilled,2); % or was successfully interpolated...
Behavior.hasNoLights{iRec} = all(samplesUnfilled,2);
Behavior.hasPositionTracking{iRec} = ~all(samplesUnfilled,2);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



