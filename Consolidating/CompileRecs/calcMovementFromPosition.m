function [Behavior] = calcMovementFromPosition(Behavior, REF_PATHS, REF_STRAIGHT_NORTH_BINS, light1, light2, iRec) 
%calcMovementFromPosition creates entire behavior struct for a given recording in recording struct.
%       IMPORTANT: For use in compileRecs function that gathers recordings for further analysis.
%       Could be altered to make iRec optional, and if it didn't exist, would just do this for 1
%       recording.
%
%       Adds to a structure all of the relevant behavior variables from a recording from Nitz lab.
%       Includes position, velocity, acceleration, head direction (aligned to the track), axis of
%       travel (aligned to the track), and angular velocity. Both instantaneous and smoothed
%       versions.
%       
%		Used to use a 10 Hz window to calculate "smoothed" velocity/angVel. So take samples from 1/10
%       second apart, and do so for each sample (we sample at 60 Hz). More like a weird form of
%       downsampling than an actual smoothing. Going to now use smoothData with a gaussian kernel with a
%       std dev of x samples (X/60 Hz). This will actually "smooth" the data. Going to smooth the velocity, then use
%       that for ang vel, acc. Going to smooth HD, then use this for axis.
%       Will keep unsmoothed (inst) measures of all as well.
%
% 	[RecStruct,NeuronStruct] = calcMovementFromPosition(RecStruct,NeuronStruct,pixelDVT,lightChoice) 
%
% INPUTS: 
%		Behavior - struct made in compileRecs containing basic behavioral variables from many
%                   recordings. Data is put together, not 1 struct/per rec, for ease of later
%                   analysis.
%		refPaths - paths to use for alignment purposes
%       refStraightNorthBins - bins to use for alignment purposes. SHould be either a M x 1 vector 
%           or a N x 1 cell array containing column vectors, where N is the number of refPaths. If 
%           it is a M x 1 vector, then each value is a bin to use for each of the refPaths. If a 
%           cell array, should contain vectors of all of the bin numbers for each corresponding
%           refPath (i.e., cell 1 contains the vector of bin numbers for the first refPath). 
%		light1 - XY position data of first light used for behavioral tracking
%		light2 - XY position data of second light used for behavioral tracking
%		iRec - index of current recording in Behavior stuct
%		                      
%
% OUTPUTS: 
%		Behavior - struct made in compileRecs containing basic behavioral variables from many
%                   recordings. Data is put together, not 1 struct/per rec, for ease of later
%                   analysis.
%		                       
% EXAMPLES: 
%       Inside a loop of recordings in compileRecs
%
%       RecStruct.Behavior = calcMovementFromPosition(RecStruct.Behavior,...
%             REF_PATHS, REF_STRAIGHT_NORTH_BINS,...
%             indRecStruct.pixelDVT(:,LIGHT1_COLS_XY),indRecStruct.pixelDVT(:,LIGHT2_COLS_XY),...
%             iOverallRecordingCount);
%		                   
% REMARKS
%
% OTHER M-FILES REQUIRED:
%   circular statistics toolbox
%   alignOrientationsToTrack
%   calcRecNorth
%   
%
% SEE ALSO:
%   alignOrientationsToTrack calcRecNorth compileRecs
%
% CREATED BY: Jacob Olson
% CREATED ON: 03/21/2020
% LAST MODIFIED BY: Jacob Olson
% LAST MODIFIED ON: 09/07/2022 - smooothing parameters changed
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%  Initiatialization
% Constant for conversion of pixels to distance.
% Looked at one recording (all should be same, in same place in room)
% Found max Y difference in binCenters for the return runs. Both ~335.
% Track (center of path to center) is ~120cms.
PIXELS_PER_CM = 2.8;
sampleRate = Behavior.trackingSampleRate_Hz(iRec);
% Initialize window size to use - can change here if desired.
VEL_SMOOTH_SIGMA_SECS = 6/60;
velSmoothSigma_Samples = round(sampleRate*VEL_SMOOTH_SIGMA_SECS);
% offsetAmount = round(velSmoothSigma_Samples/2);
recLength = Behavior.recLength_Samples(iRec);

Behavior.velSmoothStdDev_Sec(iRec) = VEL_SMOOTH_SIGMA_SECS;
Behavior.velSmoothStdDev_Sample(iRec) = velSmoothSigma_Samples;

hasTwoLights = Behavior.hasTwoLights{iRec};

%% Velocity, Angular Velocity, & Acceleration instantaneous (1 sample only).
% subtract - vel is change in pos, acc is change in vel,
% shifts forward half step each diff, so adding a NaN in front of acc.
% means velocity is between this pos and one before, acc is dif in velocity to get from this pos to
% next - vel to get from last pos to this pos.
instXYVel = [diff(Behavior.posXY{iRec}); NaN,NaN];
instXYAcc = [NaN,NaN; diff(instXYVel)];

instVelMag = sqrt(sum((instXYVel.^2),2))... % sqrt(a^2+b^2) = c
            .*sampleRate./PIXELS_PER_CM; % In cm/second.
instVelDirection = atan2(instXYVel(:,2),instXYVel(:,1)); % -pi : pi

instAccMag = sqrt(sum((instXYAcc.^2),2))... % sqrt(a^2+b^2) = c
            .*sampleRate; % In cm/second^2
instAccDirection =  atan2(instXYAcc(:,2),instXYAcc(:,1)); % -pi : pi

% Angular Velocity is change in direction of velocity.
% circ_dist calculates change of angle as from 2nd parameter to 1st parameter
instAngVel = [NaN; circ_dist(instVelDirection(2:end),instVelDirection(1:end-1))]*sampleRate; % -pi : pi

Behavior.velInst_CmPerSec{iRec} = [instVelMag,instVelDirection];
Behavior.accInst_CmPerSecSQ{iRec} = [instAccMag,instAccDirection];
Behavior.angVelInst_RadPerSec{iRec} = instAngVel;

%% Velocity, Angular Velocity, & Acceleration over smoothed window.
%  See section above for detailed step comments.
missingVel = isnan(instXYVel);
% Smoothdata uses a std dev on it's gaussian smoothing kernel equal to 5*the window size.
% So we input the window size to be std dev x 5
smoothedXyVel = smoothdata(instXYVel,'gaussian',5*velSmoothSigma_Samples);
smoothedXyVel(missingVel) = NaN;
smoothedXyAcc = [NaN,NaN; diff(instXYVel)];

velMag = sqrt(sum((smoothedXyVel.^2),2))... % sqrt(a^2+b^2) = c
         .*sampleRate./PIXELS_PER_CM; % In cm/second.
velDirection = atan2(smoothedXyVel(:,2),smoothedXyVel(:,1)); % -pi : pi

accMag = sqrt(sum((smoothedXyAcc.^2),2))... % sqrt(a^2+b^2) = c
    .*sampleRate; % In cm/second^2.
accDirection = atan2(smoothedXyAcc(:,2),smoothedXyAcc(:,1)); % -pi : pi

% circ_dist calculates change of angle as from 2nd parameter to 1st parameter
smoothedAngVel = [NaN; circ_dist(velDirection(2:end),velDirection(1:end-1))]*sampleRate; % radians/second -pi : pi

% record results
Behavior.velSmoothed_CmPerSec{iRec} = [velMag,velDirection];
Behavior.accSmoothed_CmPerSecSQ{iRec} = [accMag,accDirection];
Behavior.angVelSmoothed_RadPerSec{iRec} = smoothedAngVel;

%% Head Direction & Axis
% Not aligned to anything - just the lights for that day.
lightPosDiff = light1-light2;

% Calc angle from light vector.
hdRadians = atan2(lightPosDiff(:,2),lightPosDiff(:,1));

% if a light is missing, throw out.
hdRadians(~hasTwoLights) = NaN;

% Create orientation values (HD and Axis) aligned to the track, add to Behavior substruct
Behavior = alignOrientationsToTrack(Behavior, hdRadians, iRec,...
    REF_PATHS, REF_STRAIGHT_NORTH_BINS);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


