function [ListofCells_All_WithExclusionsStruct, includedCQ, structTooHigh, structTooLow, Include] =...
    excludeByFR_SUBAN(ListofCells_All, datafolder, regionIndexList, CQstructArray, MRminthreshold, MRmaxthreshold)
%Reduced compiler to set exclusions based on firing rates.
%Writes data in same format with excluded cells removed
%
% INPUTS:
%   ListofCells_All - 
%   datafolder - 
%	regionIndexList - code(s) for brain region to compile data for. Used to index into List of
%       Cells workspace.
%       1 - HPC
%       2 - SUB
%       3 - PPC
%   CQstructArray - cluster quality output from compileCQ_suban for the appropriate region
%   MRminthreshold - sets threshold that each neuron must have at least 1
%       bin in mean linear rates on any path where the MLR drops below
%       e.g if set to 3, then if the mean rate never drops below 3Hz the neuron is
%       excluded (indexed)
%   LRmaxthreshold - sets threshold that each neuron must have at least 1
%       bin in linear rates on any path where the LR goes above
%       e.g if set to 3, then if the linear rate never goes above 3Hz the neuron is
%       excluded (indexed)
%
% OUTPUTS:
%   ListofCells_All_WithExclusionsStruct - struct output of filtered list of cells input
%   includedCQ - CQ of included cells
%   structTooHigh - same as 1st output, but for the cells w/ too high firing
%   structTooLow - same as 1st output, but for the cells w/ too low firing
%   Include - basic neuron struct of info for included list of cells

% CQ Struct Array is a struct array of the Cluster Quality structs in the order of the region Index List

% Written by Jake Olson based on code by Alex Johnson

PATH_LIST = [1,2,3,4,9,10];
nPaths = numel(PATH_LIST);

%% Make workable vector
%get current directory to return at end
startingDir = pwd;
%migrate to the data and get ready
cd(datafolder)

%Load in List Of cells
rMapsInDir = dir('*rmaps*');
dvtsInDir = dir('*DVT*');
rMapFileList = char({rMapsInDir.name});
dvtFileList = char({dvtsInDir.name});
RatNum = (rMapFileList(:,3:6));
RecNum = str2num(rMapFileList(:,8:9));
analsysisType = str2num(rMapFileList(:,1)); % 1 for new struct version, 2 for old with more vars

for iRegion = regionIndexList
    AssemblyPos = 1;
    %Load in mean Linear and Linear Rates
    %append index to list of cells
    for iFile = 1:size(rMapFileList,1) %For every rmaps file
        %Column 4 represents recordings 1- used 2-excluded on basis of tracking/behavior/etc
        %so if included and there is data for this region for that recording
        if ListofCells_All{iFile, 4} ==1 && numel(ListofCells_All{iFile,iRegion})>0
            if analsysisType(iFile) == 1
                load(rMapFileList(iFile,:),'indRecStruct');
            else
                load(rMapFileList(iFile,:),'tfileList','MeanLinearRates');
            end
            CellsForThisRec = ListofCells_All{iFile,iRegion}; %load which cells to use (ListofCells_All is a manually coded list of which tfile is used)
            for thisCell = CellsForThisRec %For each used tfile/cell
                NeuStructMinimal(iRegion).Rat{AssemblyPos} = RatNum(iFile,:);
                NeuStructMinimal(iRegion).Rec{AssemblyPos} = RecNum(iFile);
                NeuStructMinimal(iRegion).File{AssemblyPos}= iFile;
                NeuStructMinimal(iRegion).Cell{AssemblyPos} = thisCell;
                
                for iPath = 1:nPaths %For each path scored - pull in data from ind.RecStruct
                    if analsysisType(iFile) == 1 %If Analysis type 1 - data saved as indRecStruct
                        NeuStructMinimal(iRegion).Channel{AssemblyPos} = str2double(indRecStruct.tfileList{thisCell}(5:6));
                        NeuStructMinimal(iRegion).NeuronIndOnChannel{AssemblyPos} = double(indRecStruct.tfileList{thisCell}(7))-96;% 96 is ascii val offset so a=1, b=2, etc.
                        NeuStructMinimal(iRegion).MeanRate{AssemblyPos,iPath} = squeeze(indRecStruct.MeanLinearRates{1,iPath}(thisCell,:,1));
                    else
                        NeuStructMinimal(iRegion).Channel{AssemblyPos} = str2double(tfileList{thisCell}(5:6));
                        NeuStructMinimal(iRegion).NeuronIndOnChannel{AssemblyPos} = double(tfileList{thisCell}(7))-96;% 96 is ascii val offset so a=1, b=2, etc.
                        NeuStructMinimal(iRegion).MeanRate{AssemblyPos,iPath} = squeeze(MeanLinearRates{1,iPath}(thisCell,:,1));
                    end
                end
                AssemblyPos = AssemblyPos+1; %If data was loaded then you advance to the next row
            end
        end
    end
    % reassign all tetrodes to first wire in tetrode - naming convention is first recorded wire if that
    % wire was shut.
    NeuStructMinimal(iRegion).Channel = cellfun(@(x) x - mod(x-1,4),NeuStructMinimal(iRegion).Channel,'UniformOutput',false);
end

%% Exclusions
for iRegion = regionIndexList
    frMat = cell2mat(NeuStructMinimal(iRegion).MeanRate);
    maxes = max(frMat,[],2);
    mins = min(frMat,[],2);
    tooLowFR{iRegion} = maxes < MRmaxthreshold; %Never fires over the threshold given.
    tooHighFR{iRegion} = mins > MRminthreshold; %Never fires under the threshold given.
    goodFR{iRegion} = ~tooLowFR{iRegion} & ~tooHighFR{iRegion};
end

%% Rebuild Lists in same format.
ListOfTooHigh(:,4:6) = ListofCells_All(:,4:6);
ListOfTooLow(:,4:6) = ListofCells_All(:,4:6);
ListofCells_All_WithExclusionsStruct(:,4:6) = ListofCells_All(:,4:6);

[ListOfTooLow, ExcludeTooLow, excludedTooLowCQ] = makeList(...
    NeuStructMinimal, CQstructArray, ListOfTooLow, tooLowFR,regionIndexList);
[ListOfTooHigh, ExcludeTooHigh, excludedTooHighCQ] = makeList(...
    NeuStructMinimal, CQstructArray, ListOfTooHigh, tooHighFR,regionIndexList);
[ListofCells_All_WithExclusionsArray, Include, includedCQ] = makeList(...
    NeuStructMinimal, CQstructArray, ListofCells_All_WithExclusionsStruct, goodFR,regionIndexList);

recsForBeh = false(length(ListofCells_All),1);
for iRegion = regionIndexList
    recHasCells = arrayfun(@(x) ~isempty(x{:}), ListofCells_All_WithExclusionsArray(:,iRegion));
    recsForBeh = recsForBeh | recHasCells;
end


ListofCells_All_WithExclusionsArray(:,7) = num2cell(recsForBeh);
ListofCells_All_WithExclusionsArray((~recsForBeh),7) = {[]}; % need empty, not 0s.

%% write output
%Convert List into new datastruct
fieldList = {'tCellFileIndexHPC';'tCellFileIndexSUB';'tCellFileIndexPPC';'isGoodBeh';'recIndex';...
    'ratName';'Beh_HPC_SUB'};
ListofCells_All_WithExclusionsStruct = cell2struct(ListofCells_All_WithExclusionsArray, fieldList, 2);
structTooLow = cell2struct(ListOfTooLow, fieldList(1:size(ListOfTooLow,2)), 2);
structTooHigh = cell2struct(ListOfTooHigh, fieldList(1:size(ListOfTooHigh,2)), 2);

%% end

%Go back to the starting directory
cd(startingDir)

end

function [List, DataStruct, SelectCQStruct] = makeList(NeuStruct, CQstructArray, List, cellsToUseLogical, regionIndexList)
for iRegion = regionIndexList
    CQstructArray(iRegion).channel = arrayfun(@(x) x - mod(x-1,4),CQstructArray(iRegion).channel);
    
    DataStruct{iRegion}(:,1) = NeuStruct(iRegion).Rat(cellsToUseLogical{iRegion});
    DataStruct{iRegion}(:,2) = NeuStruct(iRegion).Rec(cellsToUseLogical{iRegion});
    DataStruct{iRegion}(:,3) = NeuStruct(iRegion).File(cellsToUseLogical{iRegion});
    DataStruct{iRegion}(:,4) = NeuStruct(iRegion).Cell(cellsToUseLogical{iRegion});
    DataStruct{iRegion}(:,5) = NeuStruct(iRegion).Channel(cellsToUseLogical{iRegion});
    DataStruct{iRegion}(:,6) = NeuStruct(iRegion).NeuronIndOnChannel(cellsToUseLogical{iRegion});
    for iNeu = 1:size(DataStruct{iRegion},1)
        List{DataStruct{iRegion}{iNeu,3},iRegion} = ...
            [List{DataStruct{iRegion}{iNeu,3},iRegion}, DataStruct{iRegion}{iNeu,4}];
        cqInd = find(strcmpi(CQstructArray(iRegion).rat, DataStruct{iRegion}{iNeu,1}) &...
            CQstructArray(iRegion).rec == DataStruct{iRegion}{iNeu,2} &...
            CQstructArray(iRegion).channel == DataStruct{iRegion}{iNeu,5} &...
            CQstructArray(iRegion).neuronNumber == DataStruct{iRegion}{iNeu,6});
        if isempty(cqInd)
            SelectCQStruct(iRegion).lRatio(iNeu) = NaN;
            SelectCQStruct(iRegion).isolationDistance(iNeu) = NaN;
        else
            SelectCQStruct(iRegion).lRatio(iNeu) = CQstructArray(iRegion).lRatio(cqInd);
            SelectCQStruct(iRegion).isolationDistance(iNeu) = CQstructArray(iRegion).isolationDistance(cqInd);
        end
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
