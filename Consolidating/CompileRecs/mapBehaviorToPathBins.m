function [Behavior] = mapBehaviorToPathBins(Behavior)
%mapBehaviorToPathBins One line description of what the function or script performs (H1 line)
%       Make binned-path (spatial bins) versions of behavioral variables
%       Fill binned versions (replace NaN with intermediate value)
%       future code needs these (GLM, Decision Tree)
%
% 	[OutputField] = mapBehaviorToPathBins(BehaviorStruct)
%
% INPUTS:
%		RecStruct - Description
%
%
% OUTPUTS:
%		output1 - Description
%
%
% EXAMPLES:
%		SubRecStruct.Behavior.PathAligned = mapBehaviorToPathBins(SubRecStruct.Behavior)
%
% REMARKS
%   This method was born out of the discussion of how to correctly deal with the issue that our
%   samples do not map to bins 1:1. Because of this, sometimes we have bins with no occupancies, as
%   well as bins with multiple occupancies.
%
%   Previous attemps have simply filled the missing points (i.e. fillmissing(data,'pchip'). In this
%   case, there is no smoothing being done beyond the smoothing of the variables in their previous
%   state. 
%
%   Alternatively, we can avg the data for each bin and then smooth the data (i.e. smoothdata(data,
%   'gaussian',sigmaVal)), but then the bins with multiple data points are weighted the same as the
%   ones that have one data point, and we still have missing values.
%
%   A smoothing followed by fillmissing approach still has the weighting issue.
%
%   I'm attempting to get around this by doing a smoothing of each datapoint and a weighted mean of
%   those values, which norms for each data point entered. I'll then fillmissing afterwards.
%   
%   
% OTHER M-FILES REQUIRED:
% SUBFUNCTIONS:
% MAT-FILES REQUIRED:
%
% SEE ALSO:%
%
% CREATED BY: Alexander Johnson
% EMAIL:
% WEBSITE:
% CREATED ON: 20-Mar-2020 - (significant revamp from earlier 2018-2019 AJ code & 2020 AJ/JO code)
% LAST MODIFIED BY: Jacob Olson
% LAST MODIFIED ON: 07-Apr-2020
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Smoothing filter - settings the same from Linear Rate maps (path-aligned firing ratemaps
FILTER_WIDTH = 13; %Must be odd.
FILTER_SIGMA = 1;
filterMedian = (FILTER_WIDTH+1)/2;
filterBins = [1:FILTER_WIDTH];
filterValues = normpdf(filterBins,filterMedian,FILTER_SIGMA);
filterValuesNormed = filterValues/sum(filterValues);

nRecs = numel(Behavior.behaviorEvents);

%% Binning of Behavioral variables
maxPathIndex = 1;
for iRec = 1:nRecs
    pathList = Behavior.pathList{iRec};
    
    % Want this for initialization purposes later.
    maxPathIndex = max(max(pathList),maxPathIndex);

    %grab unbinned variables for just this rec
    posX = Behavior.posXY{iRec}(:,1);
    posY = Behavior.posXY{iRec}(:,2);
    speed =  Behavior.velSmoothed_CmPerSec{iRec}(:,1);
    acc =  Behavior.accSmoothed_CmPerSecSQ{iRec}(:,1);
    angVel = Behavior.angVelSmoothed_RadPerSec{iRec};
    hd =  Behavior.hdAlignedNorth_Rad{iRec};
    pathBins = Behavior.samplePathBins{iRec};

    % Lookup for valid data for each variable
    posIsGood = Behavior.hasPositionTracking{iRec};
    
    speedIsGood = ~isnan(speed);
    accIsGood = ~isnan(acc);
    angVelIsGood = ~isnan(angVel);
    hdIsGood = ~isnan(hd);

    for jPath = 1:Behavior.nPaths(iRec)
        %Define number of bins in this path
        nBins = Behavior.pathLengths{iRec}(jPath);
        
        %Pull out starts and stops (line index) for each recording
        starts = Behavior.pathRunsLineMarkers{iRec}{(jPath),1};
        ends = Behavior.pathRunsLineMarkers{iRec}{(jPath),2};
        nRuns = min([numel(starts) numel(ends)]);
        
        %doublecheck validity of stars/stops data - should be same number of each.
        if numel(starts) ~= numel(ends)
            error('Not the same number of starts and ends in recording %i',iRec);
        end
        
        %Initialize
        posXPathAligned = zeros(nRuns,nBins);
        posYPathAligned = zeros(nRuns,nBins);
        occPosPathAligned = zeros(nRuns,nBins);
        speedPathAligned = zeros(nRuns,nBins);
        occSpeedPathAligned = zeros(nRuns,nBins);
        accPathAligned = zeros(nRuns,nBins);
        occAccPathAligned = zeros(nRuns,nBins);
        angVelPathAligned = zeros(nRuns,nBins);
        occAngVelPathAligned = zeros(nRuns,nBins);
        hdPathAligned = zeros(nRuns,nBins);
        occHDPathAligned = zeros(nRuns,nBins);
        
        for kRun = 1:nRuns
            startInd = starts(kRun);
            endInd = ends(kRun);
            for iPoint = startInd:endInd  % each time-point during this run
                if Behavior.hasPositionTracking{iRec}(iPoint)
                    thisBin = pathBins(iPoint);
                    if thisBin < filterMedian % at the beginning, clip off gaussian
                        occIndFiltered = [1:thisBin+filterMedian-1];
                        valsToOccs = filterValuesNormed((filterMedian+1-thisBin):end);
                    elseif thisBin > nBins+1-filterMedian % at the end, clip off gaussian
                        occIndFiltered = [thisBin-filterMedian+1:nBins];
                        valsToOccs = filterValuesNormed(1:(filterMedian+(nBins-thisBin)));
                    else %somewhere in the middle
                        occIndFiltered = [thisBin-filterMedian+1:thisBin+filterMedian-1];
                        valsToOccs = filterValuesNormed;
                    end
                    % add values to weighted means
                    if posIsGood(iPoint)
                        posXPathAligned(kRun,occIndFiltered) = ... %weighted sum - will norm by weights at end for weighted mean.
                            posXPathAligned(kRun,occIndFiltered) + posX(iPoint)*valsToOccs;
                        posYPathAligned(kRun,occIndFiltered) = ... %weighted sum - will norm by weights at end for weighted mean.
                            posYPathAligned(kRun,occIndFiltered) + posY(iPoint)*valsToOccs;
                        occPosPathAligned(kRun,occIndFiltered) = ... the weights
                            occPosPathAligned(kRun,occIndFiltered)+valsToOccs;
                    end
                    if speedIsGood(iPoint)
                        speedPathAligned(kRun,occIndFiltered) = ... %weighted sum - will norm by weights at end for weighted mean.
                            speedPathAligned(kRun,occIndFiltered) + speed(iPoint)*valsToOccs;
                        occSpeedPathAligned(kRun,occIndFiltered) = ... the weights
                            occSpeedPathAligned(kRun,occIndFiltered)+valsToOccs;
                    end
                    if accIsGood(iPoint)
                        accPathAligned(kRun,occIndFiltered) = ... %weighted sum - will norm by weights at end for weighted mean.
                            accPathAligned(kRun,occIndFiltered) + acc(iPoint)*valsToOccs;
                        occAccPathAligned(kRun,occIndFiltered) = ... the weights
                            occAccPathAligned(kRun,occIndFiltered)+valsToOccs;
                    end
                    if angVelIsGood(iPoint)
                        angVelPathAligned(kRun,occIndFiltered) = ... %weighted sum - will norm by weights at end for weighted mean.
                            angVelPathAligned(kRun,occIndFiltered) + angVel(iPoint)*valsToOccs;
                        occAngVelPathAligned(kRun,occIndFiltered) = ... the weights
                            occAngVelPathAligned(kRun,occIndFiltered)+valsToOccs;
                    end
                    if hdIsGood(iPoint) %axis calced off of HD afterwards - no sense dealing w/ the weirdness of half a circle when we can just mod later.
                        % Use circ_mean instead of sum for weighted sum.
                        hdValsMat = repmat(hd(iPoint),1,length(occIndFiltered));
                        hdPathAligned(kRun,occIndFiltered) = ... %weighted mean
                            circ_mean([hdPathAligned(kRun,occIndFiltered); hdValsMat],...%values
                            [occHDPathAligned(kRun,occIndFiltered); valsToOccs],1); % weights, direction to run
                        occHDPathAligned(kRun,occIndFiltered) = ... the weights
                            occHDPathAligned(kRun,occIndFiltered)+valsToOccs;
                    end
                end
            end
        end
        %% normalize by weighted occupancies - we now have weighted sums. Need to fill NaNs with
        % fillMissing and we are done.
        posXPathAligned = posXPathAligned./occPosPathAligned;
        posYPathAligned = posYPathAligned./occPosPathAligned;
        speedPathAligned = speedPathAligned./occSpeedPathAligned;
        accPathAligned = accPathAligned./occAccPathAligned;
        angVelPathAligned = angVelPathAligned./occAngVelPathAligned;
        
        %% smooth using "shape-preserving piecewise cubic spline interpolation" (see doc fillmissing) and
        % transpose into column vectors. Data at ends of runs may be worse if actual occupancy is less since
        % it will be filled if missing.
        
        % Fill missing works along cols and right now cells are nRuns x nBins long
        % we want it to fill based on each run, not across runs. smoothing occurs within run
        % so run over rows, then transpose so we will have column vectors for each run.
        Behavior.posXPathAligned{iRec,pathList(jPath)} = fillmissing(posXPathAligned,'pchip',2,'EndValues','none');
        Behavior.posYPathAligned{iRec,pathList(jPath)} = fillmissing(posYPathAligned,'pchip',2,'EndValues','none');
        
        Behavior.speedPathAligned_CmPerSec{iRec,pathList(jPath)} = fillmissing(speedPathAligned,'pchip',2,'EndValues','none');
        Behavior.accPathAligned_CmPerSecSQ{iRec,pathList(jPath)} = fillmissing(accPathAligned,'pchip',2,'EndValues','none');
        Behavior.angVelPathAligned_RadPerSec{iRec,pathList(jPath)} = fillmissing(angVelPathAligned,'pchip',2,'EndValues','none'); % Too noisy to be useful, at least track aligned and at 1/10 hz smoothing window
        
        % interpolating HD will break across the -pi:pi part of the circle.
        % my approach is to rotate the break point and interpolate again. Then, keep values from the half
        % that is in the middle of the range for each - so the first is -pi/2:pi/2, and the second is
        % pi/2:3pi/2. This will give us values that interpolated far from the break point. Don't know of a
        % better way.
        hd_0to2Pi = hdPathAligned;
        hd_0to2Pi(hdPathAligned<0) = hd_0to2Pi(hdPathAligned<0)+ 2*pi; % shift so linear break in values is now at 0, not pi/-pi
        
        hdFilled_noRotate = fillmissing(hdPathAligned,'pchip',2,'EndValues','none');  % a bit weird to try to interpolate circular data
        hdFilled_RotateBreak = fillmissing(hd_0to2Pi,'pchip',2,'EndValues','none');
        
        hdFilled = hdFilled_noRotate;
        hdFilled(abs(hdFilled)>(pi/2)) = hdFilled_RotateBreak(abs(hdFilled)>(pi/2));
        
        %filled values need to be put on -pi:pi scale
        hdFilled = hdFilled+3*pi; % make all values positive, shift 180degrees.
        hdFilled = mod(hdFilled,2*pi); % put on 0:2pi scale
        hdFilled = hdFilled - pi; % shift back 180 degrees. Now -pi:pi
        
        Behavior.hdPathAligned_Rad{iRec,pathList(jPath)} = hdFilled;
        Behavior.axisPathAligned_Rad{iRec,pathList(jPath)} = mod(hdFilled+pi,pi); % don't actually think mod has to be positive, but easy to do and think about.
    end
end

%% Make mean vectors
xMeanTmp = cellfun(@(x) mean(x,1),...
    Behavior.posXPathAligned, 'UniformOutput',false);
yMeanTmp = cellfun(@(x) mean(x,1),...
    Behavior.posYPathAligned, 'UniformOutput',false);
% put XY values together.
Behavior.posXYMeanPathAligned_Pixels = cellfun(@(x,y) [x;y],xMeanTmp,yMeanTmp,'UniformOutput',false); 

Behavior.speedMeanPathAligned_CmPerSec = cellfun(@(x) mean(x,1),...
    Behavior.speedPathAligned_CmPerSec, 'UniformOutput',false);

Behavior.accMeanPathAligned_CmPerSecSQ = cellfun(@(x) mean(x,1),...
    Behavior.accPathAligned_CmPerSecSQ, 'UniformOutput',false);

Behavior.angVelMeanPathAligned_RadPerSec = cellfun(@(x) mean(x,1),...
    Behavior.angVelPathAligned_RadPerSec, 'UniformOutput',false);

Behavior.hdMeanPathAligned_Rad = cellfun(@(x) circ_mean(x,[],1),...
    Behavior.hdPathAligned_Rad, 'UniformOutput',false);

Behavior.axisMeanPathAligned_Rad = cellfun(@(x) (circ_mean(2*x-pi,[],1)+pi)/2,...
    Behavior.axisPathAligned_Rad, 'UniformOutput',false); % must transform from 0:pi to -pi:pi, and then back, for values to not rotate or fold after averaging.

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



