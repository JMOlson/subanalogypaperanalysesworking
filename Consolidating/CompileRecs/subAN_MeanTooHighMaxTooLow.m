function [TooHigh, TooLow] = subAN_MeanTooHighMaxTooLow(TempCells,MRminthreshold,MLRmaxthreshold)
    
for iFile = 1:length(cell2mat(TempCells.File(1,:)))
    for r = 1:6
    HPCMR{iFile,r} = TempCells.MeanRate{1,iFile,r};
    end
end


for iFile = 1:length(cell2mat(TempCells.File(2,:)))
    for r = 1:6
    SUBMR{iFile,r} = TempCells.MeanRate{2,iFile,r};
    end
end


%% Assess mfr

%if min rate is over threshold = bad cell 
temp_TooHigh{2} =[];
Index_mean_too_high(1,2) = 0;
%HPC
for i = 1:length(HPCMR)
        xx(1,6) = 100;
    for j=1:6
        xx(j)=min(HPCMR{i,j});
    end
        minrate=min(xx);
        if minrate > MRminthreshold
            temp_TooHigh{1}(i,j)= 1;
        end
end
%SUB
for i = 1:length(SUBMR)
        xx(1,6) = 100;
    for j=1:6
        xx(j)=min(SUBMR{i,j});
    end
        minrate=min(xx);
        if minrate > MRminthreshold
            temp_TooHigh{2}(i,j)= 1;
        end
end

for i = 1:2 %for ca1 and sub
    for jCells = 1:size(temp_TooHigh{i},1)

        if sum(temp_TooHigh{i}(jCells,:),2) > 0
         Index_mean_too_high(jCells,i) = 1;
        else 
         Index_mean_too_high(jCells,i) = 0;
        end
    end
end
for i = 1:2
TooHigh{1,i} = find(Index_mean_too_high(:,i)>0);
end


%% Assess mfr for too minimum of activity 
%if max rate is under threshold = bad cell 
temp_TooLow{2} =[];
Index_MLR_too_Low(1,2) = 0;

%CA1
for i = 1:length(HPCMR)
        xx(1,6)=0;
    for j = 1:6
        xx(j) = max(HPCMR{i,j});
    end
        maxrate = max(xx);
            if maxrate < MLRmaxthreshold;
                temp_TooLow{1}(i,j) = 1;
            end
end

%SUB
for i = 1:length(SUBMR)
        xx(1,6)=0;
    for j = 1:6
        xx(j) = max(SUBMR{i,j});
    end
        maxrate = max(xx);
            if maxrate < MLRmaxthreshold;
                temp_TooLow{2}(i,j) = 1;
            end
end

for i = 1:2
    for jCells = 1:size(temp_TooLow{i},1)
        if sum(temp_TooLow{i}(jCells,:))>0;
            Index_MLR_too_Low(jCells,i)=1;
        end
    end
end
for i = 1:2
TooLow{i} = find(Index_MLR_too_Low(:,i)>0);
end

%%


