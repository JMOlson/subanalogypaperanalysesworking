%% Nat Neuro Rev 1 - Analysis Instructions


%% Select local data folder, define paths, load data
% projectFolder = 'C:\Users\Aurif\Documents\Work\FinalIteration_AnalogyPaper\Processed_Data\Compiled'; %Alex Running code
% compiledDataFolder = '\FromJake\Finale';
projectFolder = 'E:\SubCa1Analogy\Data'; %Jake Running code
compiledDataFolder = '\20210904_Compiled';
% compiledDataFolder = '\20200922_Compiled';

load(fullfile(projectFolder,compiledDataFolder,'FinalDatasets.mat'),...
    'HpcRec', 'HpcNeuron', 'SubRec', 'SubNeuron', 'BehRec');

%% Axis cell ID and removal from dataset
% % Get Axis Cells - Sub - 46/480
% [SubCatAxis] = catAxisCellsWrapped(SubNeuron, SubRec, true,...
%     fullfile(projectFolder,compiledDataFolder,'mixVMMMainSubTrackRunning'));
% 
% % Get Axis Cells - CA1 - 0
% [HpcCatAxis] = catAxisCellsWrapped(HpcNeuron, HpcRec, true,...
%     fullfile(projectFolder,compiledDataFolder,'mixVMMMainHpcTrackRunning'));
% 
% % Create new data struct without the axis neurons
% [SubNeuronNoAxis, SubRecNoAxis] = removeAxisNeurons(SubNeuron, SubRec, SubCatAxis.finalAxisLIPReliableRatio);
% save(fullfile(projectFolder,compiledDataFolder,'NoAxisDataset.mat'),'SubRecNoAxis', 'SubNeuronNoAxis');

load(fullfile(projectFolder,compiledDataFolder,'NoAxisDataset.mat'),'SubRecNoAxis', 'SubNeuronNoAxis');

%% Run everything on dataset w/out axis cells.
%% Classic Spatial Measures Analyses
SpatialSummaryStats = classicSpatialMeasuresAnalysis(HpcRec, HpcNeuron, SubRecNoAxis, SubNeuronNoAxis);

%% Figure 2 - Neuron Examples
figure2_SUBAN(SubNeuronNoAxis, HpcNeuron, SubRecNoAxis, HpcRec,ClusterQuality_PostExclusions,'Mean3SD');

%% Figure 2 - Linear Ratemaps / Correlations
% Makes corrrelation across different paths & across different path segments
% Fn to be run on the compiled ratemap struct 
%Correlations are for non-overlapping portions and overlapping portions 
minPathCorrFR = 1;
HpcCorr = makePathCorrelations(HpcRec, HpcNeuron, minPathCorrFR);
SubCorrNoAxis = makePathCorrelations(SubRecNoAxis, SubNeuronNoAxis, minPathCorrFR);
% PpcCorr = makePathCorrelations(PpcRec, PpcNeuron);

Figure2Stats = figure2pt2_SUBAN(SubNeuronNoAxis, HpcNeuron, SubRecNoAxis, HpcRec, SubCorrNoAxis.corrResults_all_nonoverlap, HpcCorr.corrResults_all_nonoverlap,'Mean3SD');

%% Figure 3, 4, & 5 Prep - Make pop correlation
PATHS = [1,2,3,4,9,10];

HpcPopCorr_All = populationCorrAnalyses(HpcNeuron.MeanLinearRates, HpcNeuron.LinearRates, PATHS, false);
SubNoAxisPopCorr_All = populationCorrAnalyses(SubNeuronNoAxis.MeanLinearRates, SubNeuronNoAxis.LinearRates, PATHS, false);

corrMatList = {'oddEvenCorr', 'oddEvenCorr_maxNormed'};

%% Figure 3 - Other Population Correlation Analyses
figure3_SUBAN(HpcRec, SubRecNoAxis, HpcNeuron, SubNeuronNoAxis);

%% Figure 4 Prep - pop correlation Bootstrap and scale and rep analysis 

% Constants & Settings for Scale of Representation Analysis - Includes bootstrap for stats
ScaleAndBiasSettings.PATHS = PATHS;
ScaleAndBiasSettings.PATH_LENGTHS = [140,140,140,140,197,197];
ScaleAndBiasSettings.CORR_THRESHOLDS = [0.1:0.05:0.7];
 % rounding to deal with comparing floating point numbers.

% bias representation settings
ScaleAndBiasSettings.DIFF_OFFSET = 10;
ScaleAndBiasSettings.DIFF_SMOOTH_STD = 1.5; % https://www.mathworks.com/matlabcentral/answers/406563-how-does-smoothdata-function-using-gaussian-method-define-the-standard-deviation-for-different-w
ScaleAndBiasSettings.DO_BS = true;

% Bootstrap stats settings.
ScaleAndBiasSettings.BS_PERCENTILE_THRESHOLD = 1;
ScaleAndBiasSettings.N_BS_ITERATIONS = 1000;

% % Bootstrap is enormous ~30GB - use, save dependent results, don't save BS
%  parfor iBSIter = 1:ScaleAndBiasSettings.N_BS_ITERATIONS
%      SubNoAxisPopCorr_BS{iBSIter} = populationCorrAnalyses(SubNeuronNoAxis.MeanLinearRates, SubNeuronNoAxis.LinearRates, PATHS, true);
%  end
% 
% % Actual scale of rep and bias analysis.
% for iCorrMatStyle = 1:length(corrMatList)
%     SandBCorrResults_SubNoAxis(iCorrMatStyle) = popCorrScaleAndBias(SubNoAxisPopCorr_All, SubNoAxisPopCorr_BS,...
%         corrMatList{iCorrMatStyle}, ScaleAndBiasSettings);
% end
% save(fullfile(projectFolder,compiledDataFolder,'SandBCorrResultsNoAxis'),...
%     'SandBCorrResults_SubNoAxis');

load(fullfile(projectFolder,compiledDataFolder,'SandBCorrResultsNoAxis'),'SandBCorrResults_SubNoAxis');
load(fullfile(projectFolder,compiledDataFolder,'SandBCorrResults'),'SandBCorrResults_Hpc');

%% Figure 4 - Population Correlation Scale and Bias of Representation.
% chosenCorrMatForAnalysis = 'oddEvenCorr_MaxNormed';
chosenCorrMatForAnalysis = 'oddEvenCorr';
chosenCorrMatIndex = find(strcmpi(chosenCorrMatForAnalysis, {SandBCorrResults_Hpc.corrMarixUsed}));

ScaleAndBiasSettings.CHOSEN_THRESH = 0.5;
ScaleAndBiasSettings.thisThresh = find(round(ScaleAndBiasSettings.CORR_THRESHOLDS,2) ==...
    round(ScaleAndBiasSettings.CHOSEN_THRESH,2));

fig4_SUBAN(ScaleAndBiasSettings.thisThresh, chosenCorrMatForAnalysis, SubNeuronNoAxis,...
    SandBCorrResults_Hpc(chosenCorrMatIndex).coreStats,...
    SandBCorrResults_SubNoAxis(chosenCorrMatIndex).coreStats, SubNoAxisPopCorr_All, HpcPopCorr_All, ...
    SandBCorrResults_SubNoAxis(chosenCorrMatIndex).pathPropDistFromTurn,...
    SandBCorrResults_SubNoAxis(chosenCorrMatIndex).transitionPeaks,...
    SandBCorrResults_Hpc(chosenCorrMatIndex).transitionPeaks,...
    ScaleAndBiasSettings.PATH_LENGTHS, ScaleAndBiasSettings.CORR_THRESHOLDS);

fig4Supp_SUBAN(ScaleAndBiasSettings.thisThresh,...
    SandBCorrResults_Hpc(chosenCorrMatIndex).coreStats,...
    SandBCorrResults_SubNoAxis(chosenCorrMatIndex).coreStats,...
    SandBCorrResults_SubNoAxis(chosenCorrMatIndex).pathPropDistFromTurn,...
    SandBCorrResults_SubNoAxis(chosenCorrMatIndex).transitionPeaks,...
    SandBCorrResults_Hpc(chosenCorrMatIndex).transitionPeaks,...
    ScaleAndBiasSettings.DIFF_OFFSET, ScaleAndBiasSettings.PATH_LENGTHS, ScaleAndBiasSettings.CORR_THRESHOLDS);
