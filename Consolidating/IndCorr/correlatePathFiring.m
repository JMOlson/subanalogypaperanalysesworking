function corrResults = correlatePathFiring(RecStruct, NeuronStruct, pathsToCorr, indicesToCorr, minFR, rowParameter)
% Find correlations in mean firing rates for different (subsections of) paths.

%correlatePathFiring One line description of what the function or script performs (H1 line)
%		Optional file header info (to give more details about the function than in the H1 line)  
%
% 	corrResults  = correlatePathFiring(RecStruct, NeuronStruct, pathsToCorr, indicesToCorr)
%
% INPUTS: 
%		RecStruct - Description
%		NeuronStruct - Description
%		pathsToCorr - Description
%		indicesToCorr - put as '' if want all and just want to set rowParameter
%       rowParameter - either 'all' (default), 'complete', 'pairwise' - see corrcoef
%       minFR - minimum firing rate needed in a path for the correlation to run - otherwise, NaN
%
% OUTPUTS: 
%		corrResults - Description
%		                       
% EXAMPLES: 
%		corrResults = correlatePathFiring(sub_rec, sub_neuron, [1,2,3,4,5,6,7,8]);
%		corrResultsReturns = correlatePathFiring(sub_rec, sub_neuron, [9,10]);
%		corrResults2ndLeg = correlatePathFiring(sub_rec, sub_neuron, [1,2,3,4,5,6,7,8],[52:87]);
%		                   
% REMARKS
%   Paths fed in must be the same length. If not, will error.
%
% MAT-FILES REQUIRED: 
%       RecStruct and NeuronStruct outputs from compileRecs Fn for 2020 SUB/CA1 paper
%
% SEE ALSO:
%       corrcoef
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 25-Apr-2020
% LAST MODIFIED BY: Jake Olson
% LAST MODIFIED ON: 06/20/2021
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Input handling 
if exist('indicesToCorr','var')
    if ~isempty(indicesToCorr)
        corrSubsection = true;
    else
        corrSubsection  = false;
    end
else
    corrSubsection  = false;
end

if ~exist('rowParameter','var')
    rowParameter = 'all';
end

%% Correlation code 
% TURN_APEXES = [51,87,118];
nPathsToCorr = numel(pathsToCorr);
nNeurons = numel(NeuronStruct.neuronName);
corrResults = nan(nPathsToCorr,nPathsToCorr,nNeurons);
for iNeu = 1:nNeurons
    pathList = RecStruct.Behavior.pathList{NeuronStruct.recStructIndex(iNeu)};
    
    
    % looking for paths that don't exist - writes to Nans
    %e.g if paths 5-8 don't exist for half the dataset 
    
    pathsFoundToCorr = pathList(ismember(pathList,pathsToCorr));
    missingPaths = find(~ismember(pathsToCorr,pathList));
    justMeans = cell2mat(cellfun(@(x) x(:,1),...
        NeuronStruct.MeanLinearRates(iNeu,pathsFoundToCorr),'UniformOutput',false));
    
    % fill in any missing paths with Nans
    blankPath = nan(size(justMeans,1),1);
    for iMissingPath = missingPaths % go through missingPaths 
        justMeans = [justMeans(:,1:iMissingPath-1), blankPath, justMeans(:,iMissingPath:end)];
    end
    
    if corrSubsection
        corrResults(:,:,iNeu) = corrcoef(justMeans(indicesToCorr,:),'rows',rowParameter);
    else
        corrResults(:,:,iNeu) = corrcoef(justMeans,'rows',rowParameter);
    end
    
    % correlations of NaN if one of the paths never reaches minFR (input parameter)
    for iCol = 1:size(justMeans,2)
        if corrSubsection
            if all(justMeans(indicesToCorr,iCol) < minFR)
                corrResults(:,iCol,iNeu) = NaN;
                corrResults(iCol,:,iNeu) = NaN;
            end
        else
            if all(justMeans(:,iCol) < minFR)
                corrResults(:,iCol,iNeu) = NaN;
                corrResults(iCol,:,iNeu) = NaN;
            end
        end 
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


