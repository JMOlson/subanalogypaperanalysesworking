function [Corr] = makePathCorrelations(RecStruct, NeuronStruct, minFR)
%populationCorrAnalyses One line description of what the function or script performs (H1 line)
%		Optional file header info (to give more details about the function than in the H1 line)  
%
% 	Output = populationCorrAnalyses(neuronStruct, paths)
%
% INPUTS: 
%		input1 - Description
%		input2 - Description	                      
%
% OUTPUTS: 
%		output1 - Description
%
% EXAMPLES: 
%		Line 1 of example
%		Line 2 of exampleand 
%		Line 3 of example
%		                   
%
% REMARKS
%   Wrapper for correlatePathFiring for all of the analyses in the Sub/CA1 comparison paper
%   Olson, Johnson, Tao, Nitz 2020.
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:
%
%
% CREATED BY: Jacob M Olson - based off Alex Johnson's code.
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 20-Mar-2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Corr.corrResults_1234 = correlatePathFiring(RecStruct, NeuronStruct, [1,2,3,4], [], minFR);

Corr.corrResults_910 = correlatePathFiring(RecStruct, NeuronStruct, [9,10], [], minFR);

Corr.corrResults_1234_Leg1 = correlatePathFiring(RecStruct, NeuronStruct,...
    [1,2,3,4], [1:51], minFR);
Corr.corrResults_1234_Leg2 = correlatePathFiring(RecStruct, NeuronStruct,...
    [1,2,3,4], [52:87], minFR);
Corr.corrResults_1234_Leg3 = correlatePathFiring(RecStruct, NeuronStruct,...
    [1,2,3,4], [88:118], minFR);
Corr.corrResults_1234_Leg4 = correlatePathFiring(RecStruct, NeuronStruct,...
    [1,2,3,4], [119:140], minFR);

Corr.corrResults_1234_Leg2to4 = correlatePathFiring(RecStruct, NeuronStruct,...
    [1,2,3,4], [52:140], minFR);

%Make non-overlapping vector for figure 2 

Corr.corrResults_all_nonoverlap = makeNonoverlapResults(Corr);


end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



