function fig4Supp_SUBAN(thisThresh, ScaleAndBiasStats_Hpc,...
    ScaleAndBiasStats_Sub, pathPropDistFromTurn, transitionPeaks_Sub, transitionPeaks_Hpc,...
    DIFF_OFFSET, PATH_LENGTHS, CORR_THRESHOLDS)

%% Figure 4 Supplemental Plots

%%  SUPPLEMENTAL 2A Rep Scale Plots - All Thresholds
nCorrThresh = numel(CORR_THRESHOLDS);
nPaths = size(ScaleAndBiasStats_Sub.scaleOfRep,2);

fullHSVRainbow = ones(nCorrThresh+1,3); % never use the last val, but keeps from havina a divide by 0 error in the line below if only 1 Thresh used.
fullHSVRainbow(:,1) = 0:0.9/nCorrThresh:0.9; % leave out far red side of purple - less ambiguity
fullRGBRainbow = hsv2rgb(fullHSVRainbow);
fullRGBRainbow = fullRGBRainbow(end:-1:1,:);

figure;
sgtitle('Scale of Rep: SUB - Top vs HPC - Bot');
for iPath = 1:nPaths
    for iThresh = 1:2:nCorrThresh
        subplot(2,nPaths,iPath)
        hold on;
        plot(ScaleAndBiasStats_Sub.scaleOfRep{iThresh,iPath},'color',fullRGBRainbow(iThresh,:));
        if iThresh == nCorrThresh
            title(sprintf('Path %i',iPath));
            if any(iPath == 1:4)
                YLIMMAX = 140;
                ylim(gca, [0 YLIMMAX]);
                plot([51,51;87,87;118,118]',[0,YLIMMAX; 0,YLIMMAX; 0,YLIMMAX]','k');
            else
                YLIMMAX = 197;
                ylim(gca, [0 YLIMMAX]);
                plot([15,15;127,127]',[0,YLIMMAX; 0,YLIMMAX]','k');
            end
        end
        
        subplot(2,nPaths,iPath+nPaths)
        hold on;
        plot(ScaleAndBiasStats_Hpc.scaleOfRep{iThresh,iPath},'color',fullRGBRainbow(iThresh,:));
        if iThresh == nCorrThresh
            if any(iPath == 1:4)
                YLIMMAX = 140;
                ylim(gca, [0 YLIMMAX]);
                plot([51,51;87,87;118,118]',[0,YLIMMAX; 0,YLIMMAX; 0,YLIMMAX]','k');
            else
                YLIMMAX = 197;
                ylim(gca, [0 YLIMMAX]);
                plot([15,15;127,127]',[0,YLIMMAX; 0,YLIMMAX]','k');
            end
        end
    end
end
legend(num2str(CORR_THRESHOLDS(1:2:end)'));

%%  SUPPLEMENTAL 2B Rep Alignment Plots - All Thresholds

figure;
sgtitle('Directional Bias: SUB - Top vs HPC - Bot');
for iPath = 1:nPaths
    for iThresh = 1:2:nCorrThresh
        subplot(2,nPaths,iPath)
        hold on;
        plot(ScaleAndBiasStats_Sub.biasVal_Smoothed{iThresh,iPath},'color',fullRGBRainbow(iThresh,:));
        if iThresh == nCorrThresh
            title(sprintf('Path %i',iPath));
            if any(iPath == 1:4)
                YLIMMAX = 140;
                ylim(gca, [-YLIMMAX YLIMMAX]);
                plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
            else
                YLIMMAX = 197;
                ylim(gca, [-YLIMMAX YLIMMAX]);
                plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
            end
        end
        
        subplot(2,nPaths,iPath+nPaths)
        hold on;
        plot(ScaleAndBiasStats_Hpc.biasVal_Smoothed{iThresh,iPath},'color',fullRGBRainbow(iThresh,:));
        if iThresh == nCorrThresh
            if any(iPath == 1:4)
                YLIMMAX = 140;
                ylim(gca, [-YLIMMAX YLIMMAX]);
                plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
            else
                YLIMMAX = 197;
                ylim(gca, [-YLIMMAX YLIMMAX]);
                plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
            end
        end
    end
end
legend(num2str(CORR_THRESHOLDS(1:2:end)'));

%% SUPPLEMENTAL 2C Create graph for apex locations releative to turn - Histograms
figure;
for iThresh = 1:nCorrThresh
    peakLocations_Sub = [];
    peakLocations_Hpc = [];
    for iPath = 1:nPaths
        peakLocations_Sub = [peakLocations_Sub,...
            pathPropDistFromTurn{iPath}(transitionPeaks_Sub{iPath,iThresh})]; %Get distance of each peak, add to list.
        peakLocations_Hpc = [peakLocations_Hpc,...
            pathPropDistFromTurn{iPath}(transitionPeaks_Hpc{iPath,iThresh})]; %Get distance of each peak, add to list.
    end
    
    dataForPolarLinePlots_Sub = 2*pi*repmat(peakLocations_Sub,2,1);
    dataForPolarLinePlots_Hpc = 2*pi*repmat(peakLocations_Hpc,2,1);
    
    subplot(2,nCorrThresh,iThresh);
    polarplot(dataForPolarLinePlots_Sub(:), repmat([0;1],numel(peakLocations_Sub),1),'r');
    hold on;
    polarplot([0,0;0,1],'k');
    title(sprintf('Threshold = %1.2f',CORR_THRESHOLDS(iThresh)));
    
    subplot(2,nCorrThresh,nCorrThresh+iThresh);
    polarplot(dataForPolarLinePlots_Hpc(:), repmat([0;1],numel(peakLocations_Hpc),1),'b');
    hold on;
    polarplot([0,0;0,1],'k');
end

%% SUPPLEMENTAL 3? Rep Alignment plots - Path by Path
HIGH_THRESH = 1;
LOW_THRESH = 2;
YLIMMAX = 100;

figure;
sgtitle('Directional Bias: SUB - Top vs HPC - Bot');
for iPath = 1:nPaths
    subplot(2,6,iPath)
    hold on;
    plot(ScaleAndBiasStats_Sub.biasVal{thisThresh,iPath},'r');
    plot(ScaleAndBiasStats_Sub.biasVal_Thresholds{thisThresh,iPath}(:,HIGH_THRESH),'r--');
    plot(ScaleAndBiasStats_Sub.biasVal_Thresholds{thisThresh,iPath}(:,LOW_THRESH),'r--');
    title(sprintf('Path %i',iPath));
    if any(iPath == 1:4)
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    else
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    end
    subplot(2,6,iPath+6)
    hold on;
    plot(ScaleAndBiasStats_Hpc.biasVal{thisThresh,iPath},'b');
    plot(ScaleAndBiasStats_Hpc.biasVal_Thresholds{thisThresh,iPath}(:,HIGH_THRESH),'b--');
    plot(ScaleAndBiasStats_Hpc.biasVal_Thresholds{thisThresh,iPath}(:,LOW_THRESH),'b--');
    if any(iPath == 1:4)
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    else
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    end
end

%% SUPPLEMENTAL 3? Rep Alignment plots - smoothed - Path by Path
HIGH_THRESH = 1;
LOW_THRESH = 2;
YLIMMAX = 80;

figure;
sgtitle('Smoothed Directional Bias: SUB - Top vs HPC - Bot');
for iPath = 1:nPaths
    subplot(2,6,iPath)
    hold on;
    plot(ScaleAndBiasStats_Sub.biasVal_Smoothed{thisThresh,iPath},'r');
    plot(ScaleAndBiasStats_Sub.biasVal_Smoothed_Thresholds{thisThresh,iPath}(:,HIGH_THRESH),'r--');
    plot(ScaleAndBiasStats_Sub.biasVal_Smoothed_Thresholds{thisThresh,iPath}(:,LOW_THRESH),'r--');
    title(sprintf('Path %i',iPath));
    if any(iPath == 1:4)
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    else
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    end
    subplot(2,6,iPath+6)
    hold on;
    plot(ScaleAndBiasStats_Hpc.biasVal_Smoothed{thisThresh,iPath},'b');
    plot(ScaleAndBiasStats_Hpc.biasVal_Smoothed_Thresholds{thisThresh,iPath}(:,HIGH_THRESH),'b--');
    plot(ScaleAndBiasStats_Hpc.biasVal_Smoothed_Thresholds{thisThresh,iPath}(:,LOW_THRESH),'b--');
    if any(iPath == 1:4)
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    else
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    end
end

%% SUPPLEMENTAL 3? forward/backward alignment deriv. positive change means switch point.
YLIMMAX = 100;
YLIMMIN = -YLIMMAX;

figure;
sgtitle(sprintf('Derivative (%i bins) Directional Bias: SUB - Top vs HPC - Bot',DIFF_OFFSET));
for iPath = 1:nPaths
    DIFF_OFFSET_XS = round(DIFF_OFFSET/2)+1:PATH_LENGTHS(iPath)-round(DIFF_OFFSET/2);
    subplot(2,nPaths,iPath)
    hold on;
    plot(DIFF_OFFSET_XS,...
        ScaleAndBiasStats_Sub.biasVal_Smoothed{thisThresh,iPath}(DIFF_OFFSET+1:end)...
        - ScaleAndBiasStats_Sub.biasVal_Smoothed{thisThresh,iPath}(1:end-DIFF_OFFSET),'r');
    plot([0,PATH_LENGTHS(iPath)],[0,0],'-k');
    plot(DIFF_OFFSET_XS,ScaleAndBiasStats_Sub.biasVal_Smoothed_Diff_Thresholds{thisThresh,iPath}(:,HIGH_THRESH),'r--');
    plot(DIFF_OFFSET_XS,ScaleAndBiasStats_Sub.biasVal_Smoothed_Diff_Thresholds{thisThresh,iPath}(:,LOW_THRESH),'r--');
    if any(iPath == 1:4)
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    else
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    end
    subplot(2,nPaths,iPath+nPaths)
    hold on;
    plot(DIFF_OFFSET_XS,...
        ScaleAndBiasStats_Hpc.biasVal_Smoothed{thisThresh,iPath}(DIFF_OFFSET+1:end)...
        - ScaleAndBiasStats_Hpc.biasVal_Smoothed{thisThresh,iPath}(1:end-DIFF_OFFSET),'b');
    plot([0,PATH_LENGTHS(iPath)],[0,0],'-k');
    plot(DIFF_OFFSET_XS,ScaleAndBiasStats_Hpc.biasVal_Smoothed_Diff_Thresholds{thisThresh,iPath}(:,HIGH_THRESH),'b--');
    plot(DIFF_OFFSET_XS,ScaleAndBiasStats_Hpc.biasVal_Smoothed_Diff_Thresholds{thisThresh,iPath}(:,LOW_THRESH),'b--');
    if any(iPath == 1:4)
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    else
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    end
end

%% SUPPLEMENTAL 3? Smoothed forward/backward bias deriv. positive change means switch point.
YLIMMAX = 10;
YLIMMIN = -YLIMMAX;

figure;
sgtitle(sprintf('Smoothed Derivative (%i bins) Directional Bias: SUB - Top vs HPC - Bot',DIFF_OFFSET));
for iPath = 1:nPaths
    DIFF_OFFSET_XS = round(DIFF_OFFSET/2)+1:PATH_LENGTHS(iPath)-round(DIFF_OFFSET/2);
    subplot(2,nPaths,iPath)
    hold on;
    plot(DIFF_OFFSET_XS',ScaleAndBiasStats_Sub.biasVal_Smoothed_Diff_Smoothed{thisThresh,iPath},'r');
    plot([0,PATH_LENGTHS(iPath)],[0,0],'-k');
    plot(DIFF_OFFSET_XS',ScaleAndBiasStats_Sub.biasVal_Smoothed_Diff_Smoothed_Thresholds{thisThresh,iPath}(:,HIGH_THRESH),'r--');
    plot(DIFF_OFFSET_XS',ScaleAndBiasStats_Sub.biasVal_Smoothed_Diff_Smoothed_Thresholds{thisThresh,iPath}(:,LOW_THRESH),'r--');
    if any(iPath == 1:4)
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    else
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    end
    subplot(2,nPaths,iPath+nPaths)
    hold on;
    plot(DIFF_OFFSET_XS',ScaleAndBiasStats_Hpc.biasVal_Smoothed_Diff_Smoothed{thisThresh,iPath},'b');
    plot([0,PATH_LENGTHS(iPath)],[0,0],'-k');
    plot(DIFF_OFFSET_XS',ScaleAndBiasStats_Hpc.biasVal_Smoothed_Diff_Smoothed_Thresholds{thisThresh,iPath}(:,HIGH_THRESH),'b--');
    plot(DIFF_OFFSET_XS',ScaleAndBiasStats_Hpc.biasVal_Smoothed_Diff_Smoothed_Thresholds{thisThresh,iPath}(:,LOW_THRESH),'b--');
    if any(iPath == 1:4)
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    else
        ylim(gca, [-YLIMMAX YLIMMAX]);
        plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
    end
end

