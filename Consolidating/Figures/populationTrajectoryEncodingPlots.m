%% population Trajectory encoding plots
figure;
subplot(1,2,1)
plot(diag(trajDepCodingCorrs_Sub_OddEvenControl));
ylim([0,1]);
subplot(1,2,2)
plot(diag(SubPopCorr_All.oddEvenCorr(turnIndex+startPosRelTurn:turnIndex+endPosRelTurn,turnIndex+startPosRelTurn:turnIndex+endPosRelTurn)));
ylim([0,1]);
sgtitle('Sub Diagonal Odd/Even Lefts Comparison to path 1 of CorrMat');

figure;
subplot(1,2,1)
plot(diag(trajDepCodingCorrs_Hpc_OddEvenControl));
ylim([0,1]);
subplot(1,2,2)
plot(diag(HpcPopCorr_All.oddEvenCorr(turnIndex+startPosRelTurn:turnIndex+endPosRelTurn,turnIndex+startPosRelTurn:turnIndex+endPosRelTurn)));
ylim([0,1]);
sgtitle('Hpc Diagonal Odd/Even Lefts Comparison to path 1 of CorrMat');

figure;
sc(trajDepCodingCorrs_Sub_OddEvenControl, [0,1]);
title('Traj Dependent First segment: Sub Odd/Even Lefts Comparison');
figure;
sc(trajDepCodingCorrs_Hpc_OddEvenControl, [0,1]);
title('Traj Dependent First segment: Sub Odd/Even Lefts Comparison');
figure;
sc(trajDepCodingCorrs_Sub, [0,1]);
title('Traj Dependent First segment: SUB');
figure;
sc(trajDepCodingCorrs_Hpc, [0,1]);
title('Traj Dependent First segment: HPC');
