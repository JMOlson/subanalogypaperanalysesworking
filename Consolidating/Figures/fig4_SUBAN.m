function fig4_SUBAN(thisThresh, chosenCorrMatForAnalysis, SubNeuron, ScaleAndBiasStats_Hpc,...
    ScaleAndBiasStats_Sub, SubPopCorr_All, HpcPopCorr_All,...
    pathPropDistFromTurn, transitionPeaks_Sub, transitionPeaks_Hpc, PATH_LENGTHS, CORR_THRESHOLDS)

%% Figure 4 Plots

%% 4A Odd/Even Corr Mats
figure;
sc(SubPopCorr_All.(chosenCorrMatForAnalysis),[0,1],'parula');
figure;
sc(HpcPopCorr_All.(chosenCorrMatForAnalysis),[0,1],'parula');

%% 4B - Example position correlation - values from 1 square of the matrix showing how it works

%Set what path and position to print values for to show example correlation
path1Compare = 9;
path2Compare = 10;
positionCompare = 30;
%if first 10 neurons or last 10 neurons are not responsive (common) shift
%search window for example rates
exampleShift = 50;
for ispot = 1:10
    first10Rates(ispot,1) = (SubNeuron.MeanLinearRates{ispot+exampleShift,path1Compare}(positionCompare,1));
    first10Rates(ispot,2) = (SubNeuron.MeanLinearRates{ispot+exampleShift,path2Compare}(positionCompare,1));
    first10Rates(ispot,3) = (ispot+exampleShift);
    %last 5 go in descending order from last neuron in struct
    last10Rates(ispot,1) = (SubNeuron.MeanLinearRates{(size(SubNeuron.MeanLinearRates,1)-ispot+1)-exampleShift,path1Compare}(positionCompare,1));
    last10Rates(ispot,2) = (SubNeuron.MeanLinearRates{(size(SubNeuron.MeanLinearRates,1)-ispot+1)-exampleShift,path2Compare}(positionCompare,1));
    last10Rates(ispot,3) = ((size(SubNeuron.MeanLinearRates,1)-ispot+1)-exampleShift);
end
%Column 1 = rate Path1Compare
%Column 2 = rate Path2Compare
%Column 3 = neuron#
disp(first10Rates)
disp(last10Rates)

clear path1Compare path2Compare exampleShift first10Rates last10Rates ispot

%% 4C - Pop Dists of diagonal even/odd positional autocorrelations. - Consistency of representation
figure;
boxplot([diag(HpcPopCorr_All.(chosenCorrMatForAnalysis)), diag(SubPopCorr_All.(chosenCorrMatForAnalysis))],'notch','on');

figure;
histogram(diag(HpcPopCorr_All.(chosenCorrMatForAnalysis)),0.25:0.05:1,'normalization','probability','FaceColor','r');
axis([0.25,1,0,0.25])
figure;
histogram(diag(SubPopCorr_All.(chosenCorrMatForAnalysis)),0.25:0.05:1,'normalization','probability');
axis([0.25,1,0,0.25])

[P, H, STATS] = ranksum(diag(HpcPopCorr_All.(chosenCorrMatForAnalysis)),diag(SubPopCorr_All.(chosenCorrMatForAnalysis)),'tail','left');
% Max Normed Values
% P =   1.7776e-08
% KSSTAT = -5.5116

[H,P,KSSTAT] = kstest2(diag(HpcPopCorr_All.(chosenCorrMatForAnalysis)),diag(SubPopCorr_All.(chosenCorrMatForAnalysis)),'tail','larger'); % one sided NULL:HPC is larger.
% Max Normed Values
% P =   5.8340e-13
% KSSTAT = 0.1709

% Appropriate Caveot: more non-zero FR, higher Corrs I believe. Tingley & Peyrache's 2020 reactivation
% review I think has a ref to look at this.

%% 4DE Scale of Rep Visualizations
thisPathInd = 5;
linesToSkip = 0;

visScaleOfRep(ScaleAndBiasStats_Hpc, HpcPopCorr_All.(chosenCorrMatForAnalysis),...
    thisPathInd, thisThresh, PATH_LENGTHS,linesToSkip);
visScaleOfRep(ScaleAndBiasStats_Sub, SubPopCorr_All.(chosenCorrMatForAnalysis),...
    thisPathInd, thisThresh, PATH_LENGTHS,linesToSkip);

%% 4DE Scale examples on Scale of Rep Visualization
YLIMMAX = 120;

figure;
hold on;
area(cell2mat(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,thisPathInd)));
ylim(gca, [0 YLIMMAX]);
if numel(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,thisPathInd)) == 140
    
    plot([51,51;87,87;118,118]',[0,YLIMMAX; 0,YLIMMAX; 0,YLIMMAX]','k');
else
    plot([51,51;87,87;118,118]',[0,YLIMMAX; 0,YLIMMAX; 0,YLIMMAX]','k');
end
figure;
hold on;
area(cell2mat(ScaleAndBiasStats_Hpc.scaleOfRep(thisThresh,thisPathInd)));
ylim(gca, [0 YLIMMAX]);
if numel(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,thisPathInd)) == 140
    
    plot([51,51;87,87;118,118]',[0,YLIMMAX; 0,YLIMMAX; 0,YLIMMAX]','k');
else
    plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
end

%% 4DE Alignment examples on Scale of Rep Visualization
YLIMMAX = 100;

figure;
hold on;
area(cell2mat(ScaleAndBiasStats_Sub.biasVal(thisThresh,thisPathInd)));
ylim(gca, [-YLIMMAX, YLIMMAX]);
if numel(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,thisPathInd)) == 140
    plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
else
    plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
end

figure;
hold on;
area(cell2mat(ScaleAndBiasStats_Hpc.biasVal(thisThresh,thisPathInd)));
ylim(gca, [-YLIMMAX, YLIMMAX]);
if numel(ScaleAndBiasStats_Hpc.scaleOfRep(thisThresh,thisPathInd)) == 140
    plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
else
    plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
end

figure;
hold on;
area(cell2mat(ScaleAndBiasStats_Hpc.biasVal_Smoothed_Diff_Smoothed(thisThresh,thisPathInd)));
ylim(gca, [-YLIMMAX, YLIMMAX]);
if numel(ScaleAndBiasStats_Hpc.scaleOfRep(thisThresh,thisPathInd)) == 140
    plot([51,51;87,87;118,118]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
else
    plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
end

%% 4F Scale of Rep Dists and Histogram
AXIS_MAX = 140;
fullScaleRepDist_Hpc = cell2mat(ScaleAndBiasStats_Hpc.scaleOfRep(thisThresh,:)');
fullScaleRepDist_Sub = cell2mat(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,:)');

figure;
histogram(fullScaleRepDist_Sub,0:2:140,'normalization','probability','FaceColor','r');
axis([0,AXIS_MAX,0,0.12])
axis off

figure;
histogram(fullScaleRepDist_Hpc,0:2:140,'normalization','probability');
axis([0,AXIS_MAX,0,0.12])
axis off

figure;
plotSpread({fullScaleRepDist_Sub,fullScaleRepDist_Hpc},'distributionColors',{'r','b'});
title('Scale of Rep');
ylim([0,AXIS_MAX]);

figure;
boxplot([fullScaleRepDist_Sub,fullScaleRepDist_Hpc],'notch','on');
ylim([0,AXIS_MAX]);
% median(fullScaleRepDist_Hpc)
% % 20
% median(fullScaleRepDist_Sub)
% % 27
[p, h, stats] = ranksum(fullScaleRepDist_Sub, fullScaleRepDist_Hpc, 'tail','right');
fprintf('Ranksum: RepScale - Sub > HPC : %0.4f  n1 = %i n2 = %i\n\n',...
    p, length(fullScaleRepDist_Sub), length(fullScaleRepDist_Hpc));

% better here? care more about the dist, not median?
[H,P,KSSTAT] = kstest2(fullScaleRepDist_Sub, fullScaleRepDist_Hpc,'tail','smaller'); % one sided NULL:SUB is larger.
% P =   2.0809e-62
% KSSTAT = 0.3836

%% 4G Scale of Rep - Field sizes, w/ BS error bounds

% dealing with the way percentiles were stored coming from bootstrap
nPercentiles = size(ScaleAndBiasStats_Hpc.scaleOfRep_Thresholds{1,1},2);
HIGH_THRESH = 1;
LOW_THRESH = 2;
YLIMMAX = 120;

figure;
subplot(2,2,1)
hold on;
subOutboundScale = scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Sub, 'scaleOfRep', 'scaleOfRep_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, 0, YLIMMAX, 1:4, 'r', 'Sub_Outbound_Scale');

subplot(2,2,3)
hold on;
ca1OutboundScale = scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Hpc, 'scaleOfRep', 'scaleOfRep_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, 0, YLIMMAX, 1:4, 'b', 'CA1_Outbound_Scale');

subplot(2,2,2)
hold on;
subReturnScale = scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Sub, 'scaleOfRep', 'scaleOfRep_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, 0, YLIMMAX, 5:6, 'r', 'Sub_Return_Scale');

subplot(2,2,4)
hold on;
ca1ReturnScale = scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Hpc, 'scaleOfRep', 'scaleOfRep_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, 0, YLIMMAX, 5:6, 'b', 'CA1_Return_Scale');
title('Scale of Rep: SUB - Top, HPC - Bot');

% better here? care more about the dist, not median?
[~,p, KSSTAT] = kstest2(subReturnScale, subOutboundScale,'tail','smaller'); % one sided NULL:return is larger.
% p =   2.0809e-62
% KSSTAT = 0.3836
fprintf('Ranksum: Sub RepScale Out > Ret : %0.4f ks:%0.4f n1 = %i n2 = %i\n\n',...
    p, KSSTAT, length(subReturnScale), length(subOutboundScale));
% Ranksum: Sub RepScale Out > Ret : 0.0000 ks:0.8325 n1 = 197 n2 = 140

[~,p, KSSTAT] = kstest2(ca1ReturnScale, ca1OutboundScale,'tail','smaller'); % one sided NULL:return is larger.
% p =   2.0809e-62
% KSSTAT = 0.3836
fprintf('Ranksum: CA1 RepScale Out > Ret : %0.4f ks:%0.4f n1 = %i n2 = %i\n\n',...
    p, KSSTAT, length(ca1ReturnScale), length(ca1OutboundScale));
% Ranksum: CA1 RepScale Out > Ret : 0.0000 ks:0.4710 n1 = 197 n2 = 140

%% 4H dist compare sub/ca1 and outbound/returns
plotSpread({mean(cell2mat(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,1:4)'),2),...
    mean(cell2mat(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,5:6)'),2),...
    mean(cell2mat(ScaleAndBiasStats_Hpc.scaleOfRep(thisThresh,1:4)'),2),...
    mean(cell2mat(ScaleAndBiasStats_Hpc.scaleOfRep(thisThresh,5:6)'),2)},...
    'distributionColors',{'r','r','b','b'},'distributionMarkers',{'.','.','.','.'});

%% 4I scatter of scale of rep Sub v CA1
AXIS_MAX = 140;
DOT_SIZE = 100;
OUTBOUND_IND = 1:560;
RETURN_IND = 561:954;

fullScaleRepDist_Hpc = cell2mat(ScaleAndBiasStats_Hpc.scaleOfRep(thisThresh,:)');
fullScaleRepDist_Sub = cell2mat(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,:)');

[~, coefsAll] = fit_2D_data(fullScaleRepDist_Hpc,fullScaleRepDist_Sub,'no'); % this is a downloaded function alex
[~, coefsOut] = fit_2D_data(fullScaleRepDist_Hpc(OUTBOUND_IND),fullScaleRepDist_Sub(OUTBOUND_IND),'no');
[~, coefsRet] = fit_2D_data(fullScaleRepDist_Hpc(RETURN_IND),fullScaleRepDist_Sub(RETURN_IND),'no');
tlsAll = [0,1;AXIS_MAX,1] * coefsAll'; % just linear algebra way of getting y vals for the line
tlsOut = [0,1;AXIS_MAX,1] * coefsOut';
tlsRet = [0,1;AXIS_MAX,1] * coefsRet';

figure;
hold on;
scatter(fullScaleRepDist_Hpc(OUTBOUND_IND),fullScaleRepDist_Sub(OUTBOUND_IND),DOT_SIZE,'.');
hold on;
plot([0,AXIS_MAX],[0,AXIS_MAX],'-k') %diagonal line.
plot([0,AXIS_MAX],[tlsAll],'-') % total least squares regression lines
axis equal
axis([0,AXIS_MAX,0,AXIS_MAX])
ylabel('sub')
xlabel('CA1')

figure;
scatter(fullScaleRepDist_Hpc(RETURN_IND),fullScaleRepDist_Sub(RETURN_IND),DOT_SIZE,'.');
hold on;
plot([0,AXIS_MAX],[0,AXIS_MAX],'-k') %diagonal line.
plot([0,AXIS_MAX],[tlsAll],'-') % total least squares regression lines
axis equal
axis([0,AXIS_MAX,0,AXIS_MAX])
ylabel('sub')
xlabel('CA1')

hold on;
plot([0,AXIS_MAX],[0,AXIS_MAX],'-k') %diagonal line.
plot([0,AXIS_MAX],[tlsAll],'-') % total least squares regression lines
axis equal
axis([0,AXIS_MAX,0,AXIS_MAX])
ylabel('sub')
xlabel('CA1')

corr(fullScaleRepDist_Hpc,fullScaleRepDist_Sub);
% R = 0.4862


% playin with ways to look at scaling
% outboundScaleRepDist_Hpc = cell2mat(ScaleAndBiasStats_Hpc.scaleOfRep(thisThresh,1:4)');
% outboundScaleRepDist_Sub = cell2mat(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,1:4)');
% returnScaleRepDist_Hpc = cell2mat(ScaleAndBiasStats_Hpc.scaleOfRep(thisThresh,5:6)');
% returnScaleRepDist_Sub = cell2mat(ScaleAndBiasStats_Sub.scaleOfRep(thisThresh,5:6)');
%
% medOutbound_HPC = median(outboundScaleRepDist_Hpc);
% medOutbound_SUB = median(outboundScaleRepDist_Sub);
% medReturn_HPC = median(returnScaleRepDist_Hpc);
% medReturn_SUB = median(returnScaleRepDist_Sub);
%
% (medReturn_SUB/medOutbound_SUB)
% (medReturn_HPC/medOutbound_HPC)

%% 4J Smoothed bias plots
nPercentiles = size(ScaleAndBiasStats_Hpc.scaleOfRep_Thresholds{1,1},2);
HIGH_THRESH = 1;
LOW_THRESH = 2;
YLIMMAX = 80;

figure;
title('Smoothed Directional Bias: SUB - Top vs HPC - Bot');
subplot(2,2,1)
hold on;
scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Sub, 'biasVal_Smoothed', 'biasVal_Smoothed_Outbound_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, -YLIMMAX, YLIMMAX, 1:4, 'r', 'Sub_Outbound_Align');

subplot(2,2,3)
hold on;
scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Hpc, 'biasVal_Smoothed', 'biasVal_Smoothed_Outbound_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, -YLIMMAX, YLIMMAX, 1:4, 'b', 'Ca1_Outbound_Align');

subplot(2,2,2)
hold on;
scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Sub, 'biasVal_Smoothed', 'biasVal_Smoothed_Return_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, -YLIMMAX, YLIMMAX, 5:6, 'r', 'Sub_Return_Align');

subplot(2,2,4)
hold on;
scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Hpc, 'biasVal_Smoothed', 'biasVal_Smoothed_Return_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, -YLIMMAX, YLIMMAX, 5:6, 'b', 'Ca1_Return_Align');

%% 4K variance in alignment values
highVarThresh_Sub = cellfun(@(x) x(1),ScaleAndBiasStats_Sub.biasVar_Thresholds);
lowVarThresh_Sub = cellfun(@(x) x(2),ScaleAndBiasStats_Sub.biasVar_Thresholds);
highVarThresh_Hpc = cellfun(@(x) x(1),ScaleAndBiasStats_Hpc.biasVar_Thresholds);
lowVarThresh_Hpc = cellfun(@(x) x(2),ScaleAndBiasStats_Hpc.biasVar_Thresholds);

figure;
bar([ScaleAndBiasStats_Sub.biasVar(thisThresh),highVarThresh_Sub(thisThresh);...
    ScaleAndBiasStats_Hpc.biasVar(thisThresh),highVarThresh_Hpc(thisThresh)]);

figure;
subplot(121);
plot(CORR_THRESHOLDS,ScaleAndBiasStats_Sub.biasVar);
hold on;
plot(CORR_THRESHOLDS,lowVarThresh_Sub,'k--');
plot(CORR_THRESHOLDS,highVarThresh_Sub,'k--');
xlim([CORR_THRESHOLDS(1),CORR_THRESHOLDS(end)]);
% ylim([0,500]);
title('Sub')
xlabel('corr threshold')
ylabel('variance')

subplot(122);
plot(CORR_THRESHOLDS,ScaleAndBiasStats_Hpc.biasVar);
hold on;
plot(CORR_THRESHOLDS,lowVarThresh_Hpc,'k--');
plot(CORR_THRESHOLDS,highVarThresh_Hpc,'k--');
xlim([CORR_THRESHOLDS(1),CORR_THRESHOLDS(end)]);
% ylim([0,500]);
title('Hpc')
xlabel('corr threshold')
ylabel('variance')
title('Variance in bias forward/backward compared to BS 99% thresholds')

%% 4L Smoothed forward/backward alignment deriv. positive change means switch point.
nPercentiles = size(ScaleAndBiasStats_Hpc.biasVal_Smoothed_Diff_Smoothed_Thresholds{1,1},2);
HIGH_THRESH = 1;
LOW_THRESH = 2;
YLIMMAX = 7;

figure;
title('Smoothed Directional Bias: SUB - Top vs HPC - Bot');
subplot(2,2,1)
hold on;
scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Sub, 'biasVal_Smoothed_Diff_Smoothed', 'biasVal_Smoothed_Diff_Smoothed_Outbound_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, -YLIMMAX, YLIMMAX, 1:4, 'r', 'Sub_Outbound_Transition');

subplot(2,2,3)
hold on;
scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Hpc, 'biasVal_Smoothed_Diff_Smoothed', 'biasVal_Smoothed_Diff_Smoothed_Outbound_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, -YLIMMAX, YLIMMAX, 1:4, 'b', 'CA1_Outbound_Transition');

subplot(2,2,2)
hold on;
scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Sub, 'biasVal_Smoothed_Diff_Smoothed', 'biasVal_Smoothed_Diff_Smoothed_Return_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, -YLIMMAX, YLIMMAX, 5:6, 'r', 'Sub_Return_Transition');

subplot(2,2,4)
hold on;
scaleAndBiasStatsAndPlotter(ScaleAndBiasStats_Hpc, 'biasVal_Smoothed_Diff_Smoothed', 'biasVal_Smoothed_Diff_Smoothed_Return_Thresholds',...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, -YLIMMAX, YLIMMAX, 5:6, 'b', 'Ca1_Return_Transition');

%% 4M Plot transition points releative to turns - Histograms
peakLocations_Sub = [];
peakLocations_Hpc = [];
nPaths = numel(PATH_LENGTHS);
for iPath = 1:nPaths
    peakLocations_Sub = [peakLocations_Sub,...
        pathPropDistFromTurn{iPath}(transitionPeaks_Sub{iPath,thisThresh})]; %Get distance of each peak, add to list.
    peakLocations_Hpc = [peakLocations_Hpc,...
        pathPropDistFromTurn{iPath}(transitionPeaks_Hpc{iPath,thisThresh})]; %Get distance of each peak, add to list. 
end

dataForPolarLinePlots_Sub = 2*pi*repmat(peakLocations_Sub,2,1);
dataForPolarLinePlots_Hpc = 2*pi*repmat(peakLocations_Hpc,2,1);

figure;
polarplot(dataForPolarLinePlots_Sub(:), repmat([0;1],numel(peakLocations_Sub),1),'r');
hold on;
polarplot([0,0;0,1],'k');
figure;
polarplot(dataForPolarLinePlots_Hpc(:), repmat([0;1],numel(peakLocations_Hpc),1),'b');
hold on;
polarplot([0,0;0,1],'k');

% circ_mean(2*pi*peakLocations_Sub')
%     0.1175
% circ_mean(2*pi*peakLocations_Hpc')
%     1.9024
% circ_median(2*pi*peakLocations_Sub')
%     0.1879
% circ_median(2*pi*peakLocations_Hpc')
%     2.1991
% stats - median is 0 - turn apex
% pval = circ_medtest(2*pi*peakLocations_Sub',0)
% pval =    0.3877
% pval = circ_medtest(2*pi*peakLocations_Hpc',0)
% pval =    0.0023

end


%% Helper Functions
function visScaleOfRep(scaleDataStruct, corrMat, thisPathInd, thisThresh, PATH_LENGTHS, linesToSkip)
% Plotting helper for Scale of Rep - create the masks to apply over the corrmat

leftValPathHere = scaleDataStruct.leftVal{thisThresh,thisPathInd};
rightValPathHere = scaleDataStruct.rightVal{thisThresh,thisPathInd};
pathLengthHere = PATH_LENGTHS(thisPathInd);
offset = sum(PATH_LENGTHS(1:thisPathInd-1));

corrMatToPlot = corrMat...
    (offset+1:offset+pathLengthHere,offset+1:offset+pathLengthHere);

path1ScaleofRepBefore = false(pathLengthHere,pathLengthHere);
path1ScaleofRepAfter = false(pathLengthHere,pathLengthHere);
path1ScaleofRepBeforeEdge = false(pathLengthHere,pathLengthHere);
path1ScaleofRepAfterEdge = false(pathLengthHere,pathLengthHere);
diagonalMap = false(pathLengthHere,pathLengthHere);
for iPos = 1:linesToSkip+1:pathLengthHere
    diagonalMap(iPos,iPos) = true;
    % Area of scale
    path1ScaleofRepBefore(iPos,iPos - leftValPathHere(iPos):iPos) = true;
    path1ScaleofRepAfter(iPos,iPos:iPos + rightValPathHere(iPos)) = true;
    %edges of scale
    path1ScaleofRepBeforeEdge(iPos,iPos - leftValPathHere(iPos)) = true;
    path1ScaleofRepAfterEdge(iPos,iPos + rightValPathHere(iPos)) = true;
    
end
% for iPos = 1:pathLengthHere
%     diagonalMap(iPos,iPos) = true;
% end


figure;
sc(corrMatToPlot,'gray',[0,1],...
    'parula', path1ScaleofRepBefore, 'parula', path1ScaleofRepAfter,...
    [200/255, 0/255, 0/255],path1ScaleofRepBeforeEdge, [0/255, 140/255, 0/255],path1ScaleofRepAfterEdge,...
    'k',diagonalMap);
end

function meanData = scaleAndBiasStatsAndPlotter(dataStruct, dataToPlot, thresholds,...
    thisThresh, nPercentiles, HIGH_THRESH, LOW_THRESH, YLIMMIN, YLIMMAX, paths, color, catName)
% Little helper so i dont repeat this code over and over again.

if size(dataStruct.(thresholds),2) > 1 % need to average over paths still.
    threshCat = cell2mat(dataStruct.(thresholds)(thisThresh,paths));
    meanHighThresh = mean(threshCat(:,HIGH_THRESH:nPercentiles:end),2);
    meanLowThresh = mean(threshCat(:,LOW_THRESH:nPercentiles:end),2);
else
    meanHighThresh = dataStruct.(thresholds){thisThresh}(:,HIGH_THRESH:nPercentiles:end);
    meanLowThresh = dataStruct.(thresholds){thisThresh}(:,LOW_THRESH:nPercentiles:end);
end

meanData = mean(cell2mat(dataStruct.(dataToPlot)(thisThresh,paths)),2);
fprintf('%s > Thresh: %i/%i\n%s < Thresh: %i/%i\n\n',...
    catName, sum(meanData > meanHighThresh), length(meanData),...
    catName, sum(meanData < meanLowThresh), length(meanData));

plot(meanData,color);
plot(meanHighThresh,'k--');
plot(meanLowThresh,'k--');

ylim(gca, [YLIMMIN YLIMMAX]);
if numel(meanHighThresh) <= 140
    plot([51,51;87,87;118,118]',[YLIMMIN,YLIMMAX; YLIMMIN,YLIMMAX; YLIMMIN,YLIMMAX]','k');
else
    plot([15,15;127,127]',[-YLIMMAX,YLIMMAX; -YLIMMAX,YLIMMAX]','k');
end

end


%%%%%%%%%% End of Code %%%%%%%%%%
