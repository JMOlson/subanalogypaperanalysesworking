function figure3_SUBAN(HpcRec, SubRec, HpcNeuron, SubNeuron)
%% Figure 3 sub analogy paper - population correlation analyses = not scale/bias
% population trajectory encoding.

%% Define necessary constants, as well as the choice mappings. Need it to use the old code.
HpcRec = addTrackInfo( HpcRec );
SubRec = addTrackInfo( SubRec );

nNeu_Hpc = numel(HpcNeuron.neuronName);
nNeu_Sub = numel(SubNeuron.neuronName);

%% Fig 3A is schematic, labeled for all the comparisons about to be made.

%% Fig 3B - task phase - same dir
startPosRelTurn = 1; % First returns turn is at 15
endPosRelTurn = 110; % second is at 127
nPos = endPosRelTurn-startPosRelTurn+1;

path9longStraight = {'route',9;'progress',4};
path10longStraight =  {'route',10;'progress',4};

% Code from 2020 Olson, ... Nitz paper - slightly modified for naming conventions.
% this creates a cell array of neuron by neuron N x M linearRates, N is path loc as defined from
% startPosRelTurn, N is # of trials. Also outputs a cell array of neuron by neuron table with the
% properties of each trial. Very useful for future comparisons/sorting (logical index into the
% linear rates)
% Ptsh - peri turn space histograms - but by grabbing positions on the path far from the turn,
% doesn't have to be around the turn per se.
[PtshRasters_Hpc, PtshParameters_Hpc] = grabTurnData(HpcNeuron, HpcRec, startPosRelTurn, endPosRelTurn);
[PtshRasters_Sub, PtshParameters_Sub] = grabTurnData(SubNeuron, SubRec, startPosRelTurn, endPosRelTurn);

taskPhaseSameDir_Hpc = popCorrCatComparer(path9longStraight, path10longStraight, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
taskPhaseSameDir_Sub = popCorrCatComparer(path9longStraight, path10longStraight, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

%% Fig 3B - Actual Plot & Stats
plotTrajResults(taskPhaseSameDir_Hpc, taskPhaseSameDir_Sub, 'Return Long Straights');
title('Task Phase - Return Route Long Straights');

%% 3C task phase - opp dir
startPosRelTurn = 1; % second returns turn is at 127
endPosRelTurn = 59; % path end is at 187
nPos = endPosRelTurn-startPosRelTurn+1;

path9finalStraight = {'route',9;'progress',4};
path10finalStraight =  {'route',10;'progress',4};

% Grabbing trial data - See Fig 3B, same line, for extensive comment
[PtshRasters_Hpc, PtshParameters_Hpc] = grabTurnData(HpcNeuron, HpcRec, startPosRelTurn, endPosRelTurn);
[PtshRasters_Sub, PtshParameters_Sub] = grabTurnData(SubNeuron, SubRec, startPosRelTurn, endPosRelTurn);

taskPhaseOppDir_Hpc = popCorrCatComparer(path9finalStraight, path10finalStraight, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
taskPhaseOppDir_Sub = popCorrCatComparer(path9finalStraight, path10finalStraight, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

%% Fig 3C - Actual Plot & Stats
plotTrajResults(taskPhaseOppDir_Hpc, taskPhaseOppDir_Sub, 'Return Final Straights');
title('Task Phase - Return Route Final Straights');

% B vs C Sub Comparison Stat for text
[p, h, stats] = ranksum(diag(taskPhaseOppDir_Sub.acrossCatCorrs), diag(taskPhaseSameDir_Sub.acrossCatCorrs), 'tail','left');
fprintf('Ranksum: across group corrs, Sub opp < Sub same : %0.4f  n for 1 group = %i %i\n\n',...
    p, length(diag(taskPhaseOppDir_Sub.acrossCatCorrs)), length(diag(taskPhaseOppDir_Sub.acrossCatCorrs)));

%% 3D Analysis First Straight Trajecory Encoding
startPosRelTurn = -50; % First turn is at 51
endPosRelTurn = 0;
nPos = endPosRelTurn-startPosRelTurn+1;
turnLocation = 1;

firstTurnLefts = {'location',turnLocation;'action','L'};
firstTurnRights =  {'location',turnLocation;'action','R'};

% Grabbing trial data - See Fig 3B, same line, for extensive comment
[PtshRasters_Hpc, PtshParameters_Hpc] = grabTurnData(HpcNeuron, HpcRec, startPosRelTurn, endPosRelTurn);
[PtshRasters_Sub, PtshParameters_Sub] = grabTurnData(SubNeuron, SubRec, startPosRelTurn, endPosRelTurn);

trajDepTurn1_Hpc = popCorrCatComparer(firstTurnLefts, firstTurnRights, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
trajDepTurn1_Sub = popCorrCatComparer(firstTurnLefts, firstTurnRights, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

%% Fig 3D - Actual Plot & Stats
plotTrajResults(trajDepTurn1_Hpc, trajDepTurn1_Sub, 'Turn 1 Traj Dep');
title('Traj Dependent Diagonals - 1st Straight');

%% 3E Analysis 2nd 2 Straights Trajecory Encoding - Settings and Raster Setup
% turnIndex = 118; % turn 3
startPosRelTurn = -66; % First turn is at 51, starting at 52
endPosRelTurn = 0;
nPos = endPosRelTurn-startPosRelTurn+1;

% Grabbing trial data - See Fig 3B, same line, for extensive comment
[PtshRasters_Hpc, PtshParameters_Hpc] = grabTurnData(HpcNeuron, HpcRec, startPosRelTurn, endPosRelTurn);
[PtshRasters_Sub, PtshParameters_Sub] = grabTurnData(SubNeuron, SubRec, startPosRelTurn, endPosRelTurn);

% Path1v2
thirdTurnPath1 = {'route',1;'progress',3};
thirdTurnPath2 =  {'route',2;'progress',3};
trajDepTurn3A_Hpc = popCorrCatComparer(thirdTurnPath1, thirdTurnPath2, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
trajDepTurn3A_Sub = popCorrCatComparer(thirdTurnPath1, thirdTurnPath2, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

% Path3v4
thirdTurnPath3 = {'route',3;'progress',3};
thirdTurnPath4 =  {'route',4;'progress',3};
trajDepTurn3B_Hpc = popCorrCatComparer(thirdTurnPath3, thirdTurnPath4, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
trajDepTurn3B_Sub = popCorrCatComparer(thirdTurnPath3, thirdTurnPath4, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

% Combine two locations.
togetherTmp_Hpc = [trajDepTurn3A_Hpc,trajDepTurn3B_Hpc];
togetherTmp_Sub = [trajDepTurn3A_Sub,trajDepTurn3B_Sub];
OurFields = fieldnames(togetherTmp_Hpc);
trajDepTurn3Mean_Hpc = struct(); % scalar structure
trajDepTurn3Mean_Sub = struct();
for iField= 1:numel(OurFields)
    trajDepTurn3Mean_Hpc.(OurFields{iField}) = mean(cat(3,togetherTmp_Hpc.(OurFields{iField})),3);
    trajDepTurn3Mean_Sub.(OurFields{iField}) = mean(cat(3,togetherTmp_Sub.(OurFields{iField})),3);
end

%% Fig 3E - actual plot & Stats
plotTrajResults(trajDepTurn3Mean_Sub, trajDepTurn3Mean_Hpc, 'Turn 2/3 Traj Dep');
title('Traj Dependent Diagonals - mean of the two 2nd/3rd Straights');

%% 3F Reward Proximity - pre turn 3
startPosRelTurn = -66; % First turn is at 51, starting at 52
endPosRelTurn = 0;
% turnIndex = 118; % turn 3
nPos = endPosRelTurn-startPosRelTurn+1;

% Grabbing trial data - See Fig 3B, same line, for extensive comment
[PtshRasters_Hpc, PtshParameters_Hpc] = grabTurnData(HpcNeuron, HpcRec, startPosRelTurn, endPosRelTurn);
[PtshRasters_Sub, PtshParameters_Sub] = grabTurnData(SubNeuron, SubRec, startPosRelTurn, endPosRelTurn);

% comparing left side to right, outbound straights 2 and 3.
thirdTurnPaths12 = {'location',4};
thirdTurnPaths34 = {'location',5};
approachTurn3_Hpc = popCorrCatComparer(thirdTurnPaths12, thirdTurnPaths34, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
approachTurn3_Sub = popCorrCatComparer(thirdTurnPaths12, thirdTurnPaths34, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

%% 3F, post turn 3 - must consider all sections.
startPosRelTurn = 1; % third turn is at 118, end 140 - only 21 bins once dropping the last one, which is sometimes weird
endPosRelTurn = 21;
% turnIndex = 118; % turn 3
nPos = endPosRelTurn-startPosRelTurn+1;

[PtshRasters_Hpc, PtshParameters_Hpc] = grabTurnData(HpcNeuron, HpcRec, startPosRelTurn, endPosRelTurn);
[PtshRasters_Sub, PtshParameters_Sub] = grabTurnData(SubNeuron, SubRec, startPosRelTurn, endPosRelTurn);

% comparing left side to right, outbound straights 2 and 3.
afterTurn3Path1 = {'progress',3; 'route',1};
afterTurn3Path2 = {'progress',3; 'route',2};
afterTurn3Path3 = {'progress',3; 'route',3};
afterTurn3Path4 = {'progress',3; 'route',4};

afterTurn312_Hpc = popCorrCatComparer(afterTurn3Path1, afterTurn3Path2, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
afterTurn312_Sub = popCorrCatComparer(afterTurn3Path1, afterTurn3Path2, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

afterTurn313_Hpc = popCorrCatComparer(afterTurn3Path1, afterTurn3Path3, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
afterTurn313_Sub = popCorrCatComparer(afterTurn3Path1, afterTurn3Path3, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

afterTurn314_Hpc = popCorrCatComparer(afterTurn3Path1, afterTurn3Path4, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
afterTurn314_Sub = popCorrCatComparer(afterTurn3Path1, afterTurn3Path4, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

afterTurn323_Hpc = popCorrCatComparer(afterTurn3Path2, afterTurn3Path3, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
afterTurn323_Sub = popCorrCatComparer(afterTurn3Path2, afterTurn3Path3, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

afterTurn324_Hpc = popCorrCatComparer(afterTurn3Path2, afterTurn3Path4, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
afterTurn324_Sub = popCorrCatComparer(afterTurn3Path2, afterTurn3Path4, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

afterTurn334_Hpc = popCorrCatComparer(afterTurn3Path3, afterTurn3Path4, PtshRasters_Hpc, PtshParameters_Hpc, nPos, nNeu_Hpc);
afterTurn334_Sub = popCorrCatComparer(afterTurn3Path3, afterTurn3Path4, PtshRasters_Sub, PtshParameters_Sub, nPos, nNeu_Sub);

% Combine comparisons.
togetherTmp_Hpc = [afterTurn312_Hpc,afterTurn313_Hpc,afterTurn314_Hpc,afterTurn323_Hpc,afterTurn324_Hpc,afterTurn334_Hpc];
togetherTmp_Sub = [afterTurn312_Sub,afterTurn313_Sub,afterTurn314_Sub,afterTurn323_Sub,afterTurn324_Sub,afterTurn334_Sub];
OurFields = fieldnames(togetherTmp_Hpc);
afterTurn3All_Hpc = struct(); % scalar structure
afterTurn3All_Sub = struct();
for iField= 1:numel(OurFields)
    afterTurn3All_Hpc.(OurFields{iField}) = mean(cat(3,togetherTmp_Hpc.(OurFields{iField})),3);
    afterTurn3All_Sub.(OurFields{iField}) = mean(cat(3,togetherTmp_Sub.(OurFields{iField})),3);
end

%% Fig 3F - Actual Plots & Stats
% plotTrajResults(approachTurn3_Hpc, approachTurn3_Sub, 'pre 3 avg');
% title('Task Phase Comparison - Reward Proximity - Approach turn 3');
% 
% plotTrajResults(afterTurn3All_Hpc, afterTurn3All_Sub, 'post 3 avg');
% title('Task Phase Comparison - Reward Proximity - average after turn 3');

% before versus after diverging turnComparison Stat for text
fprintf(['Sub approach',' Across Groups Mean = %0.4f, Std +- %0.4f\n'], nanmean(diag(approachTurn3_Sub.acrossCatCorrs)), nanstd(diag(approachTurn3_Sub.acrossCatCorrs)));
fprintf(['Sub aftersplit',' Across Groups Mean = %0.4f, Std +- %0.4f\n'], nanmean(diag(afterTurn3All_Sub.acrossCatCorrs)), nanstd(diag(afterTurn3All_Sub.acrossCatCorrs)));
plotBarsAndStatsAcrossCompare(diag(approachTurn3_Sub.acrossCatCorrs), diag(afterTurn3All_Sub.acrossCatCorrs), 'Sub approach v aftersplit');

fprintf(['Hpc approach',' Across Groups Mean = %0.4f, Std +- %0.4f\n'], nanmean(diag(approachTurn3_Hpc.acrossCatCorrs)), nanstd(diag(approachTurn3_Hpc.acrossCatCorrs)));
fprintf(['Hpc aftersplit',' Across Groups Mean = %0.4f, Std +- %0.4f\n'], nanmean(diag(afterTurn3All_Hpc.acrossCatCorrs)), nanstd(diag(afterTurn3All_Hpc.acrossCatCorrs)));
plotBarsAndStatsAcrossCompare(diag(approachTurn3_Hpc.acrossCatCorrs), diag(afterTurn3All_Hpc.acrossCatCorrs), 'Hpc approach v aftersplit');

plotBarsAndStatsAcrossCompare(diag(afterTurn3All_Hpc.acrossCatCorrs), diag(afterTurn3All_Sub.acrossCatCorrs), 'Hpc v Sub aftersplit');

%% Another 3F Plot - Ind sections
% for iSet = 1:6
%     plotTrajResults(togetherTmp_Hpc(iSet), togetherTmp_Sub(iSet), 'post 3 ind. pairs');
%     title('Task Phase Comparison - Reward Proximity - after turn 3 - individual pairs');
% end

end


%% Helpers
function Results = popCorrCatComparer(cat1Props, cat2Props, PeshRasters, PeshParameters, nPos, nNeu)
% splits up turnRasters by the properties given, including an odd/even split of the first category
% for control purposes. Returns averages across trials for these categories for each position x neuron.
Results = makeLinearFRCategoriesToCompare(PeshRasters, PeshParameters,...
    cat1Props, cat2Props, nPos,nNeu);

% correlate population firing rates at each position across categories
Results.acrossCatCorrs = corr(Results.meanCategory1',Results.meanCategory2');

% correlate population firing rates at each position across odd/even split of category 1 (control)
Results.withinCat1Corrs = corr(Results.oddCategory1',Results.evenCategory1');
Results.withinCat2Corrs = corr(Results.oddCategory2',Results.evenCategory2');
end

function plotTrajResults(HpcResults, SubResults, desiredTitle)
figure;
subplot(1,4,1)
plot(diag(SubResults.withinCat1Corrs));
title('SUB Odd/Even Cat1 control');
ylim([0,1]);
subplot(1,4,2)
plot(diag(HpcResults.withinCat1Corrs));
title('Hpc Odd/Even Cat1 control');
ylim([0,1]);
subplot(1,4,3)
plot(diag(SubResults.acrossCatCorrs));
title('SUB');
ylim([0,1]);
subplot(1,4,4)
plot(diag(HpcResults.acrossCatCorrs));
ylim([0,1]);
title('HPC');

plotBarsAndStatsWithinCompare( diag(SubResults.acrossCatCorrs),...
    diag(SubResults.withinCat1Corrs), diag(SubResults.withinCat2Corrs), ['SUB ', desiredTitle]);

plotBarsAndStatsWithinCompare( diag(HpcResults.acrossCatCorrs),...
    diag(HpcResults.withinCat1Corrs), diag(HpcResults.withinCat2Corrs), ['HPC ', desiredTitle]);

plotBarsAndStatsAcrossCompare(diag(HpcResults.acrossCatCorrs), diag(SubResults.acrossCatCorrs), desiredTitle);

end

function plotBarsAndStatsWithinCompare(acrossGroups, within1, within2, desiredTitle)
figure;
boxplot([acrossGroups,within1,within2],'labels',{'Across Groups', 'Within Group 1', 'Within Group 2'},'notch','on');
title(desiredTitle);
ylim([0,1]);

fprintf([desiredTitle,' Across Groups Mean = %0.4f, Std +- %0.4f\n'], nanmean(acrossGroups), nanstd(acrossGroups));
[p1, h1, stats1] = ranksum(acrossGroups, within1, 'tail','left');
[p2, h2, stats2] = ranksum(acrossGroups, within2, 'tail','left');

fprintf('Ranksum: Across Groups Corr < w/in groups: %0.4f  n for 1 group = %i\n\n',...
    max(p1,p2), length(acrossGroups));
end

function plotBarsAndStatsAcrossCompare(HpcAcross, SubAcross, desiredTitle)
groupingVar = zeros(length(HpcAcross)+length(SubAcross),1)+2;
groupingVar(1:length(HpcAcross)) = 1;

figure;
boxplot([HpcAcross;SubAcross]',groupingVar,'labels',{'HpcAcross', 'SubAcross'},'notch','on');
title(desiredTitle);
ylim([0,1]);

[p, h, stats] = ranksum(HpcAcross, SubAcross, 'tail','left');
fprintf('Ranksum: across group corrs, Hpc < Sub : %0.4f  n for 1 group = %i\n\n',...
    p, length(HpcAcross));
end
