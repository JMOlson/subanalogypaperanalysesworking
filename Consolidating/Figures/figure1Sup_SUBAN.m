function figure1Sup_SUBAN(SubRec, HpcRec, SimPerformance)
%FIGURE1SUP_SUBAN Adding rat x rat versions of Fig 1 - beh data - Olson Johnson ... Nitz 2022
PerformanceSub = calcPerformanceTTT(SubRec, [1,2,3,4], [9,10]);
PerformanceHpc = calcPerformanceTTT(HpcRec, [1,2,3,4], [9,10]);

%% ROI Analysis
rois = [SubRec, HpcRec];
roiNames = ["Sub", "CA1"];
roiPerfs = [PerformanceSub, PerformanceHpc];

%% 1E - ROI - Proportion of Runs Rewarded by recording - histogram
%Calculate proportion of runs that were rewarded

yMax = 0;
for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    nRecs = numel(Roi.rec);
    
    nRuns = zeros(nRecs,1);
    nErrors = zeros(nRecs,1);
    nRewards = zeros(nRecs,1);
    ProportionRunsRewarded = zeros(nRecs,1);
    for iRec = 1:nRecs
        nRuns(iRec) = sum(PerfRoi.nRunsInBlock{iRec});
        nErrors(iRec) = sum(PerfRoi.nErrsInBlock{iRec});
        nRewards(iRec) = nRuns(iRec) - nErrors(iRec);
        ProportionRunsRewarded(iRec) = nRewards(iRec)/nRuns(iRec);
    end
    propRunsRew{iROI} = ProportionRunsRewarded;
    yMax = max(max(histcounts(propRunsRew{iROI},'binwidth',0.05)./nRecs),yMax);
    
end
yMaxCeil5 = ceil(yMax*100/5)*5/100;

for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    
    figure()
    histogram(propRunsRew{iROI},'binwidth',0.05, 'Normalization', 'probability')
    xlim([0 1])
    ylim([0, yMaxCeil5])
    ylabel("% recs")
    xlabel("Proportion of rewarded runs")
    title(roiNames(iROI));
    
    figure()
    boxplot(propRunsRew{iROI})
    ylabel("Proportion of rewarded runs")
    ylim([0 1])
    title(roiNames(iROI));
    
    fprintf('percent mean runs rewarded = %0.4f\n', mean(propRunsRew{iROI}));
    
    nRunsSim = cellfun(@sum, SimPerformance.nRunsInBlock);
    nErrsSim = cellfun(@sum, SimPerformance.nErrsInBlock);
    nRewardsSim = nRunsSim - nErrsSim;
    propRunsRewardedSim = nRewardsSim./nRunsSim;
    fprintf('percent mean runs rewarded by chance = %0.4f\n', mean(propRunsRewardedSim));
    % chance1E = mean([1, 0.75, 0.5, 0.25])
    [p, h, stats] = ranksum(propRunsRew{iROI},propRunsRewardedSim, 'tail','right');
    fprintf('ranksum perf dist < chance = %0.4f\n', p);
end

%% 1F - ROI - Proportion of Runs followed by short return by recording - histogram
yMax = 0;
for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    nRecs = numel(Roi.rec);
    
    yMax = max(max(histcounts(PerfRoi.probCloseReturn,'binwidth',0.05)./nRecs),yMax);
end
yMaxCeil5 = ceil(yMax*100/5)*5/100;

for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    
    figure()
    histogram(PerfRoi.probCloseReturn,'binwidth',0.05, 'Normalization', 'probability')
    ylabel("% recs")
    xlabel("Proportion of runs followed by short return")
    xlim([0 1])
    ylim([0, yMaxCeil5])
    title(roiNames(iROI));
    
    figure()
    boxplot(PerfRoi.probCloseReturn)
    ylabel("Proportion of runs followed by short return")
    ylim([0 1])
    title(roiNames(iROI));
    
    p = signtest(PerfRoi.probCloseReturn, 0.5, 'tail','right');
    mean(PerfRoi.probCloseReturn);
    std(PerfRoi.probCloseReturn);
    chance1F = 0.5;
    fprintf('signtest: short return: %0.4f < chance: %0.4f\n', mean(PerfRoi.probCloseReturn), p);
end

%% 1G - ROI - Proportion of blocks run without error - histogram
yMax = 0;
for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    nRecs = numel(Roi.rec);
    
    yMax = max(max(histcounts(PerfRoi.perfectBlockPercent./100,'binwidth',0.05)./nRecs),yMax);
end
yMaxCeil5 = ceil(yMax*100/5)*5/100;

for iROI = 1:2
    PerfRoi = roiPerfs(iROI);
    
    figure()
    histogram(PerfRoi.perfectBlockPercent./100,'binwidth',0.05, 'Normalization', 'probability')
    ylabel("% recs")
    xlabel("Proportion of blocks run without error")
    xlim([0 1])
    ylim([0, yMaxCeil5])
    title(roiNames(iROI));
    
    figure()
    boxplot(PerfRoi.perfectBlockPercent./100)
    ylabel("Proportion of blocks run without error")
    ylim([0 1])
    title(roiNames(iROI));
    
    fprintf('percent perfect blocks by chance = %0.4f\n', mean(SimPerformance.perfectBlockPercent));
    [p, h, stats] = ranksum(PerfRoi.perfectBlockPercent,SimPerformance.perfectBlockPercent, 'tail','right');
    % chance1G = (1 * 0.75 * 0.5 * 0.25)
    fprintf('ranksum: perfect blocks: %0.4f < chance: %0.4f\n', mean(PerfRoi.perfectBlockPercent), p);
end

%% 1H - ROI - Proportion of (runs 1-4 in) blocks stereotyped to most run pattern  - histogram
yMax = 0;
for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    nRecs = numel(Roi.rec);
    
    nBlocks = cellfun(@numel, PerfRoi.RunBlocks);
    nPerfectBlocks = round(PerfRoi.perfectBlockPercent/100 .* nBlocks);
    
    countMaxPattern = cellfun(@max, PerfRoi.nEachPattern, 'UniformOutput', false);
    countMaxPatternIsEmpty = cellfun(@isempty, countMaxPattern);
    if sum(countMaxPatternIsEmpty) > 0
        countMaxPattern{countMaxPatternIsEmpty} = 0;
    end
    countMaxPattern = cell2mat(countMaxPattern);
    commonBlockPercentPerfect{iROI} = countMaxPattern./nPerfectBlocks;
    commonBlockPercentAll{iROI} = countMaxPattern./nBlocks;
    
    yMax = max(max(histcounts(commonBlockPercentPerfect{iROI},'binwidth',0.05)./nRecs),yMax);
end
yMaxCeil5 = ceil(yMax*100/5)*5/100;

for iROI = 1:2
    figure()
    histogram(commonBlockPercentPerfect{iROI},'binwidth',0.05, 'Normalization', 'probability')
    ylabel("% recs")
    xlabel("Proportion of blocks stereotyped to most run pattern")
    xlim([0 1])
    ylim([0, yMaxCeil5])
    title(roiNames(iROI));
    
    figure()
    boxplot(commonBlockPercentPerfect{iROI})
    ylim([0 1])
    ylabel("Probability of blocks stereotyped to most run pattern")
    title(roiNames(iROI));
    
    % nanmean(commonBlockPercent)
    % nanstd(commonBlockPercent)
    
    nBlocksSim = cellfun(@numel, SimPerformance.RunBlocks);
    nPerfectBlocksSim = round(SimPerformance.perfectBlockPercent/100 .* nBlocksSim);
    
    countMaxPatternSim = cellfun(@max, SimPerformance.nEachPattern, 'UniformOutput', false);
    countMaxPatternSim = cell2mat(countMaxPatternSim);
    commonBlockPercentSim = countMaxPatternSim./nPerfectBlocksSim;
    
    fprintf('percent most common pattern blocks = %0.4f +- %0.4f \n', nanmean(commonBlockPercentPerfect{iROI}), nanstd(commonBlockPercentPerfect{iROI}));
    fprintf('percent most common pattern blocks chance = %0.4f +- %0.4f \n', mean(commonBlockPercentSim), std(commonBlockPercentSim));
    [p, h, stats] = ranksum(commonBlockPercentPerfect{iROI},commonBlockPercentSim, 'tail','right');
    fprintf('N = 94 cuz one rec no perfect blocks');
    fprintf('ranksum: most common pattern: %0.4f < chance: %0.4f\n', nanmean(commonBlockPercentSim), p);
end

%% 1I - ROI - Proportion of Runs alternating L/R at first turn - histogram
yMax = 0;
for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    nRecs = numel(Roi.rec);
    
    yMax = max(max(histcounts(PerfRoi.prob1stTurnAlt,'binwidth',0.05)./nRecs),yMax);
end
yMaxCeil5 = ceil(yMax*100/5)*5/100;

for iROI = 1:2
    PerfRoi = roiPerfs(iROI);
    
    figure()
    histogram(PerfRoi.prob1stTurnAlt,'binwidth',0.05, 'Normalization', 'probability')
    ylabel("% recs")
    xlabel("Proportion of runs where alternation occured @ turn1")
    xlim([0 1])
    ylim([0, yMaxCeil5])
    title(roiNames(iROI));
    
    figure()
    boxplot(PerfRoi.prob1stTurnAlt)
    ylabel("Proportion of runs where alternation occured @ turn1")
    ylim([0 1])
    title(roiNames(iROI));
    
    p = signtest(PerfRoi.prob1stTurnAlt, 0.5, 'tail','right');
    % chance1I = 0.5;
    fprintf('ranksum: alteration at turn 1: %0.4f < chance: %0.4f\n', mean(PerfRoi.prob1stTurnAlt), p);
end

%% 1J - ROI - Proportion of Runs alternating L/R at 3rd turn, grouped - histogram
yMax = 0;
for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    nRecs = numel(Roi.rec);
    
    yMax = max(max(histcounts(PerfRoi.prob3rdTurnAltGrouped,'binwidth',0.05)./nRecs),yMax);
end
yMaxCeil5 = ceil(yMax*100/5)*5/100;

for iROI = 1:2
    PerfRoi = roiPerfs(iROI);
    
    figure()
    histogram(PerfRoi.prob3rdTurnAltGrouped,'binwidth',0.05, 'Normalization', 'probability')
    ylabel("% recs")
    xlabel("Proportion of runs where alternation occured @ turn3 - grouped")
    xlim([0 1])
    ylim([0, yMaxCeil5])
    title(roiNames(iROI));
    
    figure()
    boxplot(PerfRoi.prob3rdTurnAltGrouped)
    ylabel("Proportion of runs where alternation occured @ turn3 - grouped")
    ylim([0 1])
    title(roiNames(iROI));
    
    p = signtest(PerfRoi.prob3rdTurnAltGrouped, 0.5, 'tail','right');
    % chance1I = 0.5;
    fprintf('ranksum: alteration at turn 3 - grouped: %0.4f < chance: %0.4f\n', mean(PerfRoi.prob3rdTurnAltGrouped), p);
end

%% 1K - ROI - Proportion of Runs alternating L/R at 3rd turn, separate - histogram
yMax = 0;
for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    nRecs = numel(Roi.rec);
    
    yMax = max(max(histcounts(PerfRoi.prob3rdTurnAltSep,'binwidth',0.05)./nRecs),yMax);
end
yMaxCeil5 = ceil(yMax*100/5)*5/100;

for iROI = 1:2
    PerfRoi = roiPerfs(iROI);
    
    figure()
    histogram(PerfRoi.prob3rdTurnAltSep,'binwidth',0.05, 'Normalization', 'probability')
    ylabel("% recs")
    xlabel("Proportion of runs where alternation occured @ turn3 - separate turns")
    xlim([0 1])
    ylim([0, yMaxCeil5])
    title(roiNames(iROI));
    
    figure()
    boxplot(PerfRoi.prob3rdTurnAltSep)
    ylabel("Proportion of runs where alternation occured @ turn3 - separate turns")
    ylim([0 1])
    title(roiNames(iROI));
    
    p = signtest(PerfRoi.prob3rdTurnAltSep, 0.5, 'tail','right');
    % chance1I = 0.5;
    fprintf('ranksum: alteration at turn 3 - separate: %0.4f < chance: %0.4f\n', mean(PerfRoi.prob3rdTurnAltSep), p);
end

%% Rat w/in ROI analysis - so one rat w/ both CA1 and SUB analyzed twice - 1x SUB recs, 1x CA1 recs
for iROI = 1:2
    Roi = rois(iROI);
    PerfRoi = roiPerfs(iROI);
    rats = unique(Roi.rat);
    disp(rats(end:-1:1));
    colors = distinguishable_colors(length(rats),[1,1,1]);
    
    for iRat = numel(rats):-1:1 %backwards so I don't have to init vars.
        thisRat = rats{iRat};
        thisRatRecs = strcmp(thisRat, Roi.rat);
        PerfRat = structfun(@(x) x(thisRatRecs,:,:),PerfRoi,'UniformOutput',false);
        nRecs = sum(thisRatRecs);
        
        %         fprintf("Now analyzing: %s's %s data \n", thisRat, roiNames(iROI));
        %
        %         %% 1E - rat - Proportion of Runs Rewarded by recording - histogram
        %         %Calculate proportion of runs that were rewarded
        %         for iRec = 1:nRecs
        %             NumberOfRuns(iRec) = sum(PerfRat.nRunsInBlock{iRec});
        %             NumberOfErrors(iRec) = sum(PerfRat.nErrsInBlock{iRec});
        %             NumberOfRewards(iRec) = NumberOfRuns(iRec) - NumberOfErrors(iRec);
        %             ProportionRunsRewarded(iRec) = NumberOfRewards(iRec)/NumberOfRuns(iRec);
        %         end
        %
        %         figure()
        %         histogram(ProportionRunsRewarded,'binwidth',0.05)
        %         numticks = yticks;
        %         yticklabels(numticks./size(PerfRat.nRunsInBlock,2));
        %         ylabel("% of recordings")
        %         xlabel("Proportion of rewarded runs")
        %         xlim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         figure()
        %         boxplot(ProportionRunsRewarded)
        %         ylabel("Proportion of rewarded runs")
        %         ylim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         fprintf('percent mean runs rewarded = %0.4f\n', mean(ProportionRunsRewarded));
        %
        %         nRunsSim = cellfun(@sum, SimPerformance.nRunsInBlock);
        %         nErrsSim = cellfun(@sum, SimPerformance.nErrsInBlock);
        %         nRewardsSim = nRunsSim - nErrsSim;
        %         propRunsRewardedSim = nRewardsSim./nRunsSim;
        %         fprintf('percent mean runs rewarded by chance = %0.4f\n', mean(propRunsRewardedSim));
        %         % chance1E = mean([1, 0.75, 0.5, 0.25])
        %         [p, h, stats] = ranksum(ProportionRunsRewarded,propRunsRewardedSim, 'tail','right');
        %         fprintf('ranksum perf dist < chance = %0.4f\n', p);
        %
        %         %% 1F - rat - Proportion of Runs followed by short return by recording - histogram
        %         figure()
        %         histogram(PerfRat.probCloseReturn,'binwidth',0.05)
        %         numticks = yticks;
        %         yticklabels(numticks./size(PerfRat.nRunsInBlock,2));
        %         ylabel("% of recordings")
        %         xlabel("Proportion of runs followed by short return")
        %         xlim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         figure()
        %         boxplot(PerfRat.probCloseReturn)
        %         ylabel("Proportion of runs followed by short return")
        %         ylim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         p = signtest(PerfRat.probCloseReturn, 0.5, 'tail','right');
        %         mean(PerfRat.probCloseReturn);
        %         std(PerfRat.probCloseReturn);
        %         chance1F = 0.5;
        %         fprintf('signtest: short return: %0.4f < chance: %0.4f\n', mean(PerfRat.probCloseReturn), p);
        %
        %         %% 1G - rat - Proportion of blocks run without error - histogram
        %         figure()
        %         histogram(PerfRat.perfectBlockPercent./100,'binwidth',0.05)
        %         numticks = yticks;
        %         yticklabels(numticks./size(PerfRat.nRunsInBlock,2));
        %         ylabel("% of recordings")
        %         xlabel("Proportion of blocks run without error")
        %         xlim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         figure()
        %         boxplot(PerfRat.perfectBlockPercent./100)
        %         ylabel("Proportion of blocks run without error")
        %         ylim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         fprintf('percent perfect blocks by chance = %0.4f\n', mean(SimPerformance.perfectBlockPercent));
        %         [p, h, stats] = ranksum(PerfRat.perfectBlockPercent,SimPerformance.perfectBlockPercent, 'tail','right');
        %         % chance1G = (1 * 0.75 * 0.5 * 0.25)
        %         fprintf('ranksum: perfect blocks: %0.4f < chance: %0.4f\n', mean(PerfRat.perfectBlockPercent), p);
        %
        %         %% 1H - rat - Proportion of (runs 1-4 in) blocks stereotyped to most run pattern  - histogram
        %         nBlocks = cellfun(@numel, PerfRat.RunBlocks);
        %         nPerfectBlocks = round(PerfRat.perfectBlockPercent/100 .* nBlocks);
        %
        %         countMaxPattern = cellfun(@max, PerfRat.nEachPattern, 'UniformOutput', false);
        %         countMaxPatternIsEmpty = cellfun(@isempty, countMaxPattern);
        %         if sum(countMaxPatternIsEmpty) > 0
        %             countMaxPattern{countMaxPatternIsEmpty} = 0;
        %         end
        %         countMaxPattern = cell2mat(countMaxPattern);
        %         commonBlockPercentPerfect = countMaxPattern./nPerfectBlocks;
        %         commonBlockPercentAll = countMaxPattern./nBlocks;
        %
        %         figure()
        %         histogram(commonBlockPercentPerfect,'binwidth',0.05)
        %         numticks = yticks;
        %         yticklabels(numticks./size(PerfRat.nRunsInBlock,2));
        %         ylabel("% of recordings")
        %         xlabel("Proportion of blocks stereotyped to most run pattern")
        %         xlim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         figure()
        %         boxplot(commonBlockPercentPerfect)
        %         ylim([0 1])
        %         ylabel("Probability of blocks stereotyped to most run pattern")
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         % nanmean(commonBlockPercent)
        %         % nanstd(commonBlockPercent)
        %
        %         nBlocksSim = cellfun(@numel, SimPerformance.RunBlocks);
        %         nPerfectBlocksSim = round(SimPerformance.perfectBlockPercent/100 .* nBlocksSim);
        %
        %         countMaxPatternSim = cellfun(@max, SimPerformance.nEachPattern, 'UniformOutput', false);
        %         countMaxPatternSim = cell2mat(countMaxPatternSim);
        %         commonBlockPercentSim = countMaxPatternSim./nPerfectBlocksSim;
        %
        %         fprintf('percent most common pattern blocks = %0.4f +- %0.4f \n', nanmean(commonBlockPercentPerfect), nanstd(commonBlockPercentPerfect));
        %         fprintf('percent most common pattern blocks chance = %0.4f +- %0.4f \n', mean(commonBlockPercentSim), std(commonBlockPercentSim));
        %         [p, h, stats] = ranksum(commonBlockPercentPerfect,commonBlockPercentSim, 'tail','right');
        %         fprintf('N = 94 cuz one rec no perfect blocks');
        %         fprintf('ranksum: most common pattern: %0.4f < chance: %0.4f\n', nanmean(commonBlockPercentSim), p);
        %
        %         %% 1I - rat - Proportion of Runs alternating L/R at first turn - histogram
        %         figure()
        %         histogram(PerfRat.prob1stTurnAlt,'binwidth',0.05)
        %         numticks = yticks;
        %         yticklabels(numticks./size(PerfRat.nRunsInBlock,2)); %cut in half due to each recording being made from 2 data points
        %         ylabel("% of recordings")
        %         xlabel("Proportion of runs where alternation occured @ turn1")
        %         xlim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         figure()
        %         boxplot(PerfRat.prob1stTurnAlt)
        %         ylabel("Proportion of runs where alternation occured @ turn1")
        %         ylim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         p = signtest(PerfRat.prob1stTurnAlt, 0.5, 'tail','right');
        %         % chance1I = 0.5;
        %         fprintf('ranksum: alteration at turn 1: %0.4f < chance: %0.4f\n', mean(PerfRat.prob1stTurnAlt), p);
        %
        %         %% 1J - rat - Proportion of Runs alternating L/R at 3rd turn, grouped - histogram
        %         figure()
        %         histogram(PerfRat.prob3rdTurnAltGrouped,'binwidth',0.05)
        %         numticks = yticks;
        %         yticklabels(numticks./size(PerfRat.nRunsInBlock,2)); %cut in half due to each recording being made from 2 data points
        %         ylabel("% of recordings")
        %         xlabel("Proportion of runs where alternation occured @ turn3 - grouped")
        %         xlim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         figure()
        %         boxplot(PerfRat.prob3rdTurnAltGrouped)
        %         ylabel("Proportion of runs where alternation occured @ turn3 - grouped")
        %         ylim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         p = signtest(PerfRat.prob3rdTurnAltGrouped, 0.5, 'tail','right');
        %         % chance1I = 0.5;
        %         fprintf('ranksum: alteration at turn 3 - grouped: %0.4f < chance: %0.4f\n', mean(PerfRat.prob3rdTurnAltGrouped), p);
        %
        %         %% 1K - rat - Proportion of Runs alternating L/R at 3rd turn, separate - histogram
        %         figure()
        %         histogram(PerfRat.prob3rdTurnAltSep,'binwidth',0.05)
        %         numticks = yticks;
        %         yticklabels(numticks./size(PerfRat.nRunsInBlock,2)); %cut in half due to each recording being made from 2 data points
        %         ylabel("% of recordings")
        %         xlabel("Proportion of runs where alternation occured @ turn3 - separate turns")
        %         xlim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         figure()
        %         boxplot(PerfRat.prob3rdTurnAltSep)
        %         ylabel("Proportion of runs where alternation occured @ turn3 - separate turns")
        %         ylim([0 1])
        %         title(sprintf("%s: Rat %s", roiNames(iROI), thisRat));
        %
        %         p = signtest(PerfRat.prob3rdTurnAltSep, 0.5, 'tail','right');
        %         % chance1I = 0.5;
        %         fprintf('ranksum: alteration at turn 3 - separate: %0.4f < chance: %0.4f\n', mean(PerfRat.prob3rdTurnAltSep), p);
    end
end

%% Speed analysis
plotSpeedByROIandAnimal(SubRec, HpcRec);
end

