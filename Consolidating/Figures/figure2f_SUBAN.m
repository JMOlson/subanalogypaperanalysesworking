function  [stat] = figure2f_SUBAN(SubCorr, HpcCorr)
% Figure 2F for SubCA1 analogy paper

% SubCorr & HpcCorr are all internal comps then last is returns. nNeurons x 7
Sub_numCorr = size(SubCorr,1); % nNeurons
Hpc_numCorr = size(HpcCorr,1); % nNeurons

Sub_NeuronMaxCorr = nanmax(SubCorr,[],2);
Hpc_NeuronMaxCorr = nanmax(HpcCorr,[],2);
Sub_CountNeuronMaxNoCorr = sum(isnan(Sub_NeuronMaxCorr))/Sub_numCorr;
Hpc_CountNeuronMaxNoCorr = sum(isnan(Hpc_NeuronMaxCorr))/Hpc_numCorr;

fprintf('Sub Neurons Mean Corr: %0.4f +- %0.4f\nHpc Neurons Mean Corr: %0.4f +- %0.4f\n\n',...
    nanmean(Sub_NeuronMaxCorr), nanstd(Sub_NeuronMaxCorr),...
    nanmean(Hpc_NeuronMaxCorr), nanstd(Hpc_NeuronMaxCorr));

fprintf('Sub neurons in stat test: %i\n', sum(~isnan(Sub_NeuronMaxCorr)));
fprintf('Hpc neurons in stat test: %i\n', sum(~isnan(Hpc_NeuronMaxCorr)));

fprintf('%% Sub Neurons > 50: %0.4f -- %% Hpc Neurons > 50: %0.4f\n',...
    sum(Sub_NeuronMaxCorr > 0.5)/Sub_numCorr, sum(Hpc_NeuronMaxCorr > 0.5)/Hpc_numCorr);

%KS test - are the two distributions close in space
[stat.KS_MaxCorr, stat.KS_MaxCorr_p, stat.KS_MaxCorr_stat] = kstest2(Hpc_NeuronMaxCorr,Sub_NeuronMaxCorr,'tail','larger');

%Wilcoxon Rank Sum
[stat.WRS_MaxCorr_p, stat.WRS_MaxCorr, stat.WRS_MaxCorr_stat] = ranksum(Hpc_NeuronMaxCorr,Sub_NeuronMaxCorr,'tail','left');

figure('Name',['Max Correlation Values. KS p=', num2str(stat.KS_MaxCorr_p),' WRS p=', num2str(stat.WRS_MaxCorr_p)])
subplot(1,2,1)
title("CA1")
histogram(Hpc_NeuronMaxCorr,'BinWidth',0.05,'Normalization','probability') %probability includes count of nan values
xlim([-1 1])
ylim([0 0.25])
xlabel("CA1 max r-value")
ylabel("% neurons")
subplot(1,2,2)
title("Sub")
histogram(Sub_NeuronMaxCorr,'BinWidth',0.05,'Normalization','probability') %probability includes count of nan values
xlim([-1 1])
ylim([0 0.25])
xlabel("SUB max r-value")
ylabel("% neurons")

figure('Name',['Nan Max-Correlation Count'])
bar([Hpc_CountNeuronMaxNoCorr,Sub_CountNeuronMaxNoCorr]) %probability includes count of nan values
ylim([0 0.5])
xticklabels({"CA1","SUB"})
ylabel("% neurons")

end
