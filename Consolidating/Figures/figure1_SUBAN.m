function figure1_SUBAN(BehRec, Performance, SimPerformance)
%% Make figures for Figure 1 
%Jake Olson
%Alex Johnson
%July 2020

%% Results text
nRecs = numel(BehRec.Behavior.trackingSampleRate_Hz);

% Avg trials/session
meanRuns = mean(cellfun(@sum, BehRec.Behavior.nRunsEachPath));
fprintf('Runs per session = %0.0f\n', meanRuns);

% Clean trials/session - percentage
for iRec = 1:nRecs
    nTotalRuns(iRec) = sum(abs(BehRec.Behavior.behaviorEvents{iRec}(:,3))<=10 & abs(BehRec.Behavior.behaviorEvents{iRec}(:,3))>0);
end
meanTotalRuns = mean(nTotalRuns);
fprintf('percent clean per session = %0.4f\n', meanRuns/meanTotalRuns);

% Mean vel during runs
for iRec = 1:nRecs
    nPaths = BehRec.Behavior.nPaths(iRec);
    velThisRec = BehRec.Behavior.velSmoothed_CmPerSec{iRec}(:,1); % need speed (col 1) not orientation (col 2)
    velocityDataVec = [];
    for iPath = 1:nPaths
        starts = BehRec.Behavior.pathRunsLineMarkers{iRec}{iPath,1};
        ends = BehRec.Behavior.pathRunsLineMarkers{iRec}{iPath,2};
        nRuns = numel(starts);
        for iRun = 1:nRuns
            
            velocityDataVec = [velocityDataVec; velThisRec(starts(iRun):ends(iRun))];
        end
    end
    meanVelThisRec(iRec) = nanmean(velocityDataVec);
end
fprintf('mean running velocity = %0.0f\n', mean(meanVelThisRec));

%% Load data for recording with good tracking
%Used for 1B - 1D 

GOOD_TRACKING_REC = 10; % 'rat BL02 rec 31'
PATH_LIST = [1,2,3,4,9,10]; % used in the helper script
XMIN = 50;
XMAX = 600;
YMIN = 50;
YMAX = 800;

% max bin values can change w/out being considered a jump and removed
JUMP_MAX = 35;

startEpochSample = BehRec.sessionTimeStamps{GOOD_TRACKING_REC}(3);
endEpochSample = BehRec.sessionTimeStamps{GOOD_TRACKING_REC}(4);
tracking = BehRec.Behavior.posXY{GOOD_TRACKING_REC}(startEpochSample:endEpochSample,:);

pathLM = BehRec.Behavior.pathRunsLineMarkers{GOOD_TRACKING_REC};
nRecs = size(Performance.nRunsInBlock,1);        

%% 1D - Plot path by path tracking for good runs 
%1D determines 'badspots' must be run first

for iPath = 1:size(pathLM,1)
    figure(iPath+3);
    ax = gca;
    hold on;
    for iRun = 1:size(pathLM{iPath,1},1)
        ax.ColorOrderIndex = iPath; % I must have changed the colormap - not sure where I stored that.
        xs = BehRec.Behavior.posXY{GOOD_TRACKING_REC}(pathLM{iPath,1}(iRun):pathLM{iPath,2}(iRun),1);
        ys = BehRec.Behavior.posXY{GOOD_TRACKING_REC}(pathLM{iPath,1}(iRun):pathLM{iPath,2}(iRun),2);
        %take out rows with nans
        badSpotsNans = isnan(xs) | isnan(ys);
        % remove off track from plotting
        badSpotsOffTrack = xs<XMIN | xs>XMAX | ys<YMIN | ys>YMAX;
        % remove jumps from glare from plotting.
        badSpotsJumps = [0; abs(diff(xs)) > JUMP_MAX | abs(diff(ys)) > JUMP_MAX ];
        
        anyBadSpots = badSpotsNans | badSpotsOffTrack | badSpotsJumps;
        xsClean = xs(~anyBadSpots);
        ysClean = ys(~anyBadSpots);
        
        plot(xsClean,ysClean,'k');
    end
    axis([XMIN XMAX YMIN YMAX]);
    axis off
end

%% 1B - 2D HD Map
%Redo clean xs and ys for HD and vel map
xs = tracking(:,1);
ys = tracking(:,2);
%take out rows with nans
badSpotsNans = isnan(xs) | isnan(ys);
% remove off track from plotting
badSpotsOffTrack = xs<XMIN | xs>XMAX | ys<YMIN | ys>YMAX;
% remove jumps from glare from plotting.
badSpotsJumps = [0; abs(diff(xs)) > JUMP_MAX | abs(diff(ys)) > JUMP_MAX ];

anyBadSpots = badSpotsNans | badSpotsOffTrack | badSpotsJumps;
xsClean = xs(~anyBadSpots);
ysClean = ys(~anyBadSpots);

mapToUse = parula;
hd = BehRec.Behavior.hdAlignedNorth_Rad{GOOD_TRACKING_REC}(startEpochSample:endEpochSample,:);
hdMap = mapVar2D(hd(~anyBadSpots), xsClean, ysClean, [XMAX,YMAX], true);
filtNormHDMap = filter2DMatrices(hdMap, false, 45, 15);

mapToUse = cmocean('phase');
figure(2) 
sc(filtNormHDMap,[-pi,pi],mapToUse,'w',isnan(filtNormHDMap));

%% 1C - 2D Velocity Map
mapToUse = parula;
vel = BehRec.Behavior.velSmoothed_CmPerSec{GOOD_TRACKING_REC}(startEpochSample:endEpochSample,:);
velMap = mapVar2D(vel(~anyBadSpots), xsClean, ysClean, [XMAX,YMAX]);
filtNormVelMap = filter2DMatrices(velMap, false);
% max of colorbar which = mean(velocity)+2*std(velocity)
% velMax = max(max(filtNormVelMap)); unused
velMean = nanmean(nanmean(filtNormVelMap));
velSTD = nanstd(filtNormVelMap(:));
velCAxis = velMean + 2*velSTD;

disp("(caxis)Mean Velocity + 2SD = " + velCAxis)

figure(3) 
sc(filtNormVelMap,[0,velCAxis],mapToUse,'w',isnan(filtNormVelMap));
disp(velCAxis);

%% 1E Proportion of Runs Rewarded by recording - histogram 

%Calculate proportion of runs that were rewarded
for iRec = 1:nRecs
    NumberOfRuns(iRec) = sum(Performance.nRunsInBlock{iRec});
    NumberOfErrors(iRec) = sum(Performance.nErrsInBlock{iRec});
    NumberOfRewards(iRec) = NumberOfRuns(iRec) - NumberOfErrors(iRec); 
    ProportionRunsRewarded(iRec) = NumberOfRewards(iRec)/NumberOfRuns(iRec);
end

figure(11) 
histogram(ProportionRunsRewarded,'binwidth',0.05, 'Normalization', 'probability')
ylabel("% recs")
xlabel("Proportion of rewarded runs")
xlim([0 1])

figure(12) 
boxplot(ProportionRunsRewarded)
ylabel("Proportion of rewarded runs")
ylim([0 1])

fprintf('percent mean runs rewarded = %0.4f\n', mean(ProportionRunsRewarded));

nRunsSim = cellfun(@sum, SimPerformance.nRunsInBlock);
nErrsSim = cellfun(@sum, SimPerformance.nErrsInBlock);
nRewardsSim = nRunsSim - nErrsSim;
propRunsRewardedSim = nRewardsSim./nRunsSim;
fprintf('percent mean runs rewarded by chance = %0.4f\n', mean(propRunsRewardedSim));
% chance1E = mean([1, 0.75, 0.5, 0.25])
[p, h, stats] = ranksum(ProportionRunsRewarded,propRunsRewardedSim, 'tail','right');        
fprintf('ranksum perf dist < chance = %0.4f\n', p);

%% 1F Proportion of Runs followed by short return by recording - histogram 
figure(13) 
histogram(Performance.probCloseReturn,'binwidth',0.05, 'Normalization', 'probability')
ylabel("% recs")
xlabel("Proportion of runs followed by short return")
xlim([0 1])

figure(14) 
boxplot(Performance.probCloseReturn)
ylabel("Proportion of runs followed by short return")
ylim([0 1])

p = signtest(Performance.probCloseReturn, 0.5, 'tail','right');
mean(Performance.probCloseReturn);
std(Performance.probCloseReturn);
chance1F = 0.5;
fprintf('signtest: short return: %0.4f < chance: %0.4f\n', mean(Performance.probCloseReturn), p);
 
%% 1G Proportion of blocks run without error - histogram 
figure(15) 
histogram(Performance.perfectBlockPercent./100,'binwidth',0.05, 'Normalization', 'probability')
ylabel("% recs")
xlabel("Proportion of blocks run without error")
xlim([0 1])

figure(16) 
boxplot(Performance.perfectBlockPercent./100)
ylabel("Proportion of blocks run without error")
ylim([0 1])

fprintf('percent perfect blocks by chance = %0.4f\n', mean(SimPerformance.perfectBlockPercent));
[p, h, stats] = ranksum(Performance.perfectBlockPercent,SimPerformance.perfectBlockPercent, 'tail','right');        
% chance1G = (1 * 0.75 * 0.5 * 0.25)
fprintf('ranksum: perfect blocks: %0.4f < chance: %0.4f\n', mean(Performance.perfectBlockPercent), p);

%% 1H Proportion of (runs 1-4 in) blocks stereotyped to most run pattern  - histogram 
nBlocks = cellfun(@numel, Performance.RunBlocks);
nPerfectBlocks = round(Performance.perfectBlockPercent/100 .* nBlocks);

countMaxPattern = cellfun(@max, Performance.nEachPattern, 'UniformOutput', false);
countMaxPatternIsEmpty = cellfun(@isempty, countMaxPattern);
countMaxPattern{countMaxPatternIsEmpty} = 0;
countMaxPattern = cell2mat(countMaxPattern);
commonBlockPercentPerfect = countMaxPattern./nPerfectBlocks;
commonBlockPercentAll = countMaxPattern./nBlocks;

figure(17) 
histogram(commonBlockPercentPerfect,'binwidth',0.05, 'Normalization', 'probability')
ylabel("% recs")
xlabel("Proportion of blocks stereotyped to most run pattern")
xlim([0 1])

figure(18)
boxplot(commonBlockPercentPerfect)
ylim([0 1])
ylabel("Probability of blocks stereotyped to most run pattern")

% nanmean(commonBlockPercent)
% nanstd(commonBlockPercent)

nBlocksSim = cellfun(@numel, SimPerformance.RunBlocks);
nPerfectBlocksSim = round(SimPerformance.perfectBlockPercent/100 .* nBlocksSim);

countMaxPatternSim = cellfun(@max, SimPerformance.nEachPattern, 'UniformOutput', false);
countMaxPatternSim = cell2mat(countMaxPatternSim);
commonBlockPercentSim = countMaxPatternSim./nPerfectBlocksSim;

fprintf('percent most common pattern blocks = %0.4f +- %0.4f \n', nanmean(commonBlockPercentPerfect), nanstd(commonBlockPercentPerfect));
fprintf('percent most common pattern blocks chance = %0.4f +- %0.4f \n', mean(commonBlockPercentSim), std(commonBlockPercentSim));
[p, h, stats] = ranksum(commonBlockPercentPerfect,commonBlockPercentSim, 'tail','right');   
fprintf('N = 94 cuz one rec no perfect blocks');
fprintf('ranksum: most common pattern: %0.4f < chance: %0.4f\n', nanmean(commonBlockPercentSim), p);

%% 1I Proportion of Runs alternating L/R at first turn - histogram 
figure(19) 
histogram(Performance.prob1stTurnAlt,'binwidth',0.05, 'Normalization', 'probability')
ylabel("% recs")
xlabel("Proportion of runs where alternation occured @ turn1")
xlim([0 1])

figure(20) 
boxplot(Performance.prob1stTurnAlt)
ylabel("Proportion of runs where alternation occured @ turn1")
ylim([0 1])

p = signtest(Performance.prob1stTurnAlt, 0.5, 'tail','right');       
% chance1I = 0.5;
fprintf('ranksum: alteration at turn 1: %0.4f < chance: %0.4f\n', mean(Performance.prob1stTurnAlt), p);

%% 1J Proportion of Runs alternating L/R at 3rd turn, grouped - histogram 
figure(21) 
histogram(Performance.prob3rdTurnAltGrouped,'binwidth',0.05, 'Normalization', 'probability')
ylabel("% recs")
xlabel("Proportion of runs where alternation occured @ turn3 - grouped")
xlim([0 1])

figure(22) 
boxplot(Performance.prob3rdTurnAltGrouped)
ylabel("Proportion of runs where alternation occured @ turn3 - grouped")
ylim([0 1])

p = signtest(Performance.prob3rdTurnAltGrouped, 0.5, 'tail','right');       
% chance1I = 0.5;
fprintf('ranksum: alteration at turn 3 - grouped: %0.4f < chance: %0.4f\n', mean(Performance.prob3rdTurnAltGrouped), p);

%% 1K Proportion of Runs alternating L/R at 3rd turn, separate - histogram 
figure(23) 
histogram(Performance.prob3rdTurnAltSep,'binwidth',0.05, 'Normalization', 'probability')
ylabel("% recs")
xlabel("Proportion of runs where alternation occured @ turn3 - separate turns")
xlim([0 1])

figure(24) 
boxplot(Performance.prob3rdTurnAltSep)
ylabel("Proportion of runs where alternation occured @ turn3 - separate turns")
ylim([0 1])

p = signtest(Performance.prob3rdTurnAltSep, 0.5, 'tail','right');       
% chance1I = 0.5;
fprintf('ranksum: alteration at turn 3 - separate: %0.4f < chance: %0.4f\n', mean(Performance.prob3rdTurnAltSep), p);
