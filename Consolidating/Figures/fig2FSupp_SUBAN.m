function stat = fig2FSupp_SUBAN(SubNeuron, HpcNeuron, SubCorr, HpcCorr)
%
% Run Fig 2 analyses, first, then this.
%

%% Path Corrs animal x animal
rois = [SubNeuron, HpcNeuron];
roiCorrs = {SubCorr, HpcCorr};
roiNames = ["Sub", "CA1"];

for iROI = 1:2
    Roi = rois(iROI);
    roiCorr = roiCorrs{iROI};
    rats = unique(Roi.rat);
    disp(rats(end:-1:1));
    colors = distinguishable_colors(length(rats),[1,1,1]);
    
    for iRat = numel(rats):-1:1 %backwards so I don't have to init vars.
        thisRat = rats{iRat};
        thisRatNeus = strcmp(Roi.rat, thisRat)';
        
        thisCorr = roiCorr(thisRatNeus,:);
        fprintf("Now analyzing: %s's %s data \n", thisRat, roiNames(iROI));

        numCorr = size(thisCorr,1); % nNeurons
        neuronMaxCorr = nanmax(thisCorr,[],2);
        neuronMaxNoCorr = sum(isnan(neuronMaxCorr))/numCorr;
        DistMat{iROI,iRat} = neuronMaxCorr;
        DistMatNoCorr{iROI,iRat} = neuronMaxNoCorr;
        
        fprintf('%s Mean Corr: %0.4f +- %0.4f\n\n', thisRat, nanmean(neuronMaxCorr), nanstd(neuronMaxCorr));
        fprintf('# neurons in stat test: %i\n', sum(~isnan(neuronMaxCorr)));
        fprintf('%% Neurons > 50: %0.4f \n', sum(neuronMaxCorr > 0.5)/numCorr);
        
        figure()
        histogram(neuronMaxCorr,'BinWidth',0.05,'Normalization','probability') %probability includes count of nan values
        xlim([-1 1])
        ylim([0 0.25])
        xlabel("max r-value")
        ylabel("% neurons")
        title(thisRat)
    end
end

SUBRats = unique(rois(1).rat);
CA1Rats = unique(rois(2).rat);
for iRat = numel(CA1Rats):-1:1
    disp(['CA1 ' num2str(iRat) 'is ' CA1Rats{iRat}]);
    for jRat = numel(SUBRats):-1:1
        disp(['SUB ' num2str(jRat) 'is ' SUBRats{jRat}]);
        %KS test - are the two distributions close in space
        [stat(iRat,jRat).KS_MaxCorr, stat(iRat,jRat).KS_MaxCorr_p, stat(iRat,jRat).KS_MaxCorr_stat] =...
            kstest2(DistMat{2,iRat},DistMat{1,jRat},'tail','larger');
        
        %Wilcoxon Rank Sum
        [stat(iRat,jRat).WRS_MaxCorr_p, stat(iRat,jRat).WRS_MaxCorr, stat(iRat,jRat).WRS_MaxCorr_stat] =...
            ranksum(DistMat{2,iRat},DistMat{1,jRat},'tail','left');

%         figure('Name',['Max Correlation Values. KS p=', num2str(stat(iRat,jRat).KS_MaxCorr_p),...
%             ' WRS p=', num2str(stat(iRat,jRat).WRS_MaxCorr_p)])
%         subplot(1,2,1)
%         title("CA1")
%         histogram(DistMat{2,iRat},'BinWidth',0.05,'Normalization','probability') %probability includes count of nan values
%         xlim([-1 1])
%         ylim([0 0.25])
%         xlabel([CA1Rats{iRat}, ' CA1 max r-value'])
%         ylabel("% neurons")
%         subplot(1,2,2)
%         title("Sub")
%         histogram(DistMat{1,jRat},'BinWidth',0.05,'Normalization','probability') %probability includes count of nan values
%         xlim([-1 1])
%         ylim([0 0.25])
%         xlabel([SUBRats{jRat}, ' SUB max r-value'])
%         ylabel("% neurons")
%         
%         figure('Name',['Nan Max-Correlation Count'])
%         bar([DistMatNoCorr{2,iRat},DistMatNoCorr{1,jRat}]) %probability includes count of nan values
%         ylim([0 0.5])
%         xticklabels({['CA1 ' CA1Rats{iRat}],['SUB ' SUBRats{jRat}]})
%         ylabel("% neurons")
    end
end






