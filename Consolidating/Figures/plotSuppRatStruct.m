function plotSuppRatStruct(BehRec, Performance, RatNum) 

%Simple plot function to marry plotting function and rat isolator function 

    RatStruct = makeSuppRatStruct(BehRec, Performance, RatNum) 
    SUPPFIG_RATxRAT_SUBAN(RatStruct.BehRec, RatStruct.Performance)
    
end
