function  [stat] = figure2pt2_SUBAN(SubNeuron, HpcNeuron, SubRec, HpcRec, SubCorr, HpcCorr, caxisMax)
% Old Figure 3 incorporated into end of Figure 2
%FIX: ISO DISTANCE IN PIPELINE - LOAD FROM NEURON STRUCT
%2d route-by-route coloraxis E

%% Set cell example number and define positions to do correlations on
CA1_Example = 273; %JL01 Recording 31 Cell2
SUB_Example = 210; %NS15 Recording 23 Cell6
%
% CLOSE_NO_OVERLAP = [119:1:140];
% FAR_NO_OVERLAP = [51:1:140];

%Define where in frcat to draw correlation values from
EXTERNAL_CORR = [561:757;...
    758:954]';

PATH_LIST = [1,2,3,4,9,10];

%% 2DE TwoD Ratemap CA1 neuron E & Linear Ratemapper & Correlation Value

% Get colormax from full 2d map.
%Initialize workspace
neuronStruct = HpcNeuron;
recStruct = HpcRec;
mapToUse = parula;
iNeu = CA1_Example;
recIndex = neuronStruct.recStructIndex(iNeu);

%Calculate colormax for total 2dmap to apply to route x route (consistancy)
CA1_PreFig2E = twoDRMapPlotter(neuronStruct, recStruct, iNeu, 'sampleNSpikes', false, mapToUse,[],[],[],caxisMax);
CA1_Fig2ECMax = colorAxisCalculator(CA1_PreFig2E, caxisMax);

[twoDRMaps] = twoDPathsRateMapper(recStruct,neuronStruct,1,1,1,iNeu);
twoDRMapsFiltered = filter2DMatrices(twoDRMaps,0);


for iPath = PATH_LIST
    figure('Name',['Neuron ',num2str(iNeu),'. ','Color Axis Max = ',num2str(CA1_Fig2ECMax),'. Path #',num2str(iPath)]);
    sc(twoDRMapsFiltered(:,:,iPath),[0,CA1_Fig2ECMax],mapToUse,'w',isnan(twoDRMapsFiltered(:,:,iPath)));
end
% Make Linear Ratemap and correlation
frcat = cell2mat(neuronStruct.MeanLinearRates(iNeu,:)');
frPathMax = max(frcat(:,1));
frReturn = frcat(EXTERNAL_CORR);
preReturnCorr = (corrcoef(frReturn));
returnCorr = preReturnCorr(1,2);

figure('Name',['CA1 Example Neuron; return route correlation = ',num2str(returnCorr)])
for iPath = 1:length(PATH_LIST)
    subplot(6,1,iPath)
    bar(neuronStruct.MeanLinearRates{iNeu,PATH_LIST(iPath)}(:,1))
    ylim([0 frPathMax]);
    xlim([0 recStruct.Behavior.pathLengths{recIndex}(iPath)]);
end

%% 2DE TwoD Ratemap SUB neuron E & Linear Ratemapper & Correlation Value

% Get colormax from full 2d map.
%Initialize workspace
neuronStruct = SubNeuron;
recStruct = SubRec;
mapToUse = parula;
iNeu = SUB_Example;
recIndex = neuronStruct.recStructIndex(iNeu);

%Calculate colormax for total 2dmap to apply to route x route (consistancy)
SUB_PreFig2E = twoDRMapPlotter(neuronStruct, recStruct, iNeu, 'sampleNSpikes', false, mapToUse,[],[],[],caxisMax);
SUB_Fig2ECMax = colorAxisCalculator(SUB_PreFig2E, caxisMax);

[twoDRMaps] = twoDPathsRateMapper(recStruct,neuronStruct,1,1,1,iNeu);
twoDRMapsFiltered = filter2DMatrices(twoDRMaps,0);


for iPath = PATH_LIST
    figure('Name',['Neuron ',num2str(iNeu),'. ','Color Axis Max = ',num2str(SUB_Fig2ECMax),'. Path #',num2str(iPath)]);
    sc(twoDRMapsFiltered(:,:,iPath),[0,SUB_Fig2ECMax],mapToUse,'w',isnan(twoDRMapsFiltered(:,:,iPath)));
end
% Make Linear Ratemap and correlation
frcat = cell2mat(neuronStruct.MeanLinearRates(iNeu,:)');
frPathMax = max(frcat(:,1));
frReturn = frcat(EXTERNAL_CORR);
preReturnCorr = (corrcoef(frReturn));
returnCorr = preReturnCorr(1,2);

figure('Name',['SUB Example Neuron; return route correlation = ',num2str(returnCorr)])
for iPath = 1:length(PATH_LIST)
    subplot(6,1,iPath)
    bar(neuronStruct.MeanLinearRates{iNeu,PATH_LIST(iPath)}(:,1))
    ylim([0 frPathMax]);
    xlim([0 recStruct.Behavior.pathLengths{recIndex}(iPath)]);
end

%% 2F Correlation summary by neurons -
stat = figure2f_SUBAN(SubCorr, HpcCorr);

%% 2F rat x rat
stat.RatByRat = fig2FSupp_SUBAN(SubNeuron, HpcNeuron, SubCorr, HpcCorr);

%% 2F for NS23 - only animal with dual recordings.
SubNS23neuInd = strcmp(SubNeuron.rat,'NS23');
HpcNS23neuInd = strcmp(HpcNeuron.rat,'NS23');
stat.NS23 = figure2f_SUBAN(SubCorr(SubNS23neuInd,:),...
    HpcCorr(HpcNS23neuInd,:));

%% Supplementary Figure Maybe - CDF of above
% a=cumsum(hist(Hpc_NeuronMaxCorr,[-1:0.05:1])/(size(Hpc_NeuronMaxCorr,1)));
% b=cumsum(hist(Hpc_NeuronMeanCorr,[-1:0.05:1])/(size(Hpc_NeuronMeanCorr,1)));
% c=cumsum(hist(Hpc_NeuronMinCorr,[-1:0.05:1])/(size(Hpc_NeuronMinCorr,1)));
% figure('Name',['Cumulative Distribution of CA1 correlations'])
% plot([-1:0.05:1],a)
% hold on
% plot([-1:0.05:1],b)
% plot([-1:0.05:1],c)
% ylim([0 1])
% legend({"Max","Mean","Min"},'location','northwest')
% 
% 
% a=cumsum(hist(Sub_NeuronMaxCorr,[-1:0.05:1])/(size(Sub_NeuronMaxCorr,1)));
% b=cumsum(hist(Sub_NeuronMeanCorr,[-1:0.05:1])/(size(Sub_NeuronMeanCorr,1)));
% c=cumsum(hist(Sub_NeuronMinCorr,[-1:0.05:1])/(size(Sub_NeuronMinCorr,1)));
% figure('Name',['Cumulative Distribution of SUB correlations'])
% plot([-1:0.05:1],a)
% hold on
% plot([-1:0.05:1],b)
% plot([-1:0.05:1],c)
% ylim([0 1])
% legend({"Max","Mean","Min"},'location','northwest')

end
