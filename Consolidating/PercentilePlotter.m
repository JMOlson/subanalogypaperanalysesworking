

for i = 1:size(SubCorr.corrResults_all_nonoverlap,1)
    AAAA_SUB(i) = max(SubCorr.corrResults_all_nonoverlap(i,:))
end
for i = 1:size(HpcCorr.corrResults_all_nonoverlap,1)
    AAAA_HPC(i) = max(HpcCorr.corrResults_all_nonoverlap(i,:))
end

pctl = [5,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,95,100]
for j = 1:length(pctl)

    [~, AAAA_SUBindex(j)] = (min(abs(AAAA_SUB - prctile(AAAA_SUB,pctl(j)))))
    [~, AAAA_HPCindex(j)] = (min(abs(AAAA_HPC - prctile(AAAA_HPC,pctl(j)))))
end

caxisMax = 'Mean2SD'

for iNeu = AAAA_SUBindex
    twoDRMapPlotter(SubNeuron, SubRec, iNeu, 'sampleNSpikes', true, parula,[],[],[],caxisMax);
end
for iNeu = AAAA_HPCindex
    twoDRMapPlotter(HpcNeuron, HpcRec, iNeu, 'sampleNSpikes', true, parula,[],[],[],caxisMax);
end

