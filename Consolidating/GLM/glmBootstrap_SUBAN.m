function [Results] = glmBootstrap_SUBAN(glmReadyData, predictorList, responseVar, responseDist, ...
    crossvalidationType, stat_function, foldCount, nFullDataShuffles, constantFlag)
%GLMBOOTSTRAP Wrapper function to run GLM and bootstrap stats for SUB/CA1 Comparison paper
%
% CREATED WITH MATLAB VERSION: 9.6.0.1150989 (R2019a) Update 4 on Microsoft Windows 10 Enterprise Version 10.0 (Build 17134)
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: https://www.jmolson.com
% CREATED ON: -2-AUG-2020
% LAST MODIFIED BY:
% LAST MODIFIED ON:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Seed random number generator
rng('shuffle');

%% Input Handling
if ~exist('nFullDataShuffles','var')
    doBootStrap = false;
elseif nFullDataShuffles==0 || isempty(nFullDataShuffles)
    doBootStrap = false;
else
    doBootStrap = true;
end
if ~exist('foldCount','var')
    foldCount = 10;
end
if ~exist('constantFlag','var')
    constantFlag = true;
end

%% Crossvalidation Splitting
nDatapoints = size(glmReadyData,1);
switch crossvalidationType
    case 'loocv'
        maxIterations = nDatapoints;
        trainTestLabels = [];
    case 'kfold'
        trainTestLabels = cvpartition(nDatapoints,'kfold',foldCount);
        maxIterations = foldCount;
    otherwise
end

%% Train & test actual models - CV handled here
[ModelStats, glmArray] = trainAndTestModel(glmReadyData,...
    predictorList, responseVar, responseDist,...
    crossvalidationType, stat_function, maxIterations, constantFlag, trainTestLabels);

%% Shuffle original data, repeat above - for statistical comparisons
if doBootStrap
    % parfor loop - remove par to debug
    parfor iShuffle = 1:nFullDataShuffles
        % for iShuffle = 1:nFullDataShuffles
        shuffledGlmReadyTurnData = glmReadyData;
        shuffledRowOrder = randperm(nDatapoints);
        
        %Shuffle output values relative to predictors.
        shuffledGlmReadyTurnData.(responseVar) = shuffledGlmReadyTurnData.(responseVar)(shuffledRowOrder);
        
        ModelStats_Shuffled(iShuffle) = trainAndTestModel(shuffledGlmReadyTurnData,...
            predictorList, responseVar, responseDist,...
            crossvalidationType, stat_function, maxIterations, constantFlag, trainTestLabels);
    end
else
    ModelStats_Shuffled = 'No Bootstrap Analysis Run';
end

%% Results Output
Results.ModelStats = ModelStats;
Results.ModelStats_Shuffled = ModelStats_Shuffled;
Results.glmArray = glmArray;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Actual GLM Call in this Helper Funtion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ModelStats, glmArray] = trainAndTestModel(dataset, predictorList, responseVar, responseDist,...
    crossvalidationType, stat_function, maxIterations, constantFlag, trainTestLabels)

glmArray = cell(maxIterations,1);
for k = 1:maxIterations
    switch crossvalidationType
        case 'loocv'
            isTrain = 1:maxIterations;
            isTrain(k) = [];
            isTest = k;
        case 'kfold'
            isTrain = trainTestLabels.training(k);
            isTest = trainTestLabels.test(k);
        otherwise
    end
%     % train full model
%     glm = fitglm(dataset(isTrain,:), 'PredictorVars', predictorList, 'Distribution', responseDist,...
%         'ResponseVar', responseVar, 'Intercept', constantFlag);
%     %predict test data
%     yHat = predict(glm, dataset(isTest,:));

    % train full model - lightweight no variables version
    if constantFlag 
        constantFlag = 'on';
    end
    
    glm = glmfit(dataset{isTrain,predictorList}, dataset{isTrain,responseVar}, 'normal', 'constant', constantFlag);
    %predict test data
    yHat = glmval(glm,dataset{isTest,predictorList},'identity', 'constant', constantFlag);

    %% Calculate statistics for the model
    tmpStruct(k) = stat_function(dataset{isTest,responseVar}, yHat);
    glmArray{k} = glm;
end
% Save data from tmpStruct into an output struct
structFieldNames = fieldnames(tmpStruct);
for iField = 1:length(structFieldNames)
    contentsSize = size(tmpStruct(1).(structFieldNames{iField}));
    ModelStats.(structFieldNames{iField}) = ...
        reshape([tmpStruct(:).(structFieldNames{iField})],[maxIterations,contentsSize]);
end
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
