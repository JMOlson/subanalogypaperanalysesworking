function ModelStats = statsCalcForGLM(y, yHat)
%% all statistics calculations for error of the models.

ModelStats.mae = mean(abs(y - yHat));
ModelStats.mse = mean((y - yHat).^2);
ModelStats.stddev = std(y);
ModelStats.r2_coeffOfDet = 1-(ModelStats.mse/ModelStats.stddev);

end