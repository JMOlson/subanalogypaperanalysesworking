%% Figure 5 Full Shell

%% Make necessary predictor matrices and tables

% https://en.wikipedia.org/wiki/Variance-stabilizing_transformation
% x^2 approximates turning gaussian to poisson
f_x = @(x) x;
% f_x = @(x) x.^2;

Predictors_Hpc_All = makePredictors(HpcRec, [1 2 3 4], [9 10], f_x);
Predictors_Sub_All = makePredictors(SubRec, [1 2 3 4], [9 10], f_x);

%% choose the correlation matrix to use.
chosenCorrMatForAnalysis = 'oddEvenCorr';
%chosenCorrMatForAnalysis = 'oddEvenCorr_maxNormed';

%% prep the data for the glm
% grab the predictors in table format.
PredictorTable_Sub = Predictors_Sub_All.linearizedTable;
PredictorTable_Hpc = Predictors_Hpc_All.linearizedTable;

% linear the correlation matrix as we did for predictors and append as first column
corrLinear = HpcPopCorr_All.(chosenCorrMatForAnalysis)(:);
GLMDataTable_Hpc = addvars(PredictorTable_Hpc,corrLinear, 'Before', 1);

corrLinear = SubPopCorr_All.(chosenCorrMatForAnalysis)(:);
GLMDataTable_Sub = addvars(PredictorTable_Sub,corrLinear, 'Before', 1);

% naive constant only models - just the mean
% then mse = variance
% var(GLMDataTable_Hpc.corrLinear,1)
% 0.0291
% var(GLMDataTable_Sub.corrLinear,1)
% 0.0297

% display(PredictorTable.Properties.VariableNames)
% {'similarityPropThru'}
% {'similarityAngVel'  }
% {'similarityHD'      }
% {'similarityAxis'    }
% {'similarityLinVel'  }
% {'similarityAccel'   }
% {'similarityXYPos'   }

%% Intuition Building

% naive constant only models - just the mean
% then mse = variance
% var(GLMDataTable_Hpc.corrLinear,1)
% 0.0291
% var(GLMDataTable_Sub.corrLinear,1)
% 0.0297

figure;
subplot(3,4,2)
histogram(GLMDataTable_Sub.corrLinear,[0:1/20:1],'Normalization','probability');
title('hpc');
subplot(3,4,3)
histogram(GLMDataTable_Hpc.corrLinear,[0:1/20:1],'Normalization','probability');
title('sub');
for iVar = 2:9
subplot(3,4,iVar+3)
histogram(GLMDataTable_Hpc.(iVar),[0:1/20:1],'Normalization','probability');
title(GLMDataTable_Hpc.Properties.VariableNames(iVar));
end
% sgtitle('probability histograms of predictors and region corr matrices - all predictors been squared');


%%
predictorListFull1 = {...
    'similarityPropThru',...
    'similarityAngVel',...
    'similarityHD',...
    'similarityAxis',...
    'similarityLinVel',...
    'similarityAccel',...
    'similarityXYPos'};

% task phase predictor replacing propThrough
predictorListFull2 = {...
    'similarityTaskPhase',...
    'similarityAngVel',...
    'similarityHD',...
    'similarityAxis',...
    'similarityLinVel',...
    'similarityAccel',...
    'similarityXYPos'};

predictorListAll = {...
    'similarityPropThru',...
    'similarityTaskPhase',...
    'similarityAngVel',...
    'similarityHD',...
    'similarityAxis',...
    'similarityLinVel',...
    'similarityAccel',...
    'similarityXYPos'};

predictorListFull = predictorListAll;

%% full models
FOLD_COUNT = 10;
N_DATA_SHUFFLES = 0;
% constantFlag = false;
fullGLMResults1_Sub = glmBootstrap_SUBAN(GLMDataTable_Sub, predictorListFull1, 'corrLinear', 'normal',...
    'kfold', @statsCalcForGLM, FOLD_COUNT, N_DATA_SHUFFLES); % used ~32GB ram and 16 CPU: 10 minutes w/ parfor
fullGLMResults1_Hpc = glmBootstrap_SUBAN(GLMDataTable_Hpc, predictorListFull1, 'corrLinear', 'normal',...
    'kfold', @statsCalcForGLM, FOLD_COUNT, N_DATA_SHUFFLES);

%% full models
FOLD_COUNT = 10;
N_DATA_SHUFFLES = 0;
% constantFlag = false;
fullGLMResults2_Sub = glmBootstrap_SUBAN(GLMDataTable_Sub, predictorListFull2, 'corrLinear', 'normal',...
    'kfold', @statsCalcForGLM, FOLD_COUNT, N_DATA_SHUFFLES); % used ~32GB ram and 16 CPU: 10 minutes w/ parfor
fullGLMResults2_Hpc = glmBootstrap_SUBAN(GLMDataTable_Hpc, predictorListFull2, 'corrLinear', 'normal',...
    'kfold', @statsCalcForGLM, FOLD_COUNT, N_DATA_SHUFFLES);

%% one parameter at a time models
FOLD_COUNT = 10;
N_DATA_SHUFFLES = 0;
% constantFlag = false;
for iPredictor = 1:length(predictorListAll)
    onePredictorGLMResults_Sub(iPredictor) = glmBootstrap_SUBAN(GLMDataTable_Sub,...
        predictorListAll(iPredictor), 'corrLinear', 'normal',...
        'kfold', @statsCalcForGLM, FOLD_COUNT, N_DATA_SHUFFLES);
    onePredictorGLMResults_Hpc(iPredictor) = glmBootstrap_SUBAN(GLMDataTable_Hpc,...
        predictorListAll(iPredictor), 'corrLinear', 'normal',...
        'kfold', @statsCalcForGLM, FOLD_COUNT, N_DATA_SHUFFLES);
end
for iPredictor = 1:length(predictorListFull)
    onePredictorGLMResults_Sub(iPredictor).predictor = predictorListFull(iPredictor);
    onePredictorGLMResults_Hpc(iPredictor).predictor = predictorListFull(iPredictor);
end
% onePredictorGLMResults_Sub = glmStrip(onePredictorGLMResults_Sub);
% onePredictorGLMResults_Hpc = glmStrip(onePredictorGLMResults_Hpc);

%% leave one out - loco models
FOLD_COUNT = 10;
N_DATA_SHUFFLES = 0;
for iPredictor = 1:length(predictorListFull)
    thisPredictorList = predictorListAll;
    thisPredictorList(iPredictor) = [];
    locoGLMResults_Sub(iPredictor) = glmBootstrap_SUBAN(GLMDataTable_Sub,...
        thisPredictorList, 'corrLinear', 'normal',...
        'kfold', @statsCalcForGLM, FOLD_COUNT, N_DATA_SHUFFLES);
    locoGLMResults_Hpc(iPredictor) = glmBootstrap_SUBAN(GLMDataTable_Hpc,...
        thisPredictorList, 'corrLinear', 'normal',...
        'kfold', @statsCalcForGLM, FOLD_COUNT, N_DATA_SHUFFLES);
end
for iPredictor = 1:length(predictorListFull)
    locoGLMResults_Sub(iPredictor).predictorLeftOut = predictorListFull(iPredictor);
    locoGLMResults_Hpc(iPredictor).predictorLeftOut = predictorListFull(iPredictor);
end
% locoGLMResults_Sub = glmStrip(locoGLMResults_Sub);
% locoGLMResults_Hpc = glmStrip(locoGLMResults_Hpc);













