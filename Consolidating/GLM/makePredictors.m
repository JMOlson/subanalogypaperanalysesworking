function [Predictors] = makePredictors(RecStruct, outboundPaths, returnPaths, f_x)
%makePredictors One line description of what the function or script performs (H1 line)
%		Optional file header info (to give more details about the function than in the H1 line)  
%
% 	[Predictors] = makePredictors(RecStruct, paths)
%
% INPUTS: 
%		RecStruct - Description
%		paths - Description
%		input3 - Description
%		                      
%
% OUTPUTS: 
%		Predictors - Description
%		output2 - Description
%		output3 - Description
%		                       
%
% EXAMPLES: 
%		Line 1 of example
%		Line 2 of example
%		Line 3 of example
%		                   
%
% REMARKS
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:
%
%
% CREATED BY: Jacob M Olson based on code by Xuefei Wang & Alex Johnson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 11-May-2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Input Handling
if ~exist('f_x','var')
    f_x = @(x) x; % identity transform - nothing.
end

paths = [outboundPaths, returnPaths];
%% Proportion Through

% pathLengths are same for all neurons/recs, as all FRs are in same shape
% because of this, just using first rec.
iRec = 1;
[~,pathListInds] = ismember(paths,RecStruct.Behavior.pathList{iRec});
if numel(pathListInds) ~= numel(paths)
    error('this rec doesn''t have all paths');
end

pathLengths = RecStruct.Behavior.pathLengths{iRec}(pathListInds);
propThru = cell(numel(paths),1);
for iPath = 1:numel(pathLengths)
    propThru{iPath} = (0:(1/(pathLengths(iPath)-1)):1);
end
propThruCatPaths = cell2mat(propThru');

% neat little trick to creat pairwise distance matrix of vector
Predictors.distancePropThru = squareform(pdist(propThruCatPaths'));

%similarity conversion done below so the order makes more sense in the struct.

%% Task Phase
% taskphase is same for all neurons/recs, as all FRs are in same shape
% because of this, just using first rec.
iRec = 1;
[~,pathListInds] = ismember(paths,RecStruct.Behavior.pathList{iRec});
if numel(pathListInds) ~= numel(paths)
    error('this rec doesn''t have all paths');
end
[~,outboundPathsInds] = ismember(outboundPaths,RecStruct.Behavior.pathList{iRec});
[~,returnPathsInds] = ismember(returnPaths,RecStruct.Behavior.pathList{iRec});

pathLengths = RecStruct.Behavior.pathLengths{iRec}(pathListInds);
outboundPathLength = pathLengths(outboundPathsInds(1));
returnPathLength = pathLengths(returnPathsInds(1));

taskCycleLength = outboundPathLength + returnPathLength;

taskPhase = cell(numel(paths),1);
for iPath = 1:numel(pathLengths)
    if ismember(iPath,outboundPathsInds)
        taskPhase{iPath} = (0:outboundPathLength-1)./taskCycleLength;
    else
        taskPhase{iPath} = (outboundPathLength:outboundPathLength+returnPathLength-1)./taskCycleLength;
    end
end
taskPhaseCatPaths = cell2mat(taskPhase');

% neat little trick to creat pairwise distance matrix of vector
Predictors.distanceTaskPhase = squareform(pdist(taskPhaseCatPaths'));

%similarity conversion done below so the order makes more sense in the struct.

%% Angular Velocity, HD, Axis, LinVel, Accel, XYPos

% separate xs and ys
xyInterleaved = cell2mat(RecStruct.Behavior.posXYMeanPathAligned_Pixels(:,paths));
posXMeanPathAligned_Pixels = xyInterleaved(1:2:end,:);
posYMeanPathAligned_Pixels = xyInterleaved(2:2:end,:);

% average across recordings
allRecPathAlignedMeanAngVel_RadPerSec = mean(cell2mat(...
    RecStruct.Behavior.angVelMeanPathAligned_RadPerSec(:,paths)),1);
allRecPathAlignedMeanHD_Rad = circ_mean(cell2mat(...
    RecStruct.Behavior.hdMeanPathAligned_Rad(:,paths)),[],1);
allRecPathAlignedMeanAxis_Rad = (circ_mean((...
    2.*cell2mat(RecStruct.Behavior.axisMeanPathAligned_Rad(:,paths))-pi),[],1)+pi)./2;
allRecPathAlignedMeanLinVel_CmPerSec = mean(cell2mat(...
    RecStruct.Behavior.speedMeanPathAligned_CmPerSec(:,paths)),1);
allRecPathAlignedMeanAcc_CmPerSecSQ = mean(cell2mat(...
    RecStruct.Behavior.accMeanPathAligned_CmPerSecSQ(:,paths)),1);

allRecPathAglignedMeanXPos_Pixels = mean(posXMeanPathAligned_Pixels,1);
allRecPathAglignedMeanYPos_Pixels = mean(posYMeanPathAligned_Pixels,1);

% put axis back on 2pi scale for dist calc
axisForDistCalc = 2.*allRecPathAlignedMeanAxis_Rad-pi;


Predictors.distanceAngVel = abs(squareform(pdist(allRecPathAlignedMeanAngVel_RadPerSec')));
Predictors.distanceHD = abs(circ_dist2(allRecPathAlignedMeanHD_Rad,allRecPathAlignedMeanHD_Rad));
Predictors.distanceAxis = abs(circ_dist2(axisForDistCalc,axisForDistCalc));
Predictors.distanceLinVel = abs(squareform(pdist(allRecPathAlignedMeanLinVel_CmPerSec')));
Predictors.distanceAccel = abs(squareform(pdist(allRecPathAlignedMeanAcc_CmPerSecSQ')));
Predictors.distanceXYPos = abs(squareform(pdist([allRecPathAglignedMeanXPos_Pixels',allRecPathAglignedMeanYPos_Pixels'])));


% Since we want similarity, we want to invert, then we are max norming
% Then applying the chosen function to transform the variables.
Predictors.similarityPropThru_maxNormed = f_x(rescale(-1.*Predictors.distancePropThru));
Predictors.similarityTaskPhase_maxNormed = f_x(rescale(-1.*Predictors.distanceTaskPhase));
Predictors.similarityAngVel_maxNormed = f_x(rescale(-1*Predictors.distanceAngVel));
Predictors.similarityHD_maxNormed = f_x(rescale(-1*Predictors.distanceHD));
Predictors.similarityAxis_maxNormed = f_x(rescale(-1*Predictors.distanceAxis));
Predictors.similarityLinVel_maxNormed = f_x(rescale(-1*Predictors.distanceLinVel));
Predictors.similarityAccel_maxNormed = f_x(rescale(-1*Predictors.distanceAccel));
Predictors.similarityXYPos_maxNormed = f_x(rescale(-1*Predictors.distanceXYPos));

% Linearize variables and add to table for easy handlign with GLM
% Remember, when linearized, goes down the columns (so changes the 1st dimension, rows, faster)
similarityPropThru = Predictors.similarityPropThru_maxNormed(:);
similarityTaskPhase = Predictors.similarityTaskPhase_maxNormed(:);
similarityAngVel = Predictors.similarityAngVel_maxNormed(:);
similarityHD = Predictors.similarityHD_maxNormed(:);
similarityAxis = Predictors.similarityAxis_maxNormed(:);
similarityLinVel = Predictors.similarityLinVel_maxNormed(:);
similarityAccel = Predictors.similarityAccel_maxNormed(:);
similarityXYPos = Predictors.similarityXYPos_maxNormed(:);


Predictors.linearizedTable = table(...
    similarityPropThru,...
    similarityTaskPhase,...
    similarityAngVel,...
    similarityHD,...
    similarityAxis,...
    similarityLinVel,...
    similarityAccel,...
    similarityXYPos);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


