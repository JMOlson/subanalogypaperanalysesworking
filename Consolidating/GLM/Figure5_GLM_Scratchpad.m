%% Figure 5 - GLM --- Scratchpad

onePredictorModelMSE_Sub = arrayfun(@(x) mean(x.ModelStats.mse),onePredictorGLMResults_Sub);
onePredictorModelR2_Sub = arrayfun(@(x) mean(x.ModelStats.r2_coeffOfDet),onePredictorGLMResults_Sub);
onePredictorModelMSE_Hpc = arrayfun(@(x) mean(x.ModelStats.mse),onePredictorGLMResults_Hpc);
onePredictorModelR2_Hpc = arrayfun(@(x) mean(x.ModelStats.r2_coeffOfDet),onePredictorGLMResults_Hpc);

fullGLMModelMSE_Sub = arrayfun(@(x) mean(x.ModelStats.mse),fullGLMResults1_Sub);
fullGLMModelMSE_Hpc = arrayfun(@(x) mean(x.ModelStats.mse),fullGLMResults1_Hpc);

locoGLMResultsMSE_Sub = arrayfun(@(x) mean(x.ModelStats.mse),locoGLMResults_Sub);
locoGLMResultsMSE_Hpc = arrayfun(@(x) mean(x.ModelStats.mse),locoGLMResults_Hpc);
fullGLMModel2MSE_Sub = arrayfun(@(x) mean(x.ModelStats.mse),fullGLMResults2_Sub);
fullGLMModel2MSE_Hpc = arrayfun(@(x) mean(x.ModelStats.mse),fullGLMResults2_Hpc);

normValue_Sub = var(GLMDataTable_Sub.corrLinear,1);
normValue_Hpc = var(GLMDataTable_Hpc.corrLinear,1);

%% 


onePredictorModelMAE_Sub = arrayfun(@(x) mean(x.ModelStats.mae),onePredictorGLMResults_Sub);
onePredictorModelR2_Sub = arrayfun(@(x) mean(x.ModelStats.r2_coeffOfDet),onePredictorGLMResults_Sub);
onePredictorModelMAE_Hpc = arrayfun(@(x) mean(x.ModelStats.mae),onePredictorGLMResults_Hpc);
onePredictorModelR2_Hpc = arrayfun(@(x) mean(x.ModelStats.r2_coeffOfDet),onePredictorGLMResults_Hpc);

fullGLMModelMAE_Sub = arrayfun(@(x) mean(x.ModelStats.mae),fullGLMResults1_Sub);
fullGLMModelMAE_Hpc = arrayfun(@(x) mean(x.ModelStats.mae),fullGLMResults1_Hpc);

locoGLMResultsMAE_Sub = arrayfun(@(x) mean(x.ModelStats.mae),locoGLMResults_Sub);
locoGLMResultsMAE_Hpc = arrayfun(@(x) mean(x.ModelStats.mae),locoGLMResults_Hpc);
fullGLMModel2MAE_Sub = arrayfun(@(x) mean(x.ModelStats.mae),fullGLMResults2_Sub);
fullGLMModel2MAE_Hpc = arrayfun(@(x) mean(x.ModelStats.mae),fullGLMResults2_Hpc);

normValue_Sub = var(GLMDataTable_Sub.corrLinear,1);
normValue_Hpc = var(GLMDataTable_Hpc.corrLinear,1);

%% Bootstrap Results for full model - reformatting - turns out no variability in bootstraps or in 
%  crossvalidations.

% 
% % doing some transformations  to get in a workable form. probably a better way, but this works.
% fullResultsTable_Sub = struct2table(fullGLMResults_Sub.ModelStats_Shuffled);
% fullResultsTable_Hpc = struct2table(fullGLMResults_Hpc.ModelStats_Shuffled);
% 
% vars = fullResultsTable_Hpc.Properties.VariableNames;
% nVars = numel(vars);
% for iVar = 1:nVars
%     thisVar = vars{iVar};
%     newStruct_Sub.(append('mean_',thisVar)) = cellfun(@mean, fullResultsTable_Sub.(thisVar)); %take means of each bootstrap
%     newStruct_Hpc.(append('mean_',thisVar)) = cellfun(@mean, fullResultsTable_Hpc.(thisVar));
% end
% bootstrapResults_Sub = struct2table(newStruct_Sub);
% bootstrapResultsMin_Sub = varfun(@min, bootstrapResults_Sub);
% bootstrapResultsMax_Sub = varfun(@max, bootstrapResults_Sub);
% bootstrapResults_Hpc = struct2table(newStruct_Hpc);
% bootstrapResultsMin_Hpc = varfun(@min, bootstrapResults_Hpc);
% bootstrapResultsMax_Hpc = varfun(@max, bootstrapResults_Hpc);

%% Plotting - Bar graphs
errorForBar = [fullGLMModelMSE_Sub, fullGLMModel2MSE_Sub, locoGLMResultsMSE_Sub, normValue_Sub, onePredictorModelMSE_Sub;fullGLMModelMSE_Hpc, fullGLMModel2MSE_Hpc, locoGLMResultsMSE_Hpc, normValue_Hpc, onePredictorModelMSE_Hpc];

%    fullGLMModelMSE_Sub, fullGLMModel2MSE_Sub, locoGLMResultsMSE_Sub, bootstrapResultsMin_Sub.min_mean_mse, onePredictorModelMSE_Sub;...
%    fullGLMModelMSE_Hpc, fullGLMModel2MSE_Hpc, locoGLMResultsMSE_Hpc, bootstrapResultsMin_Hpc.min_mean_mse, onePredictorModelMSE_Hpc];

labels = GLMDataTable_Sub.Properties.VariableNames(2:end);
labels = ['full model Prop', 'full model Task', labels, 'variance', labels];

labels(3:10) = cellfun(@(x) append('-',x), labels(3:10),'UniformOutput', false);

figure;

bar(errorForBar')
xticks(1:numel(labels))
xticklabels(labels);
xtickangle(-45);
ylabel('Mean Sq Error');
legend('SUB','HPC');
%% 


errorForBar = [fullGLMModelMAE_Sub, fullGLMModel2MAE_Sub, locoGLMResultsMAE_Sub, normValue_Sub, onePredictorModelMAE_Sub;fullGLMModelMAE_Hpc, fullGLMModel2MAE_Hpc, locoGLMResultsMAE_Hpc, normValue_Hpc, onePredictorModelMAE_Hpc];

%    fullGLMModelMAE_Sub, fullGLMModel2MAE_Sub, locoGLMResultsMAE_Sub, bootstrapResultsMin_Sub.min_mean_MAE, onePredictorModelMAE_Sub;...
%    fullGLMModelMAE_Hpc, fullGLMModel2MAE_Hpc, locoGLMResultsMAE_Hpc, bootstrapResultsMin_Hpc.min_mean_MAE, onePredictorModelMAE_Hpc];

labels = GLMDataTable_Sub.Properties.VariableNames(2:end);
labels = ['full model Prop', 'full model Task', labels, 'variance', labels];

labels(3:10) = cellfun(@(x) append('-',x), labels(3:10),'UniformOutput', false);

figure;

bar(errorForBar')
xticks(1:numel(labels))
xticklabels(labels);
xtickangle(-45);
ylabel('Mean Sq Error');
legend('SUB','HPC');


%%
figure;
bar(errorForBar)
xticklabels(['SUB';'HPC']);
ylabel('Mean Sq Error');
legend(labels);

%%

%normalize to variance - the constant model, essentially
normalizedError = errorForBar ./ [var(GLMDataTable_Sub.corrLinear,1);var(GLMDataTable_Hpc.corrLinear,1)];


figure;

bar(normalizedError','BaseValue',1)
set(gca, 'YDir','reverse')
xticks(1:numel(labels))
xticklabels(labels);
xtickangle(-45);
ylabel('Normalized Mean Sq Error');
legend('SUB','HPC');

figure;
bar(normalizedError,'k','BaseValue',1)
set(gca, 'YDir','reverse')
xticklabels(['SUB';'HPC']);
ylabel('Normalized Mean Sq Error');
legend(labels);