function  TTLinearRatemapper(binTemplateMatrix, varargin)
% TTLINEARRATEMAPPER Creates path templates and linear rate maps for all 
% paths of the triple t maze.
%       TTLinearRatemapper(binTemplateMatrix, varargin) Allows the user
%       to create templates for linear tracks and calculates linear rate
%       maps from the paths and corresponding templates. binTemplateMatrix
%       has the number of bins between clicks for each individual path.
%       The result is saved through a ui.
%
%   Non-built-in functions called:
%       inpaint_nans
%        
%
%   Written by Andy Alexander, pre 2014
%   Adapted by Jake Olson for Triple T track.
%   Last updated by Jake Olson, June 2016 - Changed input/output to
%   structs, sped up linear rate calc using Emily Tao's method. Removed all
%   but linear rate maps and template occupancy calculations.

%% Prompt for necessary files and load into workspace.
%List of files that contain spiking data exported from neuroexplorer.
[tfileFileName, tfilePathName] = uigetfile('*.txt', 'Choose the tfile list file.');
tfileFID = fopen(fullfile(tfilePathName,tfileFileName));
TfileCell = textscan(tfileFID,'%s');
tfileList = TfileCell{1};
fclose(tfileFID);

%behavioral scoring - spiral events and pixelDvt
[recStructFileName, recStructPathName] = uigetfile('*.mat', 'Choose the recording struct file with the events.');
load(fullfile(recStructPathName,recStructFileName));

allTfiles = cell(length(tfileList),1);
for iTfile=1:length(allTfiles)
    allTfiles{iTfile} = load(tfileList{iTfile});
end
nCells=length(allTfiles);

events = indRecStruct.events;
pos(:,1) = indRecStruct.pixelDVT(:,2);
% [leastBadSpots, leastBadSpotsInd] = min(sum(pixelDvt(:,3:end-1)==0));
% bestLight = leastBadSpotsInd(1)+2;
pos(:,2:3) = indRecStruct.pixelDVT(:,end-2:end-1); %Use the mashup light.

indRecStruct.tfileList = tfileList;
indRecStruct.allTfiles = allTfiles;
indRecStruct.nCells = nCells;
indRecStruct.binTemplateMatrix = binTemplateMatrix;
clear tfileFID TfileCell iTfile leastBadSpots*

%% Check that both processedDvt and the spikes are all sorted ascending and
% pos and processedDvt are equal.
%     if any(pos(:,1) ~= processedDVT(:,2))
%         disp('Pos times doesn''t equal processedDvt times. Yikes!');
%         return
%     end
if any(diff(pos(:,1))<0)
    disp(['Pos is not in chronological order.']);
    return
end
for iCell = 1:nCells
    if any(diff(allTfiles{iCell})<0)
        disp(['Neuron ', num2str(iCell),' spikes are not in chronological order.']);
        return;
    end
end


%% Calc nSpikes for each sample.
sampleRate = indRecStruct.trackingSampleRate;
posNSpikes = zeros(size(pos,1),nCells);
nSamples = size(pos,1);
for iCell=1:nCells
    cellnum = iCell
    tfile = allTfiles{iCell};
    posIndex = 1;
    for iSpike =1:length(tfile)
        % Cycle through dvt until you hit the time that this spike goes to.
        while tfile(iSpike) > (pos(posIndex,1)+(1/(2*sampleRate))) &&...
                posIndex <= nSamples
            posIndex = posIndex+1;
        end
        if posIndex > nSamples
            disp('Spikes exist after the last sample!');
            return
        else
            posNSpikes(posIndex,iCell) = posNSpikes(posIndex,iCell) + 1;
        end
    end
end

%% Creation of smoothing function
% Create a gaussian linear ratemap smoothing filter.
filterWidth = 13; %Must be odd.
filterMedian = (filterWidth-1)/2;
filterSigma = 1;
filterBins=0:1:filterWidth-1;
filterValues=gaussmf(filterBins,[filterSigma, filterMedian]);
filterValuesNormed=filterValues/sum(filterValues);

indRecStruct.filterWidthLinearRMaps = filterWidth;
indRecStruct.filterSigmaLinearRMaps = filterSigma;
indRecStruct.filterLinearRMaps = filterValuesNormed;

clear filterBins filterWidth filterSigma filterValues

%% Create lists for each run type.

% % In case you need to change a button code before processing the runs,
% % use this code as a model to how.
% for i=1:length(events)/2
%     if (events(2*i-1,3)==1) && (events(2*i,3)==4)
%         events(2*i-1,3)=11;
%         events(2*i,3)=12;
%     end
% end

pathList = unique(events(1:end,3));
% pathListBadRuns = pathList(pathList<0 & pathList>-99); % Bad runs only.
pathList = pathList(pathList>0 & pathList<99); % Good runs only.

nPaths = length(pathList);
runStarts = cell(nPaths,1);
runEnds = cell(nPaths,1);
pathRunsLineMarkers = cell(nPaths,2);
pathRunsTimeMarkers = cell(nPaths,2);
nRuns = zeros(nPaths,1);

for iPath = 1:nPaths
    runStarts{iPath} = find(events(:,3) == pathList(iPath));
    runEnds{iPath} = find(events(:,3) == pathList(iPath))+1;
    pathRunsLineMarkers{iPath,1} = events(runStarts{iPath},1);
    pathRunsLineMarkers{iPath,2} = events(runEnds{iPath},1);
    pathRunsTimeMarkers{iPath,1} = events(runStarts{iPath},2);
    pathRunsTimeMarkers{iPath,2} = events(runEnds{iPath},2);
    nRuns(iPath) = length(pathRunsLineMarkers{iPath,1});
end
indRecStruct.pathList = pathList;
indRecStruct.pathRunsLineMarkers = pathRunsLineMarkers;
indRecStruct.pathRunsTimeMarkers = pathRunsTimeMarkers;
indRecStruct.nRunsEachPath = nRuns;
clear iPath runStarts runEnds

nSessions = length(indRecStruct.sessionTimeStamps)/2;

%% Create master template
if ~isempty(varargin)
    masterTemplate = varargin{1};
else
    figure(1);
    clf;
    hold on;
    
    % Grab only the run portions of the data.
    for iSession = 2:nSessions; % Session one is always platform.
        masterTemplate{iSession} = [];
        nMasterPoints = 0; % Reset count for each session.
        
        % Plot correct data
        if iSession == 2 % Normal track session
            pathsDesiredIndices = find(pathList<=10);
        elseif iSession == 3 % Manipulation
            pathsDesiredIndices = find(pathList>10);
        else % Anything else - maybe 2nd platform
            break;
        end
        
        for iPathToPlot = 1:length(pathsDesiredIndices)
            for iSegment = 1:length(pathRunsLineMarkers{pathsDesiredIndices(iPathToPlot),1})
                plot(pos(pathRunsLineMarkers{pathsDesiredIndices(iPathToPlot),1}(iSegment):...
                    pathRunsLineMarkers{pathsDesiredIndices(iPathToPlot),2}(iSegment),2),...
                    pos(pathRunsLineMarkers{pathsDesiredIndices(iPathToPlot),1}(iSegment):...
                    pathRunsLineMarkers{pathsDesiredIndices(iPathToPlot),2}(iSegment),3),...
                    '.k');
            end
        end
        
        % Create template
        newInput = 0;
        while ~isempty(newInput)
            % ask for one input per loop, then immediately plot that point before
            % asking for the next input. Keep looping until one of the inputs is
            % empty (meaning the user pressed 'enter').
            newInput = ginput(1);

            if ~isempty(newInput)
                nMasterPoints = nMasterPoints + 1;
                masterTemplate{iSession}(nMasterPoints,:) = round(newInput);
                %             load('fakeClicks'); % Testing Line
                %             masterTemplate{iSession}(nMasterPoints,:) = Clicks{which?}(nMasterPoints,:); % Testing Line
                if nMasterPoints>1
                    % find the Euclidean distance between the last two input points
                    eucDist = dist(masterTemplate{iSession}(nMasterPoints-1,:),masterTemplate{iSession}(nMasterPoints,:)');
                    if eucDist < 5 %Double click - ignore second.
                        nMasterPoints = nMasterPoints-1;
                        masterTemplate{iSession} = masterTemplate{iSession}(nMasterPoints,:);
                    else
                        % plot the user-defined points in a distinct color
                        plot(masterTemplate{iSession}(:,1),masterTemplate{iSession}(:,2),'r+')
                    end
                end
                % plot the most recent input point even more distinctly
                plot(newInput(1),newInput(2),'c*');
            end
        end
    end
    save masterTemplate masterTemplate
end
indRecStruct.masterTemplate = masterTemplate;
    
%% Create templates and linear ratemaps
clear coords pathsDesiredIndices
for iSession = 2:nSessions; % Session one is always platform.
    % Plot correct data
    if iSession == 2 % Normal track session
        pathsDesiredIndices = find(pathList<=10);
    elseif iSession == 3 % Manipulation
        pathsDesiredIndices = find(pathList>10);
    else % Anything else - maybe 2nd platform
        break;
    end
    
    for iPath = 1:length(pathsDesiredIndices)
        iPathListIndex = pathsDesiredIndices(iPath);
        figure(1)
        clf;
        hold on
        for iSegment = 1:length(pathRunsLineMarkers{iPathListIndex,1})
            plot(pos(pathRunsLineMarkers{iPathListIndex,1}(iSegment):...
                pathRunsLineMarkers{iPathListIndex,2}(iSegment),2),...
                pos(pathRunsLineMarkers{iPathListIndex,1}(iSegment):...
                pathRunsLineMarkers{iPathListIndex,2}(iSegment),3),...
                '.k');
        end
        plot(masterTemplate{iSession}(:,1),masterTemplate{iSession}(:,2),'r+');
        
        currentPath = pathList(iPathListIndex);
        newInput = 0;
        nPts = 0;
        closestTemplatePoint = [];
        clickDist = [];
        thisPathBinXs = {};
        thisPathBinYs = {};
        while ~isempty(newInput)
            % ask for one input per loop, then immediately plot that point before
            % asking for the next input. Keep looping until one of the inputs is
            % empty (meaning the user pressed 'enter').
            newInput = ginput(1);
            if ~isempty(newInput)
                nPts = nPts + 1;
                [~,closestTemplatePoint(nPts)] = min(dist(newInput,masterTemplate{iSession}'));
                coords(nPts,:) = masterTemplate{iSession}(closestTemplatePoint(nPts),:);
                %             load('fakeClicks'); % Testing Line
                %             coords(nPts,:) = Clicks{iPathListIndex}(nPts,:); % Testing Line
                if nPts>1
                    if closestTemplatePoint(nPts) == closestTemplatePoint(nPts-1) %Double click - ignore second.
                        nPts = nPts-1;
                        closestTemplatePoint(nPts) = 0;
                    else
                        nBinsThisSegment = binTemplateMatrix(currentPath,nPts-1);
                    end
                    % interpolate and plot the current segment of the template
                    % find the Euclidean distance between the last two input points
                    clickDist(nPts-1,1) = coords(nPts,1) - coords(nPts-1,1);
                    clickDist(nPts-1,2) = coords(nPts,2) - coords(nPts-1,2);
                    stepSizes = clickDist(nPts-1,:).*(1/nBinsThisSegment); %Find stepsize
                    
                    if clickDist(nPts-1,1)
                        thisPathBinXs{nPts-1} = coords(nPts-1,1):stepSizes(1):coords(nPts,1);
                    else
                        thisPathBinXs{nPts-1} = repmat(coords(nPts,1), 1, nBinsThisSegment+1); 
                    end
                    if clickDist(nPts-1,2)
                        thisPathBinYs{nPts-1} = coords(nPts-1,2):stepSizes(2):coords(nPts,2); %#ok<*AGROW>
                    else
                        thisPathBinYs{nPts-1} = repmat(coords(nPts,2), 1, nBinsThisSegment+1); 
                    end
                    % WHy did we use to do Xs this way and not like the
                    % Ys?!?!?!?!?
                    % thisPathBinXs{nPts-1} = interp1(coords(nPts-1:nPts,2),...
                    %      coords(nPts-1:nPts,1),thisPathBinYs{nPts-1});
                    
                    plot(thisPathBinXs{nPts-1},thisPathBinYs{nPts-1},'b*');
                end
            end
            % plot the most recent click point even more distinctly
            plot(coords(nPts,1),coords(nPts,2),'c*');
        end
        indRecStruct.Clicks{iPathListIndex} = coords;
        
        % Removes duplicate bin issue. - We create bin centers, and we create
        % one for the last bin of each click and the first bin of each click in
        % the same place. This removes the duplicate. Note: Also removes
        % last bin on last click, because there is no duplicate here. Then
        % we build the template.
        template = zeros(0,3);
        for iBinLine=1:length(thisPathBinXs) % is clicks-1, i.e. (nPts-1)
            nBinsThisSegment =  binTemplateMatrix(currentPath,iBinLine);
            template(end+1:end+nBinsThisSegment,2:3) = [...
                thisPathBinXs{iBinLine}(1:end-1)',...
                thisPathBinYs{iBinLine}(1:end-1)'];
        end
        template(:,1)=1:length(template);
        indRecStruct.PathTemplate{iPathListIndex} = template;
        
        
        % Location to bin mappings.
        thisPathLineMarkers = [pathRunsLineMarkers{iPathListIndex,1},pathRunsLineMarkers{iPathListIndex,2}];
        clear binCenters;
        binCenters(:,1) = template(:,2);
        binCenters(:,2) = template(:,3);
        
        binOcc = cell(length(template),nRuns(iPathListIndex));
        binDistance = cell(length(template),nRuns(iPathListIndex));
        fillVals = cell(nRuns(iPathListIndex),1);
        templateLoc = zeros(length(template),nRuns(iPathListIndex),3); %3 is nOccs, xAvg, yAvg
        
        for iRun = 1:nRuns(iPathListIndex)
            clear runActPoints
            runActPoints(:,1) = pos(thisPathLineMarkers(iRun,1):thisPathLineMarkers(iRun,2),2);
            runActPoints(:,2) = pos(thisPathLineMarkers(iRun,1):thisPathLineMarkers(iRun,2),3);
            
            %matrix of distances of all run points to all bins - size is runPoints x binCenters
            binDistMat = dist(runActPoints,binCenters');
            lostTrackingRows = sum(runActPoints,2) == 2; % Should be size runActPoints;
            [dist2ClosestBin, distInd] = min(binDistMat,[],2); % Should be size runActPoints;
            badTrackingRows = dist2ClosestBin > 20; % Considered a bad bin if further than 20 pixels from the template.
            closestBin = distInd(:,1);  % Should be runActPoints rows; 1 column is in case it is equally close to 2, grab the 1st only.
            closestBin(lostTrackingRows) = -1; %BIN -1 is throwaway!
            closestBin(badTrackingRows) = -2; %BIN -2 is throwaway!
            
            for iBin = 1:length(template)
                binOcc{iBin,iRun} = [find(closestBin == iBin),runActPoints(closestBin == iBin,:)];
                if sum(closestBin == iBin) ~= 0
                    binDistance{iBin,iRun} = dist2ClosestBin(closestBin == iBin);
                    templateLoc(iBin,iRun,1) =...
                        sum(closestBin == iBin);
                    templateLoc(iBin,iRun,2:3) =...
                        mean(runActPoints(closestBin == iBin,:),1);
                end
            end
            coordinates = templateLoc(:,iRun,2:3);
            coordinates(coordinates == 0) = NaN; %Need this for my interp. Filling ends to be same as first point.
            fillFront = find(~isnan(coordinates(:,1,1)),1,'first');
            fillBack = find(~isnan(coordinates(:,1,1)),1,'last');
            fillVals{iRun} = [fillFront,fillBack];
            coordinates(1:fillFront,1,:) = repmat(coordinates(fillFront,1,:),[fillFront,1,1]);
            coordinates(fillBack:end,1,:) = repmat(coordinates(fillBack,1,:),[length(coordinates)-fillBack+1,1,1]);
            templateLoc(:,iRun,2:3) = coordinates;
            templateLoc(:,iRun,2)=inpaint_nans(templateLoc(:,iRun,2),1);
            templateLoc(:,iRun,3)=inpaint_nans(templateLoc(:,iRun,3),1);
        end
        
        meanTemplateOcc(1:length(template),pathList(iPathListIndex),1:2) = ...
            reshape(mean(templateLoc(:,:,2:3),2),[length(template),1,2]);
        meanTemplateOcc(1:length(template),pathList(iPathListIndex),3:4) = ...
            reshape(std(templateLoc(:,:,2:3),0,2),[length(template),1,2]);
        meanTemplateOcc(1:length(template),pathList(iPathListIndex),5:6) = ...
            reshape(std(templateLoc(:,:,2:3),0,2)/(sqrt(nRuns(iPathListIndex)-1)),[length(template),1,2]);
        
        timeSpent = templateLoc(:,:,1)*10000/60;
        
        templateOccupancyStats(pathList(iPathListIndex)).binOcc = binOcc;
        templateOccupancyStats(pathList(iPathListIndex)).binDistance = binDistance;
        templateOccupancyStats(pathList(iPathListIndex)).templateLoc = templateLoc;
        templateOccupancyStats(pathList(iPathListIndex)).timeSpent = timeSpent;
        templateOccupancyStats(pathList(iPathListIndex)).binCenters = binCenters;
        templateOccupancyStats(pathList(iPathListIndex)).fillVals = fillVals;
        
        indRecStruct.meanTemplateOcc = meanTemplateOcc;
        indRecStruct.templateOccupancyStats = templateOccupancyStats;
        
        % Firing Rate Vectors
        % 4th dimension is firing rate, spikes, occ
        linearRates=zeros(nCells,length(template),nRuns(iPathListIndex),3);
        for iRun=1:length(thisPathLineMarkers(:,1))  % each outbound run
            for iPoint = thisPathLineMarkers(iRun,1):thisPathLineMarkers(iRun,2)  % each time-point during this run
                if pos(iPoint,2)>1 && pos(iPoint,3)>1  % if we have valid tracking
                    templateDist = dist(pos(iPoint,2:3),template(:,2:3)');
                    [~, nearestPointInd] = min(templateDist);  % find the template point closest to this actual point
                    spikes = posNSpikes(iPoint,:)';
                    if nearestPointInd <= filterMedian
                        occIndFiltered = [1:nearestPointInd(1)+filterMedian];
                        linearRates(:,occIndFiltered,iRun,3) =...
                            linearRates(:,occIndFiltered,iRun,3)+...
                            repmat(filterValuesNormed(end-length(occIndFiltered)+1:end),nCells,1);
                        linearRates(:,occIndFiltered,iRun,2) =...
                            linearRates(:,occIndFiltered,iRun,2)+...
                            repmat(spikes,1,length(occIndFiltered)).*repmat(filterValuesNormed(end-length(occIndFiltered)+1:end),nCells,1);
                    elseif nearestPointInd >= length(template)-filterMedian
                        occIndFiltered = [nearestPointInd(1)-filterMedian:length(template)];
                        linearRates(:,occIndFiltered,iRun,3) =...
                            linearRates(:,occIndFiltered,iRun,3)+...
                            repmat(filterValuesNormed(1:length(occIndFiltered)),nCells,1);
                        linearRates(:,occIndFiltered,iRun,2) =...
                            linearRates(:,occIndFiltered,iRun,2)+...
                            repmat(spikes,1,length(occIndFiltered)).*repmat(filterValuesNormed(1:length(occIndFiltered)),nCells,1);
                    else
                        occIndFiltered = ...
                            [nearestPointInd(1)-filterMedian:...
                            nearestPointInd(1)+filterMedian];
                        linearRates(:,occIndFiltered,iRun,3) =...
                            linearRates(:,occIndFiltered,iRun,3)+...
                            repmat(filterValuesNormed,nCells,1);
                        linearRates(:,occIndFiltered,iRun,2) =...
                            linearRates(:,occIndFiltered,iRun,2)+...
                            repmat(spikes,1,length(occIndFiltered)).*repmat(filterValuesNormed,nCells,1);
                    end
                end
            end
        end
        
        
        %calc firing rates for each path
        rates = zeros(length(template),nRuns(iPathListIndex));
        meanLinearRates=zeros(nCells,length(template),3);
        for iCell = 1:nCells
            rates = rates*0;
            isOcc = squeeze(linearRates(iCell,:,:,3)>0);
            spikes = squeeze(linearRates(iCell,:,:,2));
            occs = squeeze(linearRates(iCell,:,:,3));
            rates(isOcc) = sampleRate*spikes(isOcc)./occs(isOcc);
            linearRates(iCell,:,:,1) = rates;
            
            %calc mean rates across laps
            meanLinearRates(iCell,:,1) = mean(rates,2);
            meanLinearRates(iCell,:,2) = std(rates,0,2);
            meanLinearRates(iCell,:,3) = std(rates,0,2)/(sqrt(nRuns(iPathListIndex)-1));
        end
        clf;
        
        indRecStruct.LinearRates{iPathListIndex} = linearRates;
        indRecStruct.MeanLinearRates{iPathListIndex} = meanLinearRates;
    end
end
clear newInput nPts coords nBinsOneClick clickDist thisPath* eucDist stepSize
clear template* occIndFiltered nearest*
clear rates isOcc spikes occs linearRates meanLinearRates linear_ratemap_completed
clear iBin* iCell iPath iPoint iPos iTfile
clear allXsThisRun allYsThisRun
clear runActPoints binDistMat lostTrackingrows dist2ClosestBin 
clear binOcc binDistance fillVals templateLoc binCenters pathData
clear distInd badTrackingRows closestBin coordinates fill*

%% Save
uisave('indRecStruct',strcat(fullfile(recStructPathName,recStructFileName(1:10)),'_tt_rmaps'));

%% Plotting
% whichPathPlotMapping = [1,2,3,4,0,0,0,0,5,6,7,8,9,10,0,0,0,0,11,12];
% for iCell=1:nCells
%     disp(iCell)
%     disp(tfileList{iCell})
%     for iPath = 1:nPaths
%         maxRate(iPath) = max(MeanLinearRates{iPath}(iCell,:,1));
%         nClicks(iPath) = size(Clicks{iPath},1)-1; %cumSumNBins ignores 1st click.
%     end
%     maxY = max(maxRate);
%     
%     figure(1);
%     clf;
%     for iPath = 1:nPaths
%         whichPath = pathList(iPath);
%         if find([1:4,9:14,19:20]==whichPath) %Only plot these runs.
%             
%             subplot(6,2,whichPathPlotMapping(whichPath)); % Might change 3 to 6.
%             bar(1:size(MeanLinearRates{iPath},2),MeanLinearRates{iPath}(iCell,:,1));
%             hold on;
%             plot(1:size(MeanLinearRates{iPath},2),MeanLinearRates{iPath}(iCell,:,2),'g');
%             if find([1:4,11:14]==whichPath)
%                 nClicksToPlot = 3;
%                 whichClicks = [3,5,7];
%             else
%                nClicksToPlot = 2;
%                 whichClicks = [2,4]; 
%             end
%             for iClick = 1:nClicksToPlot
%                 plot([cumSumNBins{iPath}(whichClicks(iClick)-1), cumSumNBins{iPath}(whichClicks(iClick)-1)],[0, maxY+1],'r')
%             end
%             axis([0 length(MeanLinearRates{iPath}) 0 maxY+1])
%         end
%     end
%     pause;
% end
clear iCell iPath maxY iClick maxRate nClicks whichPath whichClicks nClicksToPlot whichPathPlotMapping

%% Alternate Plotting with the struct setup.
for iCell = 1:indRecStruct.nCells
figure;
for i = 1:length(indRecStruct.pathList)
subplot(3,2,i)
hold on;
plot(squeeze(indRecStruct.LinearRates{i}(iCell,:,:,1)));
plot(indRecStruct.MeanLinearRates{i}(iCell,:,1),'k','LineWidth',3);
end
title(indRecStruct.tfileList{iCell});
end

for iCell = 1:indRecStruct.nCells
figure;
for i = 1:length(indRecStruct.pathList)
subplot(3,2,i)
plot(indRecStruct.MeanLinearRates{i}(iCell,:,1),'k');
end
subtitle(indRecStruct.tfileList{iCell});
end

end
