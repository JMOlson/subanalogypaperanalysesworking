function Performance = calcPerformanceTTT(RecStruct, rewardSiteSet, returnRunSet)
%CALCPERFORMANCETTT - compute behavioral performance metrics for triple t maze
%
% 	Performance = calcPerformanceTTT(RecStruct, rewardSiteSet, returnRunSet)
%
% INPUTS:
%		RecStruct - Compiled recording data
%		rewardSiteSet - List of outbound path IDs to analyze (i.e. [1,2,3,4]
%		returnRunSet - List of inbound path IDs to analyze (i.e. [9,10]
%
% OUTPUTS:
%		Performance - Struct with compiled behavior metrics for each rec
%
% EXAMPLES:
%		Performance = calcPerformanceTTT(BehRec, [1,2,3,4], [9,10]);
%
% REMARKS
%
% OTHER M-FILES REQUIRED:
%       createTransitionProbabilityMatrix
% SUBFUNCTIONS:
%       createPerformanceBlocks
%       calcBehTransProbs
% MAT-FILES REQUIRED:
%       RecStruct output from compileRecs Fn for 2020 SUB/CA1 paper
%
% SEE ALSO:
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 24-Apr-2020
% LAST MODIFIED BY:
% LAST MODIFIED ON:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Coding Used:
END_LABEL_MIN_VALUE = 100;

if ~exist('rewardSiteSet','var')
    rewardSiteSet = [1,2,3,4,5,6,7,8];
end
if ~exist('returnRunSet','var')
    returnRunSet = [9,10];
end
allRunSet = [rewardSiteSet,returnRunSet];

% Main code - run for each recording
nRecs = numel(RecStruct.rec);
for iRec = 1:nRecs
    % Grab run info
    events = RecStruct.Behavior.behaviorEvents{iRec}(:,3); % cols are index, time, type label
    runs = abs(events(abs(events) < END_LABEL_MIN_VALUE));
    outboundRuns = runs(ismember(runs,rewardSiteSet));
    
    % Subfunction (found below main fn in this file)
    RunBlocks = createPerformanceBlocks(outboundRuns, rewardSiteSet);
    
    nRunsInBlock = cellfun(@(x) numel(x),RunBlocks);
    % cuts the last set if the animal was taken off before doing 4 runs.
    blockFinished = nRunsInBlock >= numel(rewardSiteSet);
    RunBlocks = RunBlocks(blockFinished);
    nRunsInBlock = nRunsInBlock(blockFinished);
    
    nErrsInBlock = nRunsInBlock-numel(rewardSiteSet);
    meanErrPerBlock = sum(nErrsInBlock)/numel(RunBlocks);
    isPerfectBlock = nErrsInBlock == 0;
    perfectBlockPercent = sum(isPerfectBlock)/numel(RunBlocks)*100;
    perfectRunBlocks = cell2mat(RunBlocks(isPerfectBlock)');
    
    % perfect pattern counts
    [patterns,~,patternIndex] = unique(perfectRunBlocks,'rows');
    nEachPattern = accumarray(patternIndex,1);
    % put nEachPattern as last column in the row after the pattern which is the first 4 items in the
    % row.
    patternAndCounts = [patterns, nEachPattern];
    
    
    %% Assign Outputs Needed
    Performance.RunBlocks{iRec,1} = RunBlocks;
    Performance.nRunsInBlock{iRec,1} = nRunsInBlock;
    Performance.nErrsInBlock{iRec,1} = nErrsInBlock;
    Performance.isPerfectBlock{iRec,1} = isPerfectBlock;
    Performance.perfectBlockPercent(iRec,1) = perfectBlockPercent;
    Performance.meanErrPerBlock(iRec,1) = meanErrPerBlock;
    Performance.nEachPattern{iRec,1} = nEachPattern;
    Performance.patternAndCounts{iRec,1} = patternAndCounts;
    
    % subfunction handling all transition probability calcs
    Performance = calcBehTransProbs(Performance,iRec, outboundRuns, rewardSiteSet, runs, allRunSet);
end

end

function RunBlocks = createPerformanceBlocks(outboundRuns, rewardSiteSet)
% Loop through the runs, and segment once all reward sites have been visited.
% Used for performance evaluations, since the task is to visit all reward sites
nRewardSiteSet = numel(rewardSiteSet);
iBlock = 1;
iRunInBlock = 1;
for iRun=1:numel(outboundRuns)
    RunBlocks{iBlock}(iRunInBlock) = outboundRuns(iRun);
    iRunInBlock=iRunInBlock+1;
    
    if numel(unique(RunBlocks{iBlock})) == nRewardSiteSet
        if unique(RunBlocks{iBlock}) == rewardSiteSet
            iBlock = iBlock+1;
            iRunInBlock = 1;
        end
    end
end

end


function Performance = calcBehTransProbs(Performance,iRec, outboundRuns, rewardSiteSet, runs, allRunSet)

%% create indices to grab appropriate numbers for R->L & L->R, 1st & 3rd turns
L_GIV_R_INDICES_1 = sub2ind([4,4],[3,3,4,4],[1,2,1,2]);
R_GIV_L_INDICES_1 = sub2ind([4,4],[1,2,1,2],[3,3,4,4]);

CLOSE_RETURN_INDICES = sub2ind([6,6],[1,2,3,4],[5,5,6,6]);
FAR_RETURN_INDICES = sub2ind([6,6],[1,2,3,4],[6,6,5,5]);

L_GIV_R_INDICES_3 = sub2ind([4,4],[2,2,4,4],[1,3,1,3]);
R_GIV_L_INDICES_3 = sub2ind([4,4],[1,1,3,3],[2,4,2,4]);

TRANS_INDICES_3 = sub2ind([2,2],[1,2],[2,1]);

%% create transition probability matrices
[transProbMatOutboundRuns, transCountMatOutboundRuns, nTransPerOutbound] = ...
    createTransitionProbabilityMatrix(outboundRuns, rewardSiteSet);
[transProbMatAllRuns, transCountMatAllRuns, nTransPerAll] = ...
    createTransitionProbabilityMatrix(runs, allRunSet);

only12 = outboundRuns(ismember(outboundRuns,[1,2]));
only34 = outboundRuns(ismember(outboundRuns,[3,4]));
[~, transCountMat12, nTrans12] = createTransitionProbabilityMatrix(only12, [1,2]);
[~, transCountMat34, nTrans34] = createTransitionProbabilityMatrix(only34, [3,4]);

%% Transition Calculations
% turn 1
nLGivR = sum(transCountMatOutboundRuns(L_GIV_R_INDICES_1));
nRGivL = sum(transCountMatOutboundRuns(R_GIV_L_INDICES_1));
nOutboundRuns = sum(nTransPerOutbound);
nAlternations1 = nLGivR + nRGivL;

% turn 3
nLGivR = sum(transCountMatOutboundRuns(L_GIV_R_INDICES_3));
nRGivL = sum(transCountMatOutboundRuns(R_GIV_L_INDICES_3));
nAlternations3v1 = nLGivR + nRGivL;

% turn 3, each loc independent
nAlternations3v2(1) = sum(transCountMat12(TRANS_INDICES_3));
nAlternations3v2(2) = sum(transCountMat34(TRANS_INDICES_3));
n3v2(1) = sum(nTrans12);
n3v2(2) = sum(nTrans34);

nCloseReturns = sum(transCountMatAllRuns(CLOSE_RETURN_INDICES));
nFarReturns = sum(transCountMatAllRuns(FAR_RETURN_INDICES));

%% Write outputs
Performance.transitionProbMatOutboundRuns(iRec,:,:) = transProbMatOutboundRuns;
Performance.transitionCountsOutboundRuns(iRec,:,:) = transCountMatOutboundRuns;
Performance.transitionProbMatAllRuns(iRec,:,:) = transProbMatAllRuns;
Performance.transitionCountsAllRuns(iRec,:,:) = transCountMatAllRuns;
Performance.prob1stTurnAlt(iRec,1) = nAlternations1./nOutboundRuns;
Performance.prob3rdTurnAltGrouped(iRec,1) = nAlternations3v1./nOutboundRuns;
Performance.prob3rdTurnAltSep(iRec,1) = sum(nAlternations3v2)./sum(n3v2);
Performance.probCloseReturn(iRec,1) = nCloseReturns./(nCloseReturns+nFarReturns);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


