function [ca1_CQstruct, sub_CQstruct] = compileCQ_suban(cqFolder)

startingdir = pwd; % Set home to go back home 


t_list =  dir([cqFolder,'\CA1']); %temp directory list is CA1 data files 
cd([cqFolder,'\CA1']);
jFile = 1; %Have an offset of 3 so set new index 'j' to 1 
for iFile = 3:size(t_list,1) 
ca1_cqList{jFile,1} = (t_list(iFile).name); %copy data
jFile = jFile+1; 
end
clear jFile t_list
[ca1_CQstruct] = clusterQualityMetrics(ca1_cqList); %make the struct 

t_list =  dir([cqFolder,'\SUB']);
cd([cqFolder,'\SUB']);
jFile = 1;
for iFile = 3:size(t_list,1) 
sub_cqList{jFile,1} = (t_list(iFile).name);
jFile = jFile+1; 
end
clear jFile t_list
[sub_CQstruct] = clusterQualityMetrics(sub_cqList);

cd(startingdir);%go home 

end

