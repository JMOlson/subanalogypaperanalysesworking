function Results = makeLinearFRCategoriesToCompare(peshRasters, peshParameters, cat1Props, cat2Props, nPos,nNeu)
%SSSSSS One line description of what the function or script performs (H1 line)
%		Optional file header info (to give more details about the function than in the H1 line)  
%
% 	[output1, output2] = ssssss(input1, input2)
%
% INPUTS: 
%		cat1Props - an X by 2 cell array, where X is the number of different properties that must be
%                   true for the data to be in category 1. the first column is a string of the field
%                   name of the property. the second column is the value.
%		cat2Props - same as cat2Props but for category 2
%		input3 - Description
%		                      
%
% OUTPUTS: 
%		output1 - Description
%		output2 - Description
%		output3 - Description
%		                       
%
% EXAMPLES: 
%		Line 1 of example
%		Line 2 of example
%		Line 3 of example
%		                   
%
% REMARKS
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:%
% CREATED WITH MATLAB VERSION: 9.6.0.1150989 (R2019a) Update 4 on Microsoft Windows 10 Enterprise Version 10.0 (Build 18363)
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 20-Mar-2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

meanCat1FRs = zeros(nPos,nNeu);
meanCat2FRs = zeros(nPos,nNeu);
oddCat1FRs = zeros(nPos,nNeu);
evenCat1FRs = zeros(nPos,nNeu);
oddCat2FRs = zeros(nPos,nNeu);
evenCat2FRs = zeros(nPos,nNeu);


for iNeu = 1:nNeu
    nTrials = size(peshParameters{iNeu},1);
    trialsInCat1  = true(nTrials,1);

    for i1Prop = 1:size(cat1Props,1)
        trialsInCat1 = trialsInCat1 & peshParameters{iNeu}.(cat1Props{i1Prop,1}) == cat1Props{i1Prop,2};
    end
    
    cat1FRs = peshRasters{iNeu}(:,trialsInCat1);
    meanCat1FRs(:,iNeu) = nanmean(cat1FRs,2);
    oddCat1FRs(:,iNeu) = nanmean(cat1FRs(:,1:2:end),2);
    evenCat1FRs(:,iNeu) = nanmean(cat1FRs(:,2:2:end),2);
    
    % Cat 2
    trialsInCat2  = true(nTrials,1);
    for i2Prop = 1:size(cat2Props,1)
        trialsInCat2 = trialsInCat2 & peshParameters{iNeu}.(cat2Props{i2Prop,1}) == cat2Props{i2Prop,2};
    end
    
    cat2FRs = peshRasters{iNeu}(:,trialsInCat2);
    meanCat2FRs(:,iNeu) = nanmean(cat2FRs,2);
    oddCat2FRs(:,iNeu) = nanmean(cat2FRs(:,1:2:end),2);
    evenCat2FRs(:,iNeu) = nanmean(cat2FRs(:,2:2:end),2);
end
Results.meanCategory1 = meanCat1FRs;
Results.meanCategory2 = meanCat2FRs;
Results.oddCategory1 = oddCat1FRs;
Results.evenCategory1 = evenCat1FRs;
Results.oddCategory2 = oddCat2FRs;
Results.evenCategory2 = evenCat2FRs;
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
