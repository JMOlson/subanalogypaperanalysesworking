function [TurnRasters, TurnParameters, TurnPosition3D] = grabTurnData(NeuronStruct, RecStruct, startPoint, endPoint, ignoreNeuronFlag, isTimeAnalysis, timeStepSize)
%GRABTURNDATA transforms linear rate map data into turn-aligned vectors.
% TURNRASTERS, TURNPARAMETERS = grabTurnData(MPCNeuronStruct, MPCRecStruct,
%   startPoint, endPoint, paperNeuronsHistoVerified) turns linear rate map
%   data into turn-aligned vectors in TURNRASTERS. It also saves the parameters for each
%   turn and returns them in the table TURNPARAMETERS.
%
% MAPPINGS OF PARAMETERS:
% Left/Right turns are coded by L/R.
%
%       Spatial Turn Location Map.
%           8                   9
%                4         5
%
%                2    1    3
%
%                6         7
%           A       Start       B
%
% Progress - 1 -> 2/3 -> 4/5/6/7 -> 8/9 -> A/B
%
% In this orientation, we call times the animal is facing up in the above
%   schematic 'S'.
%
% Routes are labeled so top left reward is end of route 1, then ordered
%   left->right, top/bottom. Return run from reward 1 -> start is 9, reward
%   4 -> start is 10. animal doesn't necessarily have to come from those
%   rewards for the returns, just the route is defined when the animal
%   traverses that space.
%
% Choice is defined C/F and depends on the reward paradigm.
%
%          High/Low or Visit All 8
%           F                   F
%                C         C
%
%                C    C    C
%
%                C         C
%           F       Start       F
%
%                Visit All 4
%           F                   F
%                C         C
%
%                F    C    F
%
%                x         x
%           F       Start       F
%
% INPUTS: NeuronStruct, RecStruct, startPoint, endPoint, paperNeuronsHistoVerified
%
% OUTPUTS:
%       TurnRasters - Array of all the turn-aligned neural data. Shape is length of turn-aligned
%       sequence x nTurnInstances x 3 (firing rate in spikes/sec, spikes, occ).
%
%       TurnParameters - All parameters needed to fully define which turn
%           and action the data is from.
%
% EXAMPLES:
%
% REMARKS
%   Any time the window includes beyond the actual dataset, the data is
%   buffered with NaNs.
%
% OTHER M-FILES REQUIRED:
% SUBFUNCTIONS:
% MAT-FILES REQUIRED:
%
% SEE ALSO:% CREATED WITH MATLAB VERSION: 9.6.0.1174912 (R2019a) Update 5 on Microsoft Windows 10
%   Enterprise Version 10.0 (Build 18362)
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: https://www.jmolson.com
% CREATED ON: 20-Nov-2019
% LAST MODIFIED BY: Jacob Olson
% LAST MODIFIED ON: 11/21/2019
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RANDOMINITSIZE = 100000;
RANDOMINCREMENTSIZE = 1000;
BINSEARCH = [1,-1,2,-2,3,-3]; % if no turn apex bins, will seach around the apex this far. if none found, toss the run.
%Directions of the turns for the TT maze. NSEW are relative not absolute.
orientationCodes = [...
    'SES  ';...
    'SES  ';...
    'SWS  ';...
    'SWS  ';...
    'SEN  ';...
    'SEN  ';...
    'SWN  ';...
    'SWN  ';...
    '   EN';...
    '   WN'];

% LinearRates data is a cell array of form nNeurons x nPaths.
% Inside, data is an array of locationBin x traversals x 3.
% The 3 sheet matrices are firing rate (smoothed spikes/sec), spikes, and
% occupancies, in that order.
nNeurons = size(NeuronStruct.LinearRates,1);
nProgress = size(RecStruct.pathTurnBins,2);

if ~exist('ignoreNeuronFlag','var')
    ignoreNeuronFlag = false(nNeurons,1);
elseif isempty(ignoreNeuronFlag)
    ignoreNeuronFlag = false(nNeurons,1);
elseif length(ignoreNeuronFlag) == 1
    ignoreNeuronFlag = repmat(ignoreNeuronFlag,nNeurons,1);
end

if ~exist('isTimeAnalysis','var')
    isTimeAnalysis = false;
end
if ~exist('timeStepSize','var')
    timeStepSize = 1/20; % animals run ~20cm/sec = this gives ~same bin size/counts as space.
end
for iNeuron = 1:nNeurons
    iRec = NeuronStruct.recStructIndex(iNeuron);    
    posPathAndBin = [RecStruct.Behavior.samplePaths{iRec},RecStruct.Behavior.samplePathBins{iRec}];
    if ignoreNeuronFlag(iNeuron)
        continue; % skip this one
    end
    iMaster = 1;
    if isTimeAnalysis
        sampleRate = RecStruct.trackingSampleRateHz(iRec);
        timeStepsInSamples = round(timeStepSize * sampleRate);
        
        % filter for spike smoothing across time for time analysis.
        % for space, we used a std dev of 1 bin = 1cm.
        % since our rats run on avg 20cm/s, we want to smooth 1/20s as our time std. we use a std dev of 3 for comparable smoothing.
        filterSigma = timeStepsInSamples;
        filterWidth = 6*filterSigma+1; %Must be odd.
        filterMedian = (filterWidth-1)/2;
        filterBins=0:1:filterWidth-1;
        filterValues=gaussmf(filterBins,[filterSigma, filterMedian]);
        filterValuesNormed=filterValues/sum(filterValues);
        
        smoothedSpikeData = conv(NeuronStruct.posNSpikesAll{iNeuron},filterValuesNormed,'same');
        smoothedFRHz = smoothedSpikeData*sampleRate;
        rasterLength = length(startPoint:timeStepSize:endPoint);
    else
        rasterLength = length(startPoint:endPoint);
    end
    TurnRastersOneNeuron = nan(rasterLength,RANDOMINITSIZE);
    currentTurnRasterLength = size(TurnRastersOneNeuron,2);
    nPaths = length(RecStruct.Behavior.pathList{iRec});
    for iPath = 1:nPaths
        thisPath = RecStruct.Behavior.pathList{iRec}(iPath);
        if thisPath > 10
            continue; % ignoring paths > 10, don't want to debug.
        end
        thisPathPosMarkers = [RecStruct.Behavior.pathRunsLineMarkers{iRec}{iPath,1},RecStruct.Behavior.pathRunsLineMarkers{iRec}{iPath,2}];
        for iProgress = 1:nProgress
            if ~isnan(RecStruct.pathTurnBins(thisPath,iProgress)) % is NaN if not defined this path
                thisTurnCenter = RecStruct.pathTurnBins(thisPath,iProgress);
                % Beyond array handling
                if thisTurnCenter+startPoint < 2
                    preNans = 2-(thisTurnCenter+startPoint); % I don't want the end bins - sometimes things are weird there
                else
                    preNans = 0;
                end
                if thisTurnCenter+endPoint >= size(NeuronStruct.LinearRates{iNeuron,thisPath},1)
                    postNans = (thisTurnCenter+endPoint)- (size(NeuronStruct.LinearRates{iNeuron,thisPath},1)-1); % I don't want the end bins - sometimes things are weird there
                else
                    postNans = 0;
                end
                preNansArray = nan(preNans,1);
                postNansArray = nan(postNans,1);
                nTraversals = size(NeuronStruct.LinearRates{iNeuron,thisPath},2);
            else
                nTraversals = 0;
            end
            for iTraversal = 1:nTraversals
                posLinesThisRun = thisPathPosMarkers(iTraversal,1):thisPathPosMarkers(iTraversal,2);
                if isTimeAnalysis
                    turnApexIndex = findRightBin(thisTurnCenter, thisPathPosMarkers, iTraversal, posPathAndBin, posLinesThisRun, BINSEARCH);
                    if length(turnApexIndex) > 1
                        turnApexIndex = round(mean(turnApexIndex));
                    elseif isempty(turnApexIndex) % crap tracking, ditch this run.
                        continue;
                    end
                    perieventSmoothedFRHz = smoothedFRHz(floor(startPoint*sampleRate)+turnApexIndex:...
                        floor(endPoint*sampleRate)+turnApexIndex);
                    TurnRastersOneNeuron(:,iMaster) = blockproc(perieventSmoothedFRHz,[timeStepsInSamples,1],@(x) mean(x.data));
                    
                    TurnPositions3DOneNeuron{iMaster} = RecStruct.Behavior.posXY{iRec}(...
                        floor(startPoint*sampleRate)-timeStepsInSamples+turnApexIndex:...
                        floor(endPoint*sampleRate)+timeStepsInSamples+turnApexIndex,:);
                else
                    % Write to array
                    TurnRastersOneNeuron(:,iMaster) = [preNansArray;NeuronStruct.LinearRates{iNeuron,thisPath}(...
                        thisTurnCenter+startPoint+preNans:thisTurnCenter+endPoint-postNans,...
                        iTraversal,1);postNansArray];
                    
                    startBinPos = findRightBin(thisTurnCenter+startPoint+preNans, thisPathPosMarkers, iTraversal, posPathAndBin, posLinesThisRun, BINSEARCH);
                    endBinPos = findRightBin(thisTurnCenter+endPoint-postNans, thisPathPosMarkers, iTraversal, posPathAndBin, posLinesThisRun, BINSEARCH);
                    if isempty(startBinPos) || isempty(endBinPos) % crap tracking, ditch this run.
                        TurnPositions3DOneNeuron{iMaster} = ones(0,3);
                    else
                        startBinPos = startBinPos(1);
                        endBinPos = endBinPos(end);
                        TurnPositions3DOneNeuron{iMaster} = RecStruct.Behavior.posXY{iRec}(...
                            startBinPos:endBinPos,:);
                    end
                end
                switch RecStruct.turnLocations(thisPath,iProgress)
                    case 'A'
                        location(iMaster) = 10;
                    case 'B'
                        location(iMaster) = 11;
                    otherwise
                        location(iMaster) = str2double(RecStruct.turnLocations(thisPath,iProgress));
                end
                
                % Write related parameters
                action(iMaster) = RecStruct.pathTurnDirections(thisPath,iProgress);
                progress(iMaster) = iProgress;
                orientation(iMaster) = orientationCodes(thisPath,iProgress);
                route(iMaster) = thisPath;
                choice(iMaster) = RecStruct.turnChoiceMapping(iRec,location(iMaster));
                if isfield(RecStruct,'rewardParadigm')
                    taskStructure{iMaster} = RecStruct.rewardParadigm{iRec};
                else
                    taskStructure{iMaster} = '';
                end
                preRunEmpty(iMaster) = preNans;
                postRunEmpty(iMaster) = postNans;
                
                % Increment counter
                iMaster = iMaster+1;
                if iMaster == currentTurnRasterLength
                    TurnRastersOneNeuron = cat(2,TurnRastersOneNeuron, nan(...
                        rasterLength,RANDOMINCREMENTSIZE)); % add some more rows.
                    currentTurnRasterLength = size(TurnRastersOneNeuron,2);
                end
            end
        end
    end
    % Clean up extra rows
    extraNanRows = sum(all(isnan(TurnRastersOneNeuron),1));
    TurnRastersOneNeuron = TurnRastersOneNeuron(:,1:end-extraNanRows);
    % Save parameters
    rat = repmat(RecStruct.rat(iRec),length(action),1);
    rec = repmat(RecStruct.rec(iRec),length(action),1);
    neuron = repmat(iNeuron,length(action),1);
    action = action';
    location = location';
    progress = progress';
    orientation = orientation';
    route = route';
    choice = choice';
    taskStructure = taskStructure';
    preRunEmpty = preRunEmpty';
    postRunEmpty = postRunEmpty';
    TurnParameters{iNeuron} = table(rat, rec, neuron, action, location, progress, orientation, route,...
        choice, taskStructure, preRunEmpty, postRunEmpty);
    TurnRasters{iNeuron} = TurnRastersOneNeuron;
    TurnPosition3D{iNeuron} = TurnPositions3DOneNeuron;
    clear rat rec neuron action location progress orientation route choice taskStructure preRunEmpty postRunEmpty TurnPositions3DOneNeuron
end
goodNeurons = cellfun(@(x) ~isempty(x), TurnParameters);
TurnParameters = TurnParameters(goodNeurons);
TurnRasters = TurnRasters(goodNeurons);
TurnPosition3D = TurnPosition3D(goodNeurons);
end

function binIndex = findRightBin(thisTurnCenter, thisPathPosMarkers, iTraversal, posPathAndBin, posLinesThisRun, BINSEARCH)
origTurnCenter = thisTurnCenter;
iBinMoves = 1;
while iBinMoves <= length(BINSEARCH)
    binIndex = thisPathPosMarkers(iTraversal,1)-1+find(posPathAndBin(posLinesThisRun,2) == thisTurnCenter);
    if isempty(binIndex)% miss that bin - animal moving fast, or bad tracking. try the next one, until you find it, or give up.
        thisTurnCenter = origTurnCenter+BINSEARCH(iBinMoves);
        iBinMoves = iBinMoves+1;
    else
        return
    end
end
binIndex = []; % only hits here if it doesn't find it.
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
