function PopCorrResults = populationCorrAnalyses(MeanLinearRates, LinearRates, paths, doBootStrap)
%populationCorrAnalyses One line description of what the function or script performs (H1 line)
%		Optional file header info (to give more details about the function than in the H1 line)  
%
% 	Output = populationCorrAnalyses(neuronStruct, paths)
%
% INPUTS: 
%		input1 - Description
%		input2 - Description	                      
%
% OUTPUTS: 
%		output1 - Description
%
% EXAMPLES: 
%		Line 1 of example
%		Line 2 of exampleand 
%		Line 3 of example
%		                   
%
% REMARKS
%   All neurons must have meanLinearRates for all paths requested.
%             circular shift path data randomly, same for odd/even. keeps fields, keeps diagonal,
%             breaks field & location irregularities
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:
%
%
% CREATED BY: Jacob M Olson - based off Alex Johnson's code.
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 20-Mar-2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Init rng to random spot - for bootstrap
rng('shuffle');
  
% Paths Mean FR Matrix - nNeurons x nPositions
pathsCatFR = concatPathMeanFRs(MeanLinearRates, paths, doBootStrap);
[pathsCatFR_maxNormed, pathsCatFR_maxLocSorted, pathsCatFR_maxNormed_maxLocSorted] = maxNorming(pathsCatFR);

% Autocorrelations matrices
autoCorr = corr(pathsCatFR,pathsCatFR);
autoCorr_maxNormed = corr(pathsCatFR_maxNormed,pathsCatFR_maxNormed);

%% Odd/Even Run Analysis
% Odd/Even Runs - mean linear rates - iPath cells of nNeurons x nPositions
[pathsCatFR_oddMeanLinearRates, pathsCatFR_evenMeanLinearRates] = ...
    makeOddEvenMeanLinearRates(LinearRates, paths, doBootStrap);

% concat paths of Odd/Even Runs - Mean FR Matrix - nNeurons x nPositions(all paths concatenated)
% shift same amount for bootstrap - so diagonal from field sizes and consistency persists.
fullPathLength = size((cat(1,pathsCatFR_oddMeanLinearRates{1,paths})),1);
shiftMax = fullPathLength;
nNeu = size(pathsCatFR_oddMeanLinearRates,1);
shiftVals = randi(shiftMax,nNeu,1);
[pathsCatFR_odd] = concatPathMeanFRs(pathsCatFR_oddMeanLinearRates, paths, doBootStrap, shiftVals);
[pathsCatFR_even] = concatPathMeanFRs(pathsCatFR_evenMeanLinearRates, paths, doBootStrap, shiftVals);

% outputs of norming projects are concatenated in 3rd dim, so call (:,:,1) for odd, (:,:,2) for even
[oddEvenPathsCatFR_maxNormed, oddEvenPathsCatFR_maxLocSorted, oddEvenPathsCatFR_maxNormed_maxLocSorted] = ...
    maxNorming(pathsCatFR_odd,pathsCatFR_even);

% odd/even correlation matrices
oddEvenCorr = corr(pathsCatFR_odd,pathsCatFR_even);
oddEvenCorr_maxNormed = corr(oddEvenPathsCatFR_maxNormed(:,:,1),oddEvenPathsCatFR_maxNormed(:,:,2));

% Store outputs
PopCorrResults.oddEvenCorr_maxNormed = oddEvenCorr_maxNormed;
PopCorrResults.oddEvenCorr = oddEvenCorr;

if ~doBootStrap % Don't output for BS - array gets too big.
    PopCorrResults.autoCorr = autoCorr;
    PopCorrResults.autoCorr_maxNormed = autoCorr_maxNormed;
% PopCorrResults.pathsCatFR = pathsCatFR;
% PopCorrResults.pathsCatFR_maxNormed = pathsCatFR_maxNormed;
% PopCorrResults.pathsCatFR_maxLocSorted = pathsCatFR_maxLocSorted;
% PopCorrResults.pathsCatFR_maxNormed_maxLocSorted = pathsCatFR_maxNormed_maxLocSorted;
% 
% PopCorrResults.pathsCatFR_odd = pathsCatFR_odd;
% PopCorrResults.pathsCatFR_even = pathsCatFR_even;
% PopCorrResults.pathsCatFR_odd_maxNormed = oddEvenPathsCatFR_maxNormed(:,:,1);
% PopCorrResults.pathsCatFR_even_maxNormed = oddEvenPathsCatFR_maxNormed(:,:,2);
% PopCorrResults.pathsCatFR_odd_maxLocSorted = oddEvenPathsCatFR_maxLocSorted(:,:,1);
% PopCorrResults.pathsCatFR_even_maxLocSorted = oddEvenPathsCatFR_maxLocSorted(:,:,2);
% PopCorrResults.pathsCatFR_odd_maxNormed_maxLocSorted = oddEvenPathsCatFR_maxNormed_maxLocSorted(:,:,1);
% PopCorrResults.pathsCatFR_even_maxNormed_maxLocSorted = oddEvenPathsCatFR_maxNormed_maxLocSorted(:,:,2);
end

end

%% Helper Functions

% make the path FR matrices
function pathsCatFR = concatPathMeanFRs(meanLinearRates, paths, doBootStrap, shiftVals)
nNeu = size(meanLinearRates,1);
fullPathLength = size((cat(1,meanLinearRates{1,paths})),1);

if ~exist('shiftVals','var')
% Shift distance for bootstrap
shiftMax = fullPathLength;
shiftVals = randi(shiftMax,nNeu,1);
end
for iNeuron = 1:nNeu
    allPathsCat1Neu = cat(1,meanLinearRates{iNeuron,paths});
    if doBootStrap
        % circular shift path data randomly. keeps fields, keeps diagonal,
        % breaks field & location irregularities
        allPathsCat1Neu = circshift(allPathsCat1Neu,shiftVals(iNeuron),1); % rotate the rows
    end
    pathsCatFR(iNeuron,:) = allPathsCat1Neu(:,1)'; %#ok<AGROW>
end
end

%% make odd & even trial path FR matrices
function [oddMeanLinearRates, evenMeanLinearRates] = makeOddEvenMeanLinearRates(linearRates, paths, doBootStrap)
nNeu = size(linearRates,1);
nPaths = numel(paths);
nRuns = cellfun(@(x) size(x,2), linearRates(:,paths));
oddMeanLinearRates = cell(nNeu,max(paths));
evenMeanLinearRates = cell(nNeu,max(paths));
   
for iPath = 1:nPaths
    thisPathInd = paths(iPath);
    % Shift distance for bootstrap
    shiftMax = size(linearRates{1,thisPathInd},1);
    shifts = randi(shiftMax,nNeu,1);
    for iNeu = 1:nNeu
        dataOfInt = linearRates{iNeu,thisPathInd};
        if doBootStrap
            % circular shift path data randomly, same for odd/even. keeps fields, keeps diagonal,
            % breaks location irregularities
            oddMeanLinearRates{iNeu,thisPathInd} = circshift(mean(dataOfInt(:,1:2:nRuns(iNeu,iPath),1),2),shifts(iNeu));
            evenMeanLinearRates{iNeu,thisPathInd} = circshift(mean(dataOfInt(:,2:2:nRuns(iNeu,iPath),1),2),shifts(iNeu));
        else
            oddMeanLinearRates{iNeu,thisPathInd} = mean(dataOfInt(:,1:2:nRuns(iNeu,iPath),1),2);
            evenMeanLinearRates{iNeu,thisPathInd} = mean(dataOfInt(:,2:2:nRuns(iNeu,iPath),1),2);
        end
    end
end
end


%% max norm within rows of matrices - if given more than 1 matrix, does rows for both together (e.g., for odd/even)
function [normedPathsCatFR, pathsCatFR_MaxLocSorted, normedPathsCatFR_MaxLocSorted] = maxNorming(varargin)
% arguments in must be 2D matrices of the same size.

[nRows, nCols] = size(varargin{1});
% put all inputs together - maxing along rows, so concat rows.
catMats = cat(2,varargin{:});

[maxFRByNeu, maxFRByNeuInd] = max(catMats,[],2);
% max norming - divide all values for each neuron by that neuron (row) max
normedPathsCatFR = catMats./maxFRByNeu;
normedPathsCatFR(isnan(normedPathsCatFR)) = 0; % divide by zero errors. Putting the 0s back at 0.

[~, rowInd] = sort(maxFRByNeuInd);
pathsCatFR_MaxLocSorted = catMats(rowInd,:);
normedPathsCatFR_MaxLocSorted = normedPathsCatFR(rowInd,:);


% outputting in page fashion - so user can call (:,:,i) to get their values in same order put in.
normedPathsCatFR = reshape(normedPathsCatFR, [nRows, nCols, nargin]);
pathsCatFR_MaxLocSorted = reshape(pathsCatFR_MaxLocSorted, [nRows, nCols, nargin]);
normedPathsCatFR_MaxLocSorted = reshape(normedPathsCatFR_MaxLocSorted, [nRows, nCols, nargin]);
end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


