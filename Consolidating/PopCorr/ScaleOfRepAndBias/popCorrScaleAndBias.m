function SandBCorrResults = popCorrScaleAndBias(PopCorr_All, PopCorr_BS, chosenCorrMatForAnalysis, ScaleAndBiasSettings)
% Main Scale and Bias Analysis Function from 2021 Olson, Alexander, ... Nitz Sub/CA1 paper

%% Initialize some helpful values from inputs.
nCorrThresh = numel(ScaleAndBiasSettings.CORR_THRESHOLDS);
nPaths = numel(ScaleAndBiasSettings.PATHS);

%%  Scale of Representation and Bias Analysis including bootstrapped stats

% transform bootsrapped results (BEWARE, SLOW) for input to function below.
% (turn cell array of mats into one mat, and properly reshape) 
oddEvenCorrChosen_BS = cell2mat(reshape(cellfun(@(x) x.(chosenCorrMatForAnalysis),PopCorr_BS,...
    'UniformOutput', false),[1,1,ScaleAndBiasSettings.N_BS_ITERATIONS]));

% Scale of Rep and Bias analysis for all thresholds.
SandBCorrResults.coreStats = scaleAndBiasCore(...
    PopCorr_All.(chosenCorrMatForAnalysis), oddEvenCorrChosen_BS,...
    ScaleAndBiasSettings.BS_PERCENTILE_THRESHOLD, ScaleAndBiasSettings.CORR_THRESHOLDS, ...
    ScaleAndBiasSettings.PATHS, ScaleAndBiasSettings.PATH_LENGTHS,...
    ScaleAndBiasSettings.DIFF_OFFSET, ScaleAndBiasSettings.DIFF_SMOOTH_STD, ScaleAndBiasSettings.DO_BS);

% append the correlation matrix used
SandBCorrResults.corrMarixUsed = chosenCorrMatForAnalysis;

%% Find "Transitions" Zones, then peaks.
% Zones will be areas above theshold.
% Peaks will be highest point in that zone.
[SandBCorrResults.transitionZone, SandBCorrResults.transitionPeaks] = findTransitions(...
    SandBCorrResults.coreStats.biasVal_Smoothed_Diff_Smoothed,...
    SandBCorrResults.coreStats.biasVal_Smoothed_Diff_Smoothed_Thresholds,...
    nCorrThresh, nPaths, ScaleAndBiasSettings.PATH_LENGTHS, ScaleAndBiasSettings.DIFF_OFFSET);

%% Turn Paths into % from nearest turn
SandBCorrResults.pathPropDistFromTurn = calcPathPropDistFromTurn(ScaleAndBiasSettings.PATH_LENGTHS);

end

%% *********************************  Helper functions  *********************************

function pathPropDistFromTurn = calcPathPropDistFromTurn(PATH_LENGTHS)
%% Turn Paths into % from nearest turn
PATH_TURN_MAT = [...
    51,86,118,140;...
    51,86,118,140;...
    51,86,118,140;...
    51,86,118,140;...
    15,127, 207, NaN;...
    15,127, 207, NaN]; % Turns here are marked beyond end of run - will have to deal with below.

for iPath = 1:size(PATH_TURN_MAT,1)
    startIndex = 1;
    for iTurnIndex = 1:size(PATH_TURN_MAT,2)
        if isnan(PATH_TURN_MAT(iPath,iTurnIndex))
            break;
        end
        
        straightLength = PATH_TURN_MAT(iPath,iTurnIndex)-startIndex+1;
        pathStraightPercent{iPath}(startIndex:PATH_TURN_MAT(iPath,iTurnIndex)) = 0:1/(straightLength-1):1;
        startIndex = PATH_TURN_MAT(iPath,iTurnIndex);
        
    end
    if numel(pathStraightPercent{iPath}) > PATH_LENGTHS(iPath)
        pathStraightPercent{iPath} = pathStraightPercent{iPath}(1:PATH_LENGTHS(iPath));
    end
    
    pathStraightPercentBackwards{iPath} = pathStraightPercent{iPath}-1;
    [~,turnBackOrForwards] = min([abs(pathStraightPercent{iPath})',abs(pathStraightPercentBackwards{iPath})'],[],2);
    
    pathPropDistFromTurn{iPath} = pathStraightPercent{iPath};
    pathPropDistFromTurn{iPath}(turnBackOrForwards==2) = pathStraightPercentBackwards{iPath}(turnBackOrForwards==2);
end
end




