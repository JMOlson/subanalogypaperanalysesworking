function [transitionZoneArray, transitionPeaksArray] = findTransitions(dataArray, thresholdArray, ...
        nCorrThresh, nPaths, pathLengths, diffOffset)
%SSSSSS One line description of what the function or script performs (H1 line)
%		Optional file header info (to give more details about the function than in the H1 line)  
%
% 	[output1, output2] = ssssss(input1, input2)
%
% INPUTS: 
%		thresholdArray - nThresh x nPaths array, 
%                        inside are pathLength-diffDistance x 2 (low, high thresholds)
%		input2 - Description
%		input3 - Description
%		                      
%
% OUTPUTS: 
%		output1 - Description
%		output2 - Description
%		output3 - Description
%		                       
%
% EXAMPLES: 
%		Line 1 of example
%		Line 2 of example
%		Line 3 of example
%		                   
%
% REMARKS
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:%
% CREATED WITH MATLAB VERSION: 9.6.0.1150989 (R2019a) Update 4 on Microsoft Windows 10 Enterprise Version 10.0 (Build 18363)
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 20-Mar-2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Threshold order
if thresholdArray{1,1}(1,1) > thresholdArray{1,1}(1,2)
    highThresh = 1;
    lowThresh = 2;
else
    lowThresh = 1;
    highThresh = 2;
end

%% Find "Transitions" Zones, then peaks.
% Zones will be areas above theshold.
% Peaks will be highest point in that zone.

transitionZoneArray = cell(nPaths, nCorrThresh);
transitionPeaksArray = cell(nPaths, nCorrThresh);
for iThresh = 1:nCorrThresh
    for iPath = 1:nPaths
        diffOffsetXs = round(diffOffset/2)+1:pathLengths(iPath)-round(diffOffset/2);
        dataVec = dataArray{iThresh,iPath};
        sigVal_Low = thresholdArray{iThresh,iPath}(:,lowThresh);
        sigVal_High = thresholdArray{iThresh,iPath}(:,highThresh);
        
        transitionZone = zeros([0,2]);
        transitionPeaks = zeros([0,1]);
        % init for each path/vector
        readyForTransitionZone = false; % start w/ non-peak state.
        ongoingTransitionZone = false; % start w/ non-peak state.
        nTransitionZones = 0;
        nDataPoints = numel(dataVec);
        % walk through vector
        for iLoc = 1:nDataPoints
            if dataVec(iLoc) < sigVal_Low(iLoc)
                % once value is sig. negative for first time, set "ready for transition zone (peak)"
                readyForTransitionZone = true;
                
                
                if ongoingTransitionZone
                    % if there was an ongoing transition zone, but it stops (sig goes neg), end that zone.
                    % calc peak of that transition zone, store everything, and set "ready for new
                    % transition zone" as above.
                    transitionZone(nTransitionZones,:) =  [diffOffsetXs(transitionStart),diffOffsetXs(transitionStop)];
                    
                    [~,maxInd] = max(dataVec(transitionStart:transitionStop));
                    transitionPeaks(nTransitionZones) = diffOffsetXs(transitionStart)-1+maxInd;
                    
                    ongoingTransitionZone = false;
                end
                
            elseif dataVec(iLoc) > sigVal_High(iLoc) && ongoingTransitionZone
                % ongoing transition zone update this as last point
                transitionStop = iLoc;
                
            elseif dataVec(iLoc) > sigVal_High(iLoc) && readyForTransitionZone
                % Begin a transition zone. Set state, record start & stop points
                ongoingTransitionZone = true;
                readyForTransitionZone = false;
                
                nTransitionZones = nTransitionZones + 1;
                transitionStart = iLoc;
                transitionStop = iLoc;
            end
            if iLoc == nDataPoints && ongoingTransitionZone
                % if a peak still exists at end of run, close & calc as when things go Sig neg.
                % but if the peak is the last point, then we haven't reached the peak yet, so
                % ignore.
                
                % calc peak of that transition zone, 
                [~,maxInd] = max(dataVec(transitionStart:transitionStop));
                if transitionStart-1+maxInd ~= nDataPoints
                    % store everything, and set "ready for new transition zone"
                    transitionZone(nTransitionZones,:) =  [diffOffsetXs(transitionStart),diffOffsetXs(transitionStop)];
                    transitionPeaks(nTransitionZones) = diffOffsetXs(transitionStart)-1+maxInd;
                else
                    % ignore attempted transition zone
                    nTransitionZones = nTransitionZones - 1;
                end
                ongoingTransitionZone = false;
            end
        end
        transitionZoneArray{iPath,iThresh} = transitionZone;
        transitionPeaksArray{iPath,iThresh} = transitionPeaks;
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
