function [scaleOfRep, biasVal, afterScaleVal, previousScaleVal, thresholdMask] = calcScaleOfRep(CORRMAT, BENCHMARK, PATH_LENGTHS, PATHS)
%SSSSSS Scale of Representation Helper Function - SubCA1 Analogy Paper
%		Optional file header info (to give more details about the function than in the H1 line)  
%
% 	[output1, output2] = ssssss(input1, input2)
%
% INPUTS: 
%		input1 - Description
%		input2 - Description
%		input3 - Description
%		                      
%
% OUTPUTS: 
%		output1 - Description
%		output2 - Description
%		output3 - Description
%		                       
%
% EXAMPLES: 
%   PATHS = [1,2,3,4,9,10];
%   PATH_LENGTHS = [140,140,140,140,197,197];
%   HIGH_FID_BENCHMARK = 0.5;
%   LOW_FID_BENCHMARK = 0.25;
%   CORRMAT = HpcPopCorr_All.oddEvenCorr_maxNormed;
% 		                   
%
% REMARKS
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:%
% CREATED WITH MATLAB VERSION: 9.6.0.1150989 (R2019a) Update 4 on Microsoft Windows 10 Enterprise Version 10.0 (Build 18363)
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 20-Mar-2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Initialize Variables
previousScaleVal = cell(1,numel(PATHS));
afterScaleVal = cell(1,numel(PATHS));

startIndex = 1;
endIndex = 0;

%
for iPath = 1:numel(PATHS)
    endIndex = endIndex + PATH_LENGTHS(iPath);
    
    afterScaleVal{iPath} = nan(PATH_LENGTHS(iPath),1);
    previousScaleVal{iPath} = nan(PATH_LENGTHS(iPath),1);
    
    % Grab the portion of the correlation matrix for one route
    relData = CORRMAT(startIndex:endIndex,startIndex:endIndex);
    relDataMask = zeros(size(relData));
    relDataMaskAnySpot = ones(size(relData));
    % split into above and below diagonal, compare to benchmark
    % mask - below threshold = 1
    relDataUpper = triu(relData < BENCHMARK);
    relDataLower = tril(relData < BENCHMARK);
    
    % find first benchmark crossings
    for iPos = 1:PATH_LENGTHS(iPath)
        %looking at one direction from diagonal 
        if sum(relDataUpper(iPos,:)) % there are values below benchmark
            lastCrossing = find(relDataUpper(iPos,:),1,'first');
            afterScaleVal{iPath}(iPos) = lastCrossing-iPos;
        else % no values below benchmark
            lastCrossing = PATH_LENGTHS(iPath);
            afterScaleVal{iPath}(iPos) = lastCrossing-iPos;
        end
        %looking at other direction from diagonal 
        if sum(relDataLower(iPos,:))
            firstCrossing = find(relDataLower(iPos,:),1,'last');
            previousScaleVal{iPath}(iPos) = -1*firstCrossing+iPos;
        else
            firstCrossing = 1;
            previousScaleVal{iPath}(iPos) = -1*firstCrossing+iPos;
        end
        relDataMask(iPos,firstCrossing+1:lastCrossing-1) = 1;
    end
    relDataMaskAnySpot(relDataUpper | relDataLower) = 0;
    
%     scaleOfRep{iPath} = max(leftVal{iPath},rightVal{iPath});
    scaleOfRep{iPath} = previousScaleVal{iPath}+afterScaleVal{iPath};
    biasVal{iPath} = afterScaleVal{iPath}-previousScaleVal{iPath};
    thresholdMask{iPath} = relDataMask;
    
    % update counter
    startIndex = startIndex + PATH_LENGTHS(iPath);
end

end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%