function ScaleAndBiasStats = scaleAndBiasCore(corrMatrix, corrMatrixBS,...
        percentiles, corrThresholds, paths, pathLengths, diffOffset, diffSmoothStd, doBS)
%scaleAndBiasCore Helper Fn for Fig3 CorrMat analysis in 2020 SubCA1 paper.
%		Calculates the Figure 3 scale of representation and bias direction for real data and a
%		shuffled distribution and returns the analysis results and the BS percentiles of the
%       distribution given.
%
% 	ScaleAndBiasStats = scaleAndBiasCore(corrMatrix, corrMatrixBS,...
%        percentiles, corrThresholds, paths, pathLengths, diffOffset, diffSmoothStd, doBS)
%
% INPUTS: 
%		corrMatrix - correlation matrix - NxN matrix - we used postiional data, odd trials
%            correlated w/ even at every position n in N
%		corrMatrixBS - correlation matrix - NxNxM - Same data as corrMatrix, but already shuffled
%           M is number of shuffles completed
%           if empty matrix, [], will be ignored.
%       percentiles - values to use for percent thresholds for statistical comparators
%		corrThresholds - vector of values to use as thresholds for correlations in matrix
%		paths - vector integers representing the IDs of paths to analyze
%		pathLengths - vector of ints. lengths of the corresponding paths in paths variable
%		diffOffset - Window size to use for "derivative" operation for scale of rep analysis
%		diffSmoothStd - standard deviation of gaussian smoothing kernel to use in the two smoothing
%           steps
%		doBS - [OPTIONAL] - flag for whether or not to do bootstrap analysis. Defaults to true,
%           unless corrMatrixBS is empty
%
% OUTPUTS: 
%		ScaleAndBiasStats - struct with the following fields - each is a M x N cell array with 
%                           for M thresholds and N paths. each cell contains a vector of lenght P
%                           which equals the path length
%           rightVal - # of datapoints right of (after, ahead of animal) diagonal before value below threshold 
%           leftVal - # of datapoints left of (befpre, behind animal)  diagonal before value below threshold 
%           scaleOfRep - number of datapoints around diagonal above given theshold (left+right)
%           biasVal - bias to similarity before/after for the given datapoint (right - left)
%           biasVal_Smoothed - gaussian smoothing kernel applied to biasVal
%           biasVal_Smoothed_Diff - 'derivative' of bias value - change - using diffOffset
%           biasVal_Smoothed_Diff_Smoothed - gaussian smoothing kernel on biasVal_Smooothed_Diff
%           
%           For all of the following fields, fields are the same as above, but percentiles of the 
%               shuffled response for statistical comparions.
%           Each cells is a P x Q matrix, with P equals path length, and Q is the percentiles 
%
%           scaleOfRep_Thresholds - as above
%           biasVal_Thresholds - as above
%           biasVal_Smoothed_Thresholds - as above
%           biasVal_Smoothed_Diff_Thresholds - as above
%           biasVal_Smoothed_Diff_Smoothed_Thresholds - as above
%
% EXAMPLES: 
%		Line 1 of example
%		Line 2 of example
%		Line 3 of example
%		                   
%
% REMARKS
%       smoothdata's 3rd parameter when using a gaussian smoothing kernel is a window size
%       the standard deviation of the gaussian is 1/5 of the window size chosen, hence the 5 below.
%       https://www.mathworks.com/matlabcentral/answers/406563-how-does-smoothdata-function-using-gaussian-method-define-the-standard-deviation-for-different-w%
%
% OTHER M-FILES REQUIRED: 
% SUBFUNCTIONS: 
% MAT-FILES REQUIRED: 
%
% SEE ALSO:%
% CREATED WITH MATLAB VERSION: 9.6.0.1150989 (R2019a) Update 4 on Microsoft Windows 10 Enterprise Version 10.0 (Build 18363)
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 20-Mar-2020
% LAST MODIFIED BY: 
% LAST MODIFIED ON: 
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

OUTBOUNDS = [1:4];
RETURNS = [5,6];

%% input handling, calc needed shape values and window conversion.
if ~exist('doBS','var')
    doBS = true;
end

if isempty(corrMatrixBS)
    warning('The correlation matrix of shuffles is empty, so no thresholds on the order of the shuffles will be conducted.');
    doBS = false;
end

nCorrThresh = numel(corrThresholds);
nPaths = numel(paths);
windowSize = diffSmoothStd*5; % see Remark section in Fn helper section.

if doBS
    nBsIter = size(corrMatrixBS,3);
    
    percentilesInd  = [round((nBsIter/100)*percentiles),...
        round((nBsIter/100)*(100-percentiles))];
end

%% Initialize
scaleOfRep = cell(nCorrThresh,1);
biasVal = cell(nCorrThresh,1);
rightVal = cell(nCorrThresh,1);
leftVal = cell(nCorrThresh,1);

biasVal_Smoothed  = cell(nCorrThresh,1);
biasVal_Smoothed_Diff  = cell(nCorrThresh,1);
biasVal_Smoothed_Diff_Smoothed  = cell(nCorrThresh,1);

scaleOfRep_BS = cell(nCorrThresh,1);
biasVal_BS = cell(nCorrThresh,1);
biasVal_Smoothed_BS = cell(nCorrThresh,1);

scaleOfRep_BS_Thresholds = cell(nCorrThresh, nPaths);
biasVal_BS_Thresholds = cell(nCorrThresh, nPaths);
biasVal_Smoothed_BS_Thresholds = cell(nCorrThresh, nPaths);
biasVal_Smoothed_Diff_BS_Thresholds = cell(nCorrThresh, nPaths);
biasVal_Smoothed_Diff_Smoothed_BS_Thresholds = cell(nCorrThresh, nPaths);

biasVar = nan(nCorrThresh,1);
biasVar_BS = nan(nCorrThresh,nBsIter);
biasVar_BS_Thresholds = cell(nCorrThresh,1);

biasVal_Smoothed_BS_Outbound_Thresholds = cell(nCorrThresh,1);
biasVal_Smoothed_Diff_BS_Outbound_Thresholds = cell(nCorrThresh,1);
biasVal_Smoothed_Diff_Smoothed_BS_Outbound_Thresholds = cell(nCorrThresh,1);

biasVal_Smoothed_BS_Return_Thresholds = cell(nCorrThresh,1);
biasVal_Smoothed_Diff_BS_Return_Thresholds = cell(nCorrThresh,1);
biasVal_Smoothed_Diff_Smoothed_BS_Return_Thresholds = cell(nCorrThresh,1);

%% Calc scale of Representation & bias for all thresholds.
for iThresh = 1:nCorrThresh
    [scaleOfRep{iThresh}, biasVal{iThresh}, rightVal{iThresh}, leftVal{iThresh}] = ...
        calcScaleOfRep(corrMatrix, corrThresholds(iThresh), pathLengths, paths);

    biasVar(iThresh) = var(vertcat(biasVal{iThresh}{:}));
    % Smooth bias val data & BS'd data - changes are noisy at a bin by bin level
    biasVal_Smoothed{iThresh} = cellfun(@(x) smoothdata(x,'gaussian',windowSize),...
        biasVal{iThresh},'UniformOutput',false);
    
    % find rate of change, using a larger window (diffOffset) than just adjoining points, on the smoothed bias val data
    % This captures slower dynamics better, which is what we are interested in.
    biasVal_Smoothed_Diff{iThresh} = cellfun(@(x)...
        (x(diffOffset+1:end)-x(1:end-diffOffset))./diffOffset,...
        biasVal_Smoothed{iThresh},'UniformOutput',false);
    
    % smooth the derivative of the smoothed bias val data 
    % again, looking for the low frequency trends    
    biasVal_Smoothed_Diff_Smoothed{iThresh} = cellfun(@(x)...
        smoothdata(x,'gaussian',windowSize),...
        biasVal_Smoothed_Diff{iThresh},'UniformOutput',false);
    
    if doBS
        nBsIter = size(corrMatrixBS,3);
        for iBSIter = 1:nBsIter
            [scaleOfRep_BS{iThresh}(iBSIter,1:nPaths), biasVal_BS{iThresh}(iBSIter,1:nPaths)] = ...
                calcScaleOfRep(corrMatrixBS(:,:,iBSIter),  corrThresholds(iThresh), pathLengths, paths);
            
            % gupta redish comparison
            biasVar_BS(iThresh,iBSIter) = var(vertcat(biasVal_BS{iThresh}{iBSIter,:}));
        end
        
        biasVar_BS_Sorted = sort(biasVar_BS(iThresh,:),'descend');
        biasVar_BS_Thresholds{iThresh} = biasVar_BS_Sorted(:,percentilesInd);
        
        biasVal_Smoothed_BS{iThresh} = cellfun(@(x) smoothdata(x,'gaussian',windowSize),biasVal_BS{iThresh},'UniformOutput',false);
        
        biasVal_Smoothed_BS_Smash = cell(1,1,nPaths); % shape is helpful for catenating later
        biasVal_Smoothed_Diff_BS_Smash = cell(1,1,nPaths); 
        biasVal_Smoothed_Diff_Smoothed_BS_Smash = cell(1,1,nPaths); 
        
        for iPath = 1:nPaths
            % Put BS runs together, so I can sort and find percentiles
            scaleOfRep_BS_Smash = cell2mat(scaleOfRep_BS{iThresh}(:,iPath)');
            biasVal_BS_Smash = cell2mat(biasVal_BS{iThresh}(:,iPath)');
            biasVal_Smoothed_BS_Smash{iPath} = cell2mat(biasVal_Smoothed_BS{iThresh}(:,iPath)');
            
            % run a derivative on the smoothed bias val data - for BS data
            biasVal_Smoothed_Diff_BS_Smash{iPath} = ...
                (biasVal_Smoothed_BS_Smash{iPath}(diffOffset+1:end,:)-...
                biasVal_Smoothed_BS_Smash{iPath}(1:end-diffOffset,:))./diffOffset;
            
            % smooth the derivative of the smoothed bias val data - for BS data
            biasVal_Smoothed_Diff_Smoothed_BS_Smash{iPath} = ...
                smoothdata(biasVal_Smoothed_Diff_BS_Smash{iPath},2,'gaussian',windowSize);
            
            % Sort BS values, find thresholds
            scaleOfRep_Sorted = sort(scaleOfRep_BS_Smash,2,'descend');
            biasVal_Sorted = sort(biasVal_BS_Smash,2,'descend');
            biasVal_Smoothed_Sorted = sort(biasVal_Smoothed_BS_Smash{iPath},2,'descend');
            biasVal_Smoothed_Diff_Sorted = sort(biasVal_Smoothed_Diff_BS_Smash{iPath},2,'descend');
            biasVal_Smoothed_Diff_Smoothed_Sorted = sort(biasVal_Smoothed_Diff_Smoothed_BS_Smash{iPath},2,'descend');
            
            
            scaleOfRep_BS_Thresholds{iThresh,iPath} = scaleOfRep_Sorted(:,percentilesInd);
            biasVal_BS_Thresholds{iThresh,iPath} = biasVal_Sorted(:,percentilesInd);
            biasVal_Smoothed_BS_Thresholds{iThresh,iPath} = ...
                biasVal_Smoothed_Sorted(:,percentilesInd);
            biasVal_Smoothed_Diff_BS_Thresholds{iThresh,iPath} = ...
                biasVal_Smoothed_Diff_Sorted(:,percentilesInd);
            biasVal_Smoothed_Diff_Smoothed_BS_Thresholds{iThresh,iPath} = ...
                biasVal_Smoothed_Diff_Smoothed_Sorted(:,percentilesInd);

        end
        % outbound and return path avergaing so thresholds are appropriate height for avg plots.
        biasVal_Smoothed_BS_Outbound =  mean(cell2mat(biasVal_Smoothed_BS_Smash(OUTBOUNDS)),3);
        biasVal_Smoothed_Diff_BS_Outbound = mean(cell2mat(biasVal_Smoothed_Diff_BS_Smash(OUTBOUNDS)),3);
        biasVal_Smoothed_Diff_Smoothed_BS_Outbound = ...
            mean(cell2mat(biasVal_Smoothed_Diff_Smoothed_BS_Smash(OUTBOUNDS)),3);
    
        biasVal_Smoothed_BS_Return =  mean(cell2mat(biasVal_Smoothed_BS_Smash(RETURNS)),3);
        biasVal_Smoothed_Diff_BS_Return = mean(cell2mat(biasVal_Smoothed_Diff_BS_Smash(RETURNS)),3);
        biasVal_Smoothed_Diff_Smoothed_BS_Return = ...
            mean(cell2mat(biasVal_Smoothed_Diff_Smoothed_BS_Smash(RETURNS)),3);
        
        % Sort
        biasVal_Smoothed_BS_Outbound_Sorted =  sort(biasVal_Smoothed_BS_Outbound,2,'descend');
        biasVal_Smoothed_Diff_BS_Outbound_Sorted = sort(biasVal_Smoothed_Diff_BS_Outbound,2,'descend');
        biasVal_Smoothed_Diff_Smoothed_BS_Outbound_Sorted = ...
            sort(biasVal_Smoothed_Diff_Smoothed_BS_Outbound,2,'descend');
    
        biasVal_Smoothed_BS_Return_Sorted =  sort(biasVal_Smoothed_BS_Return,2,'descend');
        biasVal_Smoothed_Diff_BS_Return_Sorted = sort(biasVal_Smoothed_Diff_BS_Return,2,'descend');
        biasVal_Smoothed_Diff_Smoothed_BS_Return_Sorted = ...
            sort(biasVal_Smoothed_Diff_Smoothed_BS_Return,2,'descend');
        
        % Get Thresholds
        biasVal_Smoothed_BS_Outbound_Thresholds{iThresh} = biasVal_Smoothed_BS_Outbound_Sorted(:,percentilesInd);
        biasVal_Smoothed_Diff_BS_Outbound_Thresholds{iThresh} = biasVal_Smoothed_Diff_BS_Outbound_Sorted(:,percentilesInd);
        biasVal_Smoothed_Diff_Smoothed_BS_Outbound_Thresholds{iThresh} = ...
            biasVal_Smoothed_Diff_Smoothed_BS_Outbound_Sorted(:,percentilesInd);
    
        biasVal_Smoothed_BS_Return_Thresholds{iThresh} = biasVal_Smoothed_BS_Return_Sorted(:,percentilesInd);
        biasVal_Smoothed_Diff_BS_Return_Thresholds{iThresh} = biasVal_Smoothed_Diff_BS_Return_Sorted(:,percentilesInd);
        biasVal_Smoothed_Diff_Smoothed_BS_Return_Thresholds{iThresh} = ...
            biasVal_Smoothed_Diff_Smoothed_BS_Return_Sorted(:,percentilesInd);
    end
end

%% Assign outputs.

% Actual Data
% reshape outputs here to make N x 1 cell array of 1 x M cell arrays into N x M cell matrix
ScaleAndBiasStats.scaleOfRep = vertcat(scaleOfRep{:}); 
ScaleAndBiasStats.biasVal = vertcat(biasVal{:}); 
ScaleAndBiasStats.rightVal = vertcat(rightVal{:}); 
ScaleAndBiasStats.leftVal = vertcat(leftVal{:}); 
ScaleAndBiasStats.biasVal_Smoothed = vertcat(biasVal_Smoothed{:}); 
ScaleAndBiasStats.biasVal_Smoothed_Diff = vertcat(biasVal_Smoothed_Diff{:}); 
ScaleAndBiasStats.biasVal_Smoothed_Diff_Smoothed = vertcat(biasVal_Smoothed_Diff_Smoothed{:}); 
ScaleAndBiasStats.biasVar = biasVar;
if doBS
    % Bootstrapped Results
    ScaleAndBiasStats.scaleOfRep_Thresholds = scaleOfRep_BS_Thresholds;
    ScaleAndBiasStats.biasVal_Thresholds = biasVal_BS_Thresholds;
    ScaleAndBiasStats.biasVal_Smoothed_Thresholds = biasVal_Smoothed_BS_Thresholds;
    ScaleAndBiasStats.biasVal_Smoothed_Diff_Thresholds = biasVal_Smoothed_Diff_BS_Thresholds;
    ScaleAndBiasStats.biasVal_Smoothed_Diff_Smoothed_Thresholds = biasVal_Smoothed_Diff_Smoothed_BS_Thresholds;
    ScaleAndBiasStats.biasVar_Thresholds = biasVar_BS_Thresholds;
    
    ScaleAndBiasStats.biasVal_Smoothed_Outbound_Thresholds = biasVal_Smoothed_BS_Outbound_Thresholds;
    ScaleAndBiasStats.biasVal_Smoothed_Diff_Outbound_Thresholds = biasVal_Smoothed_Diff_BS_Outbound_Thresholds;
    ScaleAndBiasStats.biasVal_Smoothed_Diff_Smoothed_Outbound_Thresholds = biasVal_Smoothed_Diff_Smoothed_BS_Outbound_Thresholds;
    ScaleAndBiasStats.biasVal_Smoothed_Return_Thresholds = biasVal_Smoothed_BS_Return_Thresholds;
    ScaleAndBiasStats.biasVal_Smoothed_Diff_Return_Thresholds = biasVal_Smoothed_Diff_BS_Return_Thresholds;
    ScaleAndBiasStats.biasVal_Smoothed_Diff_Smoothed_Return_Thresholds = biasVal_Smoothed_Diff_Smoothed_BS_Return_Thresholds;
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


