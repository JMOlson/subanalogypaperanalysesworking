function [ DataStruct, NeuronStruct ] = hdRateMapperHalves( DataStruct, NeuronStruct, nTimeSegments)
%HDRATEMAPPERHALVES Calc cell firing rate at each head direction (binned) for odd and even minutes
%separately. Will  use as training and test set data.
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, December 2015.


binSizeDegrees = DataStruct.binSizeDegrees;
nHDBins = NeuronStruct.HDMapsNBins;
% IMPORTANT: Not doing track rotations for the main dataset. Change line 29
% to 3 if you want to include track rotations/the 2nd manipulation for the
% rec.
iRecCount = 1;
iRecOld = NeuronStruct.rec(1);
for iOverallNeuron = 1:length(NeuronStruct.neuronIndex)
    iNeuHDRadians = NeuronStruct.posHDRadiansAllCorrected{iOverallNeuron};
    iNeuPath = NeuronStruct.posPathAndBinAll{iOverallNeuron}(:,1);
    iNeuNSpikes = NeuronStruct.posNSpikesAll{iOverallNeuron};
        
    iRecNew = NeuronStruct.rec(iOverallNeuron);
    if iRecOld == iRecNew
        NeuronStruct.recCount(iOverallNeuron) = iRecCount;
    else
        iRecOld = iRecNew;
        iRecCount = iRecCount+1;
        NeuronStruct.recCount(iOverallNeuron) = iRecCount;
    end
    timeStamps = DataStruct.timeStamps{iRecCount};
    for iTimeSegment = 1:nTimeSegments
        for iHalf = 1:2
            if iTimeSegment == 1
                minutesToChunk = 1;
            else
                minutesToChunk = 2;
            end
            %Alternate every other minute (or 2 minutes) into half 1 and half 2.
            [hdRadiansRelevant,posNSpikesRelevant, runValsRelevant] = timeSplitter(timeStamps,...
                iTimeSegment,iHalf,DataStruct.sampleRate(iRecCount),...
                iNeuHDRadians,iNeuNSpikes,...
                DataStruct.runningLineIndicesBySegment{iTimeSegment,iRecCount},minutesToChunk);
            
        % note for the line below:
        % mod to get all positive values.
        % round to nearest bin center.
        hdRadiansRelevantBinned =...
            round(mod(radtodeg(hdRadiansRelevant),360)/binSizeDegrees);
        % put all values from bin 0 into the last bin (same bin, circle).
        hdRadiansRelevantBinned(hdRadiansRelevantBinned==0) = nHDBins;
        hdRadiansRelevantBinned(isnan(hdRadiansRelevant)) = NaN;
        
        posNSpikesRelevantRunning = posNSpikesRelevant(runValsRelevant);
        posNSpikesRelevantStill = posNSpikesRelevant(~runValsRelevant);
        hdRadiansRelevantRunningBinned = hdRadiansRelevantBinned(runValsRelevant);
        hdRadiansRelevantStillBinned = hdRadiansRelevantBinned(~runValsRelevant);
        
        hdOccs=zeros(nHDBins,1);
        hdSpikes=zeros(nHDBins,1);
        hdRates=zeros(nHDBins,1);
        
        hdOccsRunning=zeros(nHDBins,1);
        hdSpikesRunning=zeros(nHDBins,1);
        hdRatesRunning=zeros(nHDBins,1);
        
        hdOccsStill=zeros(nHDBins,1);
        hdSpikesStill=zeros(nHDBins,1);
        hdRatesStill=zeros(nHDBins,1);
        
        for iHDBins = 1:nHDBins
            hdSpikes(iHDBins) = sum(posNSpikesRelevant(hdRadiansRelevantBinned == iHDBins));
            hdOccs(iHDBins) = sum(hdRadiansRelevantBinned == iHDBins);
            
            hdSpikesRunning(iHDBins) = sum(posNSpikesRelevantRunning(hdRadiansRelevantRunningBinned == iHDBins));
            hdOccsRunning(iHDBins) = sum(hdRadiansRelevantRunningBinned == iHDBins);
            
            hdSpikesStill(iHDBins) = sum(posNSpikesRelevantStill(hdRadiansRelevantStillBinned == iHDBins));
            hdOccsStill(iHDBins) = sum(hdRadiansRelevantStillBinned == iHDBins);
        end
        isOcc = hdOccs>0;
        hdRates(isOcc) = (hdSpikes(isOcc)./hdOccs(isOcc))*DataStruct.sampleRate(iRecCount);
        hdRates(~isOcc) = NaN;
        
        isOccRunning = hdOccsRunning>0;
        hdRatesRunning(isOccRunning) = (hdSpikesRunning(isOccRunning)./hdOccsRunning(isOccRunning))*DataStruct.sampleRate(iRecCount);
        hdRatesRunning(~isOccRunning) = NaN;
        
        isOccStill = hdOccsStill>0;
        hdRatesStill(isOccStill) = (hdSpikesStill(isOccStill)./hdOccsStill(isOccStill))*DataStruct.sampleRate(iRecCount);
        hdRatesStill(~isOccStill) = NaN;
        
        NeuronStruct.hdRMapsHalves(:,iOverallNeuron,iTimeSegment,iHalf,1)=hdRates;
        NeuronStruct.hdRMapsHalves(:,iOverallNeuron,iTimeSegment,iHalf,2)=hdSpikes;
        NeuronStruct.hdRMapsHalves(:,iOverallNeuron,iTimeSegment,iHalf,3)=hdOccs;
        
        NeuronStruct.hdRMapsRunningHalves(:,iOverallNeuron,iTimeSegment,iHalf,1)=hdRatesRunning;
        NeuronStruct.hdRMapsRunningHalves(:,iOverallNeuron,iTimeSegment,iHalf,2)=hdSpikesRunning;
        NeuronStruct.hdRMapsRunningHalves(:,iOverallNeuron,iTimeSegment,iHalf,3)=hdOccsRunning;
        
        NeuronStruct.hdRMapsStillHalves(:,iOverallNeuron,iTimeSegment,iHalf,1)=hdRatesStill;
        NeuronStruct.hdRMapsStillHalves(:,iOverallNeuron,iTimeSegment,iHalf,2)=hdSpikesStill;
        NeuronStruct.hdRMapsStillHalves(:,iOverallNeuron,iTimeSegment,iHalf,3)=hdOccsStill;
        
        if iTimeSegment > 1
            % Additional state info. %Time Segment
            segmentPosNSpikes = iNeuNSpikes(...
                timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
            segmentHDRadians = iNeuHDRadians(...
                timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
            segmentPathData = iNeuPath(...
                timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2),:);
            segmentPathData(isnan(segmentPathData))=99;
            changeStarts = find(diff(segmentPathData));
            changeEnds = [changeStarts(2:end);length(segmentPathData)];
            if iTimeSegment == 2
                pathsToGrab = [1,2,3,4,9,10];
            elseif iTimeSegment == 3
                pathsToGrab = [11,12,13,14,19,20];
            end
            
            pathsWanted = [];
            iPathWantedStarts = false(length(changeStarts),length(pathsToGrab));
            for iPath = 1:length(pathsToGrab)
                iPathWantedStarts(:,iPath) = (segmentPathData(changeEnds)==pathsToGrab(iPath));
                iPathsWanted{iPath} = find(iPathWantedStarts(:,iPath));
                if ~isempty(iPathsWanted{iPath})
                    if iHalf == 1
                        nPathsHalf1(iPath) = floor(length(iPathsWanted{iPath})/2);
                        iPathsWantedAll{iPath} = randperm(length(iPathsWanted{iPath}));
                        pathsWanted = [pathsWanted;...
                            iPathsWanted{iPath}(iPathsWantedAll{iPath}(1:nPathsHalf1(iPath)))];
                    else
                        pathsWanted = [pathsWanted;...
                            iPathsWanted{iPath}(iPathsWantedAll{iPath}(nPathsHalf1(iPath)+1:end))];
                    end
                end
            end
            pathsWanted = sort(pathsWanted);
            changeStarts = changeStarts(pathsWanted);
            changeEnds = changeEnds(pathsWanted);
            
            trimmedPosNSpikesRelevant = [];
            trimmedHDRadiansRelevant = [];
            for iSegment = 1:length(changeStarts);
                trimmedPosNSpikesRelevant = [trimmedPosNSpikesRelevant;...
                    segmentPosNSpikes(changeStarts(iSegment):changeEnds(iSegment))];
                trimmedHDRadiansRelevant = [trimmedHDRadiansRelevant;...
                    segmentHDRadians(changeStarts(iSegment):changeEnds(iSegment))];
            end
            trimmedHDRadiansRelevantBinned = ...
                round(mod(radtodeg(trimmedHDRadiansRelevant),360)/binSizeDegrees);
            trimmedHDRadiansRelevantBinned(trimmedHDRadiansRelevantBinned==0) = nHDBins;
            trimmedHDRadiansRelevantBinned(isnan(segmentHDRadians)) = NaN;
            
            hdOccs=zeros(nHDBins,1);
            hdSpikes=zeros(nHDBins,1);
            hdRates=zeros(nHDBins,1);
            
            for iHDBins = 1:nHDBins
                hdSpikes(iHDBins) = sum(trimmedPosNSpikesRelevant(trimmedHDRadiansRelevantBinned == iHDBins));
                hdOccs(iHDBins) = sum(trimmedHDRadiansRelevantBinned == iHDBins);
            end
            isOcc = hdOccs>0;
            hdRates(isOcc) = (hdSpikes(isOcc)./hdOccs(isOcc)).*DataStruct.sampleRate(iRecCount);
            hdRates(~isOcc) = NaN;
            
            NeuronStruct.hdRMapsCleanRunsOnlyHalves(:,iOverallNeuron,iTimeSegment,iHalf,1)=hdRates;
            NeuronStruct.hdRMapsCleanRunsOnlyHalves(:,iOverallNeuron,iTimeSegment,iHalf,2)=hdSpikes;
            NeuronStruct.hdRMapsCleanRunsOnlyHalves(:,iOverallNeuron,iTimeSegment,iHalf,3)=hdOccs;
            end
        end
    end
%     disp(iOverallNeuron);    
end
end


function [stamps, hdRel, posRel, runVals] = timeSplitter(timeStamps,iTimeSegment,iHalf,sampleRate,HDRadians,pos,runValsAll,timeChunksInMin)
% Split it into odd minutes (half 1) and even minutes(half 2)
tOne = timeStamps(iTimeSegment*2-1);
tEnd = timeStamps(iTimeSegment*2);
totSamples = tEnd-(tOne-1);
totMinutes = ceil(totSamples/(sampleRate*60));
samplesInAMin = sampleRate*60;

allRunVals = false(totSamples,1);
allRunVals(runValsAll) = true;

runVals = false(1,0);
hdRel = [];
posRel = [];
for iMin = iHalf:timeChunksInMin*2:totMinutes
    if (tOne+(timeChunksInMin*iMin*samplesInAMin)-1) > tEnd
        stamps = [stamps;tOne+(timeChunksInMin*(iMin-1)*samplesInAMin):tEnd];
        
        hdRel = [hdRel;HDRadians(tOne+(timeChunksInMin*(iMin-1)*samplesInAMin):tEnd)];
        posRel = [posRel;pos(tOne+(timeChunksInMin*(iMin-1)*samplesInAMin):tEnd,:)];
        runVals = [runVals;allRunVals(1+(timeChunksInMin*(iMin-1)*samplesInAMin):tEnd-tOne+1)];
    else
        stamps = [stamps;tOne+(timeChunksInMin*(iMin-1)*samplesInAMin):tOne+(timeChunksInMin*iMin*samplesInAMin)-1];
        
        hdRel = [hdRel;HDRadians(tOne+(timeChunksInMin*(iMin-1)*samplesInAMin):tOne+(timeChunksInMin*iMin*samplesInAMin)-1)];
        posRel = [posRel;pos(tOne+(timeChunksInMin*(iMin-1)*samplesInAMin):tOne+(timeChunksInMin*iMin*samplesInAMin)-1,:)];
        runVals = [runVals;allRunVals(1+(timeChunksInMin*(iMin-1)*samplesInAMin):1+(timeChunksInMin*iMin*samplesInAMin)-1)];
    end
end
end

