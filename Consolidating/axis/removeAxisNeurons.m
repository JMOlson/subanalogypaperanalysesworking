function [SubNeuronNoAxis, SubRecNoAxis] = removeAxisNeurons(SubNeuron, SubRec, axisInd)
%%REMOVEAXISNEURONS remove identified axis cells from the dataset structs
%   Could easily be extended to remove any set of neuron indices fed in.
%
%   Written by Jake Olson, June 2021

%% Index of axis cells.
isAxisCell = false(length(SubNeuron.rec),1);
isAxisCell(axisInd) = true;
axisIndInRecs = cellfun(@(x) intersect(x,axisInd), SubRec.neuronStructIndices, 'UniformOutput', false);

%% make noaxis structs
SubNeuronNoAxis.rat = SubNeuron.rat(~isAxisCell);
SubNeuronNoAxis.rec = SubNeuron.rec(~isAxisCell);
SubNeuronNoAxis.recStructIndex = SubNeuron.recStructIndex(~isAxisCell);
SubNeuronNoAxis.neuronName = SubNeuron.neuronName(~isAxisCell);
SubNeuronNoAxis.spikeTimes = SubNeuron.spikeTimes(~isAxisCell);
SubNeuronNoAxis.sampleNSpikes = SubNeuron.sampleNSpikes(~isAxisCell);
SubNeuronNoAxis.linearRates_filterSigma_Bins = SubNeuron.linearRates_filterSigma_Bins(~isAxisCell);
SubNeuronNoAxis.LinearRates = SubNeuron.LinearRates(~isAxisCell,:);
SubNeuronNoAxis.MeanLinearRates = SubNeuron.MeanLinearRates(~isAxisCell,:);

% Rec Struct is trickier
keepRecs = true(numel(SubRec.rec),1);
fixBehaviorStructFlag = false;
neuRemovedSoFar = 0;
iNewRecCount = 0;
iNewNeuronCount = 0;
for iRec = 1:numel(SubRec.rec)
    iNewRecCount = iNewRecCount + 1;
        
    if isempty(axisIndInRecs{iRec})
        iNewNeuronCount = iNewNeuronCount + SubRec.nCells{iRec};
        % nothing changed, just add data in
        SubRecNoAxis.nCells{iNewRecCount} = SubRec.nCells{iRec};
        SubRecNoAxis.tfileIndexList{iNewRecCount} = SubRec.tfileIndexList{iRec};
        SubRecNoAxis.tfileNames{iNewRecCount} = SubRec.tfileNames{iRec};
        SubRecNoAxis.neuronStructIndices{iNewRecCount} = SubRec.neuronStructIndices{iRec}- neuRemovedSoFar;   
    elseif SubRec.nCells{iRec} - numel(axisIndInRecs{iRec}) == 0
        % no cells from this rec anymore - remove
        neuRemovedSoFar = neuRemovedSoFar + SubRec.nCells{iRec};
        keepRecs(iRec) = false;
        fixBehaviorStructFlag = true;
        iNewRecCount = iNewRecCount - 1;
    else
        % subtract appropriate cells
        SubRecNoAxis.nCells{iNewRecCount} = SubRec.nCells{iRec} - numel(axisIndInRecs{iRec});
        goodNeus = [];
        neuStructInd = [];
        for iNeu = 1:SubRec.nCells{iRec}
            if intersect(SubRec.neuronStructIndices{iRec}(iNeu),axisInd)
                neuRemovedSoFar = neuRemovedSoFar + 1;
            else
                iNewNeuronCount = iNewNeuronCount+1;
                goodNeus = [goodNeus, iNeu];
                neuStructInd = [neuStructInd, iNewNeuronCount];
            end
        end
        SubRecNoAxis.tfileIndexList{iNewRecCount} = SubRec.tfileIndexList{iRec}(goodNeus);
        SubRecNoAxis.tfileNames{iNewRecCount} = SubRec.tfileNames{iRec}(goodNeus);
        SubRecNoAxis.neuronStructIndices{iNewRecCount} = neuStructInd;
    end
end

SubRecNoAxis.rmapFile = SubRec.rmapFile(keepRecs);
SubRecNoAxis.dvtFile = SubRec.dvtFile(keepRecs);
SubRecNoAxis.rat = SubRec.rat(keepRecs);
SubRecNoAxis.rec = SubRec.rec(keepRecs);
SubRecNoAxis.sessionTimeStamps = SubRec.sessionTimeStamps(keepRecs);
SubRecNoAxis.Behavior = SubRec.Behavior;
if fixBehaviorStructFlag
    % go remove appropriate cells for all behavior data.
    error('Error: Removing apppropriate recs from behavior struct not yet implemented.');
end

end
