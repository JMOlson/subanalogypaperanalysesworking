function [ CategorizedAxisCells ] = categorizeAxisCells(hdRMapsTrain, hdRMapsTest, alreadyModeledFlag,...
    modelResultsName, hdRMapsForMax, spatialReliability, spatialReliabilitySampling)
%CATEGORIZEAXISCELLS Finds recorded cells fitting the axis cell definition from Olson et al 2017
%   Cells' head direction tuning must be best fit with 2 peaks, show differential firing between
%   peaks and mins, and fire over 50% of the space in the peak head directions.
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, June 2021.
modelOrderDesired = 2; % 2 for 2nd order models - when wanting axis cells

modelOrderIndexInMats = modelOrderDesired+1; % third column in a lot of matrices (we also did 0th order, so each order gets bumped over 1).
maxIterations = 50; % Maximum runs of the EM algorithm per model.
nInitPoints = 10; % Number of starting points to use per model.
nOrder = 8; % number of different order models to model. Add 1 to this when saving model results because we also do the 0th order model.

minModelFit = 0.5;
finalJumpModelFit = 0.2;

percentReliabilityThreshold = 1/2;
ratioPeaksToMinsNeeded = 1/2;

nNeurons = size(hdRMapsTrain,2);
nHDBins = size(hdRMapsTrain,1);
binSizeDegrees = 360/nHDBins;
%% Categorize Axis Cells - mixture of von mises models
% Create models. - Warning - takes a while - maybe an hour?
if alreadyModeledFlag
    load(modelResultsName)
else
    %% Train
    mixEMMain = nan(nOrder,nOrder+1,nNeurons);
    thetaEMMain = nan(nOrder,nOrder+1,nNeurons);
    mEMMain = nan(nOrder,nOrder+1,nNeurons);
    llEMMain = nan(nOrder+1,nNeurons);
    nItersEMMain = nan(nOrder+1,nNeurons);
    for iOverallNeuron=1:nNeurons
        for iVMDists = 0:nOrder
            [mixEMMain(1:max(iVMDists,1),iVMDists+1,iOverallNeuron),...
                thetaEMMain(1:max(iVMDists,1),iVMDists+1,iOverallNeuron),...
                mEMMain(1:max(iVMDists,1),iVMDists+1,iOverallNeuron),...
                llEMMain(iVMDists+1,iOverallNeuron),...
                nItersEMMain(iVMDists+1,iOverallNeuron)] = fitVMMMwEM(...
                hdRMapsTrain(:,iOverallNeuron),...
                iVMDists,maxIterations,nInitPoints);
        end
    end
    
    %% Test
    % Get sum squared error (SSE) values for models of dimensionality 0:nOrder.
    for iOverallNeuron=1:nNeurons
        for iVMDists = 0:nOrder
            [llCrossVal(iVMDists+1,iOverallNeuron),...
                modelHDVals(:,iVMDists+1,iOverallNeuron),...
                sseCrossVal(iVMDists+1,iOverallNeuron),] = ...
                mixVMMLLCalculator(...
                hdRMapsTest(:,iOverallNeuron),...
                mixEMMain(1:max(iVMDists,1),iVMDists+1,iOverallNeuron),...
                thetaEMMain(1:max(iVMDists,1),iVMDists+1,iOverallNeuron),...
                mEMMain(1:max(iVMDists,1),iVMDists+1,iOverallNeuron));
        end
    end
    save(modelResultsName, '*EMMain', 'llCrossVal', 'modelHDVals', 'sseCrossVal');
end

%% Evaulating the models:
% Find ratio of SSE of each model to the naive uniform model and the
% remaining percent of that left when that model started.
sseCrossValRatio = sseCrossVal(2:nOrder+1,:)./repmat(sseCrossVal(1,:),nOrder,1);
sseCrossValRatioRem = [zeros(1,nNeurons);sseCrossValRatio(1:nOrder-1,:)];

% Find differences between the ratio for each consecutive model.
% I append 0s on front to represent the naive uniform model. The percent
% says how much change relative to the amount of error left - so going from
% 0->50 and 50->75 would be equal in value.
sseCrossValRatioDiff = -1*diff([ones(1,nNeurons);sseCrossValRatio]);
sseCrossValRatioDiffPercent = sseCrossValRatioDiff./sseCrossValRatioRem;

% Find model that is the maximum improvement in SSE compared to the model
% with one less von mises distribution in it.
[minSSERatioDiff,minSSERatioDiffInd] = min(sseCrossValRatioDiff,[],1);
[minSSERatioDiffPercent,minSSERatioDiffPercentInd] = ...
    min(sseCrossValRatioDiffPercent,[],1);

% Find the ratio achieved at this point of maximum effect in adding
% complexity to the model.
for i=1:nNeurons
    sseRatioAtMinDiff(i) = sseCrossValRatio(minSSERatioDiffInd(i),i);
    sseRatioAtMinDiffPercent(i) = ...
        sseCrossValRatio(minSSERatioDiffPercentInd(i),i);
end

% If the value isn't less than 0.5 (half the error of the naive model),
% don't include it.
minSSERatioDiffInd(sseRatioAtMinDiff>0.5) = 0;
minSSERatioDiffPercentInd(sseRatioAtMinDiffPercent>0.5) = 0;

% % Visualize counts of each cell type.
% figure;
% subplot(211)
% hist(maxSSERatioDiffInd,[0:nOrder]);
% subplot(212)
% hist(maxSSERatioDiffPercentInd,[0:nOrder]);

%% Use jumps in improvement of model - VISUALIZATION
plotCount = 0;
maxHistVal = 0;
figure;
for iJumpModelFit = 0.025:.025:0.4
    sseLastBigImprovementIndTester = zeros(1,nNeurons);
    plotCount = plotCount+1;
    for i = 1:nNeurons
        tmpValue = find(sseCrossValRatioDiff(:,i) >= iJumpModelFit ,1,'last');
        if ~isempty(tmpValue)
            sseLastBigImprovementIndTester(i) = tmpValue;
            if sseCrossValRatio(sseLastBigImprovementIndTester(i),i) > minModelFit
                sseLastBigImprovementInd(i) = 0;
            end
        end
    end
    % Visualize counts of each cell type.
    subplot(4,4,plotCount)
    hist(sseLastBigImprovementIndTester(:),[0:nOrder]);
    modelNeuronCountsByJumpParameter(:,plotCount) = hist(sseLastBigImprovementIndTester(:),[0:nOrder])';
    title(num2str(iJumpModelFit));
    
    axisLIPTester = find(sseLastBigImprovementIndTester == modelOrderDesired); % For axis, model order 2.
    length(axisLIPTester);
    %     length(setdiff(jakeSaysAxis,axisLIPTester));
    %     length(setdiff(axisLIPTester,jakeSaysAxis));
end
maxHistVal = max(max(modelNeuronCountsByJumpParameter));
for iPlot = 1:plotCount
    subplot(4,4,iPlot)
    ylim([0,maxHistVal]);
    xlim([-.5,8.5]);
end
% pause;

%% Use jumps in improvement of model - CRITERIA 1
sseLastBigImprovementInd = zeros(1,nNeurons);
for i = 1:nNeurons
    tmpValue = find(sseCrossValRatioDiff(:,i) >= finalJumpModelFit ,1,'last');
    if ~isempty(tmpValue)
        sseLastBigImprovementInd(i) = tmpValue;
        if sseCrossValRatio(sseLastBigImprovementInd(i),i) > minModelFit
            sseLastBigImprovementInd(i) = 0;
        end
    end
end
axisLIP = find(sseLastBigImprovementInd == modelOrderDesired); % For axis, model order 2.

%% Using our reliability measure. - CRITERIA 2
minBins = nan(modelOrderDesired,nNeurons);
maxBins = nan(modelOrderDesired,nNeurons);
minVals = nan(modelOrderDesired,nNeurons);
maxVals = nan(modelOrderDesired,nNeurons);

for iNeuron = 1:nNeurons
    if ~any(isnan(modelHDVals(:,modelOrderIndexInMats,iNeuron)))
        [~,tmp] = findpeaks(-1*modelHDVals(:,modelOrderIndexInMats,iNeuron));
        if length(tmp)<modelOrderDesired % a min is on the edge of the vector
            if modelHDVals(1,modelOrderIndexInMats,iNeuron) < modelHDVals(nHDBins,modelOrderIndexInMats,iNeuron)
                tmp(modelOrderDesired) = 1;
            else
                tmp(modelOrderDesired) = nHDBins;
            end
        end
        minBins(:,iNeuron) = tmp;
        
        [~,tmp] = findpeaks(modelHDVals(:,modelOrderIndexInMats,iNeuron));
        if length(tmp)<modelOrderDesired % a peak is on the edge of the vector
            if modelHDVals(1,modelOrderIndexInMats,iNeuron) < modelHDVals(nHDBins,modelOrderIndexInMats,iNeuron)
                tmp(modelOrderDesired) = 1;
            else
                tmp(modelOrderDesired) = nHDBins;
            end
        end
        maxBins(:,iNeuron) = tmp;
        
        maxBins(maxBins == 0) = NaN;
        minBins(minBins == 0) = NaN;
        
        if ~any(isnan(minBins(:,iNeuron)) | isnan(maxBins(:,iNeuron)))
            maxVals(:,iNeuron) = hdRMapsForMax(maxBins(:,iNeuron),iNeuron);
            minVals(:,iNeuron) = hdRMapsForMax(minBins(:,iNeuron),iNeuron);
        end
    end
end

CategorizedAxisCells.hdPeaksSpatialReliabilitySampling = nan(modelOrderDesired,nNeurons);
hdPeaksSpatialReliability = nan(modelOrderDesired,nNeurons);
for iNeuron = 1:nNeurons
    if ~any(isnan(maxBins(:,iNeuron)))
        % binhigh/low x neurons x peak1/2
        CategorizedAxisCells.hdPeaksSpatialReliabilitySampling(:,iNeuron) = ...
            spatialReliabilitySampling(maxBins(:,iNeuron),iNeuron);
        hdPeaksSpatialReliability(:,iNeuron) = ...
            spatialReliability(maxBins(:,iNeuron),iNeuron);
    end
end
axisReliable = find(all(hdPeaksSpatialReliability > percentReliabilityThreshold,1));
axisReliableLIP = intersect(axisReliable,axisLIP);

% peakAngleDiffAcute = -1*abs(squeeze(abs(thetaEMMain(1,secondOrderModel,:)-thetaEMMain(2,secondOrderModel,:)))'-pi)+pi;
% peakAngleDiffObtuse = 360-peakAngleDiffAcute;
%
% % Circle starts to right and values increase as you go counterclockwise.
% peakAngle1To2 = reshape(thetaEMMain(1,secondOrderModel,:)-thetaEMMain(2,secondOrderModel,:),[1,nNeurons]);
% minAngles(1,:) = mod(peakAngles(2,:)+peakAngle1To2/2,2*pi);
% minAngles(2,:) = mod(minAngles(1,:)+pi,2*pi);

%% Model predicted minimums on each side vs model peaks - ratio. - CRITERIA 3
ratioPeaksToMinsMaxNormed =  (nanmean(maxVals,1)-nanmean(minVals,1))./nanmean(maxVals,1);
% ratioPeaksToMinsMaxNormed =  (min(thetaVals,[],1)-nanmean(minVals,1))./min(thetaVals,[],1);
axisRatio = find(ratioPeaksToMinsMaxNormed > ratioPeaksToMinsNeeded);
axisRatioLIP = intersect(axisRatio,axisLIP);
axisReliableRatio = intersect(axisRatio,axisReliable);

%% Finalizing and outputs.
finalAxisLIPReliableRatio = intersect(axisReliableLIP,axisRatio);

CategorizedAxisCells.finalAxisLIPReliableRatio = finalAxisLIPReliableRatio;
CategorizedAxisCells.minModelFit = minModelFit;
CategorizedAxisCells.finalJumpModelFit = finalJumpModelFit;
CategorizedAxisCells.percentReliabilityThreshold = percentReliabilityThreshold;
CategorizedAxisCells.ratioPeaksToMinsNeeded = ratioPeaksToMinsNeeded;
CategorizedAxisCells.binSizeDegrees = binSizeDegrees;
CategorizedAxisCells.trainDataset = hdRMapsTrain;
CategorizedAxisCells.testDataset = hdRMapsTest;
CategorizedAxisCells.mapsUsedForMaxes = hdRMapsForMax;
CategorizedAxisCells.spatialReliabilityMeasure = hdPeaksSpatialReliability;

CategorizedAxisCells.axisRatio = axisRatio;
CategorizedAxisCells.axisLIP = axisLIP;
CategorizedAxisCells.axisReliable = axisReliable;

end





