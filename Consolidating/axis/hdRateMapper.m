function [ hdMapsAll, BIN_SIZE_DEGREES ] = hdRateMapper( DataStruct, nSpikesPerSample, nTimeSegments, runningLineIndicesBySegment )
%HDRATEMAPPER Calc cell firing rate at each head direction (binned)
%   does a bunch of different HD maps.
%
%   Output is hdbins x iNeuron x iTimeSegment (x iSplit) x 3
%   3 is meanFR, spikes, occs 
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, June 2021.

BIN_SIZE_DEGREES = 10;
nHDBins = 360/BIN_SIZE_DEGREES;

MIN_SAMPLES = 5; %arbitrary small sample size - not worth running if below.
N_SPLITS = 2;

nRecs = length(DataStruct.rec);
for iRec = 1:nRecs
    neuronInds = DataStruct.neuronStructIndices{iRec};
    sampleRate = DataStruct.Behavior.trackingSampleRate_Hz(iRec);
    timeStamps = DataStruct.sessionTimeStamps{iRec};
    
    %% Actual hdRMapping
    for iNeuron = neuronInds
        % using the offset corrected radian values.
        iNeuHDRadians = DataStruct.Behavior.hdAlignedNorth_Rad{iRec};
        iNeuPath = DataStruct.Behavior.samplePaths{iRec};
        iNeuNSpikes = nSpikesPerSample{iNeuron};
        
        for iTimeSegment = 1:nTimeSegments
            if timeStamps(iTimeSegment*2)-timeStamps(iTimeSegment*2-1) > MIN_SAMPLES
                %% Select and format spikes and HD data
                hdRadiansThisSeg = iNeuHDRadians(...
                    timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
                posNSpikesThisSeg = iNeuNSpikes(...
                    timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
                % note for the line below:
                % mod to get all positive values.
                % round to nearest bin center.
                hdRadiansThisSegBinned =...
                    round(mod(rad2deg(hdRadiansThisSeg),360)/BIN_SIZE_DEGREES);
                % put all values from bin 0 into the last bin (same bin, circle).
                hdRadiansThisSegBinned(hdRadiansThisSegBinned==0) = nHDBins;
                hdRadiansThisSegBinned(isnan(hdRadiansThisSeg)) = NaN;
                
                % logical array for where animal was running
                runValsThisSeg = false(length(posNSpikesThisSeg),1);
                runValsThisSeg(runningLineIndicesBySegment{iTimeSegment,iRec}) = true;
                
                %% Split into segments for train/test sets.
                for iSplit = 1:N_SPLITS
                    if iTimeSegment == 1
                        minutesToChunk = 1;
                    else
                        minutesToChunk = 2;
                    end
                    %Alternate every other minute (or 2 minutes) into half 1 and half 2.
                    splitTimesThisSeg{iSplit} = timeSplitter(timeStamps,...
                        iTimeSegment,iSplit,sampleRate,...
                        iNeuHDRadians,iNeuNSpikes,...
                        runningLineIndicesBySegment{iTimeSegment,iRec},minutesToChunk);
                    
                    hdRadiansThisSegSplitBinned{iSplit} = hdRadiansThisSegBinned(splitTimesThisSeg{iSplit});
                    posNSpikesThisSegSplit{iSplit} = posNSpikesThisSeg(splitTimesThisSeg{iSplit});
                    
                    hdRadiansThisSegSplitRunningBinned{iSplit} = hdRadiansThisSegBinned(splitTimesThisSeg{iSplit} & runValsThisSeg);
                    posNSpikesThisSegSplitRunning{iSplit} = posNSpikesThisSeg(splitTimesThisSeg{iSplit} & runValsThisSeg);
                    
                    hdRadiansThisSegSplitStillBinned{iSplit} = hdRadiansThisSegBinned(splitTimesThisSeg{iSplit} & ~runValsThisSeg);
                    posNSpikesThisSegSplitStill{iSplit} = posNSpikesThisSeg(splitTimesThisSeg{iSplit} & ~runValsThisSeg);
                end
                
                hdRadiansThisSegRunningBinned = hdRadiansThisSegBinned(runValsThisSeg);
                posNSpikesThisSegRunning = posNSpikesThisSeg(runValsThisSeg);
                
                hdRadiansThisSegStillBinned = hdRadiansThisSegBinned(~runValsThisSeg);
                posNSpikesThisSegStill = posNSpikesThisSeg(~runValsThisSeg);
                
                
                
                
                %% Make actual HD RMaps
                hdOccs=zeros(nHDBins,1);
                hdSpikes=zeros(nHDBins,1);
                hdRates=zeros(nHDBins,1);
                
                hdOccsRunning=zeros(nHDBins,1);
                hdSpikesRunning=zeros(nHDBins,1);
                hdRatesRunning=zeros(nHDBins,1);
                
                hdOccsStill=zeros(nHDBins,1);
                hdSpikesStill=zeros(nHDBins,1);
                hdRatesStill=zeros(nHDBins,1);
                
                hdOccsSplit=zeros(nHDBins,N_SPLITS);
                hdSpikesSplit=zeros(nHDBins,N_SPLITS);
                hdRatesSplit=zeros(nHDBins,N_SPLITS);
                
                hdOccsSplitRunning=zeros(nHDBins,N_SPLITS);
                hdSpikesSplitRunning=zeros(nHDBins,N_SPLITS);
                hdRatesSplitRunning=zeros(nHDBins,N_SPLITS);
                
                hdOccsSplitStill=zeros(nHDBins,N_SPLITS);
                hdSpikesSplitStill=zeros(nHDBins,N_SPLITS);
                hdRatesSplitStill=zeros(nHDBins,N_SPLITS);
                
                
                for iHDBins = 1:nHDBins
                    hdSpikes(iHDBins) = sum(posNSpikesThisSeg(hdRadiansThisSegBinned == iHDBins));
                    hdOccs(iHDBins) = sum(hdRadiansThisSegBinned == iHDBins);
                    
                    hdSpikesRunning(iHDBins) = sum(posNSpikesThisSegRunning(hdRadiansThisSegRunningBinned == iHDBins));
                    hdOccsRunning(iHDBins) = sum(hdRadiansThisSegRunningBinned == iHDBins);
                    
                    hdSpikesStill(iHDBins) = sum(posNSpikesThisSegStill(hdRadiansThisSegStillBinned == iHDBins));
                    hdOccsStill(iHDBins) = sum(hdRadiansThisSegStillBinned == iHDBins);
                    
                    for iSplit = 1:N_SPLITS
                        hdSpikesSplit(iHDBins,iSplit) = sum(posNSpikesThisSegSplit{iSplit}(hdRadiansThisSegSplitBinned{iSplit} == iHDBins));
                        hdOccsSplit(iHDBins,iSplit) = sum(hdRadiansThisSegSplitBinned{iSplit} == iHDBins);
                        
                        hdSpikesSplitRunning(iHDBins,iSplit) = sum(posNSpikesThisSegSplitRunning{iSplit}(hdRadiansThisSegSplitRunningBinned{iSplit} == iHDBins));
                        hdOccsSplitRunning(iHDBins,iSplit) = sum(hdRadiansThisSegSplitRunningBinned{iSplit} == iHDBins);
                        
                        hdSpikesSplitStill(iHDBins,iSplit) = sum(posNSpikesThisSegSplitStill{iSplit}(hdRadiansThisSegSplitStillBinned{iSplit} == iHDBins));
                        hdOccsSplitStill(iHDBins,iSplit) = sum(hdRadiansThisSegSplitStillBinned{iSplit} == iHDBins);
                    end
                end
                isOcc = hdOccs>0;
                hdRates(isOcc) = (hdSpikes(isOcc)./hdOccs(isOcc))*sampleRate;
                hdRates(~isOcc) = NaN;
                
                isOccRunning = hdOccsRunning>0;
                hdRatesRunning(isOccRunning) = (hdSpikesRunning(isOccRunning)./hdOccsRunning(isOccRunning))*sampleRate;
                hdRatesRunning(~isOccRunning) = NaN;
                
                isOccStill = hdOccsStill>0;
                hdRatesStill(isOccStill) = (hdSpikesStill(isOccStill)./hdOccsStill(isOccStill))*sampleRate;
                hdRatesStill(~isOccStill) = NaN;
                
                for iSplit = 1:N_SPLITS
                    isOccSplit = hdOccsSplit(:,iSplit)>0;
                    hdRatesSplit(isOccSplit,iSplit) = (hdSpikesSplit(isOccSplit,iSplit)./hdOccsSplit(isOccSplit,iSplit))*sampleRate;
                    hdRatesSplit(~isOccSplit,iSplit) = NaN;
                    
                    isOccSplitRunning = hdOccsSplitRunning(:,iSplit)>0;
                    hdRatesSplitRunning(isOccSplitRunning,iSplit) = (hdSpikesSplitRunning(isOccSplitRunning,iSplit)./...
                        hdOccsSplitRunning(isOccSplitRunning,iSplit))*sampleRate;
                    hdRatesSplitRunning(~isOccSplitRunning,iSplit) = NaN;
                    
                    isOccSplitStill = hdOccsSplitStill(:,iSplit)>0;
                    hdRatesSplitStill(isOccSplitStill,iSplit) = (hdSpikesSplitStill(isOccSplitStill,iSplit)./...
                        hdOccsSplitStill(isOccSplitStill,iSplit))*sampleRate;
                    hdRatesSplitStill(~isOccSplitStill,iSplit) = NaN;
                end
                
                hdMapsAll.hdRMaps(:,iNeuron,iTimeSegment,1)=hdRates;
                hdMapsAll.hdRMaps(:,iNeuron,iTimeSegment,2)=hdSpikes;
                hdMapsAll.hdRMaps(:,iNeuron,iTimeSegment,3)=hdOccs;
                
                hdMapsAll.hdRMapsRunning(:,iNeuron,iTimeSegment,1)=hdRatesRunning;
                hdMapsAll.hdRMapsRunning(:,iNeuron,iTimeSegment,2)=hdSpikesRunning;
                hdMapsAll.hdRMapsRunning(:,iNeuron,iTimeSegment,3)=hdOccsRunning;
                
                hdMapsAll.hdRMapsStill(:,iNeuron,iTimeSegment,1)=hdRatesStill;
                hdMapsAll.hdRMapsStill(:,iNeuron,iTimeSegment,2)=hdSpikesStill;
                hdMapsAll.hdRMapsStill(:,iNeuron,iTimeSegment,3)=hdOccsStill;
                
                for iSplit = 1:N_SPLITS
                    hdMapsAll.hdRMapsHalves(:,iNeuron,iTimeSegment,iSplit,1)= hdRatesSplit(:,iSplit);
                    hdMapsAll.hdRMapsHalves(:,iNeuron,iTimeSegment,iSplit,2)= hdSpikesSplit(:,iSplit);
                    hdMapsAll.hdRMapsHalves(:,iNeuron,iTimeSegment,iSplit,3)= hdOccsSplit(:,iSplit);
                    
                    hdMapsAll.hdRMapsRunningHalves(:,iNeuron,iTimeSegment,iSplit,1)= hdRatesSplitRunning(:,iSplit);
                    hdMapsAll.hdRMapsRunningHalves(:,iNeuron,iTimeSegment,iSplit,2)= hdSpikesSplitRunning(:,iSplit);
                    hdMapsAll.hdRMapsRunningHalves(:,iNeuron,iTimeSegment,iSplit,3)= hdOccsSplitRunning(:,iSplit);
                    
                    hdMapsAll.hdRMapsStillHalves(:,iNeuron,iTimeSegment,iSplit,1)=hdRatesSplitStill(:,iSplit);
                    hdMapsAll.hdRMapsStillHalves(:,iNeuron,iTimeSegment,iSplit,2)=hdSpikesSplitStill(:,iSplit);
                    hdMapsAll.hdRMapsStillHalves(:,iNeuron,iTimeSegment,iSplit,3)=hdOccsSplitStill(:,iSplit);
                end
                
                %% Adds a clean runs only map.
                if iTimeSegment > 1
                    % Additional state info. %Time Segment
                    segmentPathData = iNeuPath(timeStamps(iTimeSegment*2-1):...
                        timeStamps(iTimeSegment*2),:);
                    changeStarts = find(diff(segmentPathData));
                    changeEnds = [changeStarts(2:end);length(segmentPathData)];
                    if iTimeSegment == 2
                        pathsToGrab = [1,2,3,4,9,10];
                    elseif iTimeSegment == 3
                        pathsToGrab = [11,12,13,14,19,20];
                    end
                    
                    wantedStarts = false(length(changeStarts),1);
                    for iPath = 1:length(pathsToGrab)
                        wantedStarts = (segmentPathData(changeEnds)==pathsToGrab(iPath))|...
                            wantedStarts;
                    end
                    pathsWanted = find(wantedStarts);
                    if ~isempty(pathsWanted)
                        changeStarts = changeStarts(pathsWanted);
                        changeEnds = changeEnds(pathsWanted);
                        
                        trimmedPosNSpikesThisSeg = [];
                        trimmedHDRadiansThisSegBinned = [];
                        for iSegment = 1:length(changeStarts)
                            trimmedPosNSpikesThisSeg = [trimmedPosNSpikesThisSeg;...
                                posNSpikesThisSeg(changeStarts(iSegment):changeEnds(iSegment))];
                            trimmedHDRadiansThisSegBinned = [trimmedHDRadiansThisSegBinned;...
                                hdRadiansThisSegBinned(changeStarts(iSegment):changeEnds(iSegment))];
                        end
                        
                        hdOccs=zeros(nHDBins,1);
                        hdSpikes=zeros(nHDBins,1);
                        hdRates=zeros(nHDBins,1);
                        
                        for iHDBins = 1:nHDBins
                            hdSpikes(iHDBins) = sum(trimmedPosNSpikesThisSeg(trimmedHDRadiansThisSegBinned == iHDBins));
                            hdOccs(iHDBins) = sum(trimmedHDRadiansThisSegBinned == iHDBins);
                        end
                        isOcc = hdOccs>0;
                        hdRates(isOcc) = (hdSpikes(isOcc)./hdOccs(isOcc))*sampleRate;
                        hdRates(~isOcc) = NaN;
                        
                        hdMapsAll.hdRMapsCleanRunsOnly(:,iNeuron,iTimeSegment,1)=hdRates;
                        hdMapsAll.hdRMapsCleanRunsOnly(:,iNeuron,iTimeSegment,2)=hdSpikes;
                        hdMapsAll.hdRMapsCleanRunsOnly(:,iNeuron,iTimeSegment,3)=hdOccs;
                    end
                end
            else
                hdMapsAll.hdRMaps(:,iNeuron,iTimeSegment,1)=NaN;
                hdMapsAll.hdRMaps(:,iNeuron,iTimeSegment,2)=NaN;
                hdMapsAll.hdRMaps(:,iNeuron,iTimeSegment,3)=NaN;
                
                hdMapsAll.hdRMapsRunning(:,iNeuron,iTimeSegment,1)=NaN;
                hdMapsAll.hdRMapsRunning(:,iNeuron,iTimeSegment,2)=NaN;
                hdMapsAll.hdRMapsRunning(:,iNeuron,iTimeSegment,3)=NaN;
                
                hdMapsAll.hdRMapsStill(:,iNeuron,iTimeSegment,1)=NaN;
                hdMapsAll.hdRMapsStill(:,iNeuron,iTimeSegment,2)=NaN;
                hdMapsAll.hdRMapsStill(:,iNeuron,iTimeSegment,3)=NaN;
                if iTimeSegment > 1
                    hdMapsAll.hdRMapsCleanRunsOnly(:,iNeuron,iTimeSegment,1)=NaN;
                    hdMapsAll.hdRMapsCleanRunsOnly(:,iNeuron,iTimeSegment,2)=NaN;
                    hdMapsAll.hdRMapsCleanRunsOnly(:,iNeuron,iTimeSegment,3)=NaN;
                end
            end
        end
    end
end
end

function stamps = timeSplitter(timeStamps,iTimeSegment,iHalf,sampleRate,HDRadians,pos,runValsAll,timeChunksInMin)
% Split it into odd minutes (half 1) and even minutes(half 2)
tOne = timeStamps(iTimeSegment*2-1);
tEnd = timeStamps(iTimeSegment*2);
totSamples = tEnd-(tOne-1);
totMinutes = ceil(totSamples/(sampleRate*60));
samplesInAMin = sampleRate*60;

stamps = false(totSamples,1);
for iMin = iHalf:timeChunksInMin*2:totMinutes
    if (1+(timeChunksInMin*iMin*samplesInAMin)-1) > totSamples
        stamps(1+(timeChunksInMin*(iMin-1)*samplesInAMin):totSamples) = true;
    else
        stamps(1+(timeChunksInMin*(iMin-1)*samplesInAMin):1+(timeChunksInMin*iMin*samplesInAMin)-1) = true;
    end
end
end