function [CatAxis] = catAxisCellsWrapped(NeuronStruct, RecStruct, alreadyModeledFlag, saveName)
%%CATAXISCELLSWRAPPED categorize axis cells from Olson... NN 2017 for Olson ... SubCA1 2021 paper
%
%   Written by Jake Olson, June 2021

runningLineIndicesBySegment = isRunningAnalysis( RecStruct.Behavior.velSmoothed_CmPerSec,...
    RecStruct.Behavior.angVelSmoothed_RadPerSec, RecStruct.sessionTimeStamps, 2);
hdMapsAll = hdRateMapper( RecStruct, NeuronStruct.sampleNSpikes, 2, runningLineIndicesBySegment );
%   Output is hdbins x iNeuron x iTimeSegment (x iSplit) x 3
%   3 is meanFR, spikes, occs 

[~, trackIsRunning2DByHDRMaps] = hdBy2DBinsRateMapper(...
    RecStruct, NeuronStruct.sampleNSpikes, NeuronStruct.recStructIndex, runningLineIndicesBySegment,2,10,6,1,1,1);
%   Output is xbins x ybins x hdbins x iNeuron x 3
%   3 is meanFR, spikes, occs 

[~, hd2dSpatialReliability, hd2dSpatialReliabilitySampling] = ...
    hdBy2DSpatialReliabilityCalculator(RecStruct.Behavior.trackingSampleRate_Hz(1),...
    hdMapsAll.hdRMapsRunning(:,:,2,1), trackIsRunning2DByHDRMaps);

% Actual model and categorize axis cells.
[ CatAxis ] = categorizeAxisCells(hdMapsAll.hdRMapsRunningHalves(:,:,2,1,1),...
    hdMapsAll.hdRMapsRunningHalves(:,:,2,2,1), alreadyModeledFlag, saveName,...
    hdMapsAll.hdRMapsRunning(:,:,2,1), hd2dSpatialReliability, hd2dSpatialReliabilitySampling);

end
