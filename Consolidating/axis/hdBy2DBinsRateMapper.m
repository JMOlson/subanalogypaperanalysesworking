function [Parameters_HDx2DRateMapper, twoDByHDRMaps, twoDByHDRMapsStill] = ...
    hdBy2DBinsRateMapper( DataStruct, nSpikesPerSample, recIndPerNeu, runningLineIndicesBySegment,...
    iTimeSegment, binSize_Degrees, binSize_Pixels, xSize_Pixels, ySize_Pixels, isRunningFlag)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%
%   Output is xbins x ybins x hdbins x iNeuron x 3
%   3 is meanFR, spikes, occs 
%
%   Written by Jake Olson, December 2015
%   Last updated by Jake Olson, June 2021.

% 6.666 pixels is ~2cm square bins for platform - RECOMMENDED
% probably 6 pixels would be about the same distance for the track, since
% it is about 1 brick lower. Try 6 pixels - RECOMMENDED
% For the platform, 300 by 300 pixelSize variables gives a 45 bin by 45 bin square.
nHDBins = 360/binSize_Degrees;
Parameters_HDx2DRateMapper.binSize_Degrees = binSize_Degrees;
Parameters_HDx2DRateMapper.nHDBins = nHDBins;

Parameters_HDx2DRateMapper.binSizePixels(iTimeSegment) = binSize_Pixels;
Parameters_HDx2DRateMapper.xSize_Pixels(iTimeSegment) = xSize_Pixels;
Parameters_HDx2DRateMapper.ySize_Pixels(iTimeSegment) = ySize_Pixels;

%For regular datasets,
maxX = 650;
maxY = 500;
% 1 Codes for use all of the data.
if xSize_Pixels == 1
    xSize_Pixels = maxX;
end
if ySize_Pixels == 1
    ySize_Pixels = maxY;
end

xSize_Bins = ceil(xSize_Pixels/binSize_Pixels);
ySize_Bins = ceil(ySize_Pixels/binSize_Pixels);
twoDByHDRMaps = nan(xSize_Bins,ySize_Bins,nHDBins,length(nSpikesPerSample),3);
if isRunningFlag
    twoDByHDRMapsStill = nan(xSize_Bins,ySize_Bins,nHDBins,length(nSpikesPerSample),3);
end
    
for iNeuron = 1:length(nSpikesPerSample)
    sampleRate = DataStruct.Behavior.trackingSampleRate_Hz(recIndPerNeu(iNeuron));
    timeStamps = DataStruct.sessionTimeStamps{recIndPerNeu(iNeuron)};
    
    iNeuHDRadians = DataStruct.Behavior.hdAlignedNorth_Rad{recIndPerNeu(iNeuron)};
    iNeuNSpikes = nSpikesPerSample{iNeuron};
    iNeuPosXY = DataStruct.Behavior.posXY{recIndPerNeu(iNeuron)};
    
    for iAnalysis = 1:isRunningFlag+1 % so 1 or 2, as the flag is 0 or 1.
        hdRadiansThisSeg = iNeuHDRadians(...
            timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2));
        posNSpikesThisSeg = iNeuNSpikes(...
            timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2),:);
        posThisSeg = iNeuPosXY(timeStamps(iTimeSegment*2-1):timeStamps(iTimeSegment*2),:);
        
        if iAnalysis == 1 && isRunningFlag
            runVals = false(timeStamps(iTimeSegment*2)-timeStamps(iTimeSegment*2-1),1);
            runVals(runningLineIndicesBySegment{iTimeSegment,recIndPerNeu(iNeuron)}) = true;
            
            hdRadiansThisSeg = hdRadiansThisSeg(runVals);
            posNSpikesThisSeg = posNSpikesThisSeg(runVals);
            posThisSeg = posThisSeg(runVals,:);
        elseif  iAnalysis == 2 && isRunningFlag
            hdRadiansThisSeg = hdRadiansThisSeg(~runVals);
            posNSpikesThisSeg = posNSpikesThisSeg(~runVals);
            posThisSeg = posThisSeg(~runVals,:);
        end
        hdRadiansBinned =...
            round(mod(rad2deg(hdRadiansThisSeg),360)/binSize_Degrees);
        % put all values from bin 0 into the last bin (same bin, circle).
        hdRadiansBinned(hdRadiansBinned==0) = nHDBins;
        hdRadiansBinned(isnan(hdRadiansThisSeg)) = NaN;
        
        % Binning the twoD bins - perhaps downsampling.
        if xSize_Pixels == maxX
            xOffset = 0;
        else
            plotMinX = nanmean(posThisSeg(posThisSeg(:,1)>1,1))-xSize_Pixels/2;
            xOffset = ceil(plotMinX/binSize_Pixels)-1;
        end
        if ySize_Pixels == maxY
            yOffset = 0;
        else
            plotMinY = nanmean(posThisSeg(posThisSeg(:,1)>1,2))-ySize_Pixels/2;
            yOffset = floor(plotMinY/binSize_Pixels)-1;
        end
        binX = ceil(posThisSeg(:,1)/binSize_Pixels)-xOffset;
        binY = ceil(posThisSeg(:,2)/binSize_Pixels)-yOffset;
        
        sampleOutOfGrid = binX <= 0 | binY <= 0 | ...
            binX > xSize_Pixels/binSize_Pixels | binY > ySize_Pixels/binSize_Pixels;
        binXGood = binX(~sampleOutOfGrid & ~isnan(binX) & ~isnan(binY)); % Throwing out points where it would be outside the grid.
        binYGood = binY(~sampleOutOfGrid & ~isnan(binX) & ~isnan(binY)); % Throwing out points where it would be outside the grid.
        posNSpikesGood = posNSpikesThisSeg(~sampleOutOfGrid & ~isnan(binX) & ~isnan(binY));
        hdRadiansBinnedGood = hdRadiansBinned(~sampleOutOfGrid & ~isnan(binX) & ~isnan(binY));
        
        spikes = zeros(xSize_Bins,ySize_Bins,nHDBins);
        occs = spikes;
        rates = nan(size(occs));
        for iSample = 1:length(posNSpikesGood)
            if ~isnan(hdRadiansBinnedGood(iSample))
                occs(binXGood(iSample),binYGood(iSample),hdRadiansBinnedGood(iSample)) = ...
                    occs(binXGood(iSample),binYGood(iSample),hdRadiansBinnedGood(iSample))+1;
                
                spikes(binXGood(iSample),binYGood(iSample),hdRadiansBinnedGood(iSample)) = ...
                    spikes(binXGood(iSample),binYGood(iSample),hdRadiansBinnedGood(iSample)) +...
                    posNSpikesGood(iSample);
            end
        end
        isOcc = occs > 0;
        rates(isOcc) = (spikes(isOcc)./occs(isOcc)).*sampleRate;
        
        if isRunningFlag
            if iAnalysis == 1
                twoDByHDRMaps(:,:,:,iNeuron,1) = rates;
                twoDByHDRMaps(:,:,:,iNeuron,2) = spikes;
                twoDByHDRMaps(:,:,:,iNeuron,3) = occs;
            else
                twoDByHDRMapsStill(:,:,:,iNeuron,1) = rates;
                twoDByHDRMapsStill(:,:,:,iNeuron,2) = spikes;
                twoDByHDRMapsStill(:,:,:,iNeuron,3) = occs;
            end
        else
            twoDByHDRMaps(:,:,:,iNeuron,1) = rates;
            twoDByHDRMaps(:,:,:,iNeuron,2) = spikes;
            twoDByHDRMaps(:,:,:,iNeuron,3) = occs;
        end
    end
end

