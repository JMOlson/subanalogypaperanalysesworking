function runningLineIndicesBySegment = isRunningAnalysis( vel, angVel, timeStamps, nTimeSegments)
%ISRUNNINGANALYSIS Adds running times in each segment
%   Running marks line markers for each segment where the animals movements are above the velocity
%   and below the angular velocity thresholds.
%
%
%   Written by Jake Olson, October 2015
%   Last updated by Jake Olson, June 2021.

MIN_RUN_VEL = 3; % cm per second
MAX_TURN_VEL = 20; % radians per second - approx equivalent to turning head a quarter turn in 1/12 of a second. Fast!

nRecs = length(timeStamps);
runningLineIndicesBySegment = cell(nTimeSegments, nRecs);
for iRec = 1:nRecs
    for i = 1:nTimeSegments
        velAvgTimeSeg = vel{iRec}(timeStamps{iRec}(2*i-1):timeStamps{iRec}(2*i),1);
        angVelTimeSeg = angVel{iRec}(timeStamps{iRec}(2*i-1):timeStamps{iRec}(2*i));
        
        isRunning = velAvgTimeSeg > MIN_RUN_VEL & abs(angVelTimeSeg) < MAX_TURN_VEL;
        runningLineIndicesBySegment{i,iRec} = find(isRunning);
    end
    
    % Plotting commented out.
    
    % for i= 1109:500:27339
    % figure(1)
    % subplot(211);
    % plot(velAvgSmo(i:i+500),'r')
    % title('Velocity')
    % subplot(212);
    % plot(angVelSmo(i:i+500))
    % title('angVel')
    % figure(2);
    % subplot(121)
    % scatter(pos(i:i+500,2),pos(i:i+500,3),10,[0:500]);
    % axis([0,400,0,400])
    % subplot(122)
    % scatter(pos(i:i+500,2),pos(i:i+500,3),10,angVelSmo(i:i+500));
    % axis([0,400,0,400])
    % colorbar;
    % pause;
    % end
    
    % breakPoints = diff(diff(DataStruct.platformRunningTimes{iRec})>1);
    % starts = [0;find(breakPoints == -1)]+1;
    % ends = [find(breakPoints == 1);length(breakPoints)]+1;
    % minLength = 30;
    %
    % runningArena = [];
    % for i = 1:length(starts)
    %     if ends(i)-starts(i) > minLength
    %     scatter(pos(DataStruct.platformRunningTimes{iRec}(starts(i)):...
    %       DataStruct.platformRunningTimes{iRec}(ends(i)),2),...
    %       pos(DataStruct.platformRunningTimes{iRec}(starts(i)):...
    %       DataStruct.platformRunningTimes{iRec}(ends(i)),3),'k.')
    %     runningArena = [runningArena, (DataStruct.platformRunningTimes{iRec}(starts(i)):DataStruct.platformRunningTimes{iRec}(ends(i)))];
    %     axis([0,600,0,700]);
    %     hold on;
    %     pause;
    %     end
    % end
    
end
end

