function [ mixOut,thetaOut,mOut,llOut,nItersOut ] = ...
    fitVMMMwEM(hdRMap, nVMDists, maxIterations, nInits)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
%
%   Initialize von mises distribution parameters.
%   While still changing:
%   E Step - Expectation - Calculate posterior probabilities of current
%       parameters.
%
%   M Step - Maximization - Estimate new parameters that are better.
%
%   CREDIT/RESOURCES - I used Bishop pg108-110 & 430-441, esp. 439.
%       I also used "INTERCHANNEL LOUDNESS VECTOR SUM BASED MULTICHANNEL AUDIO SIGNAL
%           SOURCE SEPARATION" by Taejin Park, Keunwoo Choi and Kyeong-Ok Kang
%       Electronics and Telecommunications Research Institute - They had
%       the maximum likelihood estimators for the mixture of VM
%       distributions I needed. They also provided code which I looked at
%       but found minimally helpful.
%       website: http://spturtle.blogspot.com/2014/10/blog-post.html
LOGLIKECHANGETHRESHOLDPERCENT = 0.0001; % .01 percent

% Initialize output variables
mixOut = zeros(nVMDists,1);
thetaOut = zeros(nVMDists,1);
mOut = zeros(nVMDists,1);
if nVMDists == 0
    mixOut = 0;
    thetaOut = 0;
    mOut = 0;
else
    mixOut = zeros(nVMDists,1);
    thetaOut = zeros(nVMDists,1);
    mOut = zeros(nVMDists,1);
end
llOut = 0;
nItersOut = 0;

% In case there is no firing for this neuron, so zeros everywhere.
if max(hdRMap) < 0.05
    return
end
% No longer in output - stdErr,meanConfInterval,circVar,circStdDev
% stdErr = zeros(nVMDists,1);
% meanConfInterval = zeros(nVMDists,1);
% circVar = zeros(nVMDists,1);
% circStdDev = zeros(nVMDists,1);

logLikelihoodAll = nan(nInits,1);
mixCoeffPiAll = nan(nVMDists,nInits);
thetaAll = nan(nVMDists,nInits);
mAll = nan(nVMDists,nInits);
nItersAll = nan(nInits,1);

%% Convert HD FR into samples.
% Convert data in HDRMap form into "samples" around the circle. I'm
% entering the values at the bin centers and rounding to the nearest 10th
% of a Hz.
% Create sample data from HD FR data. Start at 2*pi/(#ofsamples) - just
% like the real data being read in is binned.
dataSamples = [];
for iBin = 1:length(hdRMap)
    dataSamples = [dataSamples; repmat(iBin*2*pi/length(hdRMap),round(hdRMap(iBin)*10),1)];
end
nDataPoints = length(dataSamples);

%% Bessel lookup table.
% During the maximization step you have to "numerically invert" the
% following equation A(m) to get the new m (concentration) parameter. It
% involves bessel functions. I think the lookup table of reasonable m
% values is the simplest and what I assume they mean by numerically invert
% (Bishop pg.110).
% negative m values work because atan only goes 180 degrees - negative m
% values is equivalent to a bump in the opposite direction.
possibleMVals = 0:0.02:99.98;
aMLookupTable = besseli(1,possibleMVals)./besseli(0,possibleMVals);

%% log likelihood for uniform distribution.
if nVMDists == 0
    vonMisesVal = repmat(1/length(hdRMap),nDataPoints,1);
    llOut = sum(log(vonMisesVal),1);
    thetaOut = 0;
    mOut = 0;
    mixOut = 1;
    nItersOut = 1;
else
%% Loop for each EM algorithm run.
for iEMRuns = 1:nInits
    %% Initialize model parameters
    logLikelihood = 0;
    mixCoeffPi = repmat(1/nVMDists,1,nVMDists);
    theta = 0:2*pi/nVMDists:2*pi;
    % Shift equidistance around possibilites for the nRuns used.
    theta = theta(2:end) - iEMRuns*(2*pi/(nVMDists*nInits));
    m = repmat(nVMDists,1,nVMDists);
    
    %% Initialize variables for computation speed purposes
    vonMisesVal = zeros(nDataPoints,nVMDists);
    gamma = zeros(nDataPoints,nVMDists);
    
    %% Probabilities (pdf) of data for initalized Von Mises Distributions.
    for iDist = 1:nVMDists
        % CircStat toolbox calculation for von mises PDF for the string of angles given.
        vonMisesVal(:,iDist) = circ_vmpdf(dataSamples,theta(iDist),m(iDist));
    end
    
    %% EM Algorithm
    for iIteration = 1:maxIterations
        %% E Step
        for iDist = 1:nVMDists
            gamma(:,iDist) = repmat(mixCoeffPi(iDist),nDataPoints,1).*vonMisesVal(:,iDist)./...
                sum(repmat(mixCoeffPi,nDataPoints,1).*vonMisesVal,2);
        end
%         figure(1); hold off;
%         plot(dataSamples,gamma);
%         hold on;
%         plot(dataSamples,vonMisesVal,'--');
            
        
        %% M Step
        mixCoeffPiNew = sum(gamma,1)./nDataPoints;
        thetaNew = atan2(sum(gamma.*sin(repmat(dataSamples,1,nVMDists)),1),...
            sum(gamma.*cos(repmat(dataSamples,1,nVMDists)),1));
        aM = sum(gamma.*...
            (cos(repmat(dataSamples,1,nVMDists)).*...
            cos(repmat(thetaNew,nDataPoints,1))+...
            sin(repmat(dataSamples,1,nVMDists)).*...
            sin(repmat(thetaNew,nDataPoints,1))),1)./...
            sum(gamma,1);
        if min(aM)<0
            disp('M less than 0! :( ');
            return;
        end
        for iDist = 1:nVMDists
            [~,closestMInd] = min(abs(aM(iDist)-aMLookupTable));
            mNew(iDist) = possibleMVals(closestMInd);
        end
        
        
        %% Convergence Check
        % Converged if:
        % Log likelihood changes less than a certain percent (defined at
        % top of file). - Using 1% for this work.
        
% Played with: % All means (thetas) don't move more than one bin -
%        %       whatever the binning is of the data we are given.
%        %   All mixing parameters move less than 1/100.
%         if  any(theta-thetaNew) || any((mixCoeffPi-mixCoeffPiNew)>1/100) || any((m-mNew)>1/4)
%             figure(2);hold off;
%             plot(0:pi/36:2*pi,circ_vmpdf(0:pi/36:2*pi,thetaNew(1),mNew(1)),'b');
%             hold on;
%             plot(0:pi/36:2*pi,circ_vmpdf(0:pi/36:2*pi,thetaNew(2),mNew(2)),'r');
%             plot(0:pi/36:2*pi,circ_vmpdf(0:pi/36:2*pi,theta(1),m(1)),'b--');
%             plot(0:pi/36:2*pi,circ_vmpdf(0:pi/36:2*pi,theta(2),m(2)),'r--');
%             plot(pi/36:pi/36:2*pi,hist(dataSamples,72)./(sum(hist(dataSamples,72)).*pi./36),'g');
%             pause(0.5);
            
            theta = thetaNew;
            mixCoeffPi = mixCoeffPiNew;
            m = mNew;
            for iDist = 1:nVMDists
                % CircStat toolbox calculation for von mises PDF for the string of angles given.
                vonMisesVal(:,iDist) = circ_vmpdf(dataSamples,thetaNew(iDist),mNew(iDist));
            end
            logLikelihood(iIteration) = sum(log(sum(...
                repmat(mixCoeffPiNew,nDataPoints,1).*vonMisesVal,2)),1);
            if iIteration > 1
                if (logLikelihood(iIteration)-logLikelihood(iIteration-1))/logLikelihood(iIteration-1)...
                        < LOGLIKECHANGETHRESHOLDPERCENT
%                     disp('Likelihood Converged!!!');
%                     figure(99);
%                     subplot(1,nInits,iEMRuns);
%                     plot(logLikelihood);
                    break;
                end
            end
%         else
%             for iDist = 1:nVMDists
%                 % CircStat toolbox calculation for von mises PDF for the string of angles given.
%                 vonMisesVal(:,iDist) = circ_vmpdf(dataSamples,theta(iDist),m(iDist));
%             end
%             logLikelihood(iIteration) = sum(log(sum(...
%                 repmat(mixCoeffPiNew,nDataPoints,1).*vonMisesVal,2)),1);
%             disp('Parameters Converged!!!');
%             figure(99);
%             subplot(1,nInits,iEMRuns);
%             plot(logLikelihood);
%             break
%         end
    end
    %% Store Vals
    logLikelihoodAll(iEMRuns) = logLikelihood(iIteration);
    mixCoeffPiAll(:,iEMRuns) = mixCoeffPi;
    thetaAll(:,iEMRuns) = theta;
    mAll(:,iEMRuns) = m;
    nItersAll(iEMRuns) = iIteration;
end
%% Confidence Intervals around means - meaningless - too many samples. 
% use std dev or variance.
% Fisher pg.88-89,97-98
% alpha = 0.05;
% zOfHalfAlpha = norminv(1-alpha./2);
[~,bestRun] = max(logLikelihoodAll);
% aOne = besseli(1,mAll(:,bestRun))./besseli(0,mAll(:,bestRun));
% nDPDist = floor(mixCoeffPiAll(:,bestRun).*nDataPoints+0.5);
% stdErr = 1./((nDPDist.*aOne.*mAll(:,bestRun)).^0.5);
% meanConfInterval = asin(zOfHalfAlpha.*stdErr);
% % Fisher 1993 pg.32
% circVar = 1-aOne;
% circStdDev = (-2*log(aOne)).^0.5;

%% Set Outputs
llOut = logLikelihoodAll(bestRun);
mixOut = mixCoeffPiAll(:,bestRun);
thetaOut = thetaAll(:,bestRun);
mOut = mAll(:,bestRun);
nItersOut = nItersAll(bestRun);

end
% goodFit = VMMMGoodnessOfFitTest(

%% Plotting - 2VM mix
% figure()
% for i = 1:length(thetaEM)
% hold off;
% polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
%     NS.hdRMapsCleanRunsOnly(:,i,2,1)./sum(NS.hdRMapsCleanRunsOnly(:,i,2,1)));
% hold on;
% polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
% circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',thetaEM(1,i),mEM(1,i))*mixEM(1,i)*pi/36,'k')
% polar(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',...
% circ_vmpdf(degtorad(NS.HDMapsBinSizeDegrees:NS.HDMapsBinSizeDegrees:360)',thetaEM(2,i),mEM(2,i))*mixEM(2,i)*pi/36,'r')
% pause;
% end
%
% vonMisesVal = exp(m(iDist).*cos(data(iDataPoint)-theta(iDist)))./(2*pi*besseli(0,data(iDataPoint)));
end
