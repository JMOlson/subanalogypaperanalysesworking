function [Parameters_SpatialReliability, hd2dSpatialReliability, hd2dSpatialReliabilitySampling] = ...
    hdBy2DSpatialReliabilityCalculator(sampleRate, hdRMaps, hd2dRMaps)
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here
%
%
%   Written by Jake Olson, December 2015
%   Last updated by Jake Olson, June 2021.

PERCENT_BELOW = 0.50; % XX percent of mean is included as reliable.
CUTOFF_VAL = sampleRate/6; % 1/6th of second - 10 samples


Parameters_SpatialReliability.percentBelow = 0.50; 
Parameters_SpatialReliability.HDSpatialMinimumSampling = CUTOFF_VAL;

[nHDBins, nNeurons] = size(hdRMaps);
 
rates = hd2dRMaps(:,:,:,:,1); %   Output is xbins x ybins x hdbins x iNeuron x 3 - 1 is mean
sufficientSampling = hd2dRMaps(:,:,:,:,3)>=CUTOFF_VAL; % 3 is occupancy
rates(~sufficientSampling) = NaN;

percentBelowMeanHDFiringReshaped = reshape(hdRMaps*PERCENT_BELOW,[1,1,nHDBins,nNeurons]); % hd x neuron mean rates

% total locations with sufficient sampling of each head direction.
hd2dSpatialReliabilitySampling = squeeze(nansum(nansum(sufficientSampling,2),1)); 

% percent locations at each HD with firing above a ratio of the the mean for that HD
hd2dSpatialReliability = squeeze(nansum(nansum(...
    rates > repmat(percentBelowMeanHDFiringReshaped,[size(rates,1),size(rates,2),1,1]) & sufficientSampling,...
    2),1))... % sum all locations with sufficient firing rates for each HD
    ./hd2dSpatialReliabilitySampling; % normalized by # of possible locations

end

