mapToUse = parula;

%Sub neurons
% neuronStruct = SubNeuron;
% corrStruct = SubCorr;
% recStruct = SubRec;

%CA1 neurons
neuronStruct = HpcNeuron;
corrStruct = HpcCorr;
recStruct = HpcRec;


% neuList = find(strcmp(neuronStruct.rat,'NS23'));
corr = nanmax(corrStruct.corrResults_all_nonoverlap,[],2);
[~,neuList] = sort(corr,'descend');

for iNeu = neuList' %1:size(neuList)
    if strcmp(neuronStruct.rat{iNeu},'NS23')
    if ~isnan(corr(iNeu))
        twoDRMapPlotter(neuronStruct, recStruct, iNeu, 'sampleNSpikes', true, mapToUse,[],[],[],'Mean3SD');
        fprintf('iNeu = %i\n', iNeu);
        display(corrStruct.corrResults_all_nonoverlap(iNeu,:));
        display(corr(iNeu));
        pause();
    end
    end
end

