% Calc/ LR transition probabilities

%create indices to grab appropriate numbers for R>L & L>R
lGivR = sub2ind([4,4],[3,3,4,4],[1,2,1,2]);
rGivL = sub2ind([4,4],[1,2,1,2],[3,3,4,4]);

nRecs = numel(RatStruct.BehRec.rec);
for iRec = 1:nRecs
    % undo normalization to get back to counts
    % do dot multiplacation for every element in first matrix and second
    % matrix
    rawTransitionCounts = bsxfun(@(x,y) x.*y,...
        RatStruct.Performance.transitionProbMatOutboundRuns(:,:,iRec),...
        RatStruct.Performance.transitionCountsOutboundRuns(:,iRec));
    
    % add counts
    nLeftGivenRight(iRec) = sum(rawTransitionCounts(lGivR));
    nRightGivenLeft(iRec) = sum(rawTransitionCounts(rGivL));
end

nRights = sum(RatStruct.Performance.transitionCountsOutboundRuns(3:4,:),1);
nLefts = sum(RatStruct.Performance.transitionCountsOutboundRuns(1:2,:),1);
nOutboundRuns = nRights+nLefts;

nAlternations = nLeftGivenRight + nRightGivenLeft;

%normalize and calc probability 

pAlteration = nAlternations./nOutboundRuns;


% Calc ShortFar Return transition probabilities
shortReturn = sub2ind([6,6],[1,2,3,4],[5,5,6,6]);
farReturn = sub2ind([6,6],[1,2,3,4],[6,6,5,5]);

nRecs = numel(RatStruct.BehRec.rec);
for iRec = 1:nRecs
    % undo normalization to get back to counts
    rawTransitionCounts = bsxfun(@(x,y) x.*y,...
        RatStruct.Performance.transitionProbMatAllRuns(:,:,iRec),...
        RatStruct.Performance.transitionCountsAllRuns(:,iRec));
    
    % add counts
    nShortReturn(iRec) = sum(rawTransitionCounts(shortReturn));
    nFarReturn(iRec) = sum(rawTransitionCounts(farReturn));
end
% normalize
normVal = nShortReturn + nFarReturn;

pShortReturn = nShortReturn./normVal;
pFarReturn = nFarReturn./normVal;