function rasterPlotter(CorrStruct,PATHLENGTHS)
%% Rasters for Fig4
%
% Requires SC figure plotter (downloadable package from matlab exchange)
%
% Jake Olson
% Jan 2022

endCol = 0;
outboundMeans = figure();
hold on;
returnMeans = figure();
hold on;
for iPath = 1:numel(PATHLENGTHS)
    startCol = endCol+1;
    endCol = endCol+PATHLENGTHS(iPath);
    
    figure;
    sc(CorrStruct.pathsCatFR_maxNormed_maxLocSorted(:,startCol:endCol),'parula');
    if iPath < 5
        figure(outboundMeans);
    else
        figure(returnMeans);
    end
    plot(mean(CorrStruct.pathsCatFR_maxNormed_maxLocSorted(:,startCol:endCol),1));
end

end