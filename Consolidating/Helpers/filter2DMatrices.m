function [ outputMaps ] = filter2DMatrices( inputMaps, downsampledFlag, halfFilterWidth, std )
%FILTER2DMATRICES Applies smoothing to 2d matrices
%   Used in MPC and Sub papers
%   convolves using the nanconv function
%
% Written by Jake Olson, December 2015 updated april 2017

%2D ratemap smoothing function **************************************
if ~exist('halfFilterWidth','var')
    if downsampledFlag
        halfNarrow = 5;
        narrowStdev = 2;
    else
        % on TTT track in Nitz lab, 3.5 pixels = ~1cm
        halfNarrow = 7;
        narrowStdev = 3.5;
    end
else
    halfNarrow = halfFilterWidth;
    narrowStdev = std;
end

[xGridVals, yGridVals]=meshgrid(-halfNarrow:1:halfNarrow);
narrowGaussian = exp(-0.5 .* (xGridVals.^2+yGridVals.^2)/narrowStdev^2)/(narrowStdev*(sqrt(2*pi)));
narrowGaussianNormed=narrowGaussian./sum(sum(narrowGaussian));

outputMaps = nan(size(inputMaps));
for i = 1:size(inputMaps,3)
    for j = 1:size(inputMaps,4)
        %     different way of handling nans.
        %     outputMaps(:,:,i) = conv2nan(inputMaps(:,:,i,1),narrowGaussianNormed);
        
        outputMaps(:,:,i,j) = nanconv(inputMaps(:,:,i,j),narrowGaussianNormed, 'nanout');
    end
end
end
