function [transitionProbMat, transitionCounts, transitionDenom] = createTransitionProbabilityMatrix(dataVec,stateList)
%createTransitionProbabilityMatrix Create transition probability matrix for a vector of data.
%
% 	[transitionProbMat, transitionCounts, transitionDenom] = createTransitionProbabilityMatrix(dataVec,stateList)
%
% INPUTS: 
%		dataVec - vector of any data type? definitely works for ints
%		stateList - ordered vector of all of the possible states
%		                      
% OUTPUTS: 
%		transitionProbMat - SIZE: nStates x nStates
%           A transistion probability matrix of how the data changed states
%           To Read: row x column is the probability of moving from state in 1st dimension to state
%           in 2nd dimension. So data in row 1 column 3 is the probability that the state changes
%           from 1 to 3.
%		transitionCounts - Raw counts of transitions from one state to another. same shape as
%           transitionProbMat
%       transitionDenom - counts for each state
%		                       
% EXAMPLES: 
%		                   
% REMARKS
%
% SEE ALSO:%
%
% CREATED BY: Jacob M Olson
% EMAIL: jolson1129@gmail.com
% WEBSITE: http://www.jmolson.com
% CREATED ON: 24-Apr-2020
% LAST MODIFIED BY: Jacob M Olson
% LAST MODIFIED ON: Nov 2021
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

if ~exist('stateList','var')
    stateList = unique(dataVec);
end
nData = numel(dataVec);
nStates = numel(stateList);

transitionDenom = zeros(nStates,1);
transitionCounts = zeros(nStates,nStates);
for iData = 1:nData-1
    currStateInd = find(dataVec(iData) == stateList);
    nextStateInd = find(dataVec(iData+1) == stateList);
    %increment
    transitionDenom(currStateInd) = transitionDenom(currStateInd) + 1;
    transitionCounts(currStateInd,nextStateInd) = transitionCounts(currStateInd,nextStateInd) + 1;
end
transitionProbMat = bsxfun(@rdivide,transitionCounts,transitionDenom);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% End of Code %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


