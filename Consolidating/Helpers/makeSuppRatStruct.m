function [RatSuppStruct] = makeSuppRatStruct(BehRec, Performance, RatNum) 
%makes subselection of structs to calculate rat x rat breakdown 
%Use BehRec to index 
%RatNum inputs for Analogy paper are:
%NS14 = 14
%NS15 = 15
%NS16 = 16
%NS23 = 23
%BL02 = 02
%JL01 = 01
for iRec = 1:size(BehRec.rat,2)
    RatNameExtractor(iRec,1) = str2num(BehRec.rat{iRec}(3:4))
end
    
RatMasterIndex = find(RatNameExtractor==RatNum) 

%% extract with for loop
for iRatRec = 1:size(RatMasterIndex,1)
    ThisRec = RatMasterIndex(iRatRec);
    
    %Only need rec from original for figure 1 stats 
    RatSuppStruct.BehRec.rec(1,iRatRec) = BehRec.rec(ThisRec);

    %Move over all performance struct 
    RatSuppStruct.Performance.RunBlocks{1,iRatRec} = Performance.RunBlocks{1,ThisRec};
    RatSuppStruct.Performance.nRunsInBlock{1,iRatRec} = Performance.nRunsInBlock{1,ThisRec};
    RatSuppStruct.Performance.nErrsInBlock{1,iRatRec} = Performance.nErrsInBlock{1,ThisRec};
    RatSuppStruct.Performance.isPerfectBlock{1,iRatRec} = Performance.isPerfectBlock{1,ThisRec};

    RatSuppStruct.Performance.perfectBlockPercent(1,iRatRec) = Performance.perfectBlockPercent(1,ThisRec);
    RatSuppStruct.Performance.meanErrPerBlock(1,iRatRec) = Performance.meanErrPerBlock(1,ThisRec);

    RatSuppStruct.Performance.transitionProbMatOutboundRuns(:,:,iRatRec) = Performance.transitionProbMatOutboundRuns(:,:,ThisRec);
    RatSuppStruct.Performance.transitionCountsOutboundRuns(:,iRatRec) = Performance.transitionCountsOutboundRuns(:,ThisRec);
    RatSuppStruct.Performance.transitionProbMatAllRuns(:,:,iRatRec) = Performance.transitionProbMatAllRuns(:,:,ThisRec);
    RatSuppStruct.Performance.transitionCountsAllRuns(:,iRatRec) = Performance.transitionCountsAllRuns(:,ThisRec);
    RatSuppStruct.Performance.probCloseReturn(:,iRatRec) = Performance.probCloseReturn(:,ThisRec);

    RatSuppStruct.Performance.nEachPattern{:,iRatRec} = Performance.nEachPattern{:,ThisRec};
    RatSuppStruct.Performance.patternAndCounts{:,iRatRec} = Performance.patternAndCounts{:,ThisRec};
end

end
